spr Package
===========

:mod:`spr` Package
------------------

.. automodule:: spr
    :members:
    :undoc-members:
    :show-inheritance:


:mod:`model` Module
-------------------

.. automodule:: spr.model
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`view` Module
------------------

.. automodule:: spr.view
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`control` Module
---------------------

.. automodule:: spr.control
    :members:
    :undoc-members:
    :show-inheritance:
