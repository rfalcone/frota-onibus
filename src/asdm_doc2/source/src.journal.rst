journal Package
===============

:mod:`journal` Package
----------------------

.. automodule:: src.journal
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`journal` Module
---------------------

.. automodule:: src.journal.journal
    :members:
    :undoc-members:
    :show-inheritance:

