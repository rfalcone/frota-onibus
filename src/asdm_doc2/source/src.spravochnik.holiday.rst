holiday Package
===============

:mod:`holiday` Package
----------------------

.. automodule:: src.spravochnik.holiday
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`control` Module
---------------------

.. automodule:: src.spravochnik.holiday.control
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`test` Module
------------------

.. automodule:: src.spravochnik.holiday.test
    :members:
    :undoc-members:
    :show-inheritance:

