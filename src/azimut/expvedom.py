# -*- coding: utf-8 -*-
from datetime import date, timedelta
import datetime
from bus.buskartamarsh import vedomost

from PyQt4 import QtGui, QtCore

class exportVedomost:
    def __init__(self, mysql = None, path = '.'):
        self.ved = vedomost(None, mysql)
        self.path = path #куда сохранять отчет
    
    def readData(self,data = []):
        self.ved.redraw(data+['garnom'])

    def runReport(self):
        f = open("%s//exportazimut.csv"%self.path,"w")
        f.writelines('''"GAR_NO";"CODE";"DTBEG";"DTEND";"VOD_TAB_NO";"COND_TAB_NO"''')
        f.write("\n")
        
        for id in self.ved.datarow:
            try:
                code = self.ved.data[id]["kid"]
                garnom = self.ved.data[id]["garnom"]
                d = self.ved.data[id]["viezd"]
                dtbeg = "%02d.%02d.%04d %02d:%02d"%(d.day, d.month, d.year, d.hour, d.minute)
                d = self.ved.data[id]["vozvr"]
                dtend = "%02d.%02d.%04d %02d:%02d"%(d.day, d.month, d.year, d.hour, d.minute)
                vodtab = self.ved.data[id]["tabelvod1"]
                f.writelines('%d;%d;"%s";"%s";"%d";'%(garnom,code,dtbeg,dtend,vodtab))
                f.write("\n")
            except KeyError:
                print self.ved.data[id]
        f.close()
        
    
class exportVedomostQt(QtGui.QMdiSubWindow):
    '''Класс - справочник, для вывода в окне или с другими виджетами карты'''
    
    def __init__(self, parent = None, mysql = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Экспорт ведомости для Азимут-2')        
        self.setMinimumSize(400,200)
        self.expvedom = exportVedomost(mysql)
        self.initWidget()
                    
    def initWidget(self):
        widg = QtGui.QWidget()
        lh = QtGui.QHBoxLayout()
        btn = QtGui.QPushButton(u'Экспорт')
        lbl = QtGui.QLabel(u'Дата экспорта')
        z = date.today()
        self.date1 = QtGui.QDateTimeEdit(QtCore.QDateTime(z))
        self.date1.setCalendarPopup(True)
        self.connect(btn, QtCore.SIGNAL("clicked()"), self.runExport)
        lh.addWidget(lbl)
        lh.addWidget(self.date1)
        lh.addWidget(btn)
        widg.setLayout(lh)
        self.setWidget(widg)
        
    def runExport(self):
        z = self.date1.date().toPyDate()
        param = [str(z),str(z + timedelta(1,0))]
        
        self.expvedom.readData(param)
        self.expvedom.runReport()
        self.close()