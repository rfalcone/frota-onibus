#!/usr/bin/python
# -*- coding: utf-8  -*-
from spr.view import viewtable
from spr.model import model
from PyQt4 import QtGui, QtCore
from datetime import timedelta, datetime, date, time

class vedomost(viewtable):
    ''' Класс таблицы ведомости выезда автобусов на линию по путевым листам.
    '''
    
    def __init__(self, parent=None, mysql=None):
        viewtable.__init__(self, parent, mysql)
        self.headername_init = [u"Гаражный", u"Выезд", u"Возвращение", u"Маршрут 1",
            u"Карточка", u"Маршрут 2", u"Комментарии к маршруту"]
        self.dataview_init = ["garnom", "viezd", "vozvr", "kid", "marsh1", "name", "marsh2", "commentmarsh"]
        self.dataview = ["garnom", "viezd", "vozvr", "kid", "marsh1", "name", "marsh2", "commentmarsh"]
        self.dataparam = ["garnom", "viezd", "vozvr", "marsh1", "marsh2", "commentmarsh", "tabelvod1"]

    def _getData(self,data):
        sqlday = self.dayweek(data[0])#дополнительное условие, для sql запроса в kartarange
        kartaput = self.kartaPut()#получаем имена кароточек
        kartaday = self.dayday(sqlday)#промежутки действия карточек
        marost = self.marshostanov(sqlday)#остановки на маршруте
        sql = '''SELECT id, garnom, viezd, vozvr, marsh1, marsh2, commentmarsh, tabelvod1
                FROM putev
                WHERE viezd > '%s' AND viezd < '%s'
                ORDER BY %s'''
        self.mysql.execsql(sql%tuple(data))
        result_set = self.mysql.cursor.fetchall()
        self.data = {} #словарь со всеми полями
        self.datarow = []
        tm = time(12,0)
        maxmarsh = 0
        for row in result_set:
            tmpdata = {}
            for i in self.dataparam:
                tmpdata[i] = row[i] #проход по полям
            #добавляем в общий список данных колонку с именем карточек
            try:
                dt = kartaput[tmpdata["marsh1"]]
                if tmpdata["viezd"].time() > tm:
                    tmpdata["name"] = dt[1]
                    try:
                        l = len(marost[dt[3]])#берем временную длину массива маршрутов
                        k = 0 #счетчик
                        for i in marost[dt[3]]:
                            tmpdata["kid"] = i["idkartarange"]
                            en = i["begintime"] + timedelta(i["day"],i["endtime"]*60)
                            tmpdata["marshostanov%d"%k] = "%s [%s-%s]\n[%s-%s]"%(i["mname"],
                                i["o1name"], i["o2name"], str(i["begintime"]), str(en))
                            k += 1
                    except:
                        tmpdata["marshostanov0"] = ""
                else:
                    tmpdata["name"] = dt[0]
                    try:
                        l = len(marost[dt[2]])#берем временную длину массива маршрутов
                        k = 0 #счетчик
                        for i in marost[dt[2]]:
                            tmpdata["kid"] = i["idkartarange"]
                            en = i["begintime"] + timedelta(i["day"],i["endtime"]*60)
                            tmpdata["marshostanov%d"%k] = "%s [%s-%s]\n[%s-%s]"%(i["mname"],
                                i["o1name"], i["o2name"], str(i["begintime"]), str(en))
                            k += 1
                    except:
                        tmpdata["marshostanov0"] = ""
            except:
                tmpdata["name"] = ""
                l = 0
            if l > maxmarsh: maxmarsh = l
            #tmpdata["name"] = self.kartaput[row[""]]
            self.data[row["id"]] = tmpdata#сохраняем в данных
            self.datarow += [row["id"]]
        s = []
        s1 = []
        for i in range(maxmarsh):
            s += ["marshostanov%d"%i]
            s1 += [u"Маршрут №%d"%i]
        self.headername = self.headername_init + s1
        self.dataview = self.dataview_init + s

    def marshostanov(self,weekday):
        dataparam = ["id","idkartarange", "begintime", "day", "mname",
              "o1name", "o2name", "endtime", "idkarta"]
        #сложный запрос по доставанию данных из кучи таблиц, данные по названию маршрута, начальной и конечной остановки,
        #времени выезда и прибытия и какой карточке принадлежит
        sql = '''
            SELECT km.id, km.idkartarange, km.begintime, km.day, m.name as mname,
              o1.name as o1name, o2.name as o2name, mo2.dtime - mo1.dtime as endtime,
              kr.idkarta
            FROM kartamarsh as km, marshrut as m, ostanov as o1, ostanov as o2,
              marshostanov as mo1, marshostanov as mo2, kartarange as kr, karta as k
            WHERE km.del = 0 AND m.del = 0 AND o1.del = 0 AND o2.del = 0 AND kr.del = 0 AND k.del = 0
              AND km.idkartarange IN (SELECT id
                                 FROM kartarange
                                 WHERE del = 0 AND %s)
              AND km.idmarsh = m.id AND km.idbegin = mo1.id AND km.idend = mo2.id
              AND mo1.idostanov = o1.id AND mo2.idostanov = o2.id AND km.idkartarange = kr.id
              AND kr.idkarta = k.id
            ORDER BY km.idkartarange, km.day, km.begintime
            '''%weekday
        self.mysql.execsql(sql)
        result_set = self.mysql.cursor.fetchall()
        datakartamarsh = {}#в индексах храним id карточки
        for row in result_set:
            tmpdata = {}
            for i in dataparam[:-1]:
                tmpdata[i] = row[i]
            try: #пробуем добавить маршрут
                datakartamarsh[row[dataparam[-1]]] += [tmpdata]
            except:#если не добавляется, то просто присваиваем - очевидно такую карточки еще не было
                datakartamarsh[row[dataparam[-1]]] = [tmpdata]
        return datakartamarsh

    def kartaPut(self):
        dataparam = ["id","name","smena","put"]
        sql = '''SELECT id, name, smena, put
                 FROM karta
                 WHERE del = 0 '''
        self.mysql.execsql(sql)
        result_set = self.mysql.cursor.fetchall()
        kartaput = {}
        for row in result_set:
            #собираем путевые в соответствии со сменой вида [имя в первую смену, имя во вторую смену]
            if row["smena"] == 1:
                try:
                    kartaput[row["put"]] = [row["name"],kartaput[row["put"]][1],row["id"],kartaput[row["put"]][3]]
                except:
                    kartaput[row["put"]] = [row["name"],0,row["id"],0]
            elif row["smena"] == 2:
                try:
                    kartaput[row["put"]] = [kartaput[row["put"]][0],row["name"],kartaput[row["put"]][2],row["id"]]
                except:
                    kartaput[row["put"]] = [0,row["name"],0,row["id"]]
        return kartaput

    def dayweek(self,day):
        day = datetime.strptime(day,"%Y-%m-%d")
        #определяем что за день недели
        dataweek = ["monday", "tuesday", "wednesday",
            "thursday", "friday", "saturday", "sunday"]
        dayweek = dataweek[day.weekday()]
        #определяем праздничный/предпраздничный
        dt = day.date()
        sql = '''SELECT date
                FROM holiday
                WHERE date >= '%s' AND date <= '%s'
                ORDER BY date DESC'''%(dt,dt+ timedelta(1,0))
        self.mysql.execsql(sql)
        result_set = self.mysql.cursor.fetchall()
        dayh = 0
        s = ''
        for row in result_set:
            if dt == row["date"]:
                dayh += 1
            if dt+datetime.timedelta(1,0) == row["date"]:
                dayh += 2
        #if dayh > 3: BusKartaMarsh_DayWeek("dayh >3 %s"%str(day))
        if dayh == 0: s = ' OR noholiday = 2'
        if dayh == 1: s = ' OR holiday = 2'
        if dayh == 2: s = ' OR bholiday = 2'
        if dayh == 3: s = ' OR holiday = 2 OR bholiday = 2'
        return " %s = 2 %s"%(dayweek,s)

    def dayday(self,dayweek):
        #получаем числовые промежутки действия карточек
        dataparam = ["idkartarange", "begin", "end"]
        sql = '''
            SELECT idkartarange, begin, end
            FROM kartarangeday
            WHERE del = 0 AND idkartarange IN (SELECT id
                                               FROM kartarange
                                               WHERE %s)'''%dayweek
        self.mysql.execsql(sql)
        result_set = self.mysql.cursor.fetchall()
        data = {}
        for row in result_set:
            tmpdata = {}
            for i in dataparam[1:]:
                tmpdata[i] = row[i]
            data[row[dataparam[0]]] = tmpdata
        return data



class buskartamarsh(QtGui.QDialog):
    def __init__(self, parent = None, mysql = None):
        QtGui.QDialog.__init__(self, parent)
        #self.karta = getkarta(mysql)
        self.table = vedomost(parent,mysql)
        self.initWidget()
        self.selcol = 0
        self.dest = 0
        self.sortChanged(self.selcol)

    def initWidget(self):
        #self.initTable()
        z = date.today()
        #z = date(2011, 9, 29)
        self.date1 = QtGui.QDateEdit(QtCore.QDate(z))
        self.date1.setCalendarPopup(True)
        grid = QtGui.QGridLayout(self)
        self.tool = QtGui.QToolBar()
        self.initAction()
        #self.tool.addAction(self.addDataAct)
        #self.tool.addAction(self.editDataAct)
        #self.tool.addAction(self.delDataAct)
        self.tool.setIconSize(QtCore.QSize(48,48))
        grid.addWidget(self.tool)
        grid.addWidget(self.date1)
        grid.addWidget(self.table)

    def initAction(self):
        self.connect(self.date1, QtCore.SIGNAL("dateChanged(QDate)"),self.dateChanged)
        self.connect(self.table.horizontalHeader(), QtCore.SIGNAL("sectionClicked(int)"),self.sortChanged)

    def dateChanged(self,d):
        self.table.redraw([str(self.date1.date().toPyDate()),
                   str(self.date1.date().toPyDate()+timedelta(1,0)),
                   'garnom'])

    def sortChanged(self,i):
        if i != 4:
            if i == self.selcol:
                self.dest = 1 - self.dest
            else:
                self.selcol = i
                self.dest = 0
            if self.dest: s = ' ASC'
            else: s = ' DESC'
            d = "%s %s"%(self.table.dataview[self.selcol],s)
            self.table.redraw([str(self.date1.date().toPyDate()),
                               str(self.date1.date().toPyDate()+timedelta(1,0)),
                               d])
