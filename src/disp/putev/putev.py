#!/usr/bin/python
# -*- coding: utf-8  -*-

from datetime import timedelta, datetime, date, time
from db import db

from PyQt4 import QtGui, QtCore

class putevmodel:
    def __init__(self, mysql):
        self.mysql = db()
        self.mysql = mysql
    
    def getData(self, date, order):
        
        sql = '''SELECT id, Family, Name, Surname, tabelnmbr
                 FROM voditel'''
        self.mysql.getData(sql)
        self.datavoditel = {}
        for id in self.mysql.datarow:
            s = "%s %s.%s."%(self.mysql.data[id]["Family"], 
                self.mysql.data[id]["Name"], self.mysql.data[id]["Surname"])
            self.datavoditel [self.mysql.data[id]["tabelnmbr"]] = s
        
        sql = '''SELECT id, viezd, vozvr, nomputlista, garnom, 
                    tabelvod1, tabelvod2,marsh1, marsh2, commentmarsh
                 FROM putev as p
                 WHERE viezd>= '%s' AND viezd<= '%s' 
                 ORDER BY viezd'''%(str(date),str(date + datetime.timedelta(1,0)))
        self.mysql.getData(sql)
        #print self.mysql.fields
        #print self.mysql.data
        
class putev(putevmodel):
    def __init__(self, mysql = None):
        putevmodel.__init__(self, mysql)
        self.table = QtGui.QTableWidget()
    
    def getData(self, date, order):
        putevmodel.getData(self, date, order)

        self.table.setRowCount(len(self.mysql.datarow))
        self.table.setColumnCount(6)
        
        t = ["id", u'Выезд\nВозвращение', u'Гаражный', u'Табельный']
        self.table.setHorizontalHeaderLabels(t)
        row = 0
        for i in self.mysql.datarow:
            self.setItem(row, 0, self.mysql.data[i]["id"])
            self.setItem(row, 1, "%s\n%s"%(self.mysql.data[i]["viezd"],
                self.mysql.data[i]["vozvr"]))
            self.setItem(row, 2, self.mysql.data[i]["garnom"])
            try:#на случай, если в базе не забито соответствие фамилии с табельным номером
                self.setItem(row, 3, self.datavoditel[self.mysql.data[i]["tabelvod1"]])
            except KeyError:#выводим тогда только табельный номер
                self.setItem(row, 3, self.mysql.data[i]["tabelvod1"])
            try:
                self.setItem(row, 4, self.datavoditel[self.mysql.data[i]["tabelvod2"]])
            except KeyError:
                self.setItem(row, 4, self.mysql.data[i]["tabelvod2"])
            row += 1
        self.table.resizeColumnsToContents()
        self.table.resizeRowsToContents ()
        
    def setItem(self, row, col, data):
        s = (("%s"%str(data)).encode("utf8")).decode("utf8")#индустский код, что то не верное с кодировкой
        cell = QtGui.QTableWidgetItem(s)
        self.table.setItem(row,col,cell)

        
        
class putevQt(QtGui.QMdiSubWindow):
    '''Класс - справочник, для вывода в окне'''
    
    def __init__(self, parent = None, mysql = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Запланированные путевые листы')        
        self.setMinimumSize(400,200)
        self.putev = putev(mysql)
        self.initWidget()
                    
    def initWidget(self):
        widg = QtGui.QWidget()
        lh = QtGui.QHBoxLayout()
        btn = QtGui.QPushButton(u'Экспорт')
        lbl = QtGui.QLabel(u'Дата экспорта')
        z = date.today()
        self.date1 = QtGui.QDateTimeEdit(QtCore.QDateTime(z))
        self.date1.setCalendarPopup(True)
        self.connect(btn, QtCore.SIGNAL("clicked()"), self.getData)
        
        self.table = QtGui.QTableWidget()
        
        lh.addWidget(lbl)
        lh.addWidget(self.date1)
        lh.addWidget(btn)
        lh.addStretch()     

        lg = QtGui.QVBoxLayout()
        lg.addLayout(lh)
        lg.addWidget(self.putev.table)
        
        widg.setLayout(lg)
        self.setWidget(widg)

    def getData(self):
        self.putev.getData(self.date1.date().toPyDate(), 1)