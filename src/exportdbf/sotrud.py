#!/usr/bin/python
# -*- coding: utf-8  -*-

from PyQt4 import QtGui, QtCore
from dbfpy import dbf

class sotrud(QtGui.QMdiSubWindow):
    def __init__(self, parent = None, mysql = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Экспорт данных по сотрудникам')        
        self.mysql = mysql
        self.initSql()
        self.initWidget()
        self.initAction()
        

    def initWidget(self):
        widg = QtGui.QWidget()
        lbl = QtGui.QLabel(u'Путь к файлу для экспорта данных sotrud.dbf')        
        self.ed = QtGui.QLineEdit('/home/dark123us/tmp/SOTRUD.DBF')
        self.pbpath = QtGui.QPushButton('...')
        self.btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        vh = QtGui.QVBoxLayout()
        hbl = QtGui.QHBoxLayout()        
        hbl.addWidget(self.ed)
        hbl.addWidget(self.pbpath)
        vh.addWidget(lbl)
        vh.addLayout(hbl)
        vh.addWidget(self.btn1)
        widg.setLayout(vh)
        self.setWidget(widg)
    
    def initSql(self):
        self.dataparam = ["Family", "Name", "Surname", "tabelnmbr", "brigada", "idbus"]
        self.readDataSql = '''
            SELECT id, Family, Name, Surname, tabelnmbr, brigada, idbus
            FROM voditel '''
        self.updDataSql = '''UPDATE voditel SET brigada = %d, idbus = %d
                WHERE id = %d '''
        self.insDataSql = '''INSERT INTO voditel (Family, Name, Surname, tabelnmbr, brigada, idbus)
                VALUES ('%s', '%s', '%s', %d, %d, %d)'''
        self.databusparam = ["garnom"]
        self.readDataBusSql = '''
            SELECT id, garnom
            FROM bus '''
        
        self.readDataDbf = ["TABEL", "KOLON", "KODBR", "FAMIL", "GARNM", "DVYSL"]
        self.readDataPosDbf = [0, 1, 2, 3, 4, 8]
    
    def initAction(self):
        self.connect(self.btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(self.btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        self.connect(self.pbpath, QtCore.SIGNAL("clicked()"),self.changePath)
        
    def accept(self):
        self.readFromSql()        
        self.readFromDbf2()
        self.compareData2()
        
    def compareData(self):
        #family, name, surname, tabnmbr, brigada, idbus
        for i in self.datadbf:
            find = 0
            for id in self.datarow:
                if self.data[id]["tabelnmbr"] == i[3]:#если табельные совпали - подновляем запись, если необходимо
                    find = 1
                    if self.data[id]["brigada"] != i[4] or self.data[id]["idbus"] != i[5]:
                        self.mysql.execsql(self.updDataSql%(i[4],i[5],id))
                    break
            if find == 0:#ну раз не нашли - вставляем данные
                self.mysql.execsql(self.insDataSql%(i[0],i[1],i[2],i[3],i[4],i[5]))

    def compareData2(self):
        #tabnmbr, brigada, idbus
        for i in self.datadbf:
            for id in self.datarow:
                if self.data[id]["tabelnmbr"] == i[0]:#если табельные совпали - подновляем запись, если необходимо
                    if self.data[id]["brigada"] != i[1] or self.data[id]["idbus"] != i[2]:
                        self.mysql.execsql(self.updDataSql%(i[1],i[2],id))
                    break
    
    def reject(self):
        self.close()
    
    def changePath(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, u"Экспорт из sotrud.dbf", "./", "sotrud.dbf (sotrud.dbf, *.dbf, *)")
        self.ed.setText(filename)
        
    def readFromSql(self):
        self.data, self.datarow = self._getData(self.readDataSql, self.dataparam) 
        self.databus, self.databusrow = self._getData(self.readDataBusSql, self.databusparam) 
        print self.databus
    
    def _getData(self, sql, param):
        self.mysql.execsql(sql)
        result_set = self.mysql.cursor.fetchall()
        data = {} #словарь со всеми полями
        datarow = []
        for row in result_set:
            tmpdata = {}
            for i in param:
                tmpdata [i] = row[i]
            data[row["id"]] = tmpdata 
            datarow += [row["id"]]
        return data, datarow
    
    def readFromDbf(self):#sotrud
        try:
            name = str(self.ed.text())
            dbp = dbf.Dbf(name)
            rc = dbp.recordCount
            self.datadbf = []
            #typef = {}
            #for i in self.readDataPosDbf:
            #    typef[dbp.fieldDefs[i].fieldInfo()[0]] = dbp.fieldDefs[i].fieldInfo()[1]
            for i in range(rc):
                self.datadbf += [self.encodeDbf(dbp[i])]
        except:
            print "Error read %s"%name
            raise

    def encodeDbf(self, data):
        family = data["FAMIL"].decode("cp866").split()
        name = ''
        surname = ''
        try:
            name = family[1].split('.')[0]
            surname = family[1].split('.')[1]
        except:
            pass
        family = "%s%s"%(family[0][0], family[0][1:].lower())
        
        try:
            tabnmbr = int(data["TABEL"])
        except:
            tabnmbr = 0
        try:
            brigada = int(data["KODBR"])
        except:
            brigada = 0
        idbus = 0
        try:
            garnm = int(str(data['GARNM'])[1:4])
            for id in self.databusrow:
                if self.databus[id]["garnom"] == garnm:
                    idbus = id
                    break
        except ValueError:
            garnm = 0
            idbus = 0
        return family, name, surname, tabnmbr, brigada, idbus

    def readFromDbf2(self):#grafik
        try:
            name = str(self.ed.text())
            dbp = dbf.Dbf(name)
            rc = dbp.recordCount
            self.datadbf = []
            for i in range(rc):
                self.datadbf += [self.encodeDbf2(dbp[i])]
        except:
            print "Error read %s"%name
            raise
        
    def encodeDbf2(self, data):
        tabnmbr = brigada = idbus = 0
        if data["TDATA"] == '201110':
            try:
                tabnmbr = int(data["TABEL"])
            except:
                tabnmbr = 0
            try:
                brigada = int(data["KODBR"])
            except:
                brigada = 0
            idbus = 0
            try:
                garnm = int(str(data['GARNM'])[1:4])
                for id in self.databusrow:
                    if self.databus[id]["garnom"] == garnm:
                        idbus = id
                        break
            except ValueError:
                garnm = 0
                idbus = 0
        return tabnmbr, brigada, idbus
        