#!/usr/bin/python
# -*- coding: utf-8  -*-
from grafik.standart.getdata import getData
from grafik.standart.combo import comboData

class getBrigada(getData):
    ''' '''
    def __init__(self, mysql):
        getData.__init__(self, mysql)
    
    def getDataSql(self):
        sql = '''
            SELECT id, name, addmin
            FROM brigada
            WHERE del = 0
            ORDER BY name '''
        return sql
    
    def getSdvigSql(self, idbrigada):
        sql = '''SELECT id, typesdvig, sdvig2 FROM brigada WHERE id = %d'''%idbrigada
        return sql
    
    def getSdvig(self, idbrigada):
        sdvig = 0
        (f,dr,d) = self._mysql.getData(self.getSdvigSql(idbrigada))
        for i in dr:
            sdvig = d[i]["typesdvig"]
            sdvig2 = d[i]["sdvig2"]
        return sdvig, sdvig2 

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
class comboBrigada(comboData):
    def __init__(self, mysql, parent = None):
        self._brigada = getBrigada(mysql)
        comboData.__init__(self, mysql, parent)

    def getData(self):
        return self._brigada
        