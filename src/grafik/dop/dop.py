#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from grafik.standart.getdata import getData

class getDopoln(getData):
    ''' '''
    def __init__(self, mysql):
        getData.__init__(self, mysql)
    
    def getDataSql(self):
        sql = '''
            SELECT id, name, sokr, timeadd
            FROM dopoln
            WHERE del = 0
            ORDER BY name '''
        return sql
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
class getDop:
    ''' '''
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
    
    def initSql(self):
        self._getDataSql = '''
            SELECT id, name, sokr
            FROM dopoln
            WHERE del = 0
            ORDER BY name '''

    def getData(self):
        (f,dr,d) = self._mysql.getData(self._getDataSql)
        return (f,dr,d)   
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
class comboDop(QtGui.QComboBox):
    def __init__(self, mysql, parent = None):
        QtGui.QComboBox.__init__(self, parent)
        self._dop = getDop(mysql)
        
    def redraw(self):
        (f, self._datarow, self._data) = self._dop.getData()
        self.clear()
        s = []
        for id in self._datarow:
            s += [self._data[id]['name']]
        self.addItems(s)
        
    def setSelect(self, iddop):
        k = 0
        for i in self._datarow:
            if i == iddop:
                self.setCurrentIndex(k)
                break
            k += 1        
    
    def getSelect(self):
        id = self._datarow[self.currentIndex()]
        return id
        