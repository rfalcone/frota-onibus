#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from sotrudn.sotrudn import getSotrudnWorking
from brigada.brigada import comboBrigada
from dop.dop import getDop
from datetime import date, timedelta, time
import calendar
from kartarange.kartarange import getKartaRangeId, dayFlag
from kartasortbrigada.kartasortbrigada import selectKartaSort, getDataKartaSort
from brigada.brigada import getBrigada
from kartarange.kartabrigada import dialogGetKartaBrigada, getKartaBrigada
import xlwt
from xlwt import easyxf
from dbfpy import dbf

import pprint
class getGrafik:
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
        
    def initSql(self):
        self._getDataSql = '''
            SELECT v.id, v.idkartarange, v.idvoditel, v.date, 
                k.name, k.put, v.t, k.smena, v.hour
            FROM vodgrafik as v, karta as k, kartarange as kr
            WHERE v.brigada = %d AND v.idkartarange = kr.id AND kr.idkarta = k.id 
                AND v.date>='%s' AND v.date <='%s' AND v.t = 0

            UNION
            
            SELECT v.id, v.idkartarange, v.idvoditel, v.date, 
                d.sokr as name, 0 as put, v.t, 0 as smena, v.hour
            FROM vodgrafik as v, dopoln as d
            WHERE v.brigada = %d AND v.idkartarange = d.id 
                AND v.date>='%s' AND v.date <='%s' AND v.t = 1
        '''
        self.getCountDataSQL = '''
                    SELECT vg.id as id,  kr.idkarta as idkarta, 
                        count(kr.idkarta) as countid, vg.date, 0 as t
                    FROM vodgrafik as vg, kartarange as kr, kartarangebrigada as krb
                    WHERE vg.date>='%s' AND vg.date<='%s' AND
                        krb.idkartarange = kr.id AND
                         vg.idkartarange = kr.id AND vg.t = 0 AND krb.idbrigada = %d
                    GROUP BY vg.date, kr.idkarta
                        
                    UNION
                    
                    SELECT vg.id as id,  dop.id as idkarta, 
                        count(dop.id) as countid, vg.date, 1 as t
                    FROM vodgrafik as vg, dopoln as dop
                    WHERE vg.date>='%s' AND vg.date<='%s' AND
                        vg.idkartarange = dop.id AND vg.t = 1 AND vg.brigada = %d
                    GROUP BY vg.date, dop.id
                    ORDER BY 5, 2, 4
                    '''
    def getData(self, datewithmonth, idbrigada):
        bday = date(datewithmonth.year,datewithmonth.month,1)
        
        eday = date(datewithmonth.year,
                    datewithmonth.month,
                    calendar.monthrange(datewithmonth.year, datewithmonth.month)[1])
        sql =self._getDataSql%(idbrigada, bday, eday, idbrigada, bday, eday)
        (f,dr,d) = self._mysql.getData(sql)
        data = {}
        for idvg in dr:
            idvoditel = d[idvg]["idvoditel"]
            col = d[idvg]["date"].day - 1
            try:
                data[idvoditel]
            except KeyError:
                data[idvoditel] = {}
            tmp = {}
            tmp['id'] = idvg
            tmp['name'] = d[idvg]["name"]
            tmp['put'] = d[idvg]["put"]
            tmp['smena'] = d[idvg]["smena"]
            tmp['idtype'] = d[idvg]["t"]
            tmp['idkartarange'] = d[idvg]["idkartarange"]
            tmp['hour'] = d[idvg]["hour"]
            data[idvoditel][col] = tmp
        return data
        
    def getCountData(self,datewithmonth, idbrigada):
        bday = date(datewithmonth.year,datewithmonth.month,1)
        eday = date(datewithmonth.year,
                    datewithmonth.month,
                    calendar.monthrange(datewithmonth.year, datewithmonth.month)[1])
        sql =self.getCountDataSQL %(bday, eday, idbrigada, bday, eday,idbrigada)
        (f,dr,d) = self._mysql.getData(sql)
        data = {}
        datadop = {}

        for row in dr:
            idk = d[row]["idkarta"]
            if d[row]["t"] == 0:
                try:
                    data[idk]
                except KeyError:
                    data[idk] = {}
                    data[idk]["t"] = d[row]["t"]
                    data[idk]["col"] = {}
                col = d[row]["date"].day - 1
                data[idk]["col"][col] = d[row]["countid"]
            else:
                try:
                    datadop[idk]
                except KeyError:
                    datadop[idk] = {}
                    datadop[idk]["t"] = d[row]["t"]
                    datadop[idk]["col"] = {}
                col = d[row]["date"].day - 1
                datadop[idk]["col"][col] = d[row]["countid"]
        return data,datadop
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class editGrafik(getGrafik):
    def __init__(self,mysql):
        getGrafik.__init__(self, mysql)
        self._maxrow = 50
    
    def initSql(self):
        getGrafik.initSql(self)
        self._addDataSql = '''INSERT INTO vodgrafik (idkartarange, idvoditel, date, brigada, t) 
            VALUES (%d, %d, '%s', %d, %d)'''
        self._addDataRezSql = '''INSERT INTO vodgrafik (idkartarange, idvoditel, date, brigada, t, hour) 
            VALUES (%d, %d, '%s', %d, %d, '%s')'''
        self._delDataSql = '''DELETE FROM vodgrafik WHERE id = %d '''
        self._del2DataSql = '''DELETE FROM vodgrafik WHERE (idvoditel = %d AND date = '%s') '''
        self._updateHour = '''UPDATE vodgrafik SET hour = '%s' WHERE id = %d'''
        
    def beginIns(self):
        self.sqldata = []
    
    def addIns(self, idkartarange, idvoditel, curdate, brigada, idtype):
        self.sqldata += [{}]
        i = len(self.sqldata) - 1
        self.sqldata[i]["idkartarange"] = idkartarange
        self.sqldata[i]["idvoditel"] = idvoditel
        self.sqldata[i]["date"] = curdate
        self.sqldata[i]["brigada"] = brigada
        self.sqldata[i]["t"] = idtype
    
    def endIns(self):
        if len(self.sqldata)>0:
            fl = True
            row = 0
            for i in self.sqldata:
                if fl:
                    sql = self._addDataSql%(i["idkartarange"], i["idvoditel"], 
                                            str(i["date"]), i["brigada"], i["t"])
                    fl = False
                else:
                    sql = "%s, (%d, %d, '%s', %d, %d)"%(sql, i["idkartarange"], i["idvoditel"], 
                                            str(i["date"]), i["brigada"], i["t"])
                row += 1
                if row > self._maxrow:
                    row = 0
                    fl = True
                    self._mysql.execsql(sql)
            self._mysql.execsql(sql)
    
    def beginDel(self, typedel = 0):
        self.sqldata = []
        self._typedel = typedel
    
    def addDel(self, idvg):
        self.sqldata += [{}]
        i = len(self.sqldata) - 1
        self.sqldata[i]["idvg"] = idvg
    
    def addDel2(self, idvod, curdate):
        self.sqldata += [{}]
        i = len(self.sqldata) - 1
        self.sqldata[i]["idvod"] = idvod
        self.sqldata[i]["date"] = curdate
    
    def endDel(self):
        if len(self.sqldata)>0:
            fl = True
            row = 0
            for i in self.sqldata:
                if fl:
                    if self._typedel == 0:
                        sql = self._delDataSql%(i["idvg"])
                    else:
                        sql = self._del2DataSql%(i["idvod"], str(i["date"]))
                    fl = False
                else:
                    if self._typedel == 0:
                        sql = "%s OR id = %d"%(sql, i["idvg"])
                    else:
                        sql = "%s OR (idvoditel = %d AND date = '%s')"%(sql, i["idvod"], str(i["date"]))
                row += 1
                if row > self._maxrow:
                    row = 0
                    fl = True
                    self._mysql.execsql(sql)
            self._mysql.execsql(sql)

    def insertHour(self, idkartarange, idvoditel, curdate, brigada, t, hour):
        sql = self._addDataRezSql%(idkartarange, idvoditel, str(curdate), brigada, t, str(hour))
        self._mysql.execsql(sql) 
    
    def updateHour(self, hour, idvodgr):
        sql = self._updateHour%(str(hour), idvodgr)
        self._mysql.execsql(sql)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class data4Table:
    '''Класс с данными для графика
    redraw - перечитываем данные и делаем проверку provdata
    provdata
    '''
    def __init__(self, mysql):
        self._sotrudn = getSotrudnWorking(mysql)
        self._datagrafik = editGrafik(mysql)
        self._getBrigada = getBrigada(mysql)
        self._addTimeMinBrigada = timedelta(0,0) #дополнительное время при карточке
        self._dataheader = [] #упорядоченный список с данными вертикального заголовка (фиксировнных столбцов)        
        self._date = date(2011,1,1)
        self._brigada = 1

        self._kr = getKartaRangeId(mysql)
        self._kr.getAllData()
        self._days = self._kr.getDays(self._date)
        
    def redraw(self):
        self._kr.getAllData()
        self._days = self._kr.getDays(self._date)
        f, dr, d = self._getBrigada.getData()
        self._addTimeMinBrigada = d[self._brigada]["addmin"]
        ''' Рисуем вертикальный заголовок, с фамилиями '''
        f, dr, d = self._sotrudn.getDataMonth(self._date, self._brigada)
        dd = self._datagrafik.getData(self._date, self._brigada)
        row = 0
        self._dataheader = []
        self._data = []        
        for idvod in dr:
            f = d[idvod]["family"]
            try:
                i = "%s."%d[idvod]["name"][0]
            except IndexError:
                i = ''
            try:
                o = "%s."%d[idvod]["surname"][0]
            except IndexError:
                o = ''
            fio = "%s %s%s"%(f, i, o)
            garaj = d[idvod]["garnom"]
            if garaj == 0:
                garaj = "-"
            else:
                garaj = "%d"%garaj
            tmp = {}
            tmp["fio"] = fio
            tmp["idvod"] = idvod
            tmp["tabelnmbr"] = d[idvod]["tabelnmbr"]
            tmp["idbus"] = d[idvod]["idbus"]
            tmp["garaj"] = garaj 
            tmp["garajfull"] = d[idvod]["garnomfull"]
            tmp["alltime"] = timedelta(0,0)
            tmp["ots"] = 0
            tmp["vih"] = 0
            tmp["rez"] = 0
            self._dataheader += [tmp]
            
            self._data += [{}]
            try:
                keys = dd[idvod].keys()
            except KeyError:
                idvod = 0
            if idvod > 0:
                for col in keys:
                    self.addData(row, col, dd[idvod][col])
            row += 1
            
    def addData(self,row,col,data):
        '''Добавляем дополнительные данные для дальнейшей проверки '''
        self._data[row][col] = {}
        self._data[row][col]["idtype"] = data['idtype']

        self._data[row][col]["idkartarange"] = data['idkartarange']
        self._data[row][col]["id"] = data['id']
        self._data[row][col]["name"] = data['name']
        self._data[row][col]["smena"] = data['smena']
        self._data[row][col]["hour"] = data["hour"]
        
        #TODO жесткая привязка к ид дополнительных        
        if data['idtype'] == 1:
            if data['idkartarange'] == 8:
                self._dataheader[row]["ots"] += 1
            elif data['idkartarange'] == 1:
                self._dataheader[row]["vih"] += 1
            elif data['idkartarange'] == 2:
                self._dataheader[row]["rez"] += 1
            elif data['idkartarange'] == 9:
                self._dataheader[row]["rez"] += 1
            alltime = data['hour']   
            if (alltime == timedelta(0,0) and 
                (data["idkartarange"] == 2 or data["idkartarange"] == 9)): #если резерв1 или резерв2
                alltime = timedelta(0,25200)
            self._dataheader[row]["alltime"] += alltime
        elif data['idtype'] == 0:
            self._data[row][col]["put"] = data['put']
            data = self._kr.getDataKartaRange2(data['idkartarange'])
            try:
                for i in data["time"]:
                    #TODO жесткая привязка к ид в таблице krt_typetime, 1 - время по карточке
                    if i["idkrtt"] == 1:
                        self._data[row][col]["begintime"] = i["begintime"]
                        self._data[row][col]["endtime"] = i["endtime"]
                        self._data[row][col]["endday"] = i["endday"]
                alltime = data["timedelta"]
                alltime = alltime + self._addTimeMinBrigada
                self._dataheader[row]["alltime"] += alltime
            except KeyError:
                print "Для kartarange id = %d не указано время"%self._data[row][col]["idkartarange"] 
            
    def setBrigada(self, idbrigada):
        self._brigada = idbrigada
    
    def setDate(self,curdate):
        self._date = curdate
    
    def getHeader(self, row):
        return self._dataheader[row]
        
    def lenHeader(self):
        return len(self._dataheader)
    
    def getData(self, row, col):
        try:
            d = self._data[row][col]
        except KeyError:
            d = []
        return d
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class provData(data4Table):
    def __init__(self, mysql):
        data4Table.__init__(self, mysql)
        self._errdata = {}#данные с ошибками
        self._errtable = []#таблица с цветами ошибок

    def redraw(self):
        data4Table.redraw(self)
        self.provdata()
        
    def geterr(self, row, col, mess):
        "добавляем сообщение об ошибке"
        tmp = {}
        tmp[col] = mess
        try:
            self._errdata[row] += [tmp]
        except:
            self._errdata[row] = [tmp]
        if col >= 0:            
            self._errtable [row][col] = 1
    
    def timeBettwenSmena12(self, timelast, row, col):
        '''проверка на время между сменами должно быть более 12 часов'''
        rettime = timedelta(0,0)
        try:
            tb  = timedelta(1, 0) + self._data[row][col]["begintime"] #для начала новой смены добавляем сутки
            if timelast > timedelta(0, 0):
                if tb - timelast < timedelta(0,43200): #если между завершением предыдущей и началом новой смены меньше 12 часов
                    self.geterr(row,col,u'Между сменами меньше 12 часов')
            rettime = timedelta(self._data[row][col]["endday"], 0) + self._data[row][col]["endtime"]
        except KeyError:
            print "ERROR (in row = %d, col = %d, idkartarange = %d) - not found time"%(row, col, self._data[row][col]["idkartarange"])
            self.geterr(row,col,u'Время выезда и возвращения для карточки не установлено')
        return rettime 
    
    def repeatSmena(self, smena, row, col):
        '''проверка повторов смены'''
        if smena[1] == smena[0] and smena[0] >0:#если это уже не первый день одна и та же смена
            smena["count"] += 1
            if smena["count"] > 5:#если смены повтрились уже в 6-й раз 
                self.geterr(row ,col ,u'Больше 6-ти дней подряд %d-я смена'%smena[0])
        else:
            smena["count"] = 0
        return smena["count"]
        
    def changeSmena(self, smena, row, col):
        ch = False#проверка на изменение смены
        if smena[0] != smena[1]:
            ch = True
            #проверка правильного перехода при изменении смен
            if (smena[0] == 1# and col > 1#если текущая смена 1 и прошло 2 и более дней
                and ((smena[3] == 2 and (smena[2] !=0 or smena[1] !=0))#если смена 2 дня назад 2-я, а после нее небыло 2 выходных
                or(smena[2] == 2)  #или один день назад была 2-я смена (т.е. нет отсыпного-выходного при пересменке)
                or(smena[1] == 2))): #или прошлый день 2-я смена (см.выше)
                #генерим ошибку - неверный переход со второй смены к первой
                self.geterr(row,col,u'При переходе от второй смены к первой нет отсыпного + выходного')
        return ch
    
    def provdata(self):
        self.calcError()#старый расчет ошибок
        
    def calcError(self):        
        '''Проверка введенных данных для графика по правилам:
            
            * отработать не более 6-ти дней в одну смену подряд
            * Между сменами разрыв не менее 12 часов
            * Выходных сколько полных недель в месяце (В) + праздники 
            * Отсыпных сколько выходных - В
            * ОВ одно должно попасть на субботу – воскресенье (при переходе со 2-й смены на 1-ю)
            * Между водителями пересменку в обед предусмотреть в одном месте
                '''
        #TODO что такое полная неделя
        #проход по рядам
        self._errdata = {}
        self._errtable = []
        if len(self._data) > 0:
            rows = len(self._data)
            cols = calendar.monthrange(self._date.year, self._date.month)[1]
            for r in range(rows):
                self._errtable += [[]]
                for c in range(cols):
                    self._errtable[r] += [0]
            maxsmena = 4 #сколько смен будем рассматривать
            #подбиваем сколько полных недель в месяце, сколько праздничных
            week = 0
            hld = 0
            for day in self._days:
                if day & 64:#если попали в воскресенье
                    week += 1
                if day & 512:#праздничный день
                    hld += 1
            for row in range(rows):#идем по строкам-фамилиям
                smena = {}
                for i in range(maxsmena):
                    smena[i] = 0 #текущая смена
                smena["count"] = 0 #количество одинаковых смен
                free = {}
                free["vih"] = 0#насчитано выходных
                free["ots"] = 0#насчитано отсыпных
                free["fl"] = 0#флаг попадания ОВ на субботу-воскресенье 1 - ots попал на субботу, 2 - условие выполенно
                lasttimesmena = timedelta(0,0)
                for col in range(cols):#проход по дням
                    try:
                        self._data[row][col]#проверяем есть ли данные в этой ячейке
                        fl = True
                    except KeyError:
                        fl = False
                    if fl:#данные какие-то найдены, начинаем обрабатывать условия
                        if self._data[row][col]["idtype"] == 0:#если найденные данные это карточка
                            lasttimesmena = self.timeBettwenSmena12(lasttimesmena, row, col)
                            smena[0] = self._data[row][col]["smena"] 
                            smena["count"] = self.repeatSmena(smena, row, col)
                            if self.changeSmena(smena, row, col):
                                smena["count"] = 0
                        else:
                            smena[0] = 0
                            lasttimesmena = timedelta(0,0)
                            iddop = self._data[row][col]["idkartarange"]
                            #TODO жестка привязка к ид, в дальнешем необходимо переделать
                            if iddop == 8:#если отсыпной
                                free["ots"] += 1
                            else:#if iddop == 8:#если выходной
                                free["vih"] += 1
                                if self._days[col] & 64 and smena[1] == 0:#если выходной в воскресенье, а предыдующий день отсыпной при этом проверяем, что смена равна 0
                                    free["fl"] = 1
                    else:
                        smena[0] = 0
                        lasttimesmena = timedelta(0,0)
                        free["vih"] += 1
                        if self._days[col] & 64 and smena[1] == 0:#если выходной в воскресенье, а предыдующий день отсыпной при этом проверяем, что смена равна 0
                            free["fl"] = 1
                    for i in range(maxsmena-1):
                        smena[maxsmena-i-1] = smena[maxsmena-i-2]
                if free["fl"] == 0:
                    self.geterr(row, -1, u'Нет отсыпного-выходного попадающего на выходные')
                if free["vih"] < (week + hld):
                    self.geterr(row, -1, u'Выходных меньше, чем полных недель')
                if free["ots"] < (week - free["vih"]):
                    self.geterr(row, -1, u'Отсыпных меньше, чем выходных в месяце - В')
        else:
            print "not found rows or cols"
        
    def getErrorData(self):
        return self._errdata
    
    def getErrorTable(self, row = -1, col = -1):
        if row>=0 and col >=0:
            return self._errtable[row][col]
        else:
            return 0#self._errtable
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class freezeTable(QtGui.QTableView):
    def __init__(self, parent=None):
        QtGui.QTableView.__init__(self,parent)
        self._freezeColumnCount = 3
        self._headernamebegin = [u'Фио',u'Гаражный',u'Часов']        
        self._timer = QtCore.QTimer()
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))        
        self._mdl = QtGui.QStandardItemModel()
        self._mdls = QtGui.QItemSelectionModel(self._mdl)
        self.setModel(self._mdl)
        self.setSelectionModel(self._mdls)
        self.redrawFreeze()
        
    def redrawFreeze(self):
        try:
            self._frozenTableView 
            self.disconnect(self.horizontalHeader(),QtCore.SIGNAL('sectionResized(int,int,int)'), self.updateSectionWidth)
            self.disconnect(self.verticalHeader(),QtCore.SIGNAL('sectionResized(int,int,int)'), self.updateSectionHeight)
            self.disconnect(self._frozenTableView.verticalScrollBar(), QtCore.SIGNAL('valueChanged(int)'),
                            self.verticalScrollBar(), QtCore.SLOT('setValue(int)'))
            self.disconnect(self.verticalScrollBar(), QtCore.SIGNAL('valueChanged(int)'),
                            self._frozenTableView.verticalScrollBar(), QtCore.SLOT('setValue(int)'))
            self.disconnect(self._timer, QtCore.SIGNAL("timeout()"), self._scrollTable)#для костылей
            self._mdl.setColumnCount(0)
            del self._frozenTableView 
        except:
            pass
        
        self._frozenTableView = QtGui.QTableView(self)
        self._frozenTableView.setModel(self.model())
        self._frozenTableView.setFocusPolicy(QtCore.Qt.NoFocus)
        
        
        self._frozenTableView.verticalHeader().hide()
        self._frozenTableView.horizontalHeader().setResizeMode(QtGui.QHeaderView.Fixed)
        self.viewport().stackUnder(self._frozenTableView)
        self._frozenTableView.setStyleSheet("QTableView { border: none;"
                                      "background-color: #8EDE21;"
                                      "selection-background-color: #999}")
        self._frozenTableView.setSelectionModel(self.selectionModel())
        self._frozenTableView.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self._frozenTableView.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollMode(self.ScrollPerPixel)
        self.setVerticalScrollMode(self.ScrollPerPixel)
        self._frozenTableView.setVerticalScrollMode(self.ScrollPerPixel)
        self._frozenTableView.show()
        self.initWidget()        


    def initAction(self):
        self.connect(self.horizontalHeader(),QtCore.SIGNAL('sectionResized(int,int,int)'), self.updateSectionWidth)
        self.connect(self.verticalHeader(),QtCore.SIGNAL('sectionResized(int,int,int)'), self.updateSectionHeight)
        self.connect(self._frozenTableView.verticalScrollBar(), QtCore.SIGNAL('valueChanged(int)'),
               self.verticalScrollBar(), QtCore.SLOT('setValue(int)'))
        self.connect(self.verticalScrollBar(), QtCore.SIGNAL('valueChanged(int)'),
               self._frozenTableView.verticalScrollBar(), QtCore.SLOT('setValue(int)'))
        self.connect(self._timer, QtCore.SIGNAL("timeout()"), self._scrollTable)#для костылей

    def initWidget(self):
        "Перерисовка виджета (всей таблицы)"
        for col in range(self._freezeColumnCount,self.model().columnCount()):
             self._frozenTableView.setColumnHidden(col, True)
        for w in range(0,self._freezeColumnCount):
            self._frozenTableView.setColumnWidth(w, self.columnWidth(w))
        for w in range(0,self.model().rowCount()):
            self._frozenTableView.setRowHeight(w, self.rowHeight(w))
        self.updateFrozenTableGeometry()
        
        self.initAction()        
        
    def updateSectionWidth(self,logicalIndex,i,newSize):
        if logicalIndex <= self._freezeColumnCount:
             self._frozenTableView.setColumnWidth(logicalIndex,newSize)
             self.updateFrozenTableGeometry()

    def updateSectionHeight(self, logicalIndex, i, newSize):
        self._frozenTableView.setRowHeight(logicalIndex, newSize)

    def resizeEvent(self,event):
        super(QtGui.QTableView, self).resizeEvent(event)
        self.updateFrozenTableGeometry()

    def moveCursor(self, cursorAction, modifiers):
        current = QtGui.QTableView.moveCursor(self, cursorAction, modifiers)
        if (cursorAction == self.MoveLeft and current.column()>0
                and  self.visualRect(current).topLeft().x() < self._frozenTableView.columnWidth(0) ):
            newValue = self.horizontalScrollBar().value() + self.visualRect(current).topLeft().x() - self._frozenTableView.columnWidth(0)
            self.horizontalScrollBar().setValue(newValue)
        return current

    def scrollTo(self, index, hint):
        if (index.column()>0):
            QtGui.QTableView.scrollTo(self, index, hint)

    def updateFrozenTableGeometry(self):
        size = 0
        for i in range(0,self._freezeColumnCount):
            size += self.columnWidth(i)
        self._frozenTableView.setGeometry(self.verticalHeader().width() + self.frameWidth(),
            self.frameWidth(),
            size,
            self.viewport().height()+self.horizontalHeader().height())

    def beginRedraw(self):
        self._freeColumnCount = calendar.monthrange(self._date.year, self._date.month)[1]
        self._headername = self._headernamebegin[:]
        for i in range(self._freeColumnCount):
            self._headername += [str(i+1)]

        self._mdl.setColumnCount(self._freezeColumnCount)
        self._mdl.setHorizontalHeaderLabels(self._headername)
        self._mdl.setRowCount(0)
        self._mdl.setColumnCount(self._freeColumnCount+self._freezeColumnCount)
        self.initWidget()
    
    def endRedraw(self):
        self.resizeColumnsToContents()
        self.resizeRowsToContents()
        self.updateFrozenTableGeometry()

    def _initKostil(self):#устанавливаем при выведении какого либо диалогового окна
        self.a = self.verticalScrollBar().value()
        
    def _kostil(self):#после закрытия диалогового окна, активируем костыль
        self._timer.start(10)#костыль

    def _scrollTable(self):#костыль против скролинга после появления диалогового окна и заполнения данных
        if self.verticalScrollBar().value() != self.a:
            self._timer.stop()        
            self.verticalScrollBar().setValue(self.a)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class drawFreezeTable(freezeTable):
    def __init__(self, mysql, parent= None):
        freezeTable.__init__(self,parent)
        self._datagrafik = editGrafik(mysql)
        self._date = date(2011,1,1)#.today() #любой день месяц которого рассматриваем
        self._brigadasdvig = getBrigada(mysql)#данные по бригаде, нужен только для определения нужен ли сдвиг для напарников или нет
        self._brigada = 1 #id бригады, с которой работаем
        self._sdvig, self._sdvig2 = self._brigadasdvig.getSdvig(self._brigada)#сдвиг при дополнении для напарников (т.е. у кого один гаражный)
        #self._dop = getDop(mysql)
        self._dialog = selectKartaSort(mysql, parent)
        self._df = dayFlag(mysql)#для отрисовки таблицы в соответствии с флагами дня
        self._daysFlag = self._df.getData(self._date)
        self._ks = getDataKartaSort(mysql)
        self._drawtype = 0 #0 - рисуем имя, 1 - рисуем код из карточек
        self._data4Table = provData(mysql)
        self._kr = getKartaRangeId(mysql)
        #self._kr.getAllData()
        #self._kr.getDays(self._date)
        self._viewOtsVih = False
        self.connect(self._dialog, QtCore.SIGNAL("accepted()"),self.save)
    
    def getIdDefault(self, row, col):#если карточка не найдена какое id принимать по умолчанию
        result = 2 #если не найдем какая смена была - то выставляем  
        for i in reversed(range(col)):
            data = self._data4Table.getData(row,i)
            try:
                if data["smena"] == 1:
                    result = 2
                    break
                elif data["smena"] == 2:
                    result = 9
                    break
            except:
                pass
        return result 
    
    def setViewOtsVih(self, b):
        if b and not self._viewOtsVih:
            self._viewOtsVih = True
            self._headernamebegin = [u'Фио',u'Гаражный',u'Часов', u'Вых.', u'Отс.', u'Рез']        
        else:
            self._viewOtsVih = False
            self._headernamebegin = [u'Фио',u'Гаражный',u'Часов']
            
        self._freezeColumnCount = len(self._headernamebegin )
        self.redrawFreeze()
        self.redraw()
        
    def drawAll(self):
        ''' Рисуем вертикальный заголовок, с фамилиями '''
        self._data4Table.redraw()
        rows = self._data4Table.lenHeader()
        self._mdl.setRowCount(rows)
        for row in range(rows):
            header = self._data4Table.getHeader(row)
            self.itemDraw(row, 0, header["fio"])            
            self.itemDraw(row, 1, header["garaj"])
            alltime = "%.2f"%(header["alltime"].seconds/3600.0 + header["alltime"].days*24)
            self.itemDraw(row, 2, alltime)

            if self._viewOtsVih:
                self.itemDraw(row, 3, header["vih"])
                self.itemDraw(row, 4, header["ots"])
                self.itemDraw(row, 5, header["rez"])
            #отрисовываем выходные, праздничные
            for col in range(self._freeColumnCount):
                d = self._data4Table.getData(row,col) 
                if len(d)>0:
                    if (self._drawtype == 0) or (d['idtype']==1):
                        s = d['name']
                    else:
                        s = d['put']
                    if d["hour"] > timedelta(0,0):
                        s = '%s\n%s'%(s,str(d["hour"]))
                    self.itemDraw(row, self._freezeColumnCount + col, s)
                elif self._daysFlag[col] & 608:#32+64 (суббота, воскресенье) + 512( праздничный)
                    self.itemDraw(row, col+self._freezeColumnCount, '')
   
    def redraw(self,fl = 0):
        "Отрисовка, с перечитыванием данных"
        if fl == 0:
            self._kr.getAllData()
            self._kr.getDays(self._date)        
            self.beginRedraw()       
        self.drawAll()
        self.endRedraw()
        self.emit(QtCore.SIGNAL('redraw()'))
        
    def itemDraw(self,row,col,s):
        "Способ отрисовки ячейки"
        item = QtGui.QStandardItem("%s"%s)
        if col >= self._freezeColumnCount:
            dayflag = self._daysFlag[col - self._freezeColumnCount]
            if self._data4Table.getErrorTable(row,col - self._freezeColumnCount) == 1:
                cl = QtGui.QColor()
                cl.setNamedColor('#ff0000')
                br = QtGui.QBrush(cl)
                item.setBackground(br)                
            elif dayflag & 512:#512( праздничный)
                if dayflag & 96:#32+64 (суббота, воскресенье) 
                    cl = QtGui.QColor()
                    cl.setNamedColor('#ff7f7f')
                    br = QtGui.QBrush(cl)
                    item.setBackground(br)
                else:
                    cl = QtGui.QColor()
                    cl.setNamedColor('#ff8f8f')
                    br = QtGui.QBrush(cl)
                    item.setBackground(br)            
            elif (dayflag & 96):#32+64 - суббота или воскресенье
                br = QtGui.QBrush(QtCore.Qt.lightGray)
                item.setBackground(br)
        self._mdl.setItem(row,col,item)
    
    def setBrigada(self,idbrigada):
        self._dialog.setBrigada(idbrigada)
        self._data4Table.setBrigada(idbrigada)
        self._brigada = idbrigada
        self._sdvig, self._sdvig2 = self._brigadasdvig.getSdvig(self._brigada)
        self.redraw()
        
    def setDate(self, ddate):
        self._daysFlag = self._df.getData(ddate)
        self._data4Table.setDate(ddate)
        self._date = ddate
        self._kr.getDays(self._date)
        self.redraw()
        
    def addKarta(self):
        self._dialog.setModal(True)
        self._dialog.show()
        self._initKostil()
    
    def addDop(self):
        data = self.sender().data().toPyObject()
        sel = self.getSelectionModel()
        idkartarange = data[0]
        keys = sorted(sel.keys())
        self.clearKarta(sel)#очищаем данные под выделением
        self._datagrafik.beginIns()#инициализируем данные для вставки
        for row in keys:
            for col in sel[row]:
                idvoditel = self._data4Table.getHeader(row)["idvod"] #получаем ид водителя из таблицы вертикального заголовка
                curdate = date(self._date.year, self._date.month, col+1)#текущая, рассматриваемая дата
                self._datagrafik.addIns(idkartarange, idvoditel, curdate, self._brigada, 1)
        self._datagrafik.endIns()
        self._mdls.select(self._selmod, QtGui.QItemSelectionModel.SelectCurrent)        
        self.redraw(1)

    def sdvig(self, datarow, col):
        #«Сдвиг»
        #входной парамер - строка из таблицы картасорта
        #если необходимо взять день из 2строки выходного, а его нет — пытаемся взять из второй строки
        #если необходимо взять день из 1строки выходного, а его нет — берем из первой строки
        #если необходимо взять день из 2строки, а его нет — берем из первой строки
        if len(datarow[col]) > 0:
            idkarta = datarow[col]["idkarta"]
            idtype = datarow[col]["idtype"]
        else:#иначе делаем сдвиг
            col -= 2
            if len(datarow[col]) > 0:
                idkarta = datarow[col]["idkarta"]
                idtype = datarow[col]["idtype"]
            else:#если всеже была 4-я колонка, а при этом даже 2-й не было - берем данные из 1-й
                idkarta = datarow[0]["idkarta"]
                idtype = datarow[0]["idtype"]
        return (idkarta, idtype)

    def firstGetIdkr(self, res, row, col):
        self._brow = row #первая строка, выбранная на графике
        self._bcol = col #первая колонка, выбранная на графике +1 = первый выбранный день
        self._bcor = 0 #коррекция горизонтальных сдвигов при работе с напарниками водителями
        self._bcor2 = 0 #коррекция горизонтальных сдвигов при смене строки
        self._bselrow = res[0] #выбранная строка в картасорт бригаде
        self._bselcol = res[1] #выбранная колонка в картасорт бригаде
        self._bcurrow = self._bselrow #текущая строка в картасорт бригаде
        self._bcurcol = self._bselcol #текущая колонка в картасорт бригаде
        self._lastrow = self._brow #строка, по которой будет виден переход к другой строке
        self._dataKs = self._ks.getDataMass(self._brigada) #получаем сетку карточек в виде таблице со значениями-словарями
        self._bidbus = self._data4Table.getHeader(row)["idbus"]

    def nexgettIdkr(self, row, col):
        idkartarange = idtype = -1
        if row != self._lastrow: 
            self._bcor2 += self._sdvig2
            idbus = self._data4Table.getHeader(row)["idbus"]
            if idbus == self._bidbus and idbus>0:
                if self._bcurcol == 0:
                    self._bcurcol = 1
                else:
                    self._bcurcol = 0
                self._bidbus = idbus
                self._bcor -= self._sdvig
            else:
                self._bcurcol = 0
                self._bidbus = idbus
            self._lastrow = row
        #«Следующий»
        #строка не сменилась, кольцевой сдвиг по сетке на разницу от начального значения
        horsdvig =  ((row - self._brow) + (col - self._bcol) + self._bcor + self._bcor2 + self._bselrow )% len(self._dataKs)
        if self._daysFlag[col] & 608:#если выходной или праздничные
            curcol = self._bcurcol + 2
        else:
            curcol = self._bcurcol
        idkartarange, idtype = self.sdvig(self._dataKs[horsdvig], curcol)
        curdate = date(self._date.year, self._date.month, col+1)
        if idtype == 0:
            #проверяем на возможно ли значение в данной строке/колонке
            idkartarange, sort = self._kr.getKartaRange(idkartarange, curdate)
            if idkartarange == 0:
                idtype = 1
                idkartarange = self.getIdDefault(row,col) 
        return idkartarange, idtype 
            
    def save(self):
        #f, dr, dopsokr = self._dop.getData()#берем данные для отображения дополнительных карточек
        sel = self.getSelectionModel()#преобразовываем выделение в сетку строк и стобцов
        result = self._dialog.getResult()#получаем данные от диалогового окна
        if result[0] >= 0:#если что-то найдено
            self.clearKarta(sel)#очищаем данные под выделением
            self._datagrafik.beginIns()#инициализируем данные для вставки
            keys = sorted(sel.keys())#получаем строки в упорядоченном виде
            self.firstGetIdkr(result, keys[0], sel[keys[0]][0])#подготавливаем данные сетки для получения очердной карточки при множественном выборе
            for row in keys:
                for col in sel[row]:
                    idkartarange, idtype = self.nexgettIdkr(row,col)#получаем следующее значение сетки в виде ид карточки и ее тип
                    if idkartarange > 0:#если найдено без ошибок
                        idvoditel = self._data4Table.getHeader(row)["idvod"] #получаем ид водителя из таблицы вертикального заголовка
                        curdate = date(self._date.year, self._date.month, col+1)#текущая, рассматриваемая дата
                        self._datagrafik.addIns(idkartarange, idvoditel, curdate, self._brigada, idtype)
            self._datagrafik.endIns()
        self.redraw(1)
        self._mdls.select(self._selmod, QtGui.QItemSelectionModel.SelectCurrent)
        self._kostil()
        
    def clearKarta(self, sel = []):
        if len(sel)==0:
            sel = self.getSelectionModel()
        keys = sorted(sel.keys())
        self._datagrafik.beginDel(1)
        for row in keys:
            for col in sel[row]:
#                idvg = self._data4Table.getData(row, col)
#                if len(idvg)>0:
#                    self._datagrafik.addDel(idvg['id'])
                curdate = date(self._date.year, self._date.month, col+1)
                idvod = self._data4Table.getHeader(row)["idvod"] 
                self._datagrafik.addDel2(idvod, curdate)
                
                self.itemDraw(row,col + self._freezeColumnCount,'')
        self._datagrafik.endDel()
        self._mdls.select(self._selmod, QtGui.QItemSelectionModel.SelectCurrent)        
        self.redraw(1)
    
    def getSelectionModel(self):
        'Возвращяем выделение в виде словаря с ключами-строками и отсортированным списком колонок'
        sel = {}
        rowcol = self.selectionModel().selectedIndexes()
        for i in rowcol:
            row = i.row()
            col = i.column() - self._freezeColumnCount
            if col >= 0:
                try:
                    sel[row] 
                except KeyError:
                    sel[row] = []
                sel[row] += [col]
        self._selmod = i
        for i in sel.keys():
            sel[i].sort()
        return sel
    
    def kodChange(self,state):
        self._drawtype = 1 - state / 2
        self.redraw()
        
    def getErrorData(self):
        return self._data4Table.getErrorData()
        
    def getRezHour(self):
        '''Возвращаем время резерва в последней выбранной ячейке '''
        rrow = 0
        ccol = 0
        hour = timedelta(0,0)
        sel = self.getSelectionModel()
        keys = sorted(sel.keys())
        self._datagrafik.beginDel()
        for row in keys:
            rrow = row
            for col in sel[row]:
                ccol = col
        data = self._data4Table.getData(rrow, ccol)
        if len(data) > 0:
            if (data["idtype"] == 1 and 
                (data["idkartarange"] == 2 or data["idkartarange"] == 9)):
                    hour = data["hour"]
        return hour

    def setRezHour(self, hour):
        if hour == time(7,0):#если стоит 7 часов, изменять не нужно
            hour = timedelta(0,0)
        rrow = 0#начальные значения
        ccol = 0
        sel = self.getSelectionModel()#получаем выделение 
        keys = sorted(sel.keys())#находим координаты последнего выделенного
        for row in keys:
            rrow = row
            for col in sel[row]:
                ccol = col
        data = self._data4Table.getData(rrow, ccol)
        if len(data)>0:
            if (data["idtype"] == 1 and 
                (data["idkartarange"] == 2 or data["idkartarange"] == 9)):
                    self._datagrafik.updateHour(hour, data["id"])
            else:
                self._datagrafik.beginDel()
                self._datagrafik.addDel(data["id"])
                self._datagrafik.endDel()
        else:
            t = 1
            idkr = 2            
            idvod = self._data4Table.getHeader(rrow)["idvod"] 
            curdate = date(self._date.year, self._date.month, ccol+1)
            self._datagrafik.insertHour(idkr, idvod, curdate, self._brigada, t, hour)
        self.redraw(1)
        
    def changeSmena(self):
        '''Обмен сменами между двумя водителями, к примеру обмен выходного на 
        карточку и наоборот 
        
        1. Проверяем чтобы выделенных строк было 2, иначе соообщаем, что не верно 
        выбраны строки
        
        2. Получаем колонки, которые совпадают по дням, т.е. при выборе первой строки
        от 1 до 5, а второй от 3 до 7 обмен будет совершен только между 3 и 5 колонками
        в случае если не будет ни одного пересечения выдать сообщение, что 
        пересечения не найдены
        
        3. По полученным координатам начинаем обмен - получаем данные по выделенным
        ячейкам, очищаем обмениваемые ячейки в базе (1 запрос) и делаем вставку 
        всех элементов (2-й запрос)'''
        #1
        self._initKostil()
        sel = {}
        nrow = 0
        nrowid = []#сохраняем коды строк
        rowcol = self.selectionModel().selectedIndexes()
        try:
            #проходим по всему выделению, сохраняем строки и колонки, которые были выделены
            for i in rowcol:
                row = i.row()
                col = i.column() - self._freezeColumnCount
                if col >= 0:
                    try:
                        sel[row] 
                    except KeyError:
                        nrow += 1
                        nrowid += [row]
                        if nrow >2:
                            raise MyError(u'Выделено больше двух строк')
                        sel[row] = []
                    sel[row] += [col]
            selmod = i
            if nrow < 2:
                raise MyError(u'Выделено меньше двух строк')
            #2
            setrows = []
            for i in nrowid:
                setrows += [set(sel[i])]
            setrows = list(setrows[0] & setrows[1])
            setrows.sort()
        except MyError as e:
            dlg = QtGui.QMessageBox(self)
            dlg.setWindowTitle(u'Ошибка')
            dlg.setIcon(QtGui.QMessageBox.Critical)
            s = e.value
            dlg.setText(s)
            dlg.setStandardButtons(QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
        #3.1 сохраняем данные для обмена        
        dataold = {}
        for row in nrowid:
            dataold[row] = {}
            for col in setrows:
                dataold[row][col] = self._data4Table.getData(row, col)
        #pprint.pprint(dataold)
        #3.2 очищаем данные
        self._datagrafik.beginDel(1)
        for row in nrowid:
            for col in setrows:
                curdate = date(self._date.year, self._date.month, col+1)
                idvod = self._data4Table.getHeader(row)["idvod"] 
                self._datagrafik.addDel2(idvod, curdate)
                self.itemDraw(row,col + self._freezeColumnCount,'')
        self._datagrafik.endDel()
    
        #3.3 добавляем данные развернув строки
        self._datagrafik.beginIns()#инициализируем данные для вставки
        n = 0#для обращения строк
        for row in nrowid:
            for col in setrows:
                if len(dataold[row][col]) > 0:
                    idkartarange = dataold[row][col]["idkartarange"]
                    idtype = dataold[row][col]["idtype"]
                else:
                    idkartarange = self.getIdDefault(row,col) 
                    idtype = 1
                    
                idvoditel = self._data4Table.getHeader(nrowid[1-n])["idvod"] #получаем ид водителя из таблицы вертикального заголовка
                curdate = date(self._date.year, self._date.month, col+1)#текущая, рассматриваемая дата
                self._datagrafik.addIns(idkartarange, idvoditel, curdate, self._brigada, idtype)
            n += 1
        self._datagrafik.endIns()
        self.redraw()
        self._mdls.select(selmod, QtGui.QItemSelectionModel.SelectCurrent)
        self._kostil()
    
    def saveKarta(self, idkarta):
        self._initKostil()
        sel = self.getSelectionModel()
        self.clearKarta(sel)
        self._datagrafik.beginIns()
        keys = sorted(sel.keys())
        for row in keys:
            for col in sel[row]:
                curdate = date(self._date.year, self._date.month, col+1)#текущая, рассматриваемая дата
                idkartarange, sort = self._kr.getKartaRange(idkarta, curdate)
                idtype = 0
                if idkartarange == 0:
                    idtype = 1
                    idkartarange = self.getIdDefault(row,col) 
                idvoditel = self._data4Table.getHeader(row)["idvod"] #получаем ид водителя из таблицы вертикального заголовка
                self._datagrafik.addIns(idkartarange, idvoditel, curdate, self._brigada, idtype)
        self._datagrafik.endIns()
        self.redraw(1)
        self._mdls.select(self._selmod, QtGui.QItemSelectionModel.SelectCurrent)
        self._kostil()
    
    def saveExcel(self):
        wb = xlwt.Workbook()
        ws = wb.add_sheet(u'график')
        #подготавливаем стили, шрифты
        font1 = xlwt.Font()
        font1.name = 'Times New Roman'
        font1.bold = True
        font1.height = 14*20
        #font0.colour_index = 2

        style0 = easyxf(
            'font:name Times New Roman, height 280;'
            'alignment:horizontal left, vertical top;'
            )
        style1 = easyxf(
            'font:name Times New Roman, height 280, bold true;'
            'alignment:horizontal center, vertical top;'
            )
        style2 = easyxf(
            'font:name Times New Roman, height 280;'
            'alignment:horizontal center, vertical center, wrap true;'
            'borders: left thin, right thin, top thin, bottom thin'
            )
        style2B = easyxf(
            'font:name Times New Roman, height 420, bold true;'
            'alignment:horizontal center, vertical center, wrap true;'
            'borders: left thin, right thin, top thin, bottom thin'
            )
        style21 = easyxf(
            'font:name Times New Roman, height 280;'
            'alignment:horizontal center, vertical center, wrap true;'
            'borders: left thin, right thin, top thin, bottom thin;'
            'pattern: fore_color gray25, pattern solid_fill'
            )
        style21B = easyxf(
            'font:name Times New Roman, height 420, bold true;'
            'alignment:horizontal center, vertical center, wrap true;'
            'borders: left thin, right thin, top thin, bottom thin;'
            'pattern: fore_color gray25, pattern solid_fill'
            )
        style3 = easyxf(
            'font:name Times New Roman, height 280;'
            'alignment:horizontal left, vertical center;'
            'borders: left thin, right thin, top thin, bottom thin'
            )
        style3l = easyxf(
            'font:name Times New Roman, height 320;'
            'alignment:horizontal left, vertical center;'
            'borders: left thin, right thin, top thin, bottom thin'
            )
        style4 = easyxf(
            'font:name Times New Roman, height 280;'
            'alignment:horizontal center, vertical center;'
            'borders: left thin, right thin, top thin, bottom thin',
            num_format_str ='#,##0.00'
            )

        stylepr = easyxf(
            'font:name Times New Roman, height 280;'
            'alignment:horizontal center, vertical center, wrap true;'
            'borders: left thin, right thin, top thin, bottom thin;'
            'pattern: fore_color red, pattern solid_fill'
            )
        styleprB = easyxf(
            'font:name Times New Roman, height 420, bold true;'
            'alignment:horizontal center, vertical center, wrap true;'
            'borders: left thin, right thin, top thin, bottom thin;'
            'pattern: fore_color red, pattern solid_fill'
            )
        styleprvih = easyxf(
            'font:name Times New Roman, height 280;'
            'alignment:horizontal center, vertical center, wrap true;'
            'borders: left thin, right thin, top thin, bottom thin;'
            'pattern: fore_color dark_red, pattern solid_fill'
            )
        styleprvihB = easyxf(
            'font:name Times New Roman, height 420, bold true;'
            'alignment:horizontal center, vertical center, wrap true;'
            'borders: left thin, right thin, top thin, bottom thin;'
            'pattern: fore_color dark_red, pattern solid_fill'
            )
        #выдаем данные
        nlist = 0#номер листа
        smesh = 0
        dmonth = self._freeColumnCount
        cmonth = u'месяц'
        cyear = u'год'
        nrowlist = 24 
        #повторяем, пока данные не закончатся
        dataend = False
        while not dataend:
            #заполняем таблицу
            ncol=self._freezeColumnCount#столбцов перед данными
            #1 строка
            ws.write_merge(nlist*nrowlist +smesh, nlist*nrowlist +smesh,
                0, dmonth+ncol, "%s %s %s %s %s %s %s"%(u'Утверждаю: ',22*'_',
                u' Директор филиала "Автобусный парк №1"',90*' '
                ,u'Согласовано: ',22*'_',
                u' Профсоюзный  комитет'), style0)
            ws.row(nlist*nrowlist +smesh).height = 900
            #2 строка
            smesh +=1
            ws.write_merge(nlist*nrowlist +smesh, nlist*nrowlist +smesh,
                0 ,dmonth+ncol-1, u'График водителей филиала "Автобусный парк №1"'
                u' на %s месяц %sгод'%(cmonth, cyear),style1)
            #ws.write(nlist*self.nrowlist +smesh, dmonth+ncol, u'168 ч',style1)
            ws.row(nlist*nrowlist +smesh).height = 900
            #3 строка
            smesh+=1
            ws.col(0).width = 1500
            ws.col(1).width = 6000
            ws.col(dmonth+ncol).width = 3000

            for i in range(0,dmonth):
                ws.col(i+ncol).width = 1700
            ws.row(nlist*nrowlist +smesh).height = 700
            ws.write(nlist*nrowlist +smesh,0, u'№ п/п',style2)
            ws.write(nlist*nrowlist +smesh,1, u'ФИО',style2)
            ws.write(nlist*nrowlist +smesh,2, u'Гаражный',style2)
            ws.write(nlist*nrowlist +smesh,dmonth+ncol, u'Часов',style2)

            rabh = 0 #рабочих часов
            for i in range(0,dmonth):
                dayflag = self._daysFlag[i]
                if dayflag & 512:#512( праздничный)
                    if dayflag & 96:#32+64 (суббота, воскресенье) 
                        style = styleprvih
                    else:
                        style = stylepr
                elif dayflag & 96:#32+64 - суббота или воскресенье
                    style = style21
                else:
                    style =style2
                    rabh+=8
                ws.write(nlist*nrowlist +smesh,i+ncol, i+1,style)
            ws.write(nlist*nrowlist +smesh-1, dmonth+ncol, u'%d ч'%rabh,style1)

            #4 и следующие строки
            smesh+=1
            for j in range(nlist*nrowlist , (nlist+1)*nrowlist ):
                if j >= self._data4Table.lenHeader():
                    dataend = True
                else:
                    header = self._data4Table.getHeader(j)
                    j1=j+smesh
                    ws.row(j1).height = 680
                    ws.write(j1,0, str(j+1)+".",style3)
                    ws.write(j1,1, header["fio"] ,style3l)
                    ws.write(j1,2, header["garaj"] ,style3l)
                    tm = header["alltime"].seconds/3600.0 + header["alltime"].days*24
                    ws.write(j1,dmonth+ncol, tm,style4)
                    for i in range(0,dmonth):
                        fl = 0 #флаг вывода жирным
                        
                        data = self._data4Table.getData(j,i)
                        if len(data) == 0:
                            data = {}
                            data["idtype"] = 0
                            data["name"] = ''
                        if  data["idtype"] == 1:
                            fl = 1
                        if fl == 0:
                            style = style2
                        else:
                            style = style2B
                        dayflag = self._daysFlag[i]
                        if dayflag & 512:#512( праздничный)
                            if dayflag & 96:#32+64 (суббота, воскресенье) 
                                style = styleprvih
                            else:
                                style = stylepr
                        elif dayflag & 96:#32+64 - суббота или воскресенье
                            style = style21
                        else:
                            style =style2
                        ws.write(j1, i+ncol, data["name"],style)
                    
            nlist+=1
        ws.portrait = False#альбомная печать
        ws.print_scaling = 59#масштаб печати
        ws.top_margin = 0/2.54#поля печати
        ws.bottom_margin = 0/2.54#поля печати
        ws.left_margin = 0#поля печати
        ws.right_margin = 0#поля печати

        ws.header_str=''
        ws.footer_str=''
        #ws.header_margin = False#печать колонтитулов
        #ws.footer_margin = False

        ws.page_preview = True
        wb.save(u'график.xls')

        dlg = QtGui.QMessageBox()
        dlg.setWindowTitle(u'Создание отчета')
        dlg.setText(u'Файл отчета создан успешно')
        #dlg.setInformativeText(u'Вы действительно хотите удалить запись?')
        dlg.setStandardButtons(QtGui.QMessageBox.Ok );
        ok = dlg.exec_()
    
    def exportToDbf(self):
        db = dbf.Dbf("b:\\GRAFIK.DBF", new=True)
        days = []
        days +=[("TDATA", "C", 6),
            ("KODBR", "N", 1),
            ("GARNM", "N", 5),
            ("TABEL", "N", 5)]
        for i in range(31):
            days += [("DEN%02d"%(i+1), "C", 8)]
        
        days += [("VREMA", "N", 3, 0)]
        
        for i in days:
            try:
                db.addField(i)
            except:
                print i
        cols = calendar.monthrange(self._date.year, self._date.month)[1]
        for row in range(self._data4Table.lenHeader()):
            rec = db.newRecord()
            header = self._data4Table.getHeader(row)
            rec["TDATA"] = "%04d%02d"%(self._date.year, self._date.month)
            rec["KODBR"] = self._brigada
            rec["GARNM"] = header["garajfull"]
            rec["TABEL"] = header["tabelnmbr"]
            sm = 0
            for col in range(cols):
                namerec = "DEN%02d"%(col+1)
                data = self._data4Table.getData(row,col)
                if data["idtype"] == 0:
                    s = " %d"%data["put"]
                    sm = data["smena"]
                    s = "%s%s%d"%(s,(" "*(7-len(s))),sm)
                else:
                    if data["idkartarange"] == 2 or data["idkartarange"] == 9:
                        if data["hour"] != timedelta(0,0):
                            s = u"Р%d"%(data["hour"].seconds/3600 +data["hour"].days * 24)
                        else:
                            s = u"Р"
                        if data["idkartarange"] == 2:
                            sm = 1
                        else:
                            sm = 2
                        s = "%s%s%d"%(s,(" "*(7-len(s))), sm)
                    elif data["idkartarange"] == 1 or data["idkartarange"] == 8:
                        if sm > 0:
                            s = "%s%s%d"%(data["name"],(" "*(7-len(data["name"]))),sm)
                        else:
                            s = "%s"%(data["name"])
                    else:
                        s = "%s"%(data["name"])
                rec[namerec] = s.encode("cp866")
            rec["VREMA"] = round(header["alltime"].seconds/3600.0 + header["alltime"].days*24,0)
            rec.store()
        db.close()
        dlg = QtGui.QMessageBox()
        dlg.setWindowTitle(u'Сохранение данных')
        dlg.setText(u'Данные сохранены успешно')
        #dlg.setInformativeText(u'Вы действительно хотите удалить запись?')
        dlg.setStandardButtons(QtGui.QMessageBox.Ok );
        ok = dlg.exec_()
        
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class MyError(Exception):
    def __init__(self, value):
        self.value = value
        
    def __str__(self):
        return repr(self.value)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class sprDataGrafik(QtGui.QMdiSubWindow):
    def __init__(self, mysql, parent = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'График водителей')    
        
        self._cmbbrigada = comboBrigada(mysql, parent)
        self._cmbbrigada.redraw()
        
        self._dialog = dialogRezerv(parent)
        self.connect(self._dialog, QtCore.SIGNAL("accepted()"),self.saveRezerv)        
        
        self._dop4menu = getDop(mysql)
        self._begyear = 2011
        self._countyear = 10
        
        self._data = [] #список строк с данными о колонках в словарях
        
        self._table = drawFreezeTable(mysql, parent)
        self._table.setBrigada(self._cmbbrigada.getSelect())
        #self._table.redraw()
        self._listerror = listError(self)
        self._reptable = tableRepeat(mysql, parent)
        self.redrawerr()
        
        self._krb = dialogGetKartaBrigada(mysql, parent)
        self._krb.setIdBrigada(self._cmbbrigada.getSelect())
        self.connect(self._krb, QtCore.SIGNAL("accepted()"),self.saveKarta)        
        self.initWidget()
        self.initAction()
        
        
    def initWidget(self):
        widg = QtGui.QWidget()

        years = []
        for i in range(self._begyear,self._begyear + self._countyear):
            years += [str(i)] 
        lbl1 = QtGui.QLabel(u'Год')
        lbl2 = QtGui.QLabel(u'Месяц')
        lbl3 = QtGui.QLabel(u'Бригада')
        self._cmbyear = QtGui.QComboBox()
        self._cmbyear.addItems(years)
        months = [u'январь', u'февраль' ,u'март' ,u'апрель', u'май', u'июнь',
                  u'июль' ,u'август' ,u'сентябрь' ,u'октябрь' ,u'ноябрь', u'декабрь']
        self._cmbmonth = QtGui.QComboBox()
        self._cmbmonth.addItems(months)
        
        self._chdrawtype = QtGui.QCheckBox(u'Имя/код')
        self._chdrawtype.setCheckState(2)
        
        self._chdotsvih = QtGui.QCheckBox(u'Показать отс.вых.')
        self._chdotsvih.setCheckState(0)
        
        self.btn = QtGui.QPushButton(u'Обновить данные')
        self.btnChange = QtGui.QPushButton(u'Обменять смены')
        self.btnExcel = QtGui.QPushButton(u'Передать в Excel')
        self.btnDbf = QtGui.QPushButton(u'Передать в "Путевые листы"')
        
        hb = QtGui.QHBoxLayout()
        gr = QtGui.QGridLayout()
        
        grdate = QtGui.QGridLayout()
        grdate.addWidget(lbl1, 0, 0) 
        grdate.addWidget(lbl2, 0, 1) 
        grdate.addWidget(lbl3, 0, 2) 
        grdate.addWidget(self._chdrawtype , 0, 3) 
        grdate.addWidget(self._chdotsvih, 0, 4) 
        grdate.addWidget(self.btnExcel, 0, 5) 
        
        grdate.addWidget(self._cmbyear, 1, 0) 
        grdate.addWidget(self._cmbmonth, 1, 1) 
        grdate.addWidget(self._cmbbrigada, 1, 2) 
        grdate.addWidget(self.btnChange, 1, 3) 
        grdate.addWidget(self.btn, 1, 4) 
        grdate.addWidget(self.btnDbf, 1, 5) 
        
        hb.addLayout(grdate)
        hb.addStretch()

        gr.addLayout(hb, 0, 0)
        
        spl1 = QtGui.QSplitter(QtCore.Qt.Horizontal)        
        spl1.addWidget(self._table)
        spl2 = QtGui.QSplitter(QtCore.Qt.Vertical)
        spl2.addWidget(self._reptable)
        spl2.addWidget(self._listerror)
        spl1.addWidget(spl2)
        
        gr.addWidget(spl1)
        widg.setLayout(gr)
        self.setWidget(widg)

        
    def initAction(self):
        '''Инициализация событий и меню'''
        self.cmenu = QtGui.QMenu(self)
        self.connect(self._cmbmonth, QtCore.SIGNAL("currentIndexChanged(int)"),self.chDate)
        self.connect(self._cmbyear, QtCore.SIGNAL("currentIndexChanged(int)"),self.chDate)
        self.connect(self._cmbbrigada, QtCore.SIGNAL("currentIndexChanged(int)"),self.chBrigada)
        self.connect(self.btn, QtCore.SIGNAL("clicked()"),self._table.redraw)
        self.connect(self.btnChange, QtCore.SIGNAL("clicked()"),self._table.changeSmena)
        self.connect(self.btnExcel, QtCore.SIGNAL("clicked()"),self._table.saveExcel)
        self.connect(self.btnDbf, QtCore.SIGNAL("clicked()"),self._table.exportToDbf)
        self.connect(self._table, QtCore.SIGNAL("redraw()"), self.redrawerr)
        #self.connect(self.btnError, QtCore.SIGNAL("clicked()"),self.showError)
        #self.connect(self.btnKarta, QtCore.SIGNAL("clicked()"),self.showKarta)
        
        addAct = QtGui.QAction(u'Добавить карточку (сетка)', self)
        self.connect(addAct, QtCore.SIGNAL("triggered()"), self._table.addKarta)
        addActAll = QtGui.QAction(u'Добавить карточку (бригада)', self)
        self.connect(addActAll, QtCore.SIGNAL("triggered()"), self.addKartaBrigada)
        rmAct = QtGui.QAction(u'Очистить данные', self)
        self.connect(rmAct, QtCore.SIGNAL("triggered()"), self._table.clearKarta)
        rezAct = QtGui.QAction(u'Резерв (установить время)', self)
        self.connect(rezAct, QtCore.SIGNAL("triggered()"), self.addRez)
        
        self.cmenu.addAction(addAct)
        self.cmenu.addAction(addActAll)
        self.cmenu.addAction(rmAct)
        self.cmenu.addAction(rezAct)
        self.cmenu.addSeparator()
        f,dr,d = self._dop4menu.getData()
        for id in dr:
            tmp = QtGui.QAction(d[id]["name"], self)
            data = [id,d[id]["sokr"]]
            tmp.setData(data)
            self.connect(tmp, QtCore.SIGNAL("triggered()"), self._table.addDop)
            self.cmenu.addAction(tmp)
        self.connect(self._chdrawtype, QtCore.SIGNAL("stateChanged(int)"), self._table.kodChange)
        self.connect(self._chdotsvih, QtCore.SIGNAL("stateChanged(int)"), self.chViewOtsVih)

    def contextMenuEvent(self, event):
        "Вызов контекстного меню"
        self.cmenu.exec_(event.globalPos())
    
    def addRez(self):
        hour = self._table.getRezHour()
        if hour > timedelta(0,0):
            self._dialog.setEditMode(hour)            
        else:
            self._dialog.setAddMode()
    
    def addKartaBrigada(self):
        self._krb.setModal(1)
        self._krb.show()

    def saveRezerv(self):
        (hour, mode) = self._dialog.getResult()
        self._table.setRezHour(hour)
    
    def chDate(self):
        d = date(int(self._cmbyear.currentText()),
                    self._cmbmonth.currentIndex()+1,
                    1)
        self._table.setDate(d)
        self._reptable.setDate(d)
    
    def chBrigada(self):
        self._table.setBrigada(self._cmbbrigada.getSelect())
        self._reptable.setBrigada(self._cmbbrigada.getSelect())
        self._krb.setIdBrigada(self._cmbbrigada.getSelect())
        
    def redrawerr(self):
        self._listerror.printerr(self._table.getErrorData())
        self._reptable.redraw()
        
    def chViewOtsVih(self, b):
        if b == 2:
            self._table.setViewOtsVih(True)
        else:
            self._table.setViewOtsVih(False)
    
    def saveKarta(self):
        res = self._krb.getResult()#получаем ид карты
        self._table.saveKarta(res)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++            
class dialogRezerv(QtGui.QDialog):
    def __init__(self, parent):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(u'Установить время резерва')
        self.initWidget()
    
    def initWidget(self):
        grid = QtGui.QGridLayout(self)
        btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(btn1 , QtCore.SIGNAL("rejected()"), self.reject)

        lbl1 = QtGui.QLabel(u'часов')
        self._time = QtGui.QTimeEdit()
        grid.addWidget(lbl1)        
        grid.addWidget(self._time )
        grid.addWidget(btn1)
        
    def setAddMode(self):
        self._time.setTime(QtCore.QTime(7,0))
        self.setModal(1)
        self._mode = 0
        self.show()
    
    def setEditMode(self, timecur):
        h = timecur.seconds / 3600
        m = (timecur.seconds -h*3600)/ 60.0
        self._time.setTime(QtCore.QTime(h,m))
        self.setModal(1)
        self.show()
        self._mode = 1
    
    def getResult(self):
        hour = self._time.time().toPyTime()
        return (hour, self._mode)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class listError(QtGui.QListWidget):
    "Виджет со списком для вывода ошибок текстом"
    def __init__(self,parent):
        QtGui.QListWidget.__init__(self,parent)

    def printerr(self, dataerr):
        '''Реакция на ошибки'''
        
        self.clear()
        err = 0        
        
        for row in dataerr.keys():
            for tmp in dataerr[row]:
                for col, v in tmp.iteritems():
                    if col >= 0:
                        self.addItem(u"[строка %d, день %d] %s"%(row+1,col+1,v))
                        err += 1
                    else:
                        self.addItem(u"[cтрока %d] %s"%(row+1,v))
                        err += 1
        self.addItem("")
        self.addItem(u"Найдено %d ошибок"%err)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableRepeat(QtGui.QTableWidget):
    '''Таблица с количеством повторов смен'''
    def __init__(self, mysql, parent):
        QtGui.QTableWidget.__init__(self,parent)
        self._date = date(2011,1,1)
        self._brigada = 1
        self._getcount = getGrafik(mysql)
        self._getKarta = getKartaBrigada(mysql)#from kartasortbrigada.kartasortbrigada import getDataKartaSort
        self._kr = getKartaRangeId(mysql)#from kartarange.kartarange import getKartaRangeId
        
    def setBrigada(self, brigada):
        self._brigada = brigada
        self.redraw()
        
    def setDate(self,curdate):
        self._date = curdate
        self.redraw()
    
    def redraw(self):
        self._kr.getAllData()
        self._kr.getDays(self._date)
        
        self.setRowCount(0)        
        dd, ddop = self._getcount.getCountData(self._date, self._brigada)
        param = {}
        param["idbrigada"] = self._brigada
        f, dr, d = self._getKarta.getData(param)
        cols = calendar.monthrange(self._date.year, self._date.month)[1]
        header = [u'Имя']
        for col in range(cols):
            header += [ "%d" % (col+1)]
        self.setRowCount(len(dr)+2)#+2 - 2 резерва
        self.setColumnCount(cols+1)
        self.setHorizontalHeaderLabels(header)
        
        row = 0
        for ids in dr:
            name = d[ids]["name"]
            idk = ids#d[ids]["idkarta"]
            put = d[ids]["put"]
            s = "%s (%d)"%(name,put)
            self.itemDraw(row,0,s)
            for col in range(cols):
                try:
                    dd[idk]
                except KeyError:
                    pass
            
                curdate = date(self._date.year, self._date.month, col+1)
                idkartarange, sort = self._kr.getKartaRange(idk, curdate)
                if idkartarange == 0:                
                    self.itemDraw(row,col+1,"-")
                try:
                    dd[idk]["col"][col]
                except KeyError:
                    continue
                countid = dd[idk]["col"][col]
                self.itemDraw(row,col+1,countid )
            row += 1
            
        dr = [2,9]
        d = {}
        d[2] = {}
        d[9] = {}
        d[2]["name"] = u'Р1'
        d[9]["name"] = u'Р2'
        for ids in dr:
            name = d[ids]["name"]
            s = "%s"%(name)
            self.itemDraw(row,0,s)
            for col in range(cols):
                try:
                    ddop[ids]
                except KeyError:
                    break
                try:
                    ddop[ids]["col"][col]
                except KeyError:
                    continue 
                countid = ddop[ids]["col"][col]
                self.itemDraw(row,col+1,countid)
            row += 1

        self.resizeColumnsToContents()
        self.resizeRowsToContents()
            
    def itemDraw(self,row,col,data):
        if type(data) == str or type(data) == unicode:
            item = QtGui.QTableWidgetItem("%s"%data)
            self.setItem(row,col,item)    
        elif type(data) == int or type(data) == long:
            s = str(data)            
            if col == 1:
                if len(s)>4:
                    s = "%s-%s"%(s[:4],s[4:])
            item = QtGui.QTableWidgetItem(s)
            if col == 2:
                item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.setItem(row,col,item)
        
        
    
    