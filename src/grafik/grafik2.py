#!/usr/bin/python
# -*- coding: utf-8  -*-
from standart.getdata import getData, editDatas
from standart.table import freezeTable
from sotrudn.sotrudn import getVodBusBrigada
from kartarange.kartarange import getKartaRange
from dop.dop import getDopoln
import calendar
from datetime import date, timedelta
from PyQt4 import QtGui, QtCore

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class editGrafik(editDatas):
    def __init__(self, mysql):
        editDatas.__init__(self, mysql)
    
    def getDataSql(self, param):
        dateb = param["datebegin"]
        datee = param["dateend"]
        idbrigada = param["idbrigada"]
        sql = '''SELECT id, idkartarange, idvoditel, date, t, hour
            FROM vodgrafik
            WHERE date >= '%s' AND date <= '%s' AND brigada = %d '''%(dateb, datee, idbrigada)
        return sql
    
    def addDataSql(self, param):
        idkartarange = param["idkartarange"]
        idvoditel = param["idvoditel"]
        datecur = param["date"]
        idbrigada = param["idbrigada"]
        typekarta = param["idtypekarta"]
        sql = '''INSERT INTO vodgrafik (idkartarange, idvoditel, date, brigada, t) 
            VALUES (%d, %d, '%s', %d, %d)'''%(idkartarange, idvoditel, datecur,
            idbrigada, typekarta)
        return sql
    
    def addDatasSql(self, params):
        rep = False
        sql = '''INSERT INTO vodgrafik (idkartarange, idvoditel, date, brigada, t) '''
        for param in params:
            idkartarange = param["idkartarange"]
            idvoditel = param["idvoditel"]
            datecur = param["date"]
            idbrigada = param["idbrigada"]
            typekarta = param["idtypekarta"]
            if rep:
                sql = "%s, (%d, %d, '%s', %d, %d)"%(sql, idkartarange, idvoditel, datecur,
                    idbrigada, typekarta)
            else:
                sql = "%s (%d, %d, '%s', %d, %d)"%(sql, idkartarange, idvoditel, datecur,
                    idbrigada, typekarta)
        return sql
    
    def editDataSql(self, param, idedit):
        idkartarange = param["idkartarange"]
        idvoditel = param["idvoditel"]
        datecur = param["date"]
        idbrigada = param["idbrigada"]
        typekarta = param["idtypekarta"]
        hour = param["hour"]
        sql = '''UPDATE vodgrafik SET idkartarange = %d, idvoditel = %d, date = '%s', 
            brigada = '%d', t = %d, hour = '%s' WHERE id = %d'''%(idkartarange, 
            idvoditel, datecur, idbrigada, typekarta, hour)
        return sql
    
    def delDataSql(self, iddel):
        sql = '''DELETE FROM vodgrafik WHERE id = iddel'''
        return sql
    
    def delDatasSql(self, iddels):
        res = False
        sql = '''DELETE FROM vodgrafik WHERE '''
        for iddata in iddels:
            if res:
                sql = '%s OR id = %d'%(sql, iddata)
            else:
                sql = '%s id = %d'%(sql, iddata)
                res = True
        return sql   
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class dataForGrafik:
    '''Класс для обработки данных из базы и получения двух таблиц:
        1 - вертикальный заголовок, с водителями, временем для водителя и др.
        2 - основная таблица с указанием имен карточек, выходных и др.
        
        _header - список вертикального заголовка со значениями словарями ключи:
            idvoditel - id в таблице voditel
            fio - фамилия и инициалы
            garnom - гаражный номер автобуса или 0, если автобус не назначен
            
        _headeradd - список дополнительных колонок в виде словарей с ключами:
            rez - количество резервов
            ots - количество отсыпных
            vih - количество выходных
            workhour - отработанное время
            
        _headerIdVoditel - словарь ключи - ид водителя, значения - строка в таблице(порядок заполнения)
        
        _krdata - словарь с данными по карточкам для данной бригады, 
            ключ - id таблицы kartarange значение словарь с ключами:
                flag - флаг срабатывания карточки
                flagnot - флаг исключения срабатывания карточки
                name - имя карточки
                put - код карточки по путевым листам
                time - время работы по карточке действия
        _dopdata - словарь с данными по дополнительным значениям для графика
            с ключем id таблицы dopoln и значением словарем с ключами:
                name - полное имя
                sokr - сокращенное имя
                timeadd - время по умолчанию добавлем
        
        _data - словарь данных таблицы ключ - номер строки, значение - словарь с ключами:
            номер колонки - словарь с данными, ключи:
                t - тип данных 0 - карточка 1 - дополнительный параметр
                idkr - ссылка на id kartarange или id dopoln
                name - имя карточки или дополнительного параметра
                put  - код карточки или имя дополнительного параметра
                korrtime - подкорректированное время работы или резерва считается если не 0
                timeadd  - время работы по карточке или по резерву
            
            
        '''
    def __init__(self, mysql):
        self.initClass(mysql)
    
    def initClass(self,mysql):
        self._editGrafik = editGrafik(mysql)#для работы с графиком, вывод в таблицу, берем сразу с редактированием
        self._headerVoditel = getVodBusBrigada(mysql)#получаем данные по водителям и используемыми ими автобусы
        self._kr = getKartaRange(mysql)#данные по карточкам используемыми в бригаде
        self._dop = getDopoln(mysql)#дополнительные данные в графике выходной, отсыпной и др.
        
    def setIdBrigada(self, idbrigada):
        self._idbrigada = idbrigada
    
    def getIdBrigada(self):
        return self._idbrigada 
    
    def setCurdate(self, datecur):
        self._datecur = datecur
    
    def getCurdate(self):
        return self._datecur 
    
    def getParamDate(self):
        '''Подготавливаем параметр для отрисовки с начальными и конечными датами '''
        param = {}
        bd = date(self.getCurdate().year, self.getCurdate().month, 1)
        ed = date(self.getCurdate().year, 
                  self.getCurdate().month, 
                  calendar.monthrange(self.getCurdate().year, self.getCurdate().month)[1])
        param["datebegin"] = str(bd)
        param["dateend"] = str(ed)
        return param
    
    def getFIO(self,family,name,surname):
        #TODO вынести эту процедуру в общие функции
        "Получаем Фамилия и инициалы из полной фамилии, имени, отчества"
        f = family
        try:
            i = "%s."%name[0]
        except IndexError:
            i = ''
        try:
            o = "%s."%surname[0]
        except IndexError:
            o = ''
        fio = "%s %s%s"%(f, i, o)
        return fio
        
        
    def fillVerticalHeader(self, param):
        '''Заполняем вертикальный заголовок данными по водителям и используемыми
        ими автобусами'''
        self._header = []
        self._headerIdVoditel = {}
        f, dr, d = self._headerVoditel.getData(param)
        k = 0
        for i in dr:
            tmp = {}
            tmp["idvoditel"] = i
            tmp["fio"] = self.getFIO(d[i]["family"], d[i]["name"], d[i]["surname"])
            tmp["garnom"] = d[i]["garnom"]
            self._header += [tmp]
            self._headerIdVoditel[i] = k
            k += 1
        return self._header 
    
    def getHeader(self, row = -1):
        if row < 0:
            return self._header 
        else:
            return self._header[row]

    def clearAddHeader(self):
        'Очищаем дополнительные колонки'
        self._headeradd = []
        header = self.getHeader()
        for i in header:
            tmpadd = {}
            tmpadd["vih"] = 0
            tmpadd["ots"] = 0
            tmpadd["rez"] = 0
            tmpadd["workhour"] = timedelta(0,0)
            self._headeradd += [tmpadd]

    def addHeader(self, row, tmp):
        'Для вертикального заголовка дополнительные значения'
        if tmp["t"] == 1:
            if tmp["idkr"] == 1:#выходной
                self._headeradd[row]["vih"] += 1
            if tmp["idkr"] == 8:#отсыпной
                self._headeradd[row]["ots"] += 1
            if tmp["idkr"] == 2 or tmp["idkr"] == 9:#резерв
                self._headeradd[row]["rez"] += 1
                if tmp["korrtime"] == timedelta(0,0):
                    self._headeradd[row]["workhour"] += tmp["timeadd"]
                else:
                    self._headeradd[row]["workhour"] += tmp["korrtime"]
        else:
            if tmp["korrtime"] == timedelta(0,0):
                self._headeradd[row]["workhour"] += tmp["timeadd"]
            else:
                self._headeradd[row]["workhour"] += tmp["korrtime"]
        return self._headeradd[row]

    def getAddHeader(self, row = -1):   
        if row < 0:
            return self._headeradd                
        else:
            return self._headeradd[row]
            
    
    def getRowIdVoditel(self, idvoditel):
        "Возвращаем номер строки по ид водителя"
        row = self._headerIdVoditel[idvoditel]
        return row
    
    def getDataIdVoditel(self, idvoditel):
        "Возвращаем словарь с данными хедера по ид водителя"
        data = self._header[self.getRowIdVoditel(idvoditel)]
        return data
        
    def fillData(self, param, krdata, dopdata):
        '''Заполнение основной таблицы, а также добавление к хедеру различных 
        значений, как то, количество выходных, резервов, времени отработано'''
        self._data = {}
        f, dr, d = self._editGrafik.getData(param)
        for i in dr:
            #получаем строку и колонку для вставки туда данных
            idvod = d[i]["idvoditel"]
            row = self.getRowIdVoditel(idvod)
            col = d[i]["date"].day - 1
            try:
                self._data[row] #если записи по данной строке еще нет - создаем
            except KeyError:
                self._data[row] = {}
            #запихиваем по найденной координате данные из базы
            #проверяем есть ли в карточках действия соответствующая графику (проверка на смену бригады в карточке)
            t = d[i]["t"]            
            idkr = d[i]["idkartarange"]            
            tmp = {}
            if t == 0: 
                try:
                    rowkrdata = krdata[idkr]
                except KeyError:
                    print u"Не найдена карточка действия с id = %d для данной бригады, id в vodgarfik = %d"%(idkr,i)
                    continue
                tmp["put"] = rowkrdata["put"]
            else:
                rowkrdata = dopdata[idkr]
                tmp["put"] = rowkrdata["name"]
            tmp["t"] = t
            tmp["idkr"] = idkr
            tmp["korrtime"] = d[i]["hour"]
            tmp["name"] = rowkrdata["name"]
            tmp["timeadd"] = rowkrdata["timeadd"]
            self.addHeader(row, tmp)
            self._data[row][col] = tmp
        return self._data
    
    def getData(self, row = -1, col = -1):
        if row < 0 or col < 0:
            return self._data
        else:
            try:
                return self._data[row][col]
            except KeyError:
                return []
    
    def reReadKartaRange(self, idbrigada):
        '''Перечитываем данные для карточек действия - время работы и вообще
        какие карточки доступны для бригады'''
        param = {}
        param["idbrigada"] = idbrigada
        f, dr, d = self._kr.getData(param)
        self._krdata = {}
        for i in dr:
            idkr = d[i]["idkr"]
            try:#убираем дубляжи id, которые могут возникать, когда назначен период
                self._krdata[idkr] 
            except KeyError:
                self._krdata[idkr] = {}
                self._krdata[idkr]["flag"] = d[i]["flag"]
                self._krdata[idkr]["flagnot"] = d[i]["flagnot"]
                self._krdata[idkr]["name"] = d[i]["name"]
                self._krdata[idkr]["put"] = d[i]["put"]
            bd = d[i]["beginday"]
            bt = d[i]["begintime"]
            ed = d[i]["endday"]
            et = d[i]["endtime"]
            tm = timedelta(bd,0) + bt - (timedelta(ed,0) + et)
            try:
                tm2 = self._krdata[idkr]["timeadd"]
                self._krdata[idkr]["timeadd"] = tm2 + tm * d[i]["delta"]
            except KeyError:
                self._krdata[idkr]["timeadd"] = tm
        return self._krdata
    
    def getKrData(self):
        '''Возвращаем данные по карточкам действия '''
        return self._krdata

    def reReadDop(self):
        f,dr, self._dopdata = self._dop.getData()
        return self._dopdata 
        
    def getDop(self):
        return self._dopdata 
    
    def redraw(self, allredraw = True):#allredraw - все перечитывать
        'Перерасчет всех данных'
        param = self.getParamDate()
        param["idbrigada"] = self.getIdBrigada()
        if allredraw:#перерисовка необязательна только при глобальной
            header = self.fillVerticalHeader(param)
            self.clearAddHeader()
            krdata = self.reReadKartaRange(self.getIdBrigada())
            dopdata = self.reReadDop()
        else:
            header = self.getHeader()
            self.clearAddHeader()
            krdata = self.getKrData()
            dopdata = self.getDop()
        data = self.fillData(param, krdata, dopdata)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class grafikTable(freezeTable):
    def __init__(self, mysql, parent):
        freezeTable.__init__(self, parent)
        self._datagrafik = dataForGrafik(mysql)
        self._date = date(2012,1,1)
        self._brigada = 1 
        self._drawtype = 0 #0 - рисуем имя, 1 - рисуем код из карточек
        self._viewOtsVih = False
        
    def drawAll(self):
        ''' Рисуем вертикальный заголовок, с фамилиями '''
        self._datagrafik.redraw()
        
        header = self._datagrafik.getHeader()
        rows = len(header)
        self._mdl.setRowCount(rows)
        for row in range(rows):
            header = self._datagrafik.getHeader(row)
            self.itemDraw(row, 0, header["fio"])            
            self.itemDraw(row, 1, header["garaj"])
            alltime = "%.2f"%(header["workhour"].seconds/3600.0 + header["workhour"].days*24)
            self.itemDraw(row, 2, alltime)

            if self._viewOtsVih:
                self.itemDraw(row, 3, header["vih"])
                self.itemDraw(row, 4, header["ots"])
                self.itemDraw(row, 5, header["rez"])

            for col in range(self._freeColumnCount):
                d = self._datagrafik.getData(row,col) 
                if len(d)>0:
                    if (self._drawtype == 0) or (d['t']==1):
                        s = d['name']
                    else:
                        s = d['put']
                    if d["korrtime"] > timedelta(0,0):
                        s = '%s\n%s'%(s,str(d["korrtime"]))
                    self.itemDraw(row, self._freezeColumnCount + col, s)
                #elif self._daysFlag[col] & 608:#32+64 (суббота, воскресенье) + 512( праздничный)
                #    self.itemDraw(row, col+self._freezeColumnCount, '')
   
    def redraw(self,fl = True):
        "Отрисовка, с перечитыванием данных"
        if fl:
            self.beginRedraw()       
        self.drawAll()
        self.endRedraw()
        self.emit(QtCore.SIGNAL('redraw()'))
        
    def itemDraw(self,row,col,s):
        "Способ отрисовки ячейки"
        item = QtGui.QStandardItem("%s"%s)
#        if col >= self._freezeColumnCount:
#            dayflag = self._daysFlag[col - self._freezeColumnCount]
#            if self._data4Table.getErrorTable(row,col - self._freezeColumnCount) == 1:
#                cl = QtGui.QColor()
#                cl.setNamedColor('#ff0000')
#                br = QtGui.QBrush(cl)
#                item.setBackground(br)                
#            elif dayflag & 512:#512( праздничный)
#                if dayflag & 96:#32+64 (суббота, воскресенье) 
#                    cl = QtGui.QColor()
#                    cl.setNamedColor('#ff7f7f')
#                    br = QtGui.QBrush(cl)
#                    item.setBackground(br)
#                else:
#                    cl = QtGui.QColor()
#                    cl.setNamedColor('#ff8f8f')
#                    br = QtGui.QBrush(cl)
#                    item.setBackground(br)            
#            elif (dayflag & 96):#32+64 - суббота или воскресенье
#                br = QtGui.QBrush(QtCore.Qt.lightGray)
#                item.setBackground(br)
        self._mdl.setItem(row,col,item)
        
    def setBrigada(self,idbrigada):
        self._datagrafik.setIdBrigada(idbrigada)
        self._datagrafik.redraw()
        self._brigada = idbrigada 
        self.redraw()
        
    def setDate(self, ddate):
        self._datagrafik.setCurdate(ddate)
        self._datagrafik.redraw()
        self._date = ddate
        self.redraw()
