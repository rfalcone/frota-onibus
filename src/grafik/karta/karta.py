#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from datetime import date
from grafik.standart.getdata import getData, editData
from grafik.standart.table import tableData, tableEditData, dialogData, wData
from grafik.standart.combo import comboData

class getKarta(getData):
    '''Выдаем список карточек'''
    def __init__(self, mysql):
        getData.__init__(self, mysql)
    
    def getDataSql(self):
        sql = '''
            SELECT id, name, vihod, smena, put, datecorrect
            FROM karta
            WHERE del = 0
            ORDER BY name '''
        return sql
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class editKarta(editData):
    def __init__(self, mysql):
        self._getKarta = getKarta(mysql)
        editData.__init__(self, mysql)

    def getDataSql(self): 
        return self._getKarta.getDataSql()
 
    def addDataSql(self, param):
        name = param["name"]
        vihod = param["vihod"]
        smena = param["smena"]
        put = param["put"]
        sql = '''INSERT INTO karta (name,  vihod, smena, put) 
            VALUES ('%s', '%s', %d, %d)'''%(name, vihod, smena, put)
        return sql

    def editDataSql(self, param, idkarta):
        name = param["name"]
        vihod = param["vihod"]
        smena = param["smena"]
        put = param["put"]
        sql = '''UPDATE karta SET name = '%s',  vihod = '%s', 
            smena = %d, put = %d WHERE id = %d'''%(name, vihod, smena, put, idkarta)
        return sql
    
    def delDataSql(self, idkarta):
        sql = '''UPDATE karta SET del = 1 WHERE id = %d'''%idkarta
        return sql
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class editKartaExt(editKarta):
    '''Расширяем для возможности отметки даты редактирования карточки'''
    def __init__(self, mysql):
        editKarta.__init__(self, mysql)
    
    def updDateKartaSql(self, param, idkarta):
        datecorrect = param["datecorrect"]
        sql = '''UPDATE karta SET datecorrect = '%s' WHERE id = %d'''%(datecorrect, idkarta)
        return sql
    
    def updDate(self, param, idk):
        self._mysql.execsql(self.updDateKartaSql(param, idk))
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class comboKarta(comboData):
    def __init__(self, mysql, parent = None):
        self._karta = getKarta(mysql)
        comboData.__init__(self, mysql, parent)
    
    def getData(self):
        return self._karta 
    
    def addRow(self, data):
        ss = str(data['put'])
        if len(ss)>4:
            ss = "%s-%s"%(ss[:4],ss[4:])
        return ss 
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class tableKarta(tableData):
    def __init__(self, mysql, parent = None):
        self._karta = getKarta(mysql)        
        tableData.__init__(self, mysql, parent)
    
    def getHeader(self):
        header = [u'Карта', u'Код', u'Смена']
        return header
    
    def getData(self):
        return self._karta
    
    def addRow(self, data, row):
        name = data["name"]
        self.itemDraw(row, 0, name)
        put = data["put"]
        self.itemDraw(row, 1, put)
        smena = data["smena"]
        self.itemDraw(row, 2, smena)
            
    def itemDraw(self,row,col,data):
        if type(data) == str or type(data) == unicode:
            item = QtGui.QTableWidgetItem("%s"%data)
            self.setItem(row,col,item)    
        elif type(data) == int or type(data) == long:
            s = str(data)            
            if col == 1:
                if len(s)>4:
                    s = "%s-%s"%(s[:4],s[4:])
            item = QtGui.QTableWidgetItem(s)
            if col == 2:
                item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.setItem(row,col,item)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableEditKarta(tableEditData):
    def __init__(self, mysql, parent = None):
        self._karta = editKarta(mysql)        
        self._dialog = dialogKarta(parent)
        tableEditData.__init__(self,mysql, parent)

    def getHeader(self):
        header = [u'Карта', u'Код', u'Смена']
        return header
    
    def getData(self):
        return self._karta
    
    def addRow(self, data, row):
        name = data["name"]
        self.itemDraw(row, 0, name)
        put = data["put"]
        self.itemDraw(row, 1, put)
        smena = data["smena"]
        self.itemDraw(row, 2, smena)
            
    def itemDraw(self,row,col,data):
        if type(data) == str or type(data) == unicode:
            item = QtGui.QTableWidgetItem("%s"%data)
            self.setItem(row,col,item)    
        elif type(data) == int or type(data) == long:
            s = str(data)            
            if col == 1:
                if len(s)>4:
                    s = "%s-%s"%(s[:4],s[4:])
            item = QtGui.QTableWidgetItem(s)
            if col == 2:
                item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.setItem(row,col,item)
    
    def getDialog(self):
        return self._dialog
    
    def addData(self):        
        self._dialog.setMode(0, u'Добавить карточку')

    def editData(self):    
        param = {}
        i = self._datarow[self.currentRow()]
        param["name"] = self._data[i]["name"]
        param["vihod"] = self._data[i]["vihod"]
        param["smena"] = self._data[i]["smena"]
        param["put"] = self._data[i]["put"]
        self._dialog.setMode(1, u'Добавить карточку', param)
    
    def getStrForDel(self, data):
        s = data["name"]
        res = u'''Вы действительно хотите удалить запись '%s'?'''%s
        return res        
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableEditKartaExt(tableEditKarta):
    '''Расширяем для возможности отметки даты редактирования карточки'''
    def __init__(self, mysql, parent = None):
        tableEditKarta.__init__(self, mysql, parent)
        self._karta = editKartaExt(mysql)        

    def getHeader(self):
        header = [u'Карта', u'Код', u'Смена', u'Дата коррекции']
        return header   
   
    def updDate(self, curdate, idk):
        param = {}
        param['datecorrect'] = str(curdate)
        self._karta.updDate(param, idk)
    
    def editData(self, row=0, col=0):    
        if self.currentColumn() != 3:
            tableEditKarta.editData(self)
        else:
            idk = self._datarow[self.currentRow()]
            curdate = date.today()
            self.updDate(curdate, idk)
            self.redraw()
            self.setSelect(idk)
    
    def addRow(self, data, row):
        tableEditKarta.addRow(self, data, row)
        curdate = data["datecorrect"]
        self.itemDraw(row, 3, str(curdate))

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class dialogKarta(dialogData):#вспомогательное диалоговое окно
    def __init__(self, mysql, parent = None):
        dialogData.__init__(self, parent)

    def addComponent(self):
        grid = QtGui.QGridLayout()
        lbl1 = QtGui.QLabel(u'Название')
        lbl2 = QtGui.QLabel(u'Выход')        
        lbl3 = QtGui.QLabel(u'Смена')
        lbl4 = QtGui.QLabel(u'Код в путевых')
        self._ed1 = QtGui.QLineEdit()
        self._ed2 = QtGui.QLineEdit()
        self._ed3 = QtGui.QLineEdit()
        self._ed4 = QtGui.QLineEdit()        
        grid.addWidget(lbl1,0,0)
        grid.addWidget(lbl2,1,0)
        grid.addWidget(lbl3,2,0)
        grid.addWidget(lbl4,3,0)        
        grid.addWidget(self._ed1,0,1)
        grid.addWidget(self._ed2,1,1)
        grid.addWidget(self._ed3,2,1)
        grid.addWidget(self._ed4,3,1)        
        return grid
        
    def setAddMode(self):
        self._ed1.setText('')
        self._ed2.setText('0')
        self._ed3.setText('1')
        self._ed4.setText('1')        
    
    def setEditMode(self, param):
        name = param["name"]
        vihod = param["vihod"]
        smena = param["smena"]
        put = param["put"]
        self._ed1.setText(name)
        self._ed2.setText(vihod)
        self._ed3.setText(str(smena))
        self._ed4.setText(str(put))
    
    def getResult(self):
        param = {}
        param["name"] = self._ed1.text()
        param["vihod"] = self._ed2.text()
        param["smena"] = int(self._ed3.text())
        param["put"] = int(self._ed4.text())
        return (param, self._mode)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class wKarta(wData):
    def __init__(self, mysql, parent = None):
        self.setTable(tableEditKarta(mysql, parent))
        wData.__init__(self, mysql, parent)
        
        self.initValue(mysql, parent )

    def setTable(self, tableClass):
        self._kartatable = tableClass
    
    def getTable(self):
        return self._kartatable
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class wKartaExt(wData):
    '''Расширяем для возможности отметки даты редактирования карточки'''
    def __init__(self, mysql, parent = None):
        self.setTable(tableEditKartaExt(mysql, parent))
        wData.__init__(self, mysql, parent)

    def setTable(self, tableClass):
        self._kartatable = tableClass
    
    def getTable(self):
        return self._kartatable    
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class sprKarta(QtGui.QMdiSubWindow):
    def __init__(self, mysql, parent = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Список карточек')        
        self._widg = wKarta(mysql, parent)
        self.setWidget(self._widg)
            
    