#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from grafik.standart.getdata import editDatas
from grafik.standart.table import tableEditData, dialogData, wData

from grafik.karta.karta import wKartaExt
from grafik.kartarange.kartarange import wKartaRange, wKartaRangeDay, wKartaRangeTime

from grafik.kartarange.marsh import comboMarsh, comboMarshOstanov
from grafik.kartarange.kartabrigada import wKartaBrigada
from datetime import timedelta

class kartamarsh(editDatas):
    def __init__(self, mysql):
        editDatas.__init__(self, mysql)
    
    def getDataSql(self, param):
        idkartarange = param["idkartarange"]
        sql = '''
            SELECT km.id, km.begintime, km.idbegin, km.idend, km.day,
                m.id as idmarsh, m.name as mname, 
                o1.name as o1name, o2.name as o2name,
                mo1.dtime as mo1dtime, mo1.obrat,
                mo2.dtime as mo2dtime
            FROM kartamarsh as km, marshrut as m, marshostanov as mo1, marshostanov as mo2,
                ostanov as o1, ostanov as o2
            WHERE km.idbegin = mo1.id AND km.idend = mo2.id AND
                mo1.idostanov = o1.id AND mo2.idostanov = o2.id AND 
                mo1.idmarsh = m.id AND km.del = 0 AND km.idkartarange = %d 
            ORDER BY 5, 2'''%idkartarange 
        return sql

    def addDataSql(self,param):
        idkartarange = param["idkartarange"]
        begintime = param["begintime"]
        day = param["day"]
        idbegin = param["idbegin"]
        idend = param["idend"]
        sql = '''INSERT INTO kartamarsh (idkartarange, begintime, idbegin, idend, day)  
            VALUES (%d, '%s', %d, %d, %d)'''%(idkartarange, begintime, idbegin, idend, day)
        return sql
    
    def addDatasSql(self,param):
        rep = False
        sql = '''INSERT INTO kartamarsh (idkartarange, begintime, idbegin, idend, day) VALUES '''
        for par in param:
            idkartarange = par["idkartarange"]
            begintime = par["begintime"]
            day = par["day"]
            idbegin = par["idbegin"]
            idend = par["idend"]
            if rep:
                sql = "%s, (%d, '%s', %d, %d, %d)"%(sql, idkartarange, begintime, idbegin, idend, day)
            else:
                rep = True
                sql = "%s (%d, '%s', %d, %d, %d)"%(sql, idkartarange, begintime, idbegin, idend, day)
        return sql
    
    def editDataSql(self, param, idkartamarsh):
        begintime = param["begintime"]
        day = param["day"]
        idbegin = param["idbegin"]
        idend = param["idend"]
        sql = '''UPDATE kartamarsh SET begintime = '%s', idbegin = %d, 
            idend = %d, day = %d
            WHERE id = %d'''%(begintime, idbegin, idend, day, idkartamarsh)
        return sql
        
    def delDataSql(self, idkartamarshs):
        res = False
        sql = '''DELETE FROM kartamarsh WHERE '''
        for iddata in idkartamarshs:
            if res:
                sql = '%s OR id = %d'%(sql, iddata)
            else:
                sql = '%s id = %d'%(sql, iddata)
                res = True
        return sql        
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableKartaMarsh(tableEditData):
    def __init__(self, mysql, parent):
        self._getData = kartamarsh(mysql)
        self._dialog = dialogKartaMarsh(mysql, parent)
        self.setIdKartaRange(0)
        tableEditData.__init__(self, mysql, parent)
    
    def setIdKartaRange(self, idKartaRange):
        self._idKartaRange = idKartaRange
    
    def getIdKartaRange(self):
        return self._idKartaRange 
    
    def getHeader(self):    
        return [u'Маршрут', u'Остановка', u'Время отправления', u'Остановка', u'Время прибытия']
        
    def getData(self):
        return self._getData
    
    def getDialog(self):
        return self._dialog 
    
    def getPack(self):
        '''Добавляем параметры для получения данных из базы'''
        param = {}
        param["idkartarange"] = self.getIdKartaRange()
        return self.getData().getData(param)    
        
    def addRow(self, data, row):
        dr = [data["mname"]]
        dr += [data["o1name"]]
        tm = timedelta(data["day"],0) + data["begintime"]
        dr += [str(tm)]
        dr += [data["o2name"]]
        tm = tm + timedelta(0,(data["mo2dtime"] - data["mo1dtime"])*60)
        dr += [str(tm)]
        c = 0
        for i in dr:
            self.itemDraw(row, c, i)    
            c+=1
    
    def addData(self):
        self._dialog.setMode(0, u'Добавить маршрут')
    
    def editData(self):
        data = {}
        iddata = self._datarow[self.currentRow()]
        data["begintime"] = self._data[iddata]["begintime"]
        data["day"] = self._data[iddata]["day"]
        data["idmarsh"] = self._data[iddata]["idmarsh"]
        data["idbegin"] = self._data[iddata]["idbegin"]
        data["idend"] = self._data[iddata]["idend"]
        data["obrat"] = self._data[iddata]["obrat"]
        self._dialog.setMode(1, u'Изменить маршрут', data)
    
    def getStrForDel(self, iddatas):
        s = self._data[iddatas[0]]["mname"]
        if len(iddatas) == 1:
            res = u'''Вы действительно хотите удалить запись '%s'?'''%s
        else:
            res = u'''Вы действительно хотите удалить %d записей?'''%len(iddatas)
        return res        
    
    def addParamForSave(self,param):
        param["idkartarange"] = self.getIdKartaRange()
        return param
    
    def delData(self):    
        iddatas = []
        rows = self.selectionModel().selectedIndexes()
        sort = -1
        for irows in rows:
            row = irows.row()        
            if sort < 0 or sort < row:
                sort = row
            iddata = self._datarow[row]
            iddatas += [iddata]
        
        if len(iddatas) > 0:
            
            dlg = QtGui.QMessageBox(self)
            if len(iddatas) == 1:
                dlg.setWindowTitle(u'Удаление записи')
            else:
                dlg.setWindowTitle(u'Удаление записей')
            s = self.getStrForDel(iddatas)
            dlg.setText(s)
            dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
            if ok==QtGui.QMessageBox.Ok:
                self.getData().delData(iddatas)
                self.redraw()
                self.setSelect(0)
    
    def saveToBuffer(self):
        param = []
        rowcol = self.selectionModel().selectedIndexes()
        for i in rowcol:
            row = i.row()
            iddata = self._datarow[row]
            tmp = {}
            tmp["begintime"] = self._data[iddata]["begintime"]
            tmp["idbegin"] = self._data[iddata]["idbegin"]
            tmp["idend"] = self._data[iddata]["idend"]
            tmp["day"] = self._data[iddata]["day"]
            param += [tmp]
        self._saveBuffer = param
        return param
        
    def pasteFromBuffer(self, param):
        if len(param) > 0:
            for par in param:
                par["idkartarange"] = self.getIdKartaRange()
            iddata = self.getData().addDatas(param)
            self.redraw()
            self.setSelect(iddata)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class dialogKartaMarsh(dialogData):
    def __init__(self, mysql, parent):
        self._marsh = comboMarsh(mysql, parent)
        #self._marsh.redraw()
        idmarsh = self._marsh.getSelect()
        self._marshOstanov = []
        for i in range(2):
            self._marshOstanov += [comboMarshOstanov(mysql, parent)]
            self._marshOstanov[i].setIdMarsh(idmarsh)
            self._marshOstanov[i].setObrat(0)
            #self._marshOstanov[i].redraw()
        dialogData.__init__(self, parent)
    
    def initConnect(self):
        self.connect(self._chb1, QtCore.SIGNAL("stateChanged(int)"), self.expand)
    
    def expand(self, state):
        obrat = 1 - self._marshOstanov[0].getObrat()
        for i in range(2):
            self._marshOstanov[i].setObrat(obrat)
            self._marshOstanov[i].redraw()
        self._marshOstanov[0].setFirst()
        self._marshOstanov[1].setLast()
        
    def addComponent(self):
        grid = QtGui.QGridLayout()
        lbl1 = QtGui.QLabel(u'Маршрут')
        lbl2 = QtGui.QLabel(u'Первая остановка')
        lbl3 = QtGui.QLabel(u'Конечная остановка')
        lbl4 = QtGui.QLabel(u'Время отправления')
        lbl5 = QtGui.QLabel(u'День отправления')
        self._chb1 = QtGui.QCheckBox(u'Прямой маршрут')
        self._chb1.setCheckState(2)
        self._btime = QtGui.QTimeEdit()
        self._day = QtGui.QSpinBox()
        self.initConnect()
        grid.addWidget(lbl1,0,0)
        grid.addWidget(self._marsh,0,1)
        grid.addWidget(lbl2,1,0)
        grid.addWidget(self._marshOstanov[0],1,1)
        grid.addWidget(lbl3,2,0)
        grid.addWidget(self._marshOstanov[1],2,1)
        grid.addWidget(lbl4,3,0)
        grid.addWidget(self._btime,3,1)
        grid.addWidget(lbl5,4,0)
        grid.addWidget(self._day,4,1)
        grid.addWidget(self._chb1,5,0,1,2)
        return grid
    
    def setAddMode(self):
        self._marsh.redraw()
        self._marsh.setSelect(0)
        idmarsh = self._marsh.getSelect()
        for i in range(2):
            self._marshOstanov[i].setIdMarsh(idmarsh)
            self._marshOstanov[i].setObrat(0)
            self._marshOstanov[i].redraw()
        self._marshOstanov[0].setFirst()
        self._marshOstanov[1].setLast()
        self._chb1.setCheckState(2)
        self._btime.setTime(QtCore.QTime(8,0))
        self._day.setValue(0)        
        
    def setEditMode(self,param):
        self._marsh.redraw()
        self._marsh.setSelect(param["idmarsh"])
        idmarsh = self._marsh.getSelect()
        for i in range(2):
            self._marshOstanov[i].setIdMarsh(idmarsh)
            self._marshOstanov[i].setObrat(param["obrat"])
            self._marshOstanov[i].redraw()
        self._marshOstanov[0].setSelect(param["idbegin"])
        self._marshOstanov[1].setSelect(param["idend"])
        self._chb1.setCheckState(2 - (param["obrat"] * 2))
        t = param["begintime"].seconds         
        h = t / 3600
        m = (t - h*3600)/60
        self._btime.setTime(QtCore.QTime(h,m))
        self._day.setValue(param["day"])        
    
    def getResult(self):
        res = {}
        res["idmarsh"] = self._marsh.getSelect()
        res["idbegin"] = self._marshOstanov[0].getSelect()
        res["idend"] = self._marshOstanov[1].getSelect()
        res["begintime"] = self._btime.time().toPyTime()
        res["day"] = self._day.value()        
        return (res, self._mode)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class wKartaMarsh(wData):
    def __init__(self, mysql, parent):
        self.setTable(tableKartaMarsh(mysql, parent))
        wData.__init__(self, mysql, parent)

    def setTable(self, tableClass):
        self._table = tableClass
    
    def getTable(self):
        return self._table
    
    def setIdKartaRange(self, idKartaRange):
        self.getTable().setIdKartaRange(idKartaRange)
    
    def initTool(self):
        tool = QtGui.QToolBar()
        tool.addAction(self.addDataAct)
        tool.addAction(self.editDataAct)
        tool.addAction(self.copyAct)
        tool.addAction(self.pasteAct)
        tool.addSeparator()
        tool.addAction(self.delDataAct)
        tool.setIconSize(QtCore.QSize(36,36))
        return tool
    
    def initAction(self):
        wData.initAction(self)
        self.copyAct = QtGui.QAction(u'Скопировать в буфер',self)
        self.copyAct.setIcon(QtGui.QIcon("../ico/copy.png"))
        self.pasteAct = QtGui.QAction(u'Вставить из буфера',self)
        self.pasteAct.setIcon(QtGui.QIcon("../ico/paste.png"))

    def initConnect(self):
        wData.initConnect(self)
        self.connect(self.copyAct, QtCore.SIGNAL("triggered()"),self.emitSaveToBuffer)
        self.connect(self.pasteAct, QtCore.SIGNAL("triggered()"),self.emitPasteFromBuffer)

    def emitSaveToBuffer(self):
        self.emit(QtCore.SIGNAL('saveToBuffer()'))
    
    def emitPasteFromBuffer(self):
        self.emit(QtCore.SIGNAL('pasteFromBuffer()'))
    
    def saveToBuffer(self):
        return self.getTable().saveToBuffer()
    
    def pasteFromBuffer(self, param):
        self.getTable().pasteFromBuffer(param)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class sprKartaFull(QtGui.QMdiSubWindow):
    def __init__(self, mysql, parent = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Карточки действия')    
        self._karta = wKartaExt(mysql, parent)
        #self._karta.redraw()
        idk = self._karta.getSelect()
        self._kartaRange = wKartaRange(mysql, parent) 
        self._kartaRange.setIdKarta(idk)
        idkr = self._kartaRange.getSelect()
        self._kartaRangeDay = wKartaRangeDay(mysql, parent) 
        self._kartaRangeDay.setIdKartaRange(idkr)
        self._kartaRangeTime = wKartaRangeTime(mysql, parent) 
        self._kartaRangeTime.setIdKartaRange(idkr)
        self._kartaMarsh = wKartaMarsh(mysql, parent)
        self._kartaMarsh.setIdKartaRange(idkr)
        self._kartaMarsh.redraw()
        self._kartaBrigada = wKartaBrigada(mysql,parent)
        self._kartaBrigada.setIdKartaRange(idkr)
        self._kartaBrigada.redraw()
        self.initWidget()
        self.initConnect()
    
    def initWidget(self):
        widg = QtGui.QWidget(self)
        #grd = QtGui.QGridLayout()
        vh = QtGui.QVBoxLayout()

        grpbx1 = QtGui.QGroupBox(u'Карточки')
        vh1 = QtGui.QVBoxLayout() 
        self._grpbx2 = QtGui.QGroupBox(u'Данные по карточке')
        vh2 = QtGui.QVBoxLayout() 
        grpbx3 = QtGui.QGroupBox(u'Дни недели действия карточки')
        vh3 = QtGui.QVBoxLayout() 
        grpbx4 = QtGui.QGroupBox(u'Даты и периоды действия карточки')
        vh4 = QtGui.QVBoxLayout() 
        grpbx5 = QtGui.QGroupBox(u'Время действия карточки и маршруты')
        vh5 = QtGui.QHBoxLayout() 
        
        spl = QtGui.QSplitter(1)
        spl2 = QtGui.QSplitter(1)
        spl3 = QtGui.QSplitter(0)
        spl4 = QtGui.QSplitter(1)

        vh1.addWidget(self._karta)
        grpbx1.setLayout(vh1)
        

        vh3.addWidget(self._kartaRange)
        grpbx3.setLayout(vh3)
        
        vh4.addWidget(self._kartaRangeDay)
        grpbx4.setLayout(vh4)
        
        spl4.addWidget(self._kartaRangeTime)
        spl4.addWidget(self._kartaMarsh)
        vh5.addWidget(spl4)
        
        grpbx5.setLayout(vh5)
        
        spl2.addWidget(grpbx3)
        spl2.addWidget(grpbx4)
        spl2.addWidget(self._kartaBrigada)
        spl3.addWidget(spl2)        
        spl3.addWidget(grpbx5)      
        
        vh2.addWidget(spl3)
        self._grpbx2 .setLayout(vh2)

        spl.addWidget(grpbx1)
        spl.addWidget(self._grpbx2)
        vh.addWidget(spl)
        widg.setLayout(vh)
        self.setWidget(widg)
        
    def initConnect(self):
        self.connect(self._karta, QtCore.SIGNAL("itemSelectionChanged()"),self.chKarta)
        self.connect(self._kartaRange, QtCore.SIGNAL("itemSelectionChanged()"),self.chKartaRange)
        self.connect(self._kartaMarsh, QtCore.SIGNAL("saveToBuffer()"),self.saveToBuffer)
        self.connect(self._kartaMarsh, QtCore.SIGNAL("pasteFromBuffer()"),self.pasteFromBuffer)

    def pasteFromBuffer(self):    
        self.sender().pasteFromBuffer(self._buffer)
    
    def saveToBuffer(self):
        self._buffer = self.sender().saveToBuffer()
    
    def chKarta(self):
        idk = self._karta.getSelect()
        self._kartaRange.setIdKarta(idk)
        s = self._kartaRange.getNameKarta()
        self._grpbx2.setTitle(u'Данные по карточке [%s]'%s)
    
    def chKartaRange(self):
        idkr = self._kartaRange.getSelect()
        self._kartaRangeDay.setIdKartaRange(idkr)
        self._kartaRangeTime.setIdKartaRange(idkr)
        self._kartaMarsh.setIdKartaRange(idkr)
        self._kartaMarsh.redraw()
        self._kartaBrigada.setIdKartaRange(idkr)
        self._kartaBrigada.redraw()
        