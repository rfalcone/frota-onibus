#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from grafik.holiday.holiday import getHoliday
from grafik.karta.karta import wKartaExt
import calendar
from datetime import date, time, timedelta
from string import lower

class dayFlag:
    def __init__(self,mysql):
        self._holiday = getHoliday(mysql)
        self._flag = generateFlag()
    
    def getData(self, yearmonth):
        bday = date(yearmonth.year, yearmonth.month, 1)
        lday = calendar.monthrange(yearmonth.year, yearmonth.month)[1]
        eday = date(yearmonth.year, yearmonth.month, lday)
        f,dr,d = self._holiday.getData(bday, eday)
        days = []
        for i in range(lday):
            spday = []
            for j in range(13):
                spday += [0]
            spday [calendar.weekday(yearmonth.year, yearmonth.month, i+1)] = 2
            for j in dr:
                if d[j]["date"].day == i+1:
                    spday[9] = 2
                if d[j]["date"].day == i+2:
                    spday[10] = 2
            days += [self._flag.getFlag(spday)]
        return days
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class generateFlag:
    def __init__(self):
        self._full = [u'Понедельник', u'Вторник', u'Среда', u'Четверг', u'Пятница', 
                      u'Суббота', u'Воскресенье', u'Рабочие дни', u'Выходные дни', 
                      u'Праздничные дни', u'Предпраздничные дни', u'Все дни']
        
        self._tiny = [u'Пн',u'Вт',u'Ср',u'Чт',u'Пт',u'Сб',u'Вс',u'По рабочим',
                      u'По выходным', u'По праздничным', u'По предпраздничным', 
                      u'Все дни']
        
    def getFlag(self, days, dl = 2):
        '''Выдаем флаг на основании списка'''
        k = 1
        flag = 0
        for i in days:
            flag = flag + k * i / dl
            k <<= 1
        return flag
    
    def getData(self, flag, dl = 2):
        '''Выдаем список на основании флага'''
        fl = [] 
        for i in range(len(self._full)):
            fl += [(flag % 2) * dl]
            flag >>= 1
        return fl
        

    def addDayString(self, dist, source, flower):
        res = ''
        if len(dist)>0:
            if flower:
                res = "%s, %s"%(dist, source)
            else:
                res = "%s, %s"%(dist, lower(source))
        else:
            res = "%s"%(source)
        return res, False

    def getDayRange(self,flag):
        'По флагу возвращаем строку действия карточки'
        zagl = False
        wd = self._tiny
        spis = ''
        #f = 1
        if flag & 2048:#allday 1<<12
            spis, zagl = self.addDayString(spis, wd[11], zagl)
        else:
            if flag & 128:#workday 1<<7
                spis, zagl = self.addDayString(spis, wd[7], zagl)
            else:
                for i in range(5):
                    if flag & (1<<i):
                        spis, zagl = self.addDayString(spis, wd[i], zagl)
            if flag & 256:#weekday 1<<8
                spis, zagl = self.addDayString(spis, wd[8], zagl)
            else:
                for i in range(5,7):
                    if flag & (1<<i):
                        spis, zagl = self.addDayString(spis, wd[i], zagl)
            for i in range(9,12):
                if flag & 1<<i:#holiday, no holiday,  1<<9
                    spis, zagl = self.addDayString(spis, wd[i], zagl)
        return spis
    
    def getDayRangeAll(self,flag,flagnot):
        '''По 2-м флагам возвращаем текстовое сообщение, на какие дни недели 
        распространяется действие'''
        spis = self.getDayRange(flag)
        spis2 = self.getDayRange(flagnot)
        if len(spis2) > 0:
            if len(spis) > 0:
                spis = u"%s КРОМЕ %s"%(spis,lower(spis2))
            else:
                spis = u"КРОМЕ %s"%(lower(spis2))
        return spis
    
    def getFullNames(self):
        return self._full
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class getKartaRangeDay:
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
    
    def initSql(self):
        self._getKartaRangeDaySql = '''
            SELECT kr.id, krd.begin, krd.end, krd.id as idkrd
            FROM kartarange as kr, kartarangeday as krd
            WHERE kr.id = krd.idkartarange AND kr.del = 0 AND krd.del = 0'''
    def getData(self):
        f,dr,d = self._mysql.getData(self._getKartaRangeDaySql)
        return (f,dr,d)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++            
class getEditKartaRangeDay:
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
    
    def initSql(self):
        self._getKartaRangeDaySql = '''
            SELECT id, begin, end
            FROM kartarangeday
            WHERE idkartarange = %d 
            ORDER BY begin'''
            
        self._addKartaRangeDaySql = '''INSERT INTO kartarangeday 
            (idkartarange, begin, end) VALUES (%d, '%s', '%s')'''
        self._editKartaRangeDaySql = '''UPDATE kartarangeday SET
            begin = '%s', end = '%s' WHERE id = %d'''
        self._delKartaRangeDaySql = '''DELETE FROM kartarangeday WHERE id = %d'''

    def getData(self, idkr):
        sql = self._getKartaRangeDaySql  % (idkr)
        f,dr,d = self._mysql.getData(sql)
        return (f,dr,d)
    
    def addData(self, idkr, begin, end):
        self._mysql.execsql(self._addKartaRangeDaySql%(idkr, str(begin), str(end)))
            
    def editData(self, begin, end, idkrd):
        self._mysql.execsql(self._editKartaRangeDaySql%(str(begin), str(end), idkrd))
        
    def delData(self, idkrd):
        self._mysql.execsql(self._delKartaRangeDaySql%idkrd)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class getKartaRangeTimeType:
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
    
    def initSql(self):
        self._getKartaRangeTimeTypeSql = '''
            SELECT id, name
            FROM krt_typetime
            WHERE del = 0'''
    
    def getData(self):
        f,dr,d = self._mysql.getData(self._getKartaRangeTimeTypeSql)
        return (f,dr,d)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++            
class getKartaRangeTime:
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
    
    def initSql(self):
        self._getKartaRangeTimeSql = '''
            SELECT krt.id, krt.begintime, krt.beginday, krt.endtime, krt.endday, 
                krtt.id as idkrtt, krtt.delta, kr.id as idkr
            FROM kartarange as kr, kartarangetime as krt, krt_typetime as krtt
            WHERE kr.id = krt.idkartarange AND kr.del = 0 AND 
                krt.idkrt_typetime = krtt.id
            ORDER BY 3, 2, 6'''
    
    def getData(self):
        f,dr,d = self._mysql.getData(self._getKartaRangeTimeSql)
        return (f,dr,d)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class getEditKartaRangeTime:
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()

    def initSql(self):
        self._getKartaRangeTimeSql = '''
            SELECT krt.id, krt.begintime, krt.beginday, krt.endtime, krt.endday, 
                krt_t.id as idkrtt, krt_t.delta, krt_t.name
            FROM kartarangetime as krt, krt_typetime as krt_t
            WHERE krt.idkartarange = %d AND krt.idkrt_typetime = krt_t.id 
            ORDER BY 3, 2, 6'''

        self._addKartaRangeTimeSql = '''INSERT INTO kartarangetime
            (idkartarange, begintime, beginday, endtime, endday, idkrt_typetime)
            VALUES (%d, '%s', %d, '%s', %d, %d) '''
        self._editKartaRangeTimeSql = '''UPDATE kartarangetime SET
            begintime = '%s', beginday = %d, endtime = '%s', endday = %d, 
            idkrt_typetime = %d WHERE id = %d'''
        self._delKartaRangeTimeSql = '''DELETE FROM kartarangetime WHERE id = %d'''

    def getData(self, idkr):
        f,dr,d = self._mysql.getData(self._getKartaRangeTimeSql%idkr)
        return (f,dr,d)
    
    def addData(self, idkr, begintime, beginday, endtime, endday, idkrt_type):
        self._mysql.execsql(self._addKartaRangeTimeSql%(idkr, begintime, beginday, endtime, endday, idkrt_type))
            
    def editData(self, begintime, beginday, endtime, endday, idkrt_type, idkrt):
        self._mysql.execsql(self._editKartaRangeTimeSql%(begintime, beginday, endtime, endday, idkrt_type, idkrt))
        
    def delData(self, idkrt):
        self._mysql.execsql(self._delKartaRangeTimeSql%idkrt)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class getKartaRange:
    '''Класс получаем список карточек-действия завазанных на карточке с основными данными'''
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
        self._flag = generateFlag()
        
    def initSql(self):
        self._getKartaRangeSql = ''' 
            SELECT kr.id, kr.sort, k.name, k.vihod, k.smena, k.put, k.id as idk,
                kr.flag, kr.flagnot
            FROM karta as k, kartarange as kr
            WHERE kr.idkarta = k.id AND k.del = 0 AND kr.del = 0
            ORDER BY k.name, kr.sort'''
    
    def getData(self):
        ''' Возвращаем данные в виде справочников - 
        первый idkartarange хранит idkarta
        второй idkarta список с данными по kartarange
        [idkarta] - словарь по id карточек с ключами idkarta
            ["name"] - имя карточки
            ["vihod"] - номер выход
            ["smena"] - смена
            ["put"] - по путевым листам
            ["kartarange"] - список со словарями данных карточек действия (список 
                    нужен, чтобы знать в каком порядке просматривать 
                    карточки-действия, т.к. время и дата действия могут перекрываться)
                ["id"] - id карточки действия
                ["sort"] - порядок (хотя при переборе будет он ясен, зачем он здесь - хз)
                ["flag"] - сумма флагов действия данной карточки-действия
                ["date"] - список времен действия в виде словаря с ключами
                    #заполнение дат - в другом модуле
                    ["begin"] - начальная дата
                    ["end"] - конечная дата
        '''
        #print self._getKartaRangeSql
        f,dr,d = self._mysql.getData(self._getKartaRangeSql)
        kartadata = {}#данные карточек
        idkartarange = {}#какое idkartarange относится к idkarta
        for idkr in dr:
            idk = d[idkr]['idk']
            idkartarange[idkr] = idk
            try:
                kartadata[idk]
            except KeyError: 
                kartadata[idk] = {}
                kartadata[idk]["name"] = d[idkr]["name"]
                kartadata[idk]["vihod"] = d[idkr]["vihod"]
                kartadata[idk]["smena"] = d[idkr]["smena"]
                kartadata[idk]["put"] = d[idkr]["put"]
                kartadata[idk]["kartarange"] = []
            tmp = {}
            tmp["id"] = idkr
            tmp["flag"] = d[idkr]["flag"]
            tmp["flagnot"] = d[idkr]["flagnot"]
            tmp["date"] = []
            kartadata[idk]["kartarange"] += [tmp]
        return (idkartarange, kartadata)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class getEditKartaRange(getKartaRange):
    def __init__(self, mysql):
        getKartaRange.__init__(self,mysql)
    
    def initSql(self):
        getKartaRange.initSql(self)
        self._addKartaRangeSql = '''INSERT INTO kartarange (idkarta, flag, flagnot, sort) 
            VALUES (%d, %d, %d, %d) '''
        self._editKartaRangeSql = '''UPDATE kartarange SET flag = %d, flagnot = %d WHERE id = %d'''
        self._delKartaRangeSql = '''UPDATE kartarange SET del = 1 WHERE id = %d'''
        self._delDownKartaRangeSql = '''UPDATE kartarange SET sort = sort - 1  WHERE sort > %d AND idkarta = %d'''
        self._upKartaRangeSql = '''UPDATE kartarange SET sort = sort + 1  WHERE sort = %d AND idkarta = %d'''
        self._downKartaRangeSql = '''UPDATE kartarange SET sort = sort - 1  WHERE sort = %d AND idkarta = %d'''
        self._setKartaRangeSql = '''UPDATE kartarange SET sort = %d WHERE id = %d'''
    
    def addData(self,idkarta, flag, flagnot, sort):
        self._mysql.execsql(self._addKartaRangeSql%(idkarta, flag, flagnot, sort))
        return self._mysql.getlastid()
    
    def editData(self, flag, flagnot, idkr):
        self._mysql.execsql(self._editKartaRangeSql%(flag, flagnot, idkr))
    
    def delData(self, idkr, sort, idk):
        self._mysql.execsql(self._delKartaRangeSql%idkr)
        self._mysql.execsql(self._delDownKartaRangeSql%(sort,idk))
    
    def upData(self, sort, idkr, idk):
        sql = self._upKartaRangeSql%(sort, idk)
        self._mysql.execsql(sql)
        sql = self._setKartaRangeSql%(sort, idkr)
        self._mysql.execsql(sql)

    def downData(self, sort, idkr, idk):
        sql = self._downKartaRangeSql%(sort, idk)
        self._mysql.execsql(sql)
        sql = self._setKartaRangeSql%(sort, idkr)
        self._mysql.execsql(self._setKartaRangeSql%(sort, idkr))
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class getKartaRangeId:
    '''Работаем с карточками действия и флагами
    для начала получения id карточки действия по id карточки в определенный день
    необходимо инициализировать переменные:
        getAllData 
        getDays
    После чего по функциям:
        getKartaRange -по id карточки и дню проверки, получаем idkartarange и 
            позицию в сортировке, где она хранится в словаре  self.kartadata[idkarta]["kartarange"]
        getDataKartaRange по id карточки и позиции в словаре self.kartadata[idkarta]["kartarange"]
            получаем данные о карточке действия
                ["id"] - id карточки действия
                ["sort"] - порядок (хотя при переборе будет он ясен, зачем он здесь - хз)
                ["flag"] - сумма флагов действия данной карточки-действия
                ["flagnot"] - сумма флагов действия ("кроме"), данной карточки-действия
                
                ["timedelta"] - время с плюсом или минусом для данной карточки действия
                ["time"] - список времени действия
                    ["begintime"]
                    ["endtime"]
                    ["beginday"]
                    ["endday"]
                    ["delta"] - коэффициент прироста или убавления времени
                    ["idkrtt"] - id в таблице krt_typetime
                    ["timedelta"] - время с плюсом или минусом для данной строки времени
                    
                ["begintime"] - время начало действия по путевому листу
                ["endtime"] - конец времени действия по путевому листу
                ["endday"] - сколько раз 00:00 часа между началом и концом времени
                
                ["date"] - список дат действия в виде словаря с ключами
                    #заполнение дат - в другом модуле
                    ["begin"] - начальная дата
                    ["end"] - конечная дата
        
        '''
    def __init__(self, mysql):
        self._kr = getKartaRange(mysql)
        self._krt = getKartaRangeTime(mysql)
        self._krd = getKartaRangeDay(mysql)
        self._dayFlag = dayFlag(mysql)
    
    def addDateToData(self, idkartarange, kartadata):
        #расширяем данные для карточек сроками действия карточек действия
        f, dr, d = self._krd.getData()
        for i in dr:
            try:
                idk = idkartarange[i]
                sort = 0
                for j in kartadata[idk]["kartarange"]:
                    if j["id"] == i:
                        tmp = {}
                        tmp["begin"] = d[i]["begin"]
                        tmp["end"] = d[i]["end"]
                        kartadata[idk]["kartarange"][sort]["date"] += [tmp]
                        break
                    sort += 1
            except KeyError:
                print u"По id = %d таблицы kartarangeday не найдено соответствие"%(i)
        return kartadata
    
    def addTimeToData(self, idkartarange, kartadata):
        #расширяем данные для карточек действия значениями времени
        f, dr, d = self._krt.getData()
        for i in dr:
            idkr = d[i]["idkr"]
            try:
                idk = idkartarange[idkr]
                fl = True
            except KeyError:
                print u"По id = %d таблицы kartarangetime не найдено соответствие"%(i)
                fl = False
            if fl:
                sort = 0
                for j in kartadata[idk]["kartarange"]:
                    if j["id"] == idkr:
                        try:
                            kartadata[idk]["kartarange"][sort]["time"] 
                        except KeyError:
                            kartadata[idk]["kartarange"][sort]["time"] = []
                            kartadata[idk]["kartarange"][sort]["timedelta"] = timedelta(0,0)
                        tmp = {}
                        bd = d[i]["beginday"]
                        ed = d[i]["endday"]
                        bt = d[i]["begintime"]
                        et = d[i]["endtime"]
                        dl = d[i]["delta"]
                        tmp["begintime"] = bt
                        tmp["endtime"] = et
                        tmp["beginday"] = bd
                        tmp["endday"] = ed
                        tmp["idkrtt"] = d[i]["idkrtt"]
                        tmp["delta"] = dl
                        td = timedelta(ed,0) + et - (timedelta(bd,0) + bt)
                        tmp["timedelta"] = td
                        kartadata[idk]["kartarange"][sort]["time"] += [tmp]  
                        tdd = kartadata[idk]["kartarange"][sort]["timedelta"]
                        if dl > 0:
                             tdd += td 
                        elif dl < 0:
                            tdd = tdd - td 
                        kartadata[idk]["kartarange"][sort]["timedelta"] = tdd
                        break
                    sort += 1
        return kartadata

    def getAllData(self):
        '''Дополняем словарь карточек дейсвия днями действия и дополнительной
        информацией в частности временем действия по путевым'''
        self.idkartarange, kartadata = self._kr.getData()
        kartadata = self.addDateToData(self.idkartarange, kartadata)
        kartadata = self.addTimeToData(self.idkartarange, kartadata)
        self.kartadata = kartadata
        
        return kartadata
    
    def getDays(self, yearmonth):
        self._days = self._dayFlag.getData(yearmonth)
        return(self._days)
    
    def getKartaRange(self,idkarta, curdate):
        #перед запуском необходимы расчеты self.getDays и self.getAllData
        idkartarange = sort = 0
        #находим все карточки действия для данной карточки
        row = 0
        try:
            self.kartadata[idkarta]["kartarange"]
            noerror = True
        except KeyError:
            noerror = False
            print "для карточки id = %d не существует времени действия"%idkarta
            print "-"*20
        if noerror:   
            for kr in self.kartadata[idkarta]["kartarange"]:
                find = True
                for i in kr["date"]:
                    find = False
                    if i["begin"] <= curdate and i["end"] >= curdate:
                        find = True
                        break
                if find:
                    if self.compareFlag(self._days[curdate.day-1], kr["flag"], kr["flagnot"]):
                        idkartarange = kr["id"]
                        sort = row
                        break
                row += 1
        return (idkartarange, sort)
    
    def compareFlag(self, flagday, krflag, krflagnot):
        cmf = False
        if krflagnot & flagday: #флаг кроме совпал - значит не верный выбор
            cmf = False
        elif krflag & 2048:#если флаг карты действия все дни
            cmf = True
        elif krflag & flagday:#если есть хоть какое совпадение с флагом дня
            cmf = True
        return cmf
    
    def getDataKartaRange(self, idkarta, sort):
        data = self.kartadata[idkarta]["kartarange"][sort]
        return data
        
    def getDataKartaRange2(self, idkartarange):
        data = {}
        f = True
        try:
            idk = self.idkartarange[idkartarange]
        except KeyError:
            print "kartarange.getDataKartaRange2 KeyError"
            print "idkartarange = %d"%idkartarange
            print "-"*40
            f = False
        if f:
            #sort = 0
            for i in self.kartadata[idk]["kartarange"]:
                if i["id"] == idkartarange:
                    data = i#self.kartadata[idk]["kartarange"][sort]
                    break
                #sort += 1
        return data
    
    def getDataKarta(self, idkartarange):
        data = {}
        idk = self.idkartarange[idkartarange]
        data = self.kartadata[idk]
        return data
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class comboKrtType(QtGui.QComboBox):
    def __init__(self, mysql, parent = None):
        QtGui.QComboBox.__init__(self, parent)
        self._krtt = getKartaRangeTimeType(mysql)
        
    def redraw(self):
        (f, self._dr, self._d) = self._krtt.getData()
        self.clear()
        s = []
        for id in self._dr:
            s += [self._d[id]['name']]
        self.addItems(s)
        
    def setSelect(self, idkrtt):
        if self.count() > 0:
            self.setCurrentIndex(0)
            k = 0
            for i in self._dr:
                if i == idkrtt:
                    self.setCurrentIndex(k)
                    break
                k += 1
    
    def getSelect(self):
        idkrtt = 0
        if self.count() > 0:
            idkrtt = self._dr[self.currentIndex()]
        return idkrtt
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class listKartaRange(QtGui.QListWidget):
    def __init__(self, mysql, parent = None):
        QtGui.QListWidget.__init__(self, parent)
        self._kr = getKartaRangeId(mysql)
        self._kre = getEditKartaRange(mysql)
        self._gf = generateFlag()
        self._idkarta = 7#Храним ид карты
        self._kd = []
    
    def redraw(self):
        self.clear()
        if self._idkarta > 0:
            self._kd = self._kr.getAllData()
            f = True
            try:
                self._kd[self._idkarta ]
            except KeyError:
                print u"Для id = %d нет карточек действия"%self._idkarta 
                f = False
            if f:
                for i in self._kd[self._idkarta]["kartarange"]:
                    s = self._gf.getDayRangeAll(i["flag"],i["flagnot"])
                    for j in i["date"]:
                        s = "%s [%s-%s]"%(s, j["begin"], j["end"])
                    it = QtGui.QListWidgetItem(s)
                    self.addItem(it)
        self.setCurrentRow(0)
                
    def setIdKarta(self, idk):
        self._idkarta = idk
        self.redraw()
    
    def setSelect(self, idkr):
        row = 0
        self.setCurrentRow(row)
        for i in self._kd[self._idkarta]["kartarange"]:
            if i["id"] == idkr:
                self.setCurrentRow(row)
                break
            row += 1
    
    def getSelect(self):
        i = 0
        if self.count() > 0:
            try:
                i= self._kd[self._idkarta]["kartarange"][self.currentRow()]["id"]
            except KeyError:
                print u"Для id = %d нет карточек действия"%self._idkarta 
            except IndexError:#ложное срабатывание
                pass
                #print self.currentRow()
        return i
        
    def getNameKarta(self):
        i = ''
        if self.count() > 0:
            try:
                i = self._kd[self._idkarta]["name"]
            except KeyError:
                print u"Для id = %d нет карточек действия"%self._idkarta 
                #print self.currentRow()
        return i
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class listEditKartaRange(listKartaRange):
    def __init__(self, mysql, parent= None):
        listKartaRange.__init__(self, mysql, parent)
        self._dialog = dialogKartaRange(parent)
        self.connect(self._dialog, QtCore.SIGNAL("accepted()"),self.save)
        self._idkr = 0
    
    def addData(self):
        self._dialog.setAddMode()
    
    def editData(self, item=None):
        if self.count() >0:
            tidkr = self._kd[self._idkarta]["kartarange"][self.currentRow()]
            self._idkr = tidkr["id"]
            flag = tidkr["flag"]
            flagnot = tidkr["flagnot"]
            self._dialog.setEditMode(flag, flagnot)
    
    def delData(self):
        if self.count()>0:
            row = self.currentRow()
            idkr = self._kd[self._idkarta]["kartarange"][row]["id"]
            s = u" в строке %d"%(row+1)
            dlg = QtGui.QMessageBox(self)
            dlg.setWindowTitle(u'Удаление записи')
            dlg.setText(u'''Вы действительно хотите удалить запись '%s'?'''%s)
            dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
            if ok==QtGui.QMessageBox.Ok:
                self._kre.delData(idkr, row, self._idkarta)
                self.redraw()
    
    def upData(self):
        if self.count() > 0:
            row = self.currentRow()
            if row > 0:
                idkr = self._kd[self._idkarta]["kartarange"][row]["id"]
                self._kre.upData(row-1, idkr, self._idkarta)
                self.redraw()
    
    def downData(self):
        if self.count() > 0:
            row = self.currentRow()
            if row < self.count()-1:
                idkr = self._kd[self._idkarta]["kartarange"][row]["id"]
                self._kre.downData(row+1, idkr, self._idkarta)
                self.redraw()
    
    def save(self):
        flag, flagnot, mode = self._dialog.getResult()
        if mode == 0:#добавляем данные
            self._kre.addData(self._idkarta, flag, flagnot, self.count())
        else:
            self._kre.editData(flag, flagnot, self._idkr)
        self.redraw()
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class listEditKartaRangeExt(listEditKartaRange):
    '''Расширяем класс для работы с копированием элементов подчиненных, а также
    удаление не только карты действия, а и дней и времени в подчиненных таблицах
    '''
    def __init__(self, mysql, parent= None):
        listEditKartaRange.__init__(self, mysql, parent)
        #даты и время для копирования/удаления
        self._krd = getEditKartaRangeDay(mysql)
        self._krt = getEditKartaRangeTime(mysql)
        
    def copyData(self):
        kr = self._kd[self._idkarta]["kartarange"][self.currentRow()]
        idkr_cur = kr["id"]
        flag = kr["flag"]
        flagnot = kr["flagnot"]
        idkr_new = self._kre.addData(self._idkarta, flag, flagnot, self.count())
        f, dr, d = self._krd.getData(idkr_cur)
        for i in dr:
            self._krd.addData(idkr_new, d[i]["begin"], d[i]["end"])
        f, dr, d = self._krt.getData(idkr_cur)
        for i in dr:
            self._krt.addData(idkr_new, d[i]["begintime"], d[i]["beginday"],
                              d[i]["endtime"], d[i]["endday"], d[i]["idkrtt"])
        self.redraw()
    
    def delData(self):
        '''Переопределяем класс для удаления зависящих дат и времени '''
        if self.count()>0:
            row = self.currentRow()
            idkr = self._kd[self._idkarta]["kartarange"][row]["id"]
            s = u" в строке %d"%(row+1)
            dlg = QtGui.QMessageBox(self)
            dlg.setWindowTitle(u'Удаление записи')
            dlg.setText(u'''Вы действительно хотите удалить запись '%s'?'''%s)
            dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
            if ok==QtGui.QMessageBox.Ok:
                f, dr, d = self._krd.getData(idkr)
                for i in dr:
                    self._krd.delData(i)
                f, dr, d = self._krt.getData(idkr)
                for i in dr:
                    self._krt.delData(i)
                self._kre.delData(idkr, row, self._idkarta)
                self.redraw()
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class listKartaRangeDay(QtGui.QListWidget):
    def __init__(self, mysql, parent = None):
        QtGui.QListWidget.__init__(self, parent)
        self._krd = getEditKartaRangeDay(mysql)
        self._idkr = 0#Храним ид карты действия
        self._d = {} #словарь данных
        self._dr = []#список данных
    
    def redraw(self):
        self.clear()
        if self._idkr > 0:
            f, self._dr, self._d = self._krd.getData(self._idkr)
            for i in self._dr:            
                s = "[%s - %s]"%(str(self._d[i]["begin"]), str(self._d[i]["end"]))
                it = QtGui.QListWidgetItem(s)
                self.addItem(it)
                
    def setIdKartaRange(self, idkr):
        self._idkr = idkr
        self.redraw()        
        
    def setSelect(self, idkrt):
        row = 0
        self.setCurrentRow(row)
        for i in self._dr:
            if i == idkrt:
                self.setCurrentRow(row)
                break
            row += 1
    
    def getSelect(self):
        if self.count() > 0:
            return self._dr[self.currentRow()]
        else:
            return 0
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
class listEditKartaRangeDay(listKartaRangeDay):
    def __init__(self, mysql, parent = None):
        listKartaRangeDay.__init__(self, mysql, parent)
        self._dialog = dialogKartaRangeDay(parent)
        self.connect(self._dialog, QtCore.SIGNAL("accepted()"),self.save)
    
    def addData(self):
        if self._idkr > 0:
            self._dialog.setAddMode()
    
    def editData(self, item = None):
        if self.count() > 0:
            idkrd = self._dr[self.currentRow()]
            begin = self._d[idkrd]["begin"]
            end = self._d[idkrd]["end"]
            self._dialog.setEditMode(begin, end)
    
    def delData(self):
        if self.count()>0:
            idkrd = self._dr[self.currentRow()]
            s = u" с периодом [%s-%s]"%(str(self._d[idkrd]["begin"]), str(self._d[idkrd]["end"]) )
            dlg = QtGui.QMessageBox(self)
            dlg.setWindowTitle(u'Удаление записи')
            dlg.setText(u'''Вы действительно хотите удалить запись '%s'?'''%s)
            dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
            if ok==QtGui.QMessageBox.Ok:
                self._krd.delData(idkrd)
                self.redraw()
                
    def save(self):
        res, mode = self._dialog.getResult()
        if mode == 0:#добавляем данные
            self._krd.addData(self._idkr, str(res[0]), str(res[1]))
        else:
            self._krd.editData(str(res[0]), str(res[1]), self._dr[self.currentRow()])
        self.redraw()
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
class tableKartaRangeTime(QtGui.QTableWidget):
    def __init__(self, mysql, parent):
        QtGui.QTableWidget.__init__(self, parent)
        self._krt = getEditKartaRangeTime(mysql)
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        self._headername = [u'Тип промежутка', u'Начало', u'Завершение']
        self._datarow = []
        self._data = {}
        self._idkr = 0
        self._alltime = timedelta(0,0)
    
    def _initTable(self):
        self.setRowCount(0)
        self.setRowCount(len(self._datarow))
        self.setColumnCount(len(self._headername))
        self.setHorizontalHeaderLabels(self._headername)
    
    def _resizeTable(self):
        self.resizeColumnsToContents()
        self.resizeRowsToContents ()
        self.setCurrentCell(0,0)

    def retStrFromDate(self, day, curtime):
        if day > 0:
            s = u"%d-й день %s"%(day,str(curtime))
        else:
            s = "%s"%(str(curtime))
        return s
        
    def redraw(self):
        (f, self._datarow, self._data) = self._krt.getData(self._idkr)
        self._initTable()
        row = 0
        self._alltime = timedelta(0,0)
        for i in self._datarow:
            name = self._data[i]["name"]
            self.itemDraw(row, 0, name)
            bd = self._data[i]["beginday"]
            bt = self._data[i]["begintime"]
            begin = self.retStrFromDate(bd, bt)
            self.itemDraw(row, 1, begin)
            ed = self._data[i]["endday"]
            et = self._data[i]["endtime"]
            end = self.retStrFromDate(ed, et)
            self.itemDraw(row, 2, end)
            dl = self._data[i]["delta"]
            if dl > 0:
                self._alltime +=  timedelta(ed,0) + et - timedelta(bd,0) - bt
            elif dl < 0:
                self._alltime -=  (timedelta(ed,0) + et - timedelta(bd,0) - bt)
            row += 1 
        self._resizeTable()
        self.emit(QtCore.SIGNAL("redraw()"))
            
    def itemDraw(self,row,col,data):
        if type(data) == str or type(data) == unicode:
            item = QtGui.QTableWidgetItem("%s"%data)
            self.setItem(row,col,item)    
        elif type(data) == int or type(data) == long:
            s = str(data)            
            item = QtGui.QTableWidgetItem(s)
            self.setItem(row,col,item)
            
    def getSelect(self):
        i = 0
        if self.rowCount > 0:
            i = self._datarow[self.currentRow()]
        return i
        
    def setSelect(self, idkrt):
        self.setCurrentCell(0,0)
        k = 0
        for i in self._datarow:
            if i == idkrt:
                self.setCurrentCell(k,0)
                break
            k += 1
            
    def setIdKartaRange(self, idkr):
        self._idkr = idkr
        self.redraw()
    
    def getTime(self):
        return self._alltime
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableEditKartaRangeTime(tableKartaRangeTime):
    def __init__(self, mysql, parent = None):
        tableKartaRangeTime.__init__(self, mysql, parent)
        self._dialog = dialogKartaRangeTime(mysql, parent)
        self.connect(self._dialog, QtCore.SIGNAL("accepted()"),self.save)
    
    def addData(self):        
        self._dialog.setAddMode()

    def editData(self, row= 0, col =0):    
        idkrt = self._datarow[self.currentRow()]
        beginday = self._data[idkrt]["beginday"]
        begintime = self._data[idkrt]["begintime"]
        endday = self._data[idkrt]["endday"]
        endtime = self._data[idkrt]["endtime"]
        idkrtt = self._data[idkrt]["idkrtt"]
        self._dialog.setEditMode(idkrtt, begintime, beginday, endtime, endday)
        
    def delData(self):    
        if self.rowCount()>0:
            row = self.currentRow()
            i = self._datarow[row]
            s = self._data[i]["name"]
            dlg = QtGui.QMessageBox(self)
            dlg.setWindowTitle(u'Удаление записи')
            dlg.setText(u'''Вы действительно хотите удалить запись '%s'?'''%s)
            dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
            if ok==QtGui.QMessageBox.Ok:
                self._krt.delData(i)
                self.redraw()

    def save(self):    
        (res, mode) = self._dialog.getResult()
        begintime, beginday,  endtime, endday, idkrtt = res
        row = self.currentRow()
        if mode == 0:
            idkrt = self._krt.addData(self._idkr, str(begintime), beginday, str(endtime), endday, idkrtt)
        else:
            idkrt = self._datarow[row]
            self._krt.editData(str(begintime), beginday, str(endtime), endday, idkrtt, idkrt)
        self.redraw()
        self.setSelect(idkrt)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
class dialogKartaRange(QtGui.QDialog):
    def __init__(self, parent):
        QtGui.QDialog.__init__(self, parent)
        self._gf = generateFlag()
        self.setWindowTitle(u'Добавить срок действия')
        self.initWidget()

    def initWidget(self):
        grid = QtGui.QGridLayout(self)
        btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        self._chs = self._gf.getFullNames()
        lbl1 = QtGui.QLabel(u'Входит')
        lbl2 = QtGui.QLabel(u'Кроме')
        
        self._chboxs = QtGui.QButtonGroup()
        self._chboxs.setExclusive(False)
        self._chboxs2 = QtGui.QButtonGroup()
        self._chboxs2.setExclusive(False)
        self.connect(self._chboxs, QtCore.SIGNAL("buttonClicked(int)"), self.chstate)
        self.connect(self._chboxs2, QtCore.SIGNAL("buttonClicked(int)"), self.chstate2)

        grid.addWidget(lbl1, 0, 0)
        grid.addWidget(lbl2, 0, 1)
        row = 0
        for i in self._chs:
            tmp = QtGui.QCheckBox(i)
            self._chboxs.addButton(tmp,row)
            grid.addWidget(tmp,row+1,0)
            tmp = QtGui.QCheckBox(i)
            self._chboxs2.addButton(tmp,row)
            grid.addWidget(tmp,row+1,1)
            row += 1
        grid.addWidget(btn1,row+1,0,1,2)

    def allflag(self,flag):
        '''Проверка на включение всех кнопок - и включение кнопки все дни, либо 
        ее выключение в противном случае'''
        if (flag == 2047) or(flag == 4095):#если включились все флаги
            flag = 4095#включаем все флаги
        else:
            flag = (flag | 2048) ^ 2048#выключаем все дни
        return flag

    def calcflag(self, flag, cmpFlag, addFlag, xorFlag):
        '''Проверка выполнения условия и включение/выключение заданных флагов '''
        if flag & cmpFlag == cmpFlag:#если флаг включен
            flag = flag | addFlag#включаем 
            flag = self.allflag(flag)
        else:
            flag = (flag | xorFlag) ^ xorFlag#выключаем 
        return flag

    def rangeflag(self, buttons, idbutton):
        '''Проход по всем флагам и в зависимости от кнопки нажатия - реагирование 
        на событие - включени/выключение групп флагов '''
        st = []
        for i in range(len(self._chs)):
            st += [buttons(i).checkState()]        
        fl = self._gf.getFlag(st)
        if idbutton == 11:#если кнопка на "все дни"
            fl = self.calcflag(fl, 2048, 4095, 4095)
        elif idbutton == 8:#если кнопка на выходные
            fl = self.calcflag(fl, 256, 96, 2144)
        elif idbutton == 7:#если кнопка на рабочие
            fl = self.calcflag(fl, 128, 31, 2079)
        elif idbutton < 5:#если кнопка из разряда пн-пт
            fl = self.calcflag(fl, 31, 128, 2176)
        elif idbutton < 7:#если кнопка из разряда сб-вс
            fl = self.calcflag(fl, 96, 256, 2304)
        else:
            fl = self.allflag(fl)
        st = self._gf.getData(fl)
        for i in range(len(self._chs)):
            buttons(i).setCheckState(st[i])
        return buttons

    def chstate(self, idbutton):
        self._chboxs.blockSignals(True)
        self._chboxs.button = self.rangeflag(self._chboxs.button,idbutton)
        for i in range(len(self._chs)):
            if self._chboxs.button(i).checkState() == 2:
                self._chboxs2.button(i).setCheckState(0)
                self._chboxs2.button = self.rangeflag(self._chboxs2.button,i)
        self._chboxs.blockSignals(False)
    
    def chstate2(self, idbutton):
        self._chboxs2.blockSignals(True)
        self._chboxs2.button = self.rangeflag(self._chboxs2.button,idbutton)
        for i in range(len(self._chs)):
            if self._chboxs2.button(i).checkState() == 2:
                self._chboxs.button(i).setCheckState(0)
                self._chboxs.button = self.rangeflag(self._chboxs.button,i)
        self._chboxs2.blockSignals(False)

    def setAddMode(self):
        for i in range(len(self._chs)):
            self._chboxs.button(i).setCheckState(0)
            self._chboxs2.button(i).setCheckState(0)
        self.setModal(1)
        self._mode = 0
        self.show()
    
    def setEditMode(self, flag, flagnot):
        sp = self._gf.getData(flag) 
        sp2 = self._gf.getData(flagnot) 
        for i in range(len(self._chs)):
            if sp[i] == 0:
                self._chboxs.button(i).setCheckState(0)
            else:
                self._chboxs.button(i).setCheckState(2)
            if sp2[i] == 0:
                self._chboxs2.button(i).setCheckState(0)
            else:
                self._chboxs2.button(i).setCheckState(2)
        self.setModal(1)
        self.show()
        self._mode = 1
    
    def getResult(self):
        sp = []
        for i in range(len(self._chs)):
            sp += [self._chboxs.button(i).checkState()]
        flag = self._gf.getFlag(sp)
        sp2 = []
        for i in range(len(self._chs)):
            sp2 += [self._chboxs2.button(i).checkState()]
        flagnot = self._gf.getFlag(sp2)
        return (flag, flagnot, self._mode)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
class dialogKartaRangeDay(QtGui.QDialog):
    def __init__(self, parent):
        QtGui.QDialog.__init__(self, parent)
        self._gf = generateFlag()
        self.setWindowTitle(u'Добавить период')
        self.initWidget()

    def initWidget(self):
        grid = QtGui.QGridLayout(self)
        btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        
        lbl1 = QtGui.QLabel(u'Начальная дата')
        lbl2 = QtGui.QLabel(u'Начальная дата')
        self._date1 = QtGui.QDateEdit()
        self._date1.setCalendarPopup(True)
        self._date2 = QtGui.QDateEdit()
        self._date2.setCalendarPopup(True)
        grid.addWidget(lbl1,0,0)
        grid.addWidget(lbl2,1,0)
        grid.addWidget(self._date1,0,1)
        grid.addWidget(self._date2,1,1)
        grid.addWidget(btn1)
        
    def setAddMode(self):
        self._date1.setDate(QtCore.QDate(date.today()))
        self._date2.setDate(QtCore.QDate(date.today()))
        self.setModal(1)
        self._mode = 0
        self.show()
    
    def setEditMode(self, begin, end):
        self._date1.setDate(QtCore.QDate(begin))
        self._date2.setDate(QtCore.QDate(end))
        self.setModal(1)
        self.show()
        self._mode = 1
    
    def getResult(self):
        res = [self._date1.date().toPyDate(), self._date2.date().toPyDate()]
        return (res, self._mode)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
class dialogKartaRangeTime(QtGui.QDialog):
    def __init__(self, mysql, parent):
        QtGui.QDialog.__init__(self, parent)
        self._ckrtt = comboKrtType(mysql,  parent)
        self._ckrtt.redraw() 
        self.setWindowTitle(u'Добавить период')
        self.initWidget()

    def initWidget(self):
        grid = QtGui.QGridLayout(self)
        btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        
        lbl1 = QtGui.QLabel(u'Тип промежутка')
        lbl2 = QtGui.QLabel(u'Начало')
        lbl3 = QtGui.QLabel(u'Завершение')
        lbl4 = QtGui.QLabel(u'День')
        lbl5 = QtGui.QLabel(u'Время')
        self._time1 = QtGui.QTimeEdit()
        self._time2 = QtGui.QTimeEdit()
        self._spn1 = QtGui.QSpinBox()
        self._spn2 = QtGui.QSpinBox()
        grid.addWidget(lbl1,0,0)
        
        grid.addWidget(lbl4,1,1)
        grid.addWidget(lbl5,1,2)
        grid.addWidget(lbl2,2,0)
        grid.addWidget(lbl3,3,0)
        grid.addWidget(self._ckrtt,0,1,1,2)
        grid.addWidget(self._spn1,2,1)
        grid.addWidget(self._time1,2,2)
        grid.addWidget(self._spn2,3,1)
        grid.addWidget(self._time2,3,2)
        grid.addWidget(btn1)
        
    def setAddMode(self):
        self._ckrtt.setSelect(0)
        self._time1.setTime(QtCore.QTime(time(8,0)))
        self._spn1.setValue(0)
        self._time2.setTime(QtCore.QTime(time(16,0)))
        self._spn2.setValue(0)
        self.setModal(1)
        self._mode = 0
        self.show()
    
    def setEditMode(self, idkrtt, begintime, beginday, endtime, endday):
        self._ckrtt.setSelect(idkrtt)
        h = begintime.seconds / 3600
        m = (begintime.seconds - h*3600) / 60
        self._time1.setTime(QtCore.QTime(time(h,m)))
        self._spn1.setValue(beginday)
        h = endtime.seconds / 3600
        m = (endtime.seconds - h*3600) / 60
        self._time2.setTime(QtCore.QTime(time(h,m)))
        self._spn2.setValue(endday)
        self.setModal(1)
        self.show()
        self._mode = 1
    
    def getResult(self):
        idkrtt = self._ckrtt.getSelect()
        beginday = self._spn1.value()        
        begintime = self._time1.time().toPyTime()
        endday = self._spn2.value()
        endtime = self._time2.time().toPyTime()
        res = [begintime, beginday, endtime, endday, idkrtt]
        return (res, self._mode)        
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
class wKartaRange(QtGui.QWidget):
    def __init__(self, mysql, parent= None):
        QtGui.QWidget.__init__(self, parent)
        self._list = listEditKartaRangeExt(mysql,parent)
        #self._list.redraw()
        self.initAction()        
        self.initWidget()
        self.initConnect()        
        
    def initWidget(self):
        #widg = QtGui.QWidget(self)
        grd = QtGui.QGridLayout()
        vh = QtGui.QVBoxLayout()
        tool = QtGui.QToolBar()
        tool.addAction(self.addDataAct)
        tool.addAction(self.editDataAct)
        tool.addAction(self.upDataAct)
        tool.addAction(self.downDataAct)
        tool.addAction(self.copyDataAct)
        tool.addSeparator()
        tool.addAction(self.delDataAct)
        tool.setIconSize(QtCore.QSize(48,48))
        grd.addWidget(tool)
        grd.addWidget(self._list)
        vh.addLayout(grd)
        #widg.setLayout(vh)
        self.setLayout(vh)
    
    def initAction(self):
        self.addDataAct = QtGui.QAction(u'Добавить',self)
        self.addDataAct.setIcon(QtGui.QIcon("../ico/dbplus2.png"))
        self.editDataAct = QtGui.QAction(u'Изменить',self)
        self.editDataAct .setIcon(QtGui.QIcon("../ico/db_update.png"))
        self.upDataAct = QtGui.QAction(u'Выше',self)
        self.upDataAct .setIcon(QtGui.QIcon("../ico/1uparrow.png"))
        self.downDataAct = QtGui.QAction(u'Ниже',self)
        self.downDataAct .setIcon(QtGui.QIcon("../ico/1downarrow.png"))

        self.copyDataAct = QtGui.QAction(u'Копировать',self)
        self.copyDataAct .setIcon(QtGui.QIcon("../ico/copy.png"))        
        
        self.delDataAct = QtGui.QAction(u'Удалить',self)
        self.delDataAct .setIcon(QtGui.QIcon("../ico/dbmin2.png"))
        
    def initConnect(self):
        self.connect(self.addDataAct, QtCore.SIGNAL("triggered()"),self._list.addData)
        self.connect(self.editDataAct, QtCore.SIGNAL("triggered()"),self._list.editData)
        self.connect(self._list, QtCore.SIGNAL("itemDoubleClicked(QListWidgetItem *)"),self._list.editData)
        self.connect(self.delDataAct, QtCore.SIGNAL("triggered()"),self._list.delData)
        self.connect(self.upDataAct, QtCore.SIGNAL("triggered()"),self._list.upData)
        self.connect(self.downDataAct, QtCore.SIGNAL("triggered()"),self._list.downData)
        self.connect(self.copyDataAct, QtCore.SIGNAL("triggered()"),self._list.copyData)
        self.connect(self._list, QtCore.SIGNAL("itemSelectionChanged()"),self.emitChangeRow)
            
    def redraw(self):
        self._list.redraw()

    def setIdKarta(self, idk):
        self._list.setIdKarta(idk)
    
    def emitChangeRow(self):
        self.emit(QtCore.SIGNAL('itemSelectionChanged()'))
    
    def getSelect(self):
        return self._list.getSelect()
        
    def getNameKarta(self):
        return self._list.getNameKarta()
    
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
class wKartaRangeDay(QtGui.QWidget):
    def __init__(self, mysql, parent= None):
        QtGui.QWidget.__init__(self, parent)
        self._list = listEditKartaRangeDay(mysql,parent)
        self.initAction()        
        self.initWidget()
        self.initConnect()        
        
    def initWidget(self):
        grd = QtGui.QGridLayout()
        vh = QtGui.QVBoxLayout()
        tool = QtGui.QToolBar()
        tool.addAction(self.addDataAct)
        tool.addAction(self.editDataAct)
        tool.addSeparator()
        tool.addAction(self.delDataAct)
        tool.setIconSize(QtCore.QSize(48,48))
        grd.addWidget(tool)
        grd.addWidget(self._list)
        vh.addLayout(grd)
        self.setLayout(vh)
    
    def initAction(self):
        self.addDataAct = QtGui.QAction(u'Добавить',self)
        self.addDataAct.setIcon(QtGui.QIcon("../ico/dbplus2.png"))
        self.editDataAct = QtGui.QAction(u'Изменить',self)
        self.editDataAct .setIcon(QtGui.QIcon("../ico/db_update.png"))
        self.delDataAct = QtGui.QAction(u'Удалить',self)
        self.delDataAct .setIcon(QtGui.QIcon("../ico/dbmin2.png"))
        
    def initConnect(self):
        self.connect(self.addDataAct, QtCore.SIGNAL("triggered()"),self._list.addData)
        self.connect(self.editDataAct, QtCore.SIGNAL("triggered()"),self._list.editData)
        self.connect(self._list, QtCore.SIGNAL("itemDoubleClicked ( QListWidgetItem *)"),self._list.editData)
        self.connect(self.delDataAct, QtCore.SIGNAL("triggered()"),self._list.delData)
            
    def redraw(self):
        self._list.redraw()

    def setIdKartaRange(self, idkr):
        self._list.setIdKartaRange(idkr)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
class wKartaRangeTime(QtGui.QWidget):
    def __init__(self, mysql, parent= None):
        QtGui.QWidget.__init__(self, parent)
        self._table = tableEditKartaRangeTime(mysql,parent)
        self.initAction()        
        self.initWidget()
        self.initConnect()        
        
    def initWidget(self):
        grd = QtGui.QGridLayout()
        vh = QtGui.QVBoxLayout()
        self._lb1 = QtGui.QLabel(u'Время: 0,0ч')
        tool = QtGui.QToolBar()
        tool.addAction(self.addDataAct)
        tool.addAction(self.editDataAct)
        tool.addSeparator()
        tool.addAction(self.delDataAct)
        tool.addWidget(self._lb1)
        tool.setIconSize(QtCore.QSize(48,48))
        grd.addWidget(tool)
        grd.addWidget(self._table)
        vh.addLayout(grd)
        self.setLayout(vh)
    
    def initAction(self):
        self.addDataAct = QtGui.QAction(u'Добавить',self)
        self.addDataAct.setIcon(QtGui.QIcon("../ico/dbplus2.png"))
        self.editDataAct = QtGui.QAction(u'Изменить',self)
        self.editDataAct .setIcon(QtGui.QIcon("../ico/db_update.png"))
        self.delDataAct = QtGui.QAction(u'Удалить',self)
        self.delDataAct .setIcon(QtGui.QIcon("../ico/dbmin2.png"))
        
    def initConnect(self):
        self.connect(self.addDataAct, QtCore.SIGNAL("triggered()"),self._table.addData)
        self.connect(self.editDataAct, QtCore.SIGNAL("triggered()"),self._table.editData)
        self.connect(self._table, QtCore.SIGNAL("cellDoubleClicked(int,int)"),self._table.editData)
        self.connect(self.delDataAct, QtCore.SIGNAL("triggered()"),self._table.delData)
        self.connect(self._table, QtCore.SIGNAL("redraw()"),self.drawTime)
            
    def redraw(self):
        self._table.redraw()

    def setIdKartaRange(self, idkr):
        self._table.setIdKartaRange(idkr)
    
    def drawTime(self):
        tm = self._table.getTime()
        d = tm.days
        s = tm.seconds
        h = s/3600.0 + d*24
        self._lb1.setText(u'Время: %.2f ч'%h)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class sprDataKartaRange(QtGui.QMdiSubWindow):
    def __init__(self, mysql, parent = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Карточки действия')    
        self._karta = wKartaExt(mysql, parent)
        #self._karta.redraw()
        idk = self._karta.getSelect()
        self._kartaRange = wKartaRange(mysql, parent) 
        self._kartaRange.setIdKarta(idk)
        idkr = self._kartaRange.getSelect()
        self._kartaRangeDay = wKartaRangeDay(mysql, parent) 
        self._kartaRangeDay.setIdKartaRange(idkr)
        self._kartaRangeTime = wKartaRangeTime(mysql, parent) 
        self._kartaRangeTime.setIdKartaRange(idkr)
        self.initWidget()
        self.initConnect()
    
    def initWidget(self):
        widg = QtGui.QWidget(self)
        #grd = QtGui.QGridLayout()
        vh = QtGui.QVBoxLayout()

        grpbx1 = QtGui.QGroupBox(u'Карточки')
        vh1 = QtGui.QVBoxLayout() 
        self._grpbx2 = QtGui.QGroupBox(u'Данные по карточке')
        vh2 = QtGui.QVBoxLayout() 
        grpbx3 = QtGui.QGroupBox(u'Дни недели действия карточки')
        vh3 = QtGui.QVBoxLayout() 
        grpbx4 = QtGui.QGroupBox(u'Даты и периоды действия карточки')
        vh4 = QtGui.QVBoxLayout() 
        grpbx5 = QtGui.QGroupBox(u'Время действия карточки')
        vh5 = QtGui.QVBoxLayout() 
        
        spl = QtGui.QSplitter(1)
        spl2 = QtGui.QSplitter(1)
        spl3 = QtGui.QSplitter(0)

        vh1.addWidget(self._karta)
        grpbx1.setLayout(vh1)
        vh3.addWidget(self._kartaRange)
        grpbx3.setLayout(vh3)
        
        vh4.addWidget(self._kartaRangeDay)
        grpbx4.setLayout(vh4)
        
        vh5.addWidget(self._kartaRangeTime)
        grpbx5.setLayout(vh5)
        
        spl2.addWidget(grpbx3)
        spl2.addWidget(grpbx4)
        spl3.addWidget(spl2)        
        spl3.addWidget(grpbx5)      
        
        vh2.addWidget(spl3)
        self._grpbx2 .setLayout(vh2)

        spl.addWidget(grpbx1)
        spl.addWidget(self._grpbx2)
        vh.addWidget(spl)
        widg.setLayout(vh)
        self.setWidget(widg)
        
    def initConnect(self):
        self.connect(self._karta, QtCore.SIGNAL("itemSelectionChanged()"),self.chKarta)
        self.connect(self._kartaRange, QtCore.SIGNAL("itemSelectionChanged()"),self.chKartaRange)
    
    def chKarta(self):
        idk = self._karta.getSelect()
        self._kartaRange.setIdKarta(idk)
        s = self._kartaRange.getNameKarta()
        self._grpbx2.setTitle(u'Данные по карточке [%s]'%s)
    
    def chKartaRange(self):
        idkr = self._kartaRange.getSelect()
        self._kartaRangeDay.setIdKartaRange(idkr)
        self._kartaRangeTime.setIdKartaRange(idkr)
        