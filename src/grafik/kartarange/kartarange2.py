#!/usr/bin/python
# -*- coding: utf-8  -*-
from standart.getdata import getData, editDatas

class getKartaRange(getData):
    '''Данные по карточкам для графика водителей '''
    def __init__(self, mysql):
        getData.__init__(self, mysql)

    def getDataSql(self, param):
        #TODO в будущем можно установить отбор по дате, чтобы не выбирать карточки диапазон, которые не будут работать для этого графика
        idkartarange = param["idkartarange"]
        sql = '''
            SELECT krt.id, kr.id as idkr, kr.flag, kr.flagnot, kr.sort, 
                k.name, k.vihod, k.smena, k.put, k.id as idkarta, 
                krt.begintime, krt.beginday, krt.endtime, krt.endday,
                krttt.delta
            FROM karta as k, kartarange as kr, kartarangebrigada as krb,
                kartarangetime as krt, krt_typetime as krttt
            WHERE kr.idkarta = k.id AND k.del = 0 AND kr.del = 0 AND
                krt.idkartarange = kr.id AND 
                krb.idkartarange = kr.id AND krb.idbrigada = %d AND
                krt.idkrt_typetime = krttt.id AND krttt.delta != 0
            
            ORDER BY 6, 5, 12,11
            '''%(idkartarange)
        return sql
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++