#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from grafik.standart.getdata import getData, editData, editSortData
from grafik.standart.table import tableEditData, dialogData, tableSortData, wData, wDataSort
from grafik.standart.combo import comboData
import pprint

class editOstanov(editData):
    def __init__(self, mysql):
        editData.__init__(self, mysql)

    def getDataSql(self):
        return '''SELECT id, name FROM ostanov WHERE del = 0 ORDER BY name'''
    
    def addDataSql(self, param):
        name = param["name"]
        return '''INSERT INTO ostanov (name) VALUES ('%s')'''%name
    
    def editDataSql(self, param, idostanov):
        name = param["name"]
        return '''UPDATE  ostanov SET name = '%s' WHERE id = %d'''%(name, idostanov)
    
    def delDataSql(self, idostanov):
        return '''UPDATE  ostanov SET del = 1 WHERE id = %d'''%idostanov
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableEditOstanov(tableEditData):
    def __init__(self, mysql, parent):
        self._getData = editOstanov(mysql)
        self._dialog = dialogOstanov(mysql, parent)
        tableEditData.__init__(self, mysql, parent)
    
    def getHeader(self):    
        return [u'Имя']
        
    def getData(self):
        return self._getData
    
    def getDialog(self):
        return self._dialog 
    
    def addRow(self, data, row):
        name = data["name"]
        self.itemDraw(row, 0, name)    
    
    def addData(self):
        self.getDialog().setMode(0, u'Добавить остановку')
    
    def editData(self):
        data = []
        iddata = self._datarow[self.currentRow()]
        data += [self._data[iddata]["name"]]
        self.getDialog().setMode(1, u'Изменить остановку', data)
    
    def getStrForDel(self, data):
        res = self._data[self._datarow[self.currentRow()]]["name"]
        return res

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class dialogOstanov(dialogData):
    def __init__(self, mysql, parent):
        dialogData.__init__(self, parent)
        
    def addComponent(self):
        grid = QtGui.QGridLayout()
        lbl1 = QtGui.QLabel(u'Название')
        self._ed1 = QtGui.QLineEdit(u'')
        grid.addWidget(lbl1,0,0)
        grid.addWidget(self._ed1,0,1)
        return grid
    
    def setAddMode(self):
        self._ed1.setText('')
    
    def setEditMode(self,param):
        self._ed1.setText(param[0])
    
    def getResult(self):
        res = {}
        res["name"] = self._ed1.text()
        return (res, self._mode)
    
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class wOstanov(wData):
    def __init__(self, mysql, parent):
        self.setTable(tableEditOstanov(mysql, parent))
        wData.__init__(self, mysql, parent)

    def setTable(self, tableClass):
        self._table = tableClass
    
    def getTable(self):
        return self._table
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class marsh(getData):
    def __init__(self, mysql):
        getData.__init__(self, mysql)
    
    def getDataSql(self):
        return   '''SELECT id, name, comment FROM marshrut WHERE del = 0 ORDER BY name'''
    
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class comboMarsh(comboData):
    def __init__(self, mysql, parent):
        self.setData(marsh(mysql))
        comboData.__init__(self, mysql, parent)
    
    def setData(self, classMarsh):
        self._classMarsh= classMarsh

    def getData(self):
        return self._classMarsh

    def addRow(self, data):
        return data["name"]
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class editMarsh(editData):
    def __init__(self, mysql):
        self._marsh = marsh(mysql)
        editData.__init__(self, mysql)

    def getDataSql(self):
        return self._marsh.getDataSql()
    
    def addDataSql(self, param):
        name = param["name"]
        comment = param["comment"]
        return '''INSERT INTO marshrut (name, comment) VALUES ('%s', '%s')'''%(name, comment)
    
    def editDataSql(self, param, idmarsh):
        name = param["name"]
        comment = param["comment"]
        return '''UPDATE marshrut SET name = '%s', comment = '%s' WHERE id = %d'''%(name, comment, idmarsh)
    
    def delDataSql(self, idmarsh):
        return '''UPDATE marshrut SET del = 1 WHERE id = %d'''%idmarsh
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableEditMarsh(tableEditData):
    def __init__(self, mysql, parent):
        self._getData = editMarsh(mysql)
        self._dialog = dialogMarsh(mysql, parent)
        tableEditData.__init__(self, mysql, parent)
    
    def getHeader(self):    
        return [u'Имя', u'Комментарий']
        
    def getData(self):
        return self._getData
    
    def getDialog(self):
        return self._dialog 
    
    def addRow(self, data, row):
        name = data["name"]
        comment = data["comment"]
        self.itemDraw(row, 0, name)    
        self.itemDraw(row, 1, comment)
    
    def addData(self):
        self._dialog.setMode(0, u'Добавить маршрут')    
        
    def editData(self):
        data = {}
        iddata = self._datarow[self.currentRow()]
        data["name"] = self._data[iddata]["name"]
        data["comment"] = self._data[iddata]["comment"]
        self._dialog.setMode(1, u'Изменить маршрут', data)
    
    def getStrForDel(self, data):
        res = self._data[self._datarow[self.currentRow()]]["name"]
        return res
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class dialogMarsh(dialogData):
    def __init__(self, mysql, parent):
        dialogData.__init__(self, parent)
        
    def addComponent(self):
        grid = QtGui.QGridLayout()
        lbl1 = QtGui.QLabel(u'Название')
        self._ed1 = QtGui.QLineEdit(u'')
        lbl2 = QtGui.QLabel(u'Комментарий')
        self._ed2 = QtGui.QLineEdit(u'')
        grid.addWidget(lbl1,0,0)
        grid.addWidget(self._ed1,0,1)
        grid.addWidget(lbl2,1,0)
        grid.addWidget(self._ed2,1,1)
        return grid
    
    def setAddMode(self):
        self._ed1.setText('')
        self._ed2.setText('')
    
    def setEditMode(self,param):
        self._ed1.setText(param["name"])
        self._ed2.setText(param["comment"])
    
    def getResult(self):
        res = {}
        res["name"] = self._ed1.text()
        res["comment"] = self._ed2.text()
        return (res, self._mode)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class wMarsh(wData):
    def __init__(self, mysql, parent):
        self.setTable(tableEditMarsh(mysql, parent))
        wData.__init__(self, mysql, parent)

    def setTable(self, tableClass):
        self._table = tableClass
    
    def getTable(self):
        return self._table
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class marshOstanov(getData):
    def __init__(self, mysql):
        getData.__init__(self, mysql)
    
    def getDataSql(self, param):
        idmarsh = param["idmarsh"]
        obrat = param["obrat"]
        sql = '''
            SELECT mo.id, mo.sort, mo.dtime, mo.idostanov, o.name as oname
            FROM marshostanov as mo, marshrut as m, ostanov as o
            WHERE mo.idostanov = o.id AND mo.idmarsh = m.id AND m.id = %d
                AND mo.obrat = %d
            ORDER BY 2'''%(idmarsh, obrat)
        return sql
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class comboMarshOstanov(comboData):
    def __init__(self, mysql, parent):
        self.setData(marshOstanov(mysql))
        self.setIdMarsh(0)
        self.setObrat(0)
        comboData.__init__(self, mysql, parent)

    def setIdMarsh(self, idmarsh):
        self._idmarsh = idmarsh
    
    def getIdMarsh(self):
        return self._idmarsh 
    
    def setObrat(self, obrat):
        self._obrat = obrat
    
    def getObrat(self):
        return self._obrat
    
    def setData(self, classMarshOstanov):
        self._classMarshOstanov = classMarshOstanov

    def getData(self):
        return self._classMarshOstanov 

    def addRow(self, data):
        return data["oname"]
    
    def getPack(self):
        '''Получаем данные из базы данных'''
        param = {}
        param["idmarsh"] = self.getIdMarsh()
        param["obrat"] = self.getObrat()
        return self.getData().getData(param)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class editMarshOstanov(editSortData):
    def __init__(self, mysql):
        self._marshOstanov = marshOstanov(mysql)
        editSortData.__init__(self, mysql)

    def getDataSql(self, param):
        return self._marshOstanov.getDataSql(param)
            
    def addDataSql(self, param):
        idmarsh = param["idmarsh"]
        idostanov = param["idostanov"]
        obrat = param["obrat"]
        sort = param["sort"]
        dtime = param["dtime"]
        return '''INSERT INTO marshostanov (idmarsh, idostanov, obrat, sort, dtime)
            VALUES (%d, %d, %d, %d, %d)'''%(idmarsh, idostanov, obrat, sort, dtime)
    
    def addDatasSql(self, param):
        rep = False
        sql = '''INSERT INTO marshostanov (idmarsh, idostanov, obrat, sort, dtime) VALUES '''
        for par in param:
            idmarsh = par["idmarsh"]
            idostanov = par["idostanov"]
            obrat = par["obrat"]
            sort = par["sort"]
            dtime = par["dtime"]
            if rep:
                sql = "%s, (%d, %d, %d, %d, %d)"%(sql, idmarsh, idostanov, obrat, sort, dtime)
            else:
                rep = True
                sql = "%s (%d, %d, %d, %d, %d)"%(sql, idmarsh, idostanov, obrat, sort, dtime)
        return sql
            
    
    def editDataSql(self, param, idmarhostanov):
        idmarsh = param["idmarsh"]
        idostanov = param["idostanov"]
        obrat = param["obrat"]
        sort = param["sort"]
        dtime = param["dtime"]
        return '''UPDATE marshostanov SET idmarsh = %d, idostanov = %d, 
            obrat = %d, sort = %d, dtime = %d WHERE id = %d'''%(idmarsh,
            idostanov, obrat, sort, dtime, idmarhostanov)
    
    def delDataSql(self, idmarhostanovs):
        res = False
        sql = '''DELETE FROM marshostanov WHERE '''
        for iddata in idmarhostanovs:
            if res:
                sql = '%s OR id = %d'%(sql, iddata)
            else:
                sql = '%s id = %d'%(sql, iddata)
                res = True
        return sql
        
    def upRowSql(self, sort, param):
        idmarsh = param["idmarsh"]
        obrat = param["obrat"]
        sql = ''' UPDATE marshostanov SET sort = sort + 1
                WHERE sort = %d AND idmarsh = %d AND obrat = %d'''%(sort, idmarsh, obrat)
        return sql
    
    def downRowSql(self, sort, param):
        idmarsh = param["idmarsh"]
        obrat = param["obrat"]
        sql = ''' UPDATE marshostanov SET sort = sort - 1
                WHERE sort = %d AND idmarsh = %d AND obrat = %d'''%(sort, idmarsh, obrat)
        return sql
    
    def setSortRowSql(self, sort, iddata):
        idmarshostanov = iddata
        sql = ''' UPDATE marshostanov SET sort = %d WHERE id = %d'''%(sort, idmarshostanov)
        return sql

    def updSortRowSql(self, param):
        sort = param["sort"]
        idmarsh = param["idmarsh"]
        obrat = param["obrat"]
        l = param["len"]
        return ''' UPDATE marshostanov SET sort = sort - %d 
            WHERE sort > %d AND idmarsh = %d AND obrat = %d'''%(l, sort, idmarsh, obrat)

    def reverseDataSql(self, idmarsh, obrat, l):    
        sql = ''' UPDATE marshostanov SET sort = %d - sort 
            WHERE idmarsh = %d AND obrat = %d'''%(l, idmarsh, obrat)
        return sql
    
    def setDTime(self, dtime, idmarshostanov):
        sql = '''UPDATE marshostanov SET dtime = %d WHERE id = %d'''
        self._mysql.execsql(sql%(dtime, idmarshostanov))
    
    def addDatas(self,param):
        sql = self.addDatasSql(param)
        self._mysql.execsql(sql)
        iddata = self._mysql.getlastid()#становимся на первый добавленый, если необходимо стать на последний добавленный + len(param)
        return iddata
    
    def reverseData(self, idmarsh, obrat, l):
        self._mysql.execsql(self.reverseDataSql(idmarsh, obrat, l))
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableMarshOstanov(tableSortData):
    def __init__(self, mysql, parent):
        self._getData = editMarshOstanov(mysql)
        self._dialog = dialogMarshOstanovDtime(parent)
        self.setIdMarsh(0)
        self.setObrat(0)
        tableEditData.__init__(self, mysql, parent)
    
    def setIdMarsh(self, idmarsh):
        self._idMarsh = idmarsh

    def getIdMarsh(self):
        return self._idMarsh

    def setObrat(self, obrat):
        self._obrat = obrat

    def getObrat(self):
        return self._obrat
 
    def getHeader(self):    
        return [u'Остановка', u'Время']
        
    def getData(self):
        return self._getData
    
    def getDialog(self):
        return self._dialog 

    def getPack(self):
        '''Добавляем параметры для получения данных из базы'''
        param = {}
        param["idmarsh"] = self.getIdMarsh()
        param["obrat"] = self.getObrat()
        return self.getData().getData(param)
        
    def getParam(self, data):
        "Возвращаем данные для манипуляциях при измнении порядка сортировок"
        param = {}
        param["idmarsh"] = self.getIdMarsh()
        param["obrat"] = self.getObrat()
        param["idostanov"] = data["idostanov"]
        param["sort"] = data["sort"]
        param["dtime"] = data["dtime"]
        return param
    
    def addRow(self, data, row):
        oname = data["oname"]
        self.itemDraw(row, 0, oname)    
        dtime = data["dtime"]
        self.itemDraw(row, 1, dtime)    
    
    def editData(self):
        iddata = self._datarow[self.currentRow()]
        data = {}
        data["dtime"] = self._data[iddata]["dtime"]
        self._dialog.setMode(1, u'Изменить время', data)
    
    def getStrForDel(self, iddatas):
        s = self._data[iddatas[0]]["oname"]
        if len(iddatas) == 1:
            res = u'''Вы действительно хотите удалить запись '%s'?'''%s
        else:
            res = u'''Вы действительно хотите удалить %d записей?'''%len(iddatas)
        return res
        
    def save(self):    
        (param, mode) = self.getDialog().getResult()
        row = self.currentRow()
        iddata = self._datarow[row]
        dtime = param["dtime"]
        self.getData().setDTime(dtime, iddata)
        self.redraw()
        self.setSelect(iddata)
        
    def delData(self):    
        iddatas = []
        rows = self.selectionModel().selectedIndexes()
        sort = -1
        for irows in rows:
            row = irows.row()        
            if sort < 0 or sort < row:
                sort = row
            iddata = self._datarow[row]
            iddatas += [iddata]
        
        if len(iddatas) > 0:
            param = {}
            param["sort"] = row
            param["idmarsh"] = self.getIdMarsh()
            param["obrat"] = self.getObrat()
            param["len"] = len(iddatas)
            
            dlg = QtGui.QMessageBox(self)
            if len(iddatas) == 1:
                dlg.setWindowTitle(u'Удаление записи')
            else:
                dlg.setWindowTitle(u'Удаление записей')
            s = self.getStrForDel(iddatas)
            dlg.setText(s)
            dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
            if ok==QtGui.QMessageBox.Ok:
                self.getData().delData(param, iddatas)
                self.redraw()
                self.setSelect(0)
    
    def addMarshOstanov(self, idmarsh, idostanov):
        param = {}
        param["idmarsh"] = idmarsh
        param["idostanov"] = idostanov
        param["obrat"] = self.getObrat()
        param["sort"] = self.rowCount()
        param["dtime"] = 0
        iddata = self.getData().addData(param)
        self.redraw()
        self.setSelect(iddata)
    
    def editMarshOstanov(self, idmarsh, idostanov):
        param = {}
        row = self.currentRow()
        iddata = self._datarow[row]
        param["idmarsh"] = idmarsh
        param["idostanov"] = idostanov
        param["obrat"] = self.getObrat()
        param["sort"] = self._data[iddata]["sort"]
        param["dtime"] = self._data[iddata]["dtime"]
        self.getData().editData(param, iddata)
        self.redraw()
        self.setSelect(iddata)
    
    def saveToBuffer(self):
        param = []
        rowcol = self.selectionModel().selectedIndexes()
        for i in rowcol:
            row = i.row()
            iddata = self._datarow[row]
            tmp = {}
            tmp["idmarsh"] = self.getIdMarsh()
            tmp["idostanov"] = self._data[iddata]["idostanov"]
            tmp["obrat"] = self.getObrat()
            tmp["sort"] = 0
            tmp["dtime"] = self._data[iddata]["dtime"]
            param += [tmp]
        self._saveBuffer = param
        return param
            
    def pasteFromBuffer(self, param):
        if len(param) > 0:
            rows = self.rowCount()
            for par in param:
                par["idmarsh"] = self.getIdMarsh()
                par["sort"] = rows
                par["obrat"] = self.getObrat()
                rows += 1
            iddata = self.getData().addDatas(param)
            self.redraw()
            self.setSelect(iddata)
    
    def reverse(self):
        idmarsh = self.getIdMarsh()
        obrat = self.getObrat()
        l = len(self._datarow) - 1
        row = self.currentRow()
        iddata = self._datarow[row]
        self.getData().reverseData(idmarsh, obrat, l)
        self.redraw()
        self.setSelect(iddata)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class dialogMarshOstanovDtime(dialogData):
    def __init__(self, parent):
        dialogData.__init__(self, parent)
        
    def addComponent(self):
        grid = QtGui.QGridLayout()
        lbl1 = QtGui.QLabel(u'Время от начала маршрута')
        self._ed1 = QtGui.QLineEdit(u'0')
        grid.addWidget(lbl1,0,0)
        grid.addWidget(self._ed1,1,0)
        return grid
    
    def setEditMode(self,param):
        dtime = param["dtime"]
        self._ed1.setText("%d"%dtime)
    
    def getResult(self):
        res = {}
        res["dtime"] = int(self._ed1.text())
        return (res, self._mode)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class wMarshOstanov(wDataSort):
    def __init__(self, mysql, parent):
        self.setTable(tableMarshOstanov(mysql, parent))
        wDataSort.__init__(self, mysql, parent)
    
    def setTable(self, tableClass):
        self._table = tableClass
    
    def getTable(self):
        return self._table
    
    def setIdMarsh(self, idMarsh):
        self.getTable().setIdMarsh(idMarsh)
    
    def setObrat(self, obrat):
        self.getTable().setObrat(obrat)
    
    def addMarshOstanov(self, idmarsh, idostanov):
        self.getTable().addMarshOstanov(idmarsh, idostanov)
    
    def editMarshOstanov(self, idmarsh, idostanov):
        self.getTable().editMarshOstanov(idmarsh, idostanov)
    
    def initTool(self):
        tool = QtGui.QToolBar()
        tool.addAction(self.addMarshOstanovAct)
        tool.addAction(self.editMarshOstanovAct)
        tool.addAction(self.clockOstanovAct)
        tool.addAction(self.upDataAct)
        tool.addAction(self.downDataAct)
        tool.addAction(self.copyAct)
        tool.addAction(self.pasteAct)
        tool.addAction(self.reverseAct)
        
        tool.addSeparator()
        tool.addAction(self.delDataAct)
        tool.setIconSize(QtCore.QSize(36,36))
        return tool
    
    def initAction(self):
        wDataSort.initAction(self)
        self.addMarshOstanovAct = QtGui.QAction(u'Добавить',self)
        self.addMarshOstanovAct.setIcon(QtGui.QIcon("../ico/dbplus2.png"))
        self.editMarshOstanovAct = QtGui.QAction(u'Изменить',self)
        self.editMarshOstanovAct.setIcon(QtGui.QIcon("../ico/db_update.png"))
        self.clockOstanovAct = QtGui.QAction(u'Изменить время',self)
        self.clockOstanovAct.setIcon(QtGui.QIcon("../ico/gnome-panel-clock.png"))
        
        self.copyAct = QtGui.QAction(u'Скопировать в буфер',self)
        self.copyAct.setIcon(QtGui.QIcon("../ico/copy.png"))
        self.pasteAct = QtGui.QAction(u'Вставить из буфера',self)
        self.pasteAct.setIcon(QtGui.QIcon("../ico/paste.png"))
        self.reverseAct = QtGui.QAction(u'Развернуть',self)
        self.reverseAct.setIcon(QtGui.QIcon("../ico/arrow_cycle.png"))
    
    def initConnect(self):
        wDataSort.initConnect(self)
        self.connect(self.clockOstanovAct, QtCore.SIGNAL("triggered()"),self.getTable().editData)
        self.connect(self.addMarshOstanovAct, QtCore.SIGNAL("triggered()"),self.emitAdd)
        self.connect(self.editMarshOstanovAct, QtCore.SIGNAL("triggered()"),self.emitEdit)
        self.connect(self.copyAct, QtCore.SIGNAL("triggered()"),self.emitSaveToBuffer)
        self.connect(self.pasteAct, QtCore.SIGNAL("triggered()"),self.emitPasteFromBuffer)
        self.connect(self.reverseAct, QtCore.SIGNAL("triggered()"),self.getTable().reverse)

    def emitSaveToBuffer(self):
        self.emit(QtCore.SIGNAL('saveToBuffer()'))
    
    def emitPasteFromBuffer(self):
        self.emit(QtCore.SIGNAL('pasteFromBuffer()'))
    
    def emitAdd(self):
        self.emit(QtCore.SIGNAL('clickAdd()'))
    
    def emitEdit(self):
        self.emit(QtCore.SIGNAL('clickEdit()'))
    
    def saveToBuffer(self):
        return self.getTable().saveToBuffer()
    
    def pasteFromBuffer(self, param):
        self.getTable().pasteFromBuffer(param)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class sprMarshOstanov(QtGui.QMdiSubWindow):
    def __init__(self, mysql, parent = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Маршруты, остановки')   
        self._marsh = wMarsh(mysql, parent)
        self._ostanov = wOstanov(mysql, parent)
        self._buffer = []

        self._marshOstanov = []
        for i in range(2):
            self._marshOstanov += [wMarshOstanov(mysql, parent)]
            self._marshOstanov[i].setIdMarsh(self._marsh.getSelect())
            self._marshOstanov[i].setObrat(i)
            self._marshOstanov[i].redraw()
        self.initWidget()
        self.initConnection()
        
    def initWidget(self):
        grid = QtGui.QGridLayout()
        spl1 = QtGui.QSplitter(2)
        for i in range(2):
            spl1.addWidget(self._marshOstanov[i])
        spl2 = QtGui.QSplitter(1)
        spl2.addWidget(self._marsh)
        spl2.addWidget(spl1)
        spl2.addWidget(self._ostanov)
        grid.addWidget(spl2)
        widg = QtGui.QWidget()
        widg.setLayout(grid)
        self.setWidget(widg)
    
    def initConnection(self):
        self.connect(self._marsh, QtCore.SIGNAL("itemSelectionChanged()"),self.marshChangeRow)
        for i in range(2):
            self.connect(self._marshOstanov[i], QtCore.SIGNAL("clickAdd()"),self.addMarshOstanov)
            self.connect(self._marshOstanov[i], QtCore.SIGNAL("clickEdit()"),self.editMarshOstanov)
            self.connect(self._marshOstanov[i], QtCore.SIGNAL("saveToBuffer()"),self.saveToBuffer)
            self.connect(self._marshOstanov[i], QtCore.SIGNAL("pasteFromBuffer()"),self.pasteFromBuffer)

    def pasteFromBuffer(self):    
        self.sender().pasteFromBuffer(self._buffer)
    
    def saveToBuffer(self):
        self._buffer = self.sender().saveToBuffer()
    
    def marshChangeRow(self):
        for i in range(2):
            self._marshOstanov[i].setIdMarsh(self._marsh.getSelect())
            self._marshOstanov[i].redraw()

    def addMarshOstanov(self):
        self.sender().addMarshOstanov(self._marsh.getSelect(), self._ostanov.getSelect())
        
    def editMarshOstanov(self):
        self.sender().editMarshOstanov(self._marsh.getSelect(), self._ostanov.getSelect())