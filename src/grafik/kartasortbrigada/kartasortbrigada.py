#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from dialogkartasortbrigada import dialogKartaSortBrigada
from grafik.brigada.brigada import comboBrigada
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class getDataKartaSort:
    '''Данные для сетки '''
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
        self._maxcol = 4 #4 колонки 1- для первого водителя, 2-я для напарника, 3-я для 1го в выходные, 4-я для напарника в выходные и пр.
    
    def initSql(self):
        self._getDataSql = '''
            SELECT vt.id, v.sort, k.name as name, k.put, 
                vt.idkarta, vt.idtype, vt.col, v.id as idvodgrsort 
            FROM vodgrsort as v, karta as k, vodgrsortkartatype as vt
            WHERE vt.idvodgrsort = v.id AND vt.idkarta = k.id AND vt.idtype = 0
                AND v.brigada = %d
            UNION
            SELECT vt.id, v.sort, k.name as name, 0 as put, 
                vt.idkarta, vt.idtype, vt.col, v.id as idvodgrsort 
            FROM vodgrsort as v, dopoln as k, vodgrsortkartatype as vt
            WHERE vt.idvodgrsort = v.id AND vt.idkarta = k.id AND vt.idtype = 1
                AND v.brigada = %d
            ORDER BY 2, 7
            '''
    
        self._getCountDataSql = '''
            SELECT vt.id, k.name, k.put, vt.idkarta
            FROM vodgrsort as v, karta as k, vodgrsortkartatype as vt
            WHERE vt.idvodgrsort = v.id AND vt.idkarta = k.id AND vt.idtype = 0
                AND v.brigada = %d
            GROUP BY vt.idkarta

            ORDER BY 2
            '''    
        

    
    def getData(self, brigada = 1):
        '''Возвращаем результат запроса _getDataSql'''
        sql = self._getDataSql%(brigada, brigada)
        (f,dr,d) = self._mysql.getData(sql)
        return (f,dr,d)
        
    def getDataMass(self, brigada = 1):
        '''Возвращаем таблицу сетки, там где не пустые ячейки - словарь с данными '''
        f,dr,d = self.getData(brigada)
        data = []
        row = -1
        for i in dr:
            if row != d[i]["sort"]:
                row += 1
                data += [[]]
                for j in range(self._maxcol):
                    data[row] += [[]]
            col = d[i]["col"]
            data[row][col] = {}
            data[row][col]["idkarta"] = d[i]["idkarta"]
            data[row][col]["idtype"] = d[i]["idtype"]
        return data
            
    def getCountData(self, brigada):
        sql = self._getCountDataSql%(brigada)
        (f,dr,d) = self._mysql.getData(sql)
        return (f,dr,d)

        
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
class editDataKartaSort(getDataKartaSort):
    '''Редактирование данные ''' 
    def __init__(self, mysql):
        getDataKartaSort.__init__(self,mysql)

    def initSql(self):
        getDataKartaSort.initSql(self)
        self._addRowSql = 'INSERT INTO vodgrsort (brigada, sort) VALUES (%d, %d) '
        self._addDataSql = '''INSERT INTO vodgrsortkartatype 
            (idvodgrsort, idkarta, idtype, col) VALUES ('%d', '%d', '%d', '%d') '''
        self._findDataSql = '''SELECT id FROM vodgrsortkartatype WHERE id = %d'''
            
        self._editRowSql = '''UPDATE vodgrsort SET brigada = %d, sort = %d WHERE id = %d'''
        self._editDataSql = '''UPDATE vodgrsortkartatype 
                              SET idkarta = %d, idtype = %d 
                              WHERE id = %d'''
        self._upRowSql = ''' UPDATE vodgrsort SET sort = sort + 1
                WHERE sort = %d AND brigada = %d'''
        self._downRowSql = ''' UPDATE vodgrsort SET sort = sort - 1
                WHERE sort = %d AND brigada = %d'''
        self._sortRowSql = ''' UPDATE vodgrsort SET sort = %d WHERE id = %d'''

        self._delRowSql = '''DELETE FROM vodgrsort WHERE id = %d '''
        self._delRowDataSql = '''DELETE FROM vodgrsortkartatype WHERE idvodgrsort = %d '''
        self._delDownRowSql = ''' UPDATE vodgrsort SET sort = sort - 1
                WHERE sort > %d AND brigada = %d'''
        
        self._delDataSql = '''DELETE FROM vodgrsortkartatype WHERE id = %d '''
    
    def addRow(self, brigada, sort, idkarta, idtype):
        self._mysql.execsql(self._addRowSql%(brigada, sort))
        idvodgrsort = self._mysql.getlastid()
        self.addData(idvodgrsort, idkarta, idtype, 0)
    
    def addData(self, idvodgrsort,  idkarta, idtype, column):
        s = self._addDataSql%(idvodgrsort, idkarta, idtype, column)
        self._mysql.execsql(s)
        
    def editRow(self, idvodgrsort, brigada, sort):
        self._mysql.execsql(self._editRowSql%(brigada, sort, idvodgrsort))
    
    def editData(self, idkarta, idtype, iddata):
        self._mysql.execsql(self._editDataSql%(idkarta, idtype, iddata))
    
    def upRow(self, sort, brigada, idvodgrsort):
        self._mysql.execsql(self._upRowSql%(sort, brigada))
        self._mysql.execsql(self._sortRowSql%(sort, idvodgrsort))
        
    def downRow(self, sort, brigada, idvodgrsort):
        self._mysql.execsql(self._downRowSql%(sort, brigada))
        self._mysql.execsql(self._sortRowSql%(sort, idvodgrsort))
    
    def delRow(self, idvodgrsort, sort, brigada):
        self._mysql.execsql(self._delRowSql%(idvodgrsort))
        self._mysql.execsql(self._delDownRowSql%(sort,brigada))    
        self._mysql.execsql(self._delRowDataSql%(idvodgrsort))

    def delData(self, idvodgrsortkartatype):
        self._mysql.execsql(self._delDataSql%(idvodgrsortkartatype))        
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class tableDataKartaSort(QtGui.QTableWidget):
    def __init__(self,  mysql, parent = None):
        QtGui.QTableWidget.__init__(self, parent)
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        self._editKartaSort = editDataKartaSort(mysql)
        self._brigada = 1 #бригада по умолчанию
        self._headername = [u'Порядок', u'1 строка', u'2 строка', 
                            u'1 строка вых', u'2 строка вых'] #поля, которые будем отображать
        self._viewput = False#показывать имена или коды путевых листов
        self._maxcolumn = 4 #сколько колонок будем выводить

    def _initTable(self, rowcount, columncount):
        self.setRowCount(0)
        self.setRowCount(rowcount)
        self.setColumnCount(columncount)
        self.setHorizontalHeaderLabels(self._headername)
    
    def _resizeTable(self):
        self.resizeColumnsToContents()
        self.resizeRowsToContents ()
        self.setCurrentCell(0,0)
        
    def redraw(self):
        '''Отрисовка и заполнение ассоциативного массива _data[id vodgrsort][column][тип записи] '''
        (f, datarow, data) = self._editKartaSort.getData(self._brigada)
        #переписываем данные
        self._datarow = [] 
        self._data = {}
        for i in datarow:
            try:
                id = data[i]["idvodgrsort"]
                self._data[id]#проверяем существование записи с данным id
            except KeyError:
                self._datarow += [id] 
                self._data[id] = {}
                self._data[id]["sort"] = data[i]["sort"]
                
            column = data[i]["col"]
            self._data[id][column] = {}

            self._data[id][column]["idkarta"] = data[i]["idkarta"]
            self._data[id][column]["idtype"] = data[i]["idtype"]
            self._data[id][column]["name"] = data[i]["name"]
            self._data[id][column]["put"] = data[i]["put"]
            self._data[id][column]["id"] = data[i]["id"]
        #отрисовываем данные                
        self._initTable(len(self._datarow),self._maxcolumn+1)
        for id in self._datarow:
            row = self._data[id]["sort"]
            self.itemDraw(row, 0, row)
            for col in self._data[id].keys():
                if type(col) == int or type(col) == long:
                    if self._viewput and  self._data[id][col]["idtype"] == 0:#проверяем что будем показывать имя карты или код путевого
                        s = str(self._data[id][col]["put"])
                        if len(s)>4:#если код путевого длинный - разбиваем его на части
                            s = "%s-%s"%(s[:4],s[4:])
                    else:
                        s = self._data[id][col]["name"]
                    self.itemDraw(row, col+1, s)
        self._resizeTable()
            
    def itemDraw(self,row,col,data):
        if type(data) == str or type(data) == unicode:
            item = QtGui.QTableWidgetItem("%s"%data)
            self.setItem(row,col,item)    
        elif type(data) == int or type(data) == long:
            item = QtGui.QTableWidgetItem("%d"%data)
            self.setItem(row,col,item)    
    
    def changeView(self):
        self._viewput = not self._viewput 
        self.redraw()
    
    def setBrigada(self, i):
        self._brigada = i
        self.redraw()
    
    def setSelect(self, id):
        row = 0
        for i in self._datarow:
            if i == id:
                self.setCurrentCell(row,0)
            row += 1
            
    def getSelect(self):
        '''Возвращяем что было выделено '''
        row = -1
        col = -1
        if self.rowCount() > 0:
            row = self.currentRow()
            col = self.currentColumn()-1
            if col < 0:#если стали на колонку сортировки
                col = 0
            if col >1:#если стали на колонку праздничных, выходных
                col -= 2
#            idkr = idtype = 0            
#            idvod = self._datarow[row]
#            try:
#                idkr = self._data[idvod][col]["idkarta"]
#                idtype = self._data[idvod][col]["idtype"]
#            except KeyError:#если неудачно прицелились и выбрали пустую ячейку, то выдаем значение, хранящаеся в первой колонке
#                idkr = self._data[idvod][0]["idkarta"]
#                idtype = self._data[idvod][0]["idtype"]
        result = row, col
        return result
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableEditDataKartaSort(tableDataKartaSort):
    def __init__(self,  mysql, parent = None):
        tableDataKartaSort.__init__(self, mysql, parent)
        self._dialog = dialogKartaSortBrigada(mysql)
        self.connect(self._dialog, QtCore.SIGNAL("accepted()"),self.save)
        self._mode = 0 #режимы 0-добавить строку и данные в первую колонку, 1 - добавления в колонку, 2 - изменение данны
    
    def addData(self):        
        self._dialog.setAddMode()
        self._mode = 0

    def editData(self):    
        idvodgrsort = self._datarow[self.currentRow()]
        column = self.currentColumn() - 1
        try:
            idkarta = self._data[idvodgrsort][column]["idkarta"]
            idtype = self._data[idvodgrsort][column]["idtype"]
            self._mode = 1
            self._dialog.setEditMode(idvodgrsort, idkarta, idtype)
        except KeyError:
            idkarta = 0
            idtype = 0
            self._mode = 2
            self._dialog.setAddMode()

    def save(self):    
        row = self.currentRow()
        col = self.currentColumn()-1
        (idkarta, idtype) = self._dialog.getResult()        
        if self._mode == 0:
            sort = self.rowCount()#добавляем в последнюю строку
            self._editKartaSort.addRow(self._brigada, sort, idkarta, idtype)
        elif self._mode == 1:
            idvodgrsort = self._datarow[row]
            iddata = self._data[idvodgrsort][col]["id"]
            self._editKartaSort.editData(idkarta, idtype, iddata)
        elif self._mode == 2:
            idvodgrsort = self._datarow[row]
            self._editKartaSort.addData(idvodgrsort, idkarta, idtype, col)
        self.redraw()
        self.setCurrentCell(row,col+1)
        
    def delData(self):    
        if self.rowCount()>0:
            delrow = 0
            col = self.currentColumn()-1
            row = self.currentRow()
            if col < 1:
                delrow = 1
            id = self._datarow[row]
            sort = self._data[id]["sort"]
            brigada = self._brigada
            try:
                id2 = self._data[id][col]["id"]
                s = self._data[id][col]["name"]
            except KeyError:
                delrow = 1
                
            dlg = QtGui.QMessageBox(self)
            dlg.setWindowTitle(u'Удаление записи')
            if delrow:
                dlg.setText(u'''Вы действительно хотите удалить строку %d?'''%row)
            else:
                dlg.setText(u'''Вы действительно хотите удалить запись '%s'?'''%s)
            dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
            if ok==QtGui.QMessageBox.Ok:
                if delrow:
                    self._editKartaSort.delRow(id, sort, brigada)
                else:
                    self._editKartaSort.delData(id2)
                self.redraw()
    
    def upData(self):
        if self.rowCount()>0:
            row = self.currentRow()
            idvodgrsort = self._datarow[row]
            sort = self._data[idvodgrsort]["sort"]
            if sort > 0:
                self._editKartaSort.upRow(sort-1, self._brigada, idvodgrsort)
                self.redraw()
                self.setSelect(idvodgrsort)
    
    def downData(self):
        if self.rowCount()>0:
            row = self.currentRow()
            idvodgrsort = self._datarow[row]
            sort = self._data[idvodgrsort]["sort"]
            if sort < self.rowCount()-1:
                self._editKartaSort.downRow(sort+1, self._brigada, idvodgrsort)
                self.redraw()
                self.setSelect(idvodgrsort)
    
    

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class sprDataKartaSort(QtGui.QMdiSubWindow):
    def __init__(self, mysql, parent = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Список карточек для графика')        
        self._table = tableEditDataKartaSort(mysql, parent)
        self._table.redraw()
        self._brigadacombo = comboBrigada(mysql)
        self._brigadacombo.redraw()
        self.initAction()        
        self.initWidget()
        self.initConnect()
            
    def initWidget(self):
        widg = QtGui.QWidget()
        grd = QtGui.QGridLayout()
        vh = QtGui.QVBoxLayout()
        tool = QtGui.QToolBar()
        
        tool.addAction(self.addDataAct)
        tool.addAction(self.editDataAct)
        tool.addAction(self.upDataAct)
        tool.addAction(self.downDataAct)
        tool.addAction(self.chViewAct)        
        tool.addWidget(self._brigadacombo)
        tool.addAction(self.delDataAct)
        tool.setIconSize(QtCore.QSize(48,48))
        grd.addWidget(tool)
        grd.addWidget(self._table)
        vh.addLayout(grd)
        widg.setLayout(vh)
        self.setWidget(widg)
    
    def initAction(self):
        self.addDataAct = QtGui.QAction(u'Добавить',self)
        self.addDataAct.setIcon(QtGui.QIcon("../ico/dbplus2.png"))
        self.editDataAct = QtGui.QAction(u'Изменить',self)
        self.editDataAct .setIcon(QtGui.QIcon("../ico/db_update.png"))
        self.delDataAct = QtGui.QAction(u'Удалить',self)
        self.delDataAct .setIcon(QtGui.QIcon("../ico/dbmin2.png"))
        self.upDataAct = QtGui.QAction(u'Выше',self)
        self.upDataAct.setIcon(QtGui.QIcon("../ico/1uparrow.png"))
        self.downDataAct = QtGui.QAction(u'Ниже',self)
        self.downDataAct.setIcon(QtGui.QIcon("../ico/1downarrow.png"))
        self.chViewAct = QtGui.QAction(u'Вид',self)
        self.chViewAct.setIcon(QtGui.QIcon("../ico/1downarrow.png"))
        
    def initConnect(self):
        self.connect(self._brigadacombo, QtCore.SIGNAL("currentIndexChanged(int)"),self.changeBrigada)        
        
        self.connect(self.addDataAct, QtCore.SIGNAL("triggered()"),self._table.addData)
        self.connect(self.editDataAct, QtCore.SIGNAL("triggered()"),self._table.editData)
        self.connect(self.delDataAct, QtCore.SIGNAL("triggered()"),self._table.delData)
        self.connect(self.upDataAct, QtCore.SIGNAL("triggered()"),self._table.upData)
        self.connect(self.downDataAct, QtCore.SIGNAL("triggered()"),self._table.downData)
        self.connect(self.chViewAct, QtCore.SIGNAL("triggered()"),self._table.changeView)
        
    def changeBrigada(self, i):
        self._table.setBrigada(self._brigadacombo.getSelect())
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class selectKartaSort(QtGui.QDialog):
    def __init__(self, mysql, parent = None):
        QtGui.QDialog.__init__(self,parent)
        self._table = tableDataKartaSort(mysql, parent)
        self._table.redraw()
        btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        gr = QtGui.QGridLayout(self)
        gr.addWidget(self._table)
        gr.addWidget(btn1)
    
    def setBrigada(self, idbrigada):
        self._table.setBrigada(idbrigada)
    
    def getResult(self):
        result = self._table.getSelect()
        return result
        