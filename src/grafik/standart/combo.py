#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui

class comboData(QtGui.QComboBox):
    def __init__(self, mysql, parent = None):
        QtGui.QComboBox.__init__(self, parent)
        #self._krtt = getKartaRangeTimeType(mysql)
        
    def getPack(self):
        '''Получаем данные из базы данных'''
        return self.getData().getData()

    def getData(self): 
        '''Возвращаем объект класса getData или editData или editSortData'''
        raise Exception('Method getData need reinitilaze')

    def addRow(self, data):
        return data['name']

    def redraw(self):
        (f, self._datarow, self._data) = self.getPack()
        self.clear()
        s = []
        for idr in self._datarow:
            s += [self.addRow(self._data[idr])]
        self.addItems(s)
        
    def setSelect(self, iddata):
        if self.count() > 0:
            self.setCurrentIndex(0)
            k = 0
            for i in self._datarow:
                if i == iddata :
                    self.setCurrentIndex(k)
                    break
                k += 1
    
    def getSelect(self):
        iddata = 0
        if self.count() > 0:
            iddata = self._datarow[self.currentIndex()]
        return iddata 

    def setFirst(self):
        self.setCurrentIndex(0)
    
    def setLast(self):
        self.setCurrentIndex(len(self._datarow)-1)