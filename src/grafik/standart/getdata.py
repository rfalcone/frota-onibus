#!/usr/bin/python
# -*- coding: utf-8  -*-

class getData:
    '''Класс выдачи данных по запросу '''
    def __init__(self, mysql):
        self._mysql = mysql

    def getData(self, param = []):
        if len(param) > 0:
            (f,dr,d) = self._mysql.getData(self.getDataSql(param))
        else:
            (f,dr,d) = self._mysql.getData(self.getDataSql())
        return (f,dr,d)       
    
    def getDataSql(self, param):
        '''Возвращаем строку запроса для вывода информации'''
        raise Exception('Method getDataSql need reinitilaze')
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class editData(getData):
    '''Класс выдачи данных по запросу, а также их редактирование (добавление, 
        изменение, удаление)'''
    def __init__(self, mysql):
        getData.__init__(self, mysql)
    
    def addData(self, param):
        self._mysql.execsql(self.addDataSql(param))
        iddata = self._mysql.getlastid()
        return iddata
    
    def editData(self, param, idedit):
        self._mysql.execsql(self.editDataSql(param, idedit))
    
    def delData(self, iddel):
        self._mysql.execsql(self.delDataSql(iddel))
        
    def addDataSql(self, param):
        '''Возвращаем строку запроса для добавления значений'''
        raise Exception('Method getAddDataSql need reinitilaze')
    
    def editDataSql(self, param, idedit):
        '''Возвращаем строку запроса для добавления значений'''
        raise Exception('Method getEditDataSql need reinitilaze')

    def delDataSql(self, iddel):
        '''Возвращаем строку запроса для удаления значений'''
        raise Exception('Method getDelDataSql need reinitilaze')
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
class editSortData(editData):
    '''Класс выдачи данных по запросу, а также их редактирование (добавление, 
        изменение, удаление), порядок сортировки в списке'''

    def __init__(self, mysql):
        editData.__init__(self, mysql)
        
    def upRow(self, sort, param, iddata):
        self._mysql.execsql(self.upRowSql(sort, param))
        self._mysql.execsql(self.setSortRowSql(sort, iddata))
        
    def downRow(self, sort, param, iddata):
        self._mysql.execsql(self.downRowSql(sort, param))
        self._mysql.execsql(self.setSortRowSql(sort, iddata))

    def delData(self, param, iddata):
        self._mysql.execsql(self.delDataSql(iddata))
        self._mysql.execsql(self.updSortRowSql(param))
        
    def upRowSql(self, sort, param):
        '''Возвращаем строку запроса перемещения в сортировке элемента вверх'''
        raise Exception('Method upRowSql need reinitilaze')
        
    def downRowSql(self, sort, param):
        '''Возвращаем строку запроса перемещения в сортировке элемента вниз'''
        raise Exception('Method downRowSql need reinitilaze')
    
    def setSortRowSql(self, param, iddata):
        '''Возвращаем строку запроса установки нового значения сортировки для элемента после поднятия, опускания других'''
        raise Exception('Method setSortRowSql need reinitilaze')

    def updSortRowSql(self, param):
        '''Возвращаем строку запроса установки новых значений сортировки для элемента после удаления элемента'''
        raise Exception('Method updSortRowSql need reinitilaze')
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class editDatas(editData):
    '''Расширяем класс для возможности множественного добавления при копировании 
    к примеру, а также множественному удалению '''
    def __init__(self, mysql):
        editData.__init__(self, mysql)

    def addDatas(self,param):
        sql = self.addDatasSql(param)
        self._mysql.execsql(sql)
        iddata = self._mysql.getlastid()#становимся на первый добавленый, если необходимо стать на последний добавленный + len(param)
        return iddata
    
    def delDatas(self, iddels):
        sql = self.delDatasSql(iddels)
        self._mysql.execsql(sql)
        iddata = self._mysql.getlastid()#становимся на первый добавленый, если необходимо стать на последний добавленный + len(param)
        return iddata
    
    def addDatasSql(self,param):
        '''Возвращаем строку запроса добавления списка элементов param - список словарей'''
        raise Exception('Method addDatasSql need reinitilaze')
    
    def delDatasSql(self, iddatas):
        '''Возвращаем строку запроса удаления списка элементов iddatas - список id для удаления'''
        raise Exception('Method delDatasSql need reinitilaze')
    
