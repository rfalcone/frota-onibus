#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
import calendar

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
class tableData(QtGui.QTableWidget):
    def __init__(self, mysql, parent = None):
        QtGui.QTableWidget.__init__(self,parent)
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
    
    def _initTable(self):
        self.setRowCount(0)
        self.setRowCount(len(self._datarow))
        self.setColumnCount(len(self.getHeader()))
        self.setHorizontalHeaderLabels(self.getHeader())
    
    def _resizeTable(self):
        self.resizeColumnsToContents()
        self.resizeRowsToContents ()
        self.setCurrentCell(0,0)
    
    def getPack(self):
        '''Получаем данные из базы данных если необходимо передавать с 
        параметром - переопределить в наследнике'''
        return self.getData().getData()
        
    def redraw(self):
        (f, self._datarow, self._data) = self.getPack()
        self._initTable()
        row = 0
        for i in self._datarow:
            self.addRow(self._data[i], row)
            row += 1 
        self._resizeTable()
            
    def itemDraw(self,row,col,data):
        if type(data) == str or type(data) == unicode:
            item = QtGui.QTableWidgetItem("%s"%data)
            self.setItem(row,col,item)    
        elif type(data) == int or type(data) == long:
            s = str(data)            
            item = QtGui.QTableWidgetItem(s)
            self.setItem(row,col,item)
            
    def getSelect(self):
        i = self._datarow[self.currentRow()]
        return i
        
    def setSelect(self, idSelect):
        k = 0
        for i in self._datarow:
            if i == idSelect:
                self.setCurrentCell(k,0)
                break
            k += 1

    def getHeader(self):
        '''Возвращаем объект типа списка уникод строк с названиями колонок'''
        raise Exception('Method getHeader need reinitilaze')

    def getData(self):
        '''Возвращаем объект класса getData или editData или editSortData'''
        raise Exception('Method getData need reinitilaze')

    def addRow(self, data, row):
        '''Достаем данные из data и в строку row заносим значения'''
        raise Exception('Method addRow need reinitilaze')
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableEditData(tableData):
    def __init__(self, mysql, parent = None):
        tableData.__init__(self, mysql, parent)
        self.connect(self.getDialog(), QtCore.SIGNAL("accepted()"),self.save)
        
    def delData(self):    
        if self.rowCount()>0:
            row = self.currentRow()
            i = self._datarow[row]
            s = self.getStrForDel(self._data[i])
            dlg = QtGui.QMessageBox(self)
            dlg.setWindowTitle(u'Удаление записи')
            dlg.setText(u'''Вы действительно хотите удалить запись '%s'?'''%s)
            dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
            if ok==QtGui.QMessageBox.Ok:
                self.getData().delData(i)
                self.redraw()
                self.setSelect(0)

    def addParamForSave(self, param):
        '''Добавить дополнительные значения перед сохранением '''
        return param

    def save(self):    
        (param, mode) = self.getDialog().getResult()
        param = self.addParamForSave(param)
        row = self.currentRow()
        if mode == 0:
            iddata = self.getData().addData(param)
        else:
            iddata = self._datarow[row]
            self.getData().editData(param, iddata)
        self.redraw()
        self.setSelect(iddata)
        
    def getDialog(self):
        '''Возвращаем объект класса dialogData для редактирования значений в диалоговом окне'''
        raise Exception('Method getDialog need reinitilaze')
    
    def addData(self):        
        '''Метод инициализации диалога для добавления значений'''
        raise Exception('Method addData need reinitilaze')

    def editData(self):    
        '''Метод инициализации диалога для редактирования значений'''
        raise Exception('Method editData need reinitilaze')

    def getStrForDel(self, data):
        '''Метод возвращающий строку, которую будем удалять на входе словарь с данными'''
        raise Exception('Method getStrForDel need reinitilaze')
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableSortData(tableEditData):
    def __init__(self, mysql, parent = None):
        tableData.__init__(self, mysql, parent)

    def getParam(self, data):        
        "Возвращает переменную, группирующую данные, к примеру ид бригады, ид маршрута"
        raise "Method getParam need reinitilaze "
        
    def upData(self):
        if self.rowCount()>0:
            row = self.currentRow()
            iddata = self._datarow[row]
            param = self.getParam(self._data[iddata])
            sort = param["sort"]
            if sort > 0:
                self.getData().upRow(sort-1, param, iddata)
                self.redraw()
                self.setSelect(iddata)
    
    def downData(self):
        if self.rowCount()>0:
            row = self.currentRow()
            iddata = self._datarow[row]
            param = self.getParam(self._data[iddata])
            sort = param ["sort"]
            if sort < self.rowCount()-1:
                self.getData().downRow(sort+1, param, iddata)
                self.redraw()
                self.setSelect(iddata)
    
    def delData(self):    
        if self.rowCount()>0:
            row = self.currentRow()
            i = self._datarow[row]
            s = self.getStrForDel(self._data[i])            
            dlg = QtGui.QMessageBox(self)
            dlg.setWindowTitle(u'Удаление записи')
            dlg.setText(u'''Вы действительно хотите удалить запись '%s'?'''%s)
            dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
            if ok==QtGui.QMessageBox.Ok:
                self.getData().delData(self.getParam(self._data[i]) ,i)
                self.redraw()
                self.setSelect(0)
                
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class freezeTable(QtGui.QTableView):
    def __init__(self, parent=None):
        QtGui.QTableView.__init__(self,parent)
        self._freezeColumnCount = 3
        self._headernamebegin = [u'Фио',u'Гаражный',u'Часов']        
        self._timer = QtCore.QTimer()
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))        
        self._mdl = QtGui.QStandardItemModel()
        self._mdls = QtGui.QItemSelectionModel(self._mdl)
        self.setModel(self._mdl)
        self.setSelectionModel(self._mdls)
        self.redrawFreeze()

    def getFreezeColumnCount(self):
        return len(self._freezeColumnCount)
    
    def redrawFreeze(self):
        try:
            self._frozenTableView 
            self.disconnect(self.horizontalHeader(),QtCore.SIGNAL('sectionResized(int,int,int)'), self.updateSectionWidth)
            self.disconnect(self.verticalHeader(),QtCore.SIGNAL('sectionResized(int,int,int)'), self.updateSectionHeight)
            self.disconnect(self._frozenTableView.verticalScrollBar(), QtCore.SIGNAL('valueChanged(int)'),
                            self.verticalScrollBar(), QtCore.SLOT('setValue(int)'))
            self.disconnect(self.verticalScrollBar(), QtCore.SIGNAL('valueChanged(int)'),
                            self._frozenTableView.verticalScrollBar(), QtCore.SLOT('setValue(int)'))
            self.disconnect(self._timer, QtCore.SIGNAL("timeout()"), self._scrollTable)#для костылей
            self._mdl.setColumnCount(0)
            del self._frozenTableView 
        except:
            pass
        
        
        self._frozenTableView = QtGui.QTableView(self)
        self._frozenTableView.setModel(self.model())
        self._frozenTableView.setFocusPolicy(QtCore.Qt.NoFocus)
        
        
        self._frozenTableView.verticalHeader().hide()
        self._frozenTableView.horizontalHeader().setResizeMode(QtGui.QHeaderView.Fixed)
        self.viewport().stackUnder(self._frozenTableView)
        self._frozenTableView.setStyleSheet("QTableView { border: none;"
                                      "background-color: #8EDE21;"
                                      "selection-background-color: #999}")
        self._frozenTableView.setSelectionModel(self.selectionModel())
        self._frozenTableView.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self._frozenTableView.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollMode(self.ScrollPerPixel)
        self.setVerticalScrollMode(self.ScrollPerPixel)
        self._frozenTableView.setVerticalScrollMode(self.ScrollPerPixel)
        self._frozenTableView.show()
        self.initWidget()        


    def initAction(self):
        self.connect(self.horizontalHeader(),QtCore.SIGNAL('sectionResized(int,int,int)'), self.updateSectionWidth)
        self.connect(self.verticalHeader(),QtCore.SIGNAL('sectionResized(int,int,int)'), self.updateSectionHeight)
        self.connect(self._frozenTableView.verticalScrollBar(), QtCore.SIGNAL('valueChanged(int)'),
               self.verticalScrollBar(), QtCore.SLOT('setValue(int)'))
        self.connect(self.verticalScrollBar(), QtCore.SIGNAL('valueChanged(int)'),
               self._frozenTableView.verticalScrollBar(), QtCore.SLOT('setValue(int)'))
        self.connect(self._timer, QtCore.SIGNAL("timeout()"), self._scrollTable)#для костылей

    def initWidget(self):
        "Перерисовка виджета (всей таблицы)"
        for col in range(self._freezeColumnCount,self.model().columnCount()):
             self._frozenTableView.setColumnHidden(col, True)
        for w in range(0,self._freezeColumnCount):
            self._frozenTableView.setColumnWidth(w, self.columnWidth(w))
        for w in range(0,self.model().rowCount()):
            self._frozenTableView.setRowHeight(w, self.rowHeight(w))
        self.updateFrozenTableGeometry()
        
        self.initAction()        
        
    def updateSectionWidth(self,logicalIndex,i,newSize):
        if logicalIndex <= self._freezeColumnCount:
             self._frozenTableView.setColumnWidth(logicalIndex,newSize)
             self.updateFrozenTableGeometry()

    def updateSectionHeight(self, logicalIndex, i, newSize):
        self._frozenTableView.setRowHeight(logicalIndex, newSize)

    def resizeEvent(self,event):
        super(QtGui.QTableView, self).resizeEvent(event)
        self.updateFrozenTableGeometry()

    def moveCursor(self, cursorAction, modifiers):
        current = QtGui.QTableView.moveCursor(self, cursorAction, modifiers)
        if (cursorAction == self.MoveLeft and current.column()>0
                and  self.visualRect(current).topLeft().x() < self._frozenTableView.columnWidth(0) ):
            newValue = self.horizontalScrollBar().value() + self.visualRect(current).topLeft().x() - self._frozenTableView.columnWidth(0)
            self.horizontalScrollBar().setValue(newValue)
        return current

    def scrollTo(self, index, hint):
        if (index.column()>0):
            QtGui.QTableView.scrollTo(self, index, hint)

    def updateFrozenTableGeometry(self):
        size = 0
        for i in range(0,self._freezeColumnCount):
            size += self.columnWidth(i)
        self._frozenTableView.setGeometry(self.verticalHeader().width() + self.frameWidth(),
            self.frameWidth(),
            size,
            self.viewport().height()+self.horizontalHeader().height())

    def beginRedraw(self):
        self._freeColumnCount = calendar.monthrange(self._date.year, self._date.month)[1]
        self._headername = self._headernamebegin[:]
        for i in range(self._freeColumnCount):
            self._headername += [str(i+1)]

        self._mdl.setColumnCount(self._freezeColumnCount)
        self._mdl.setHorizontalHeaderLabels(self._headername)
        self._mdl.setRowCount(0)
        self._mdl.setColumnCount(self._freeColumnCount+self._freezeColumnCount)
        self.initWidget()
    
    def endRedraw(self):
        self.resizeColumnsToContents()
        self.resizeRowsToContents()
        self.updateFrozenTableGeometry()

    def _initKostil(self):#устанавливаем при выведении какого либо диалогового окна
        self.a = self.verticalScrollBar().value()
        
    def _kostil(self):#после закрытия диалогового окна, активируем костыль
        self._timer.start(10)#костыль

    def _scrollTable(self):#костыль против скролинга после появления диалогового окна и заполнения данных
        if self.verticalScrollBar().value() != self.a:
            self._timer.stop()        
            self.verticalScrollBar().setValue(self.a)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class dialogData(QtGui.QDialog):#вспомогательное диалоговое окно
    def __init__(self, parent = None):
        QtGui.QDialog.__init__(self, parent)
        self.initWidget()

    def initWidget(self):
        grid = QtGui.QGridLayout(self)
        btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        l = self.addComponent()
        grid.addLayout(l,0,0)
        grid.addWidget(btn1,1,0,1,2)

    def setMode(self, mode, title, param = []):
        if mode == 0:
            if len(param) > 0:
                self.setAddMode(param)
            else:
                self.setAddMode()
            self.setWindowTitle(title)
            self._mode = 0
        else:
            self.setEditMode(param)
            self.setWindowTitle(title)
            self._mode = 1
        self.setModal(1)
        self.show()
        
    def setAddMode(self):
        '''Initilaze widgets for add data'''
        raise 'Method setAddMode need reinitilaze'        
    
    def setEditMode(self,param):
        '''Initilaze widgets for editing data'''
        raise 'Method setEditMode need reinitilaze'        

    def addComponent(self):
        '''Вернуть layout с расположенными виджетами'''
        raise Exception('Method addComponent need reinitilaze')
    
    def getResult(self):
        '''Вернуть результат с режимом обработки '''
        raise Exception('Method getResult need reinitilaze')
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class wData(QtGui.QWidget):
    def __init__(self, mysql, parent = None):
        QtGui.QWidget.__init__(self, parent)
        self.initAction()        
        self.initWidget()
        self.initConnect()        
        self.redraw()

    def setTable(self, tableClass):
        raise 'Method setTable need reinitilaze'                
    
    def getTable(self):
        raise 'Method getTable need reinitilaze'                
        
    def initTool(self):
        tool = QtGui.QToolBar()
        tool.addAction(self.addDataAct)
        tool.addAction(self.editDataAct)
        tool.addAction(self.delDataAct)
        tool.setIconSize(QtCore.QSize(48,48))
        return tool
    
    def initWidget(self):
        grd = QtGui.QGridLayout()
        vh = QtGui.QVBoxLayout()
        grd.addWidget(self.initTool())
        grd.addWidget(self.getTable())
        vh.addLayout(grd)
        self.setLayout(vh)
    
    def initAction(self):
        self.addDataAct = QtGui.QAction(u'Добавить',self)
        self.addDataAct.setIcon(QtGui.QIcon("../ico/dbplus2.png"))
        self.editDataAct = QtGui.QAction(u'Изменить',self)
        self.editDataAct .setIcon(QtGui.QIcon("../ico/db_update.png"))
        self.delDataAct = QtGui.QAction(u'Удалить',self)
        self.delDataAct .setIcon(QtGui.QIcon("../ico/dbmin2.png"))
        
    def initConnect(self):
        self.connect(self.addDataAct, QtCore.SIGNAL("triggered()"),self.getTable().addData)
        self.connect(self.editDataAct, QtCore.SIGNAL("triggered()"),self.getTable().editData)
        self.connect(self.getTable(), QtCore.SIGNAL("cellDoubleClicked(int,int)"),self.getTable().editData)
        self.connect(self.delDataAct, QtCore.SIGNAL("triggered()"),self.getTable().delData)
        self.connect(self.getTable(), QtCore.SIGNAL("itemSelectionChanged()"),self.emitChangeRow)
            
    def redraw(self):
        self.getTable().redraw()
    
    def getSelect(self):
        return self.getTable().getSelect()
    
    def emitChangeRow(self):
        self.emit(QtCore.SIGNAL('itemSelectionChanged()'))    
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class wDataSort(wData):
    def __init__(self, mysql, parent = None):
        wData.__init__(self, mysql, parent)
    
    def initTool(self):
        tool = QtGui.QToolBar()
        tool.addAction(self.addDataAct)
        tool.addAction(self.editDataAct)
        tool.addAction(self.upDataAct)
        tool.addAction(self.downDataAct)
        tool.addSeparator()
        tool.addAction(self.delDataAct)
        tool.setIconSize(QtCore.QSize(48,48))
        return tool
    
    def initAction(self):
        wData.initAction(self)
        self.upDataAct = QtGui.QAction(u'Выше',self)
        self.upDataAct.setIcon(QtGui.QIcon("../ico/1uparrow.png"))
        self.downDataAct = QtGui.QAction(u'Ниже',self)
        self.downDataAct.setIcon(QtGui.QIcon("../ico/1downarrow.png"))
    
    def initConnect(self):
        wData.initConnect(self)
        self.connect(self.upDataAct, QtCore.SIGNAL("triggered()"),self.getTable().upData)
        self.connect(self.downDataAct, QtCore.SIGNAL("triggered()"),self.getTable().downData)