#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from datetime import datetime
import time
from map.tilenames import lengthMetr

class journalDialog(QtGui.QDialog):
    def __init__(self, parent, mysql):
        QtGui.QDialog.__init__(self, parent)
        self.mysql = mysql
        self.setMinimumSize(600,400)
        self.initWidget()
        self.loadComboBox()
        self.initSignals()
        self.drawvector = []
        self.id = 0
        

    def initWidget(self):
        gl = QtGui.QGridLayout()
        self.table = QtGui.QTableWidget()
        self.setLayout(gl)
        t1 = QtGui.QLabel(u'Гаражный')
        t2 = QtGui.QLabel(u'Начало периода')
        t3 = QtGui.QLabel(u'Конец периода')
        self.ngarag = QtGui.QComboBox()
        self.datebegin = QtGui.QDateTimeEdit()
        self.dateend = QtGui.QDateTimeEdit()
        self.datebegin.setDisplayFormat('dd.MM.yy HH:mm')
        self.datebegin.setCalendarPopup(1)
        self.dateend.setDisplayFormat('dd.MM.yy HH:mm')
        self.dateend.setCalendarPopup(1)
        self.datebegin.setDate(datetime.now())
        self.dateend.setDateTime(datetime.now())
        gl.addWidget(t1,0,0)
        gl.addWidget(self.ngarag,1,0)
        gl.addWidget(t2,0,1)
        gl.addWidget(self.datebegin,1,1)
        gl.addWidget(t3,0,2)
        gl.addWidget(self.dateend,1,2)
        gl.setColumnStretch(3,1)
        gl.addWidget(self.table,2,0,1,4)

        self.table.setRowCount(2)
        self.table.setColumnCount(5)
        nameHeader = [u'Дата и время',u'Координаты',u'Расстояние',u'Точек']
        self.table.setHorizontalHeaderLabels(nameHeader)
        self.data = []
        self.x = 0

    ''

    def initSignals(self):
        self.connect(self.ngarag ,QtCore.SIGNAL("currentIndexChanged(int) "),self.showData)
        self.connect(self.table ,QtCore.SIGNAL("cellClicked(int,int) "),self.showMap)
        self.connect(self.table ,QtCore.SIGNAL("cellActivated  (int,int) "),self.showMap)


    def loadComboBox(self):
        self.bus = []
        items = []
        
        sql = """SELECT garnom, gosnom FROM bus ORDER BY garnom"""
        self.mysql.execsql(sql)
        result_set = self.mysql.cursor.fetchall()
        for row in result_set:
            self.bus += [[row["garnom"],row["gosnom"]]]
            items += ["%d %s"%(row["garnom"],row["gosnom"])]

        self.ngarag.clear()
        self.ngarag.addItems(items)

    def closeEvent(self,event):
        self.mysql.disconnect()

    def showData(self,index):
        st = time.time()
        datebegin = QtCore.QDateTime(self.datebegin.dateTime())
        dateend = QtCore.QDateTime(self.dateend.dateTime())
        datebegin = datebegin.toPyDateTime()
        dateend = dateend.toPyDateTime()
        yearmonth = "%04d%02d"%(datebegin.date.year,datebegin.date.month)
        print (yearmonth,)
        sql = """SELECT p.x,p.y,p.datetime, p.npoint, p.nmbr
                 FROM point%s as p, bus, pribor
                 WHERE bus.garnom = %d AND pribor.idbus = bus.id AND pribor.nmbr = p.nmbr AND
                    p.datetime >= '%s' AND p.datetime <= '%s'

                 ORDER BY p.datetime"""%(yearmonth, self.bus[index][0],
                    str(datebegin), str(dateend) )
        self.mysql.execsql(sql)
        
        i = 0
        result_set = self.mysql.cursor.fetchall()
        self.table.setRowCount(self.mysql.cursor.rowcount)
        mm = 0
        x1 = 0
        x2 = 0.0
        y1 = 0
        y2 = 0.0
        npoint = 0
        fl = 1
        self.data = []
        for row in result_set:
            #print row["nmbr"]
            bibl = {}
            bibl['x']= row["x"]
            bibl['y']= row["y"]
            x2 = bibl['x']
            y2 = bibl['y']
            if x1 == 0 and y1 == 0:
                x1 = x2
                y1 = y2

            if x1 == x2 and y1 == y2:
                lenghtM = [0,0]
            else:
                lenghtM = lengthMetr(x1,y1,x2,y2)
            if (lenghtM[0]>10) or fl:
                fl = 0
                x1 = x2
                y1 = y2
                bibl['datetime']= row["datetime"]
                bibl['npoint']= npoint
                bibl['metr'] = lenghtM[0]
                bibl['angle'] = lenghtM[1]
                self.data += [bibl]

                item = QtGui.QTableWidgetItem("%s"%str(bibl['datetime']))
                self.table.setItem(i,0,item)
                item = QtGui.QTableWidgetItem("%.4f %.4f"%(bibl['x'],bibl['y']))
                self.table.setItem(i,1,item)
                mm += bibl['metr']
                item = QtGui.QTableWidgetItem("%d"%bibl['metr'])
                self.table.setItem(i,2,item)
                item = QtGui.QTableWidgetItem("%d"%bibl['npoint'])
                self.table.setItem(i,3,item)
                npoint = 0
                i+=1
            else:
                npoint +=1
        self.table.setRowCount(i)
        self.table.resizeColumnsToContents()
        self.table.resizeRowsToContents ()
        self.id += 1
        print time.time()-st
        print mm

    def showMap(self,x=-1,y=-1):
        self.x = x
        #print self.x

    def setVector(self):
        return [self.x,self.data]

    def getChange(self):
        return self.id