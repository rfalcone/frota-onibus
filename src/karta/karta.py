#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from spr.control import controltable2, editWind

class modEditWind(editWind):
    def __init__(self, parent = None):
        editWind.__init__(self, parent)
        lbl1 = QtGui.QLabel(u'Название')
        lbl2 = QtGui.QLabel(u'Смена')
        lbl3 = QtGui.QLabel(u'В путевых')
        self.ed1 = QtGui.QLineEdit()
        self.ed2 = QtGui.QLineEdit()
        self.ed3 = QtGui.QLineEdit()
        self.grid.addWidget(lbl1,0,0)
        self.grid.addWidget(self.ed1,0,1)
        self.grid.addWidget(lbl2,1,0)
        self.grid.addWidget(self.ed2,1,1)
        self.grid.addWidget(lbl3,2,0)
        self.grid.addWidget(self.ed3,2,1)
        
        
        self.grid.addWidget(self.btn1,3,0,3,2)

class karta(controltable2):
    def __init__(self, parent = None, mysql = None):
        self.initSql()
        self.dataview = ["name", "smena", "put"]
        self.headername = [u"Название", u"Смена", u"В путевых"]
        controltable2.__init__(self, parent, mysql)
        self.edit = modEditWind(self)
        self.connect(self.edit, QtCore.SIGNAL("accepted()"),self.save)
        

    def initSql(self):
        self.dataparam = ["name", "smena", "put"]
        self.getDataSQL = '''SELECT id, name, smena, put FROM karta WHERE del = 0 ORDER BY name'''
        self.addDataSQL = '''INSERT INTO karta (name, smena, put ) VALUES ('%s', %d, %d)'''
        self.editDataSQL = '''UPDATE karta SET name = '%s', smena = %d, put = %d  WHERE id = %d'''
        self.delDataSQL ='''UPDATE karta SET del=1 WHERE id = %d'''
    
    def itemtable(self,row,col,s):
        if col == 2:
            if len(s)>4:
                s = "%s-%s"%(s[:4],s[4:])
        item = QtGui.QTableWidgetItem("%s"%s)
        self.table.setItem(row,col,item)

    def addDataControl(self):
        self.edit.ed1.setText("")
        self.edit.ed2.setText("")
        self.edit.ed3.setText("")
        controltable2.addDataControl(self)

    def editDataControl(self):
        r = self.table.currentRow()
        self.edit.ed1.setText(self.data[self.datarow[r]][self.dataview[0]])
        self.edit.ed2.setText(str(self.data[self.datarow[r]][self.dataview[1]]))
        self.edit.ed3.setText(str(self.data[self.datarow[r]][self.dataview[2]]))
        controltable2.editDataControl(self)

    def delDataControl(self):
        controltable2.delDataControl(self)
        self.redraw([])

    def save(self):
        r1 = self.table.currentRow()
        try:
            n2 = int(self.edit.ed2.text())
        except ValueError:
            n2 = 0
        try:
            n3 = int(self.edit.ed3.text())
        except ValueError:
            n3 = 0
        self.datasave = [self.edit.ed1.text(), n2, n3]
        controltable2.save(self)
        self.redraw([])
        if self.mode == 1:
            id2 = self._getLastId()
            k = 0
            for id in self.datarow:
                if id == id2:
                    self.r = k
                k += 1
        else:
            self.r = r1
        self.table.setCurrentCell(self.r,0)
