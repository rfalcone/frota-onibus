#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from spr.view import viewtable
from function import formatstr

class kartaRangeForAzimut(viewtable):
    '''Класс для показа id таблицы KartaRange для заполнения в Азимуте-2 
    дополнительных полей
    '''
    def __init__(self, parent, mysql):
        self.initSql()        
        viewtable.__init__(self, parent, mysql)
        
        
    def initSql(self):
        self.dataview = ["id", "name", "put", "week"]
        self.sortcolumn = ['kr.id', 'k.name', 'k.put']
        self.headername = ["id", u"Карточка", u"По путевым", u"Действие"]
        
        self.dataparam = ['monday', 'tuesday', 'wednesday', 'thursday', 
                'friday', 'saturday', 'sunday', 'workday', 'weekend', 
                'holiday', 'bholiday', 'allday', 'noholiday',  'name', 'put']
        self.getDataSQL = '''
            SELECT kr.id as id, kr.monday, kr.tuesday, kr.wednesday, kr.thursday, 
                kr.friday, kr.saturday, kr.sunday, kr.workday, kr.weekend, 
                kr.holiday, kr.bholiday, kr.noholiday, kr.allday, k.name, k.put
            FROM karta as k, kartarange as kr
            WHERE k.id = kr.idkarta AND kr.del = 0
            ORDER BY %s
        '''
        self.dest = 1
        self.selcol = 0

    
    def _getData(self, data =[]):
        try:
            self.datarow = []
            self.mysql.execsql(self.getDataSQL%tuple(data))
            result_set = self.mysql.cursor.fetchall()
            self.data = {} #словарь со всеми полями
            self.datarow = []
            for row in result_set:
                tmpdata = {}
                tmpweek = []
                for i in self.dataparam:
                    tmpdata[i] = row[i] #проход по полям
                    tmpweek += [row[i]] #проход по полям
                tmpdata ["week"] = formatstr(tmpweek)
                tmpdata ["id"] = row["id"]
                self.data[row["id"]] = tmpdata#сохраняем в данных
                self.datarow += [row["id"]]
        except:
            pass

    def redraw(self, data = []):
        if self.dest: s = ' ASC'
        else: s = ' DESC'
        if self.selcol < 3:
            namecolumn = self.sortcolumn[self.selcol]
        viewtable.redraw(self,["%s %s"%(namecolumn,s)])
        
    
    def itemtable(self,row,col,s):
        if col == 2 and len(s)>4:
            item = QtGui.QTableWidgetItem("%s-%s"%(s[0:4],s[4:]))
        else:
            item = QtGui.QTableWidgetItem("%s"%s)
        self.setItem(row,col,item)
    
    def sortChanged(self,i):
        if i == self.selcol:
            self.dest = 1 - self.dest
        elif i < 3:
            self.selcol = i
            self.dest = 0
        self.redraw()
    
        
class kartaazimut(QtGui.QMdiSubWindow):
    '''Окно вывода справочника карточек и id kartarange с днями действия'''    
    def __init__(self, parent = None, mysql = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Карточки со сроком действия') 
        self.table = kartaRangeForAzimut(self,mysql)
        self.table.redraw()
        self.initWidget()
    
    def initWidget(self):
        '''Инициализация виджетов, расстановка по диалоговому окну
            '''
        widg = QtGui.QWidget()
        grd = QtGui.QGridLayout()
        grd.addWidget(self.table)
        widg.setLayout(grd)
        self.setWidget(widg)
        self.connect(self.table.horizontalHeader(), QtCore.SIGNAL("sectionClicked(int)"),self.table.sortChanged)
        
        