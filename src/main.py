#!/usr/bin/python
# -*- coding: utf-8  -*-
import sys
from PyQt4 import QtGui, QtCore
from db import db

class main(QtGui.QMainWindow):
    '''Класс запуска всех окон
    '''
    
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        #mainwidget = QtGui.QWidget()
        self.setWindowTitle(u'АСДУ')
        self.setMinimumSize(600,400)
        self.central()
        print "Connect to MySQL database..."
        self.mysql = db()
        self.config = {}
        self.config["db"] = {}
        self.config["db"]["host"] = "localhost"
        self.config["db"]["host"] = "10.1.12.52"
        self.config["db"]["user"] = "asdm"
        self.config["db"]["passwd"] = "f4E4HwTcBWQRwDAy"
        self.config["db"]["db"] = "asdm"
        self._iduser = 1
        self.mysql.config = self.config["db"]
        self.mysql.connect()
        self.initSql()
        self.initmenu()
        self.initwidget()

        print "Connecting"
        self._listCommand = []
        self._listCommand += [self.close]
        
        #self.connect(self, QtCore.SIGNAL("closed()"),self.mysql.disconnect)
    def closeEvent(self, event):
        self.mysql.disconnect()
        #self.close()
        print "Disconnected"

    def initwidget(self):
        self.mdi = QtGui.QMdiArea()
        self.mdi.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded);
        self.mdi.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.setCentralWidget(self.mdi )


    def central(self):
        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2,
            (screen.height()-size.height())/2)

    def initSql(self):
        self._getDataSql = '''SELECT id, name, parentid, command, pack, 
                    module, typemodule, shortcut
                 FROM menu 
                 WHERE iduser = %d AND parentid = %d
                 ORDER BY sort'''

    def menuAdd(self, menu, userid, parentid):
        #menu = QtGui.QMenu()
        typemenu = 0        
        (f, dr, d) = self.mysql.getData(self._getDataSql %(userid, parentid))
        for i in dr:
            t = self.menuAdd(menu, userid, i)
            if t == 0:
                tmp = QtGui.QAction(d[i]["name"],self)
                data = [d[i]["command"], d[i]["pack"], d[i]["module"], d[i]["typemodule"]]
                tmp.setData(data)
                tmp.setShortcut(d[i]["shortcut"])               
                self.connect(tmp, QtCore.SIGNAL("triggered()"), self.execFunc)
                menu.addAction(tmp)
            else:
                menu.addMenu(d[i]["name"])
        return typemenu

    def initmenu(self):
        self.menu = self.menuBar()
        self.setMenuBar(self.menu)
        userid = self._iduser
        (f, dr, d) = self.mysql.getData(self._getDataSql %(userid, 0))
        for i in dr:
            menu = self.menu.addMenu(d[i]["name"])
            self.menuAdd(menu, userid, i)
        statusbar = QtGui.QStatusBar()
        self.setStatusBar(statusbar);

    def execFunc(self):
        "Метод группового запуска классов после получения сигнала от меню"
        com, pack, mod, t = self.sender().data().toPyObject()

        if t == 0:#выполняем команды данного класа
            if com >0:
                self._listCommand[com-1]()
        elif t == 1:#подгружаем окно, без подключения к базе
            s = pack.split(".")
            m = __import__(str(pack))
            if len (s)>1:
                for a in s[1:]:
                    m = getattr(m,str(a))
            mm = getattr(m,str(mod))
            myform = mm(self)#formKartaMarsh(self, self.mysql)
            self.mdi.addSubWindow(myform)
            myform.show()
        if t == 2:#подгружаем окно, с подключения к базе
            s = pack.split(".")
            m = __import__(str(pack))
            if len (s)>1:
                for a in s[1:]:
                    m = getattr(m,str(a))
            mm = getattr(m,str(mod))
            myform = mm(self.mysql, self)#formKartaMarsh(self, self.mysql)
            self.mdi.addSubWindow(myform)
            #print myform.windowTitle()
            myform.show()

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    qb = main()
    qb.show()
    sys.exit(app.exec_())

