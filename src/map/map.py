#!/usr/bin/python
# -*- coding: utf-8  -*-
import pygame
import time
from pygame.locals import *
import tilenames
import generate_tiles as gt
import sys, os
import threading
import Image, ImageDraw

class ShowMap(threading.Thread):#поток для отрисовки недостатющих тайлов
    def __init__(self):
        super(ShowMap,self).__init__()
        self.x = 0
        self.y = 0
        self.z = 0
        self.flag = 0
        self.id = 0
        self.initgentile()

    def start(self):
        super(ShowMap,self).start()

    def run(self):
        self.gentile()
        self.flag = 1
        print ("%d off work"%self.id)

    def gentile(self):
        v = tilenames.tileEdges(self.x,self.y,self.z)
        bbox = (v[1],v[0],v[3],v[2])
        gt.render_tiles(bbox, self.mapfile, self.tile_dir, self.z, self.z, "Belarus")

    def initgentile(self):
        home = os.environ['HOME']
        try:
            self.mapfile = os.environ['MAPNIK_MAP_FILE']
        except KeyError:
            self.mapfile = "/home/dark123us/project/osm/bin/mapnik/osm.xml"
        try:
            self.tile_dir = os.environ['MAPNIK_TILE_DIR']
        except KeyError:
            self.tile_dir = home + "/osm/tiles2/"

        if not self.tile_dir.endswith('/'):
            self.tile_dir = self.tile_dir + '/'


class mapOsm():
    def __init__(self):
        pygame.init()
        pygame.display.set_caption(u'Карта')
        print pygame.display.get_wm_info()
        self.mode = (700,768)
        self.initScreen()
        self.path = '/home/dark123us/osm/tiles2/'#где храним тайлы

        a = self.initTiles()
        self.xyt = a[0]#храним (x,y,x1,y1] для каждого зума
        self.noimage = a[1]#если изображения нет - будем отображать пустую картинку

        self.zoom = 14 #начальный уровень зума
        self.x = -105297 #смещение правого края карты
        self.y = -145016 #смещение верхнего края карты
        self.xk = 0 #координаты курсора
        self.yk = 0 #
        self.fps = 0

        self.tileimage = []#углы загруженных тайлов
        self.tileimage = []#x,y,тип картинки - 0 нет, 1 - есть
        self.reDraw()
        self.initFont()
        self.moveflag = 0 #флаг указывающий что перемещении мыши карту двигают
        self.runflag = 1 #флаг цикла запуска отрисовки карты

        self.vector = []
        self.bus = []
        self.drawlayerid = 0 #флаг изменений для перерисовки, при изменении увеличивается на 1, что говорит о необходимости перерисовать

    def initScreen(self):
        self.screen = pygame.display.set_mode(self.mode,RESIZABLE,32)
        self.screen.fill((255,255,255,0))
    ''
    def reDraw(self):
        self.tileangle = self.calcScreenTile()
        self.tileimage = self.loadScreenTile()
    ''
    def initTiles(self):
        xyt = []
        noimage = []
        for z in range(1,21):
            x,y = tilenames.tileXY(56.4,23.0, z)
            x1,y1 = tilenames.tileXY(51.14,33.0, z)
            xyt += [[x,y,x1,y1]]
        for i in range (1,21):
            map = ("./map/%d.png"%(i))
            noimage += [pygame.image.load(map).convert()]
        return [xyt,noimage]
    ''
    def run(self):
        fps = 0
        start = time.time()
        while self.runflag:
            self.readevent()
            self.draw()
            self.drawLayer1()
            if time.time() - start>1:
                self.fps = fps
                fps = 0
                start = time.time()
                self.drawinfo(1)
            else:
                fps += 1
                self.drawinfo(0)
            self.getLatLon(self.xk, self.yk)
            pygame.display.update()
        pygame.quit()
    ''
    def readevent(self):
        for event in pygame.event.get():
            #self.keyb(event)
            if event.type == QUIT: self.runflag = 0
            if event.type == VIDEORESIZE:
                self.mode = event.size
                self.initScreen()
                self.reDraw()
            if event.type == MOUSEMOTION:
                self.xk = event.pos[0]
                self.yk = event.pos[1]
                if self.moveflag: self.moveMap(event.pos)
            if event.type == MOUSEBUTTONUP:
                if event.button == 1:
                    if self.moveflag: self.endMoveMap(event.pos)
            if event.type == MOUSEBUTTONDOWN:
                if event.button == 1: self.beginMoveMap(event.pos)
                if event.button == 4:self.zoomUpMap(event.pos)
                if event.button == 5:self.zoomDownMap(event.pos)
    ''
    def draw(self):
        for image in self.tileimage:
            x = (image[0] - self.xyt[self.zoom][0])*256 + self.x
            y = (image[1] - self.xyt[self.zoom][1])*256 + self.y
            self.screen.blit(image[3],(x,y))
    ''
    def calcScreenTile(self):
        #получаем углы тайлов
        xt1 = self.xyt[self.zoom][0]
        yt1 = self.xyt[self.zoom][1]
        xt2 = self.xyt[self.zoom][2]
        yt2 = self.xyt[self.zoom][3]
        #получаем разницу между тайлами
        dxt = xt2 - xt1 + 1
        dyt = yt2 - yt1 + 1
        #если тайлов меньше чем размер экрана
        #то эти тайлы центрируем на экране
        if dxt*256<=self.mode[0]:
            self.x = (self.mode[0]-dxt*256)/2
        if dyt*256<=self.mode[1]:
            self.y = (self.mode[1]-dyt*256)/2
        #если же тайлов больше, чем размер экрана
        #а в предудыщем не хватало тайлов на экран, то отсчет карты в угол экрана
        if dxt*256>self.mode[0]:
            if self.x >0:
                self.x =0
        if dyt*256>self.mode[1]:
            if self.y >0:
                self.y =0
        #получаем номера тайлов для вывода на экран
        xt1a = xt1-(self.x/256)-1#-1 - чтобы при движении карты не было рывков
        yt1a = yt1-(self.y/256)-1
        xt2a = xt1a + self.mode[0]/256+2#+2 тайла за пределы экрна
        yt2a = yt1a + self.mode[1]/256+2
        #если разница в пикселях меньше размеров экрана,
        #то экранные тайлы равны всем тайлам
        if dxt*256<=self.mode[0]:
            xt1a = xt1
            xt2a = xt2
        if dyt*256<=self.mode[1]:
            yt1a = yt1
            yt2a = yt2
        #если номера экранных тайлов выходят за границы, то присвоить границы
        if xt1a < xt1: xt1a = xt1
        if yt1a < yt1: yt1a = yt1
        if xt2a > xt2: xt2a = xt2
        if yt2a > yt2: yt2a = yt2
        #получаем отсчет для экранных тайлов
        self.xs = (xt1a - xt1) * 256
        self.ys = (yt1a - yt1) * 256
        #возвращаем экранные тайлы
        return [xt1a,yt1a,xt2a,yt2a]
    ''
    def loadScreenTile(self):
        #создаем массив на количество тайлов на экране
        mas = []
        for i in range(self.tileangle[0],self.tileangle[2]+1):
            mas2 = []
            for j in range(self.tileangle[1],self.tileangle[3]+1):
                mas2 += [[i,j,0,0]]
            mas += [mas2]
        #проход по всем загруженным тайлам
        #если не вписываются - выбрасываем из массива
        for i in self.tileimage:
            x = i[0]- self.tileangle[0]
            y = i[1]- self.tileangle[1]
            if x>=0 and y>=0 and i[0]<=self.tileangle[2] and i[1]<=self.tileangle[3] and i[2]:
                #если есть ставим отметку в массив- такой тайл есть
                mas[x][y][2] = 1
                mas[x][y][3] = i[3]
        #проходим по массиву - дополняем недостающие массивы
        for i in mas:
            for j in i:
                if j[2] == 0:
                    try:
                        map = self.path+str(self.zoom+1)+'/'+str(j[0])+'/'+str(j[1])+'.png'
                        j[3] = pygame.image.load(map).convert()
                        j[2] = 1
                    except Exception, e:
                        #map = ("./map/%d.png"%(self.zoom+1))
                        j[3] = self.noimage[self.zoom]
                        j[2] = 0
                        print e
        tileimage = []
        for i in mas:
            for j in i:
                tileimage += [j]
        return tileimage
    ''
    def zoomUpMap(self,eventpos):#приближение
        #увеличиваем зум
        if self.zoom < 17:
            self.zoom += 1
            if self.xyt[self.zoom-1][0] != self.xyt[self.zoom][0]*2:
                self.x += 128
            if self.xyt[self.zoom-1][1] != self.xyt[self.zoom][1]*2:
                self.y += 128
            x = - self.mode[0]/2 #eventpos[0] - self.mode[0]/2
            y = - self.mode[1]/2#eventpos[1] - self.mode[1]/2
            self.x = (self.x)*2 + x
            self.y = (self.y)*2 + y
            if self.getChange()>0:
                self.createpng()
            self.reDraw()
    ''
    def zoomDownMap(self,eventpos):#приближение
        #увеличиваем зум
        if self.zoom >0:
            self.zoom -= 1
            if self.xyt[self.zoom+1][0] != self.xyt[self.zoom][0]*2:
                self.x -= 128
            if self.xyt[self.zoom+1][1] != self.xyt[self.zoom][1]*2:
                self.y -= 128

            x = - self.mode[0]/2 #eventpos[0] - self.mode[0]/2
            y = - self.mode[1]/2#eventpos[1] - self.mode[1]/2
            self.x = (self.x - x)/2
            self.y = (self.y - y)/2
            if self.getChange()>0:
                self.createpng()
            self.reDraw()
            if self.x>0 or self.y>0:
                self.screen.fill((255,255,255,0))

    ''
    def beginMoveMap(self,eventpos):
        self.dx = eventpos[0]#смещение от угла экрана
        self.dy = eventpos[1]# 
        self.x1 = self.x #старые координаты
        self.y1 = self.y
        self.moveflag = 1
    ''
    def moveMap(self,eventpos):
        #если угол карты остается меньше угла экрана, то двигаем карту
        xx = self.x1 - self.dx
        yy = self.y1 - self.dy
        if eventpos[0] + xx<0:
            self.x = eventpos[0] + xx
        if eventpos[1] + yy <0:
            self.y = eventpos[1] + yy
#        self.screen.fill((255,255,255,0))
        #если сдвиг больше тайла, то подкгружаем граничные тайлы
        if (self.x + xx<-256 or
            self.x + xx> 0 or
            self.y + yy<-256 or
            self.y + yy>0):

            self.x = eventpos[0] + xx
            self.y = eventpos[1] + yy
            self.dx = eventpos[0]#
            self.dy = eventpos[1]#
            self.x1 = self.x
            self.y1 = self.y
            self.reDraw()
    ''
    def endMoveMap(self,eventpos):
        if eventpos[0] - self.dx + self.x1<0:
            self.x = eventpos[0] - self.dx + self.x1
        if eventpos[1] - self.dy + self.y1<0:
            self.y = eventpos[1] - self.dy + self.y1
        self.moveflag = 0
    ''
    def initFont(self):
        '''
        подгружаем шрифты для отображения информации на карте - фпс
        '''
        verdana = pygame.font.match_font('Verdana')
        verdanaFont = pygame.font.Font(verdana, 13)
        self.font = pygame.font.Font(None, 36)
        self.font2 = pygame.font.Font(None, 18)
        self.textRect = (0,0,0,0)
        self.textRect2 = (0,0,0,0)
        self.textRect3 = (0,0,0,0)

        self.text = self.font.render('%d'%self.fps, True, (255,255, 255), (159, 182, 205))
        self.textRect = self.text.get_rect()
    ''

    def drawinfo(self,d):
        if d:
            pygame.draw.rect(self.screen,(255,255, 255),(0,0,self.textRect[2],self.textRect[3]))
            self.text = self.font.render('%d'%self.fps, True, (255,255, 255), (159, 182, 205))
            self.textRect = self.text.get_rect()
        self.screen.blit(self.text, (0,0))

    def drawLayer1(self):
        #переопределяемая функция, если она переопределена, то значения будут больше 0
        gc = self.getChange()
        if gc>0:
            #если флаг изменения меньше значения из функции - необходима перерисовка
            if self.drawlayerid < gc:
                mm = time.time()
                self.drawlayerid = gc
                self.vector = []
                self.vector = self.setVector()
                #если функция не переопределена, то длина возвращаемых значений будет 0
                if len(self.vector) > 0:
                    self.createpng()
                print "self.drawlayerid < gc: %f сек"%(time.time()-mm)
            mm = time.time()
            x = self.picsx + self.x
            y = self.picsy + self.y
            x1 = 0
            y1 = 0
            if len(self.pathimage)>0:
                for i in self.pathimage:
                    x1 = 0
                    for j in i:
                        if (x+x1>-256 and x+x1-512 < self.mode[0] and
                            y+y1>-256 and y+y1-512 < self.mode[1]):
                            #кодируем здесь на лету, т.к. видимо память pygame небольшая и нельзя загрузить много тайлов
                            jj = pygame.image.fromstring(j.tostring(),(256,256),"RGBA")
                            self.screen.blit(jj,(x+x1,y+y1))
                        x1 += 256
                    y1 += 256
            print "Draw layer %f сек"%(time.time()-mm)

    def createpng(self):#создаем изображение маршрутов, нарезанное на тайлы
        mm = time.time()
        line = self.vector[0]#какая точка выделена
        vec = []#массив всех точек в lat/lon
        a = self.drawpil(self.vector[1])
        self.picsx = a[0] - self.x
        self.picsy = a[1] - self.y
        tiles = self.piltotile(a[3])
        self.pathimage = []
        for x in tiles:
            img = []
            for y in x:
                img += [y]
            self.pathimage += [img]
        print self.pathimage
        print "Create png %f сек"%(time.time()-mm)

    def piltotile(self,image): #если изображение большое, то нарезаем на тайлы
        #основа взята отсюда http://blog.odonnell.nu/posts/creating-tiles-image-python-and-pil/
        tiles = []
        tile_width = 256
        tile_height = 256
        print image.size[0]+image.size[1]
        if (image.size[0]+image.size[1]>0):
            if (image.size[0] % tile_width != 0 or image.size[1] % tile_height !=0):
                currentx = 0
                currenty = 0
                while currenty < image.size[1]:
                    tile = []
                    while currentx < image.size[0]:
                        tile += [image.crop((currentx,currenty,currentx + tile_width,currenty + tile_height))]
                        #tile.save("x" + str(currentx) + "y" + str(currenty) + "z" + zoom_level + ".png","PNG")
                        currentx += tile_width
                    currenty += tile_height
                    currentx = 0
                    tiles += [tile]
            else:
                tiles = [[image]]
        return tiles

    def drawpil(self,vec):#создаем изображение с треками движения
        xmin = 0
        ymin = 0
        xmax = 0
        ymax = 0
        fl = 1
        pics = []
        sizes = [0,0]
        im = Image.new('RGBA',sizes)
        for i in vec:
            #переводим из координат lat/lon в пиксели карты
            a = self.getXY(i['x'],i['y'])#зум берем из класса
            x = a[0] 
            y = a[1]
            #если треки куда-то уползли - блокируем их
            if x>-5000 and x<5000 and y>-5000 and y<5000:
                pics += [[x,y]]

                #находим углы изображения
                if fl:#флаг для установки начальных углов
                    fl = 0
                    xmin = x
                    xmax = x
                    ymin = y
                    ymax = y
                if xmin>x:
                    xmin = x
                if ymin>y:
                    ymin = y
                if xmax<x:
                    xmax = x
                if ymax<y:
                    ymax = y
        sizes = (xmax-xmin,ymax-ymin)
        if sizes[0]<30000 and sizes[1]<30000:
            #sizes = self.mode
            #получив все точки и углы рисуем на pil
            im = Image.new('RGBA',sizes)
            draw = ImageDraw.Draw(im)
            fl = 1
            for i in pics:
                if fl:#проверка 1-го вхождения, не рисуем первую линию. т.к. 1 точка только
                    fl =0
                    x1 = i[0] - xmin
                    y1 = i[1] - ymin
                else:
                    draw.line((x1,y1,i[0] - xmin,i[1] - ymin),fill="red")
                    x1 = i[0] - xmin
                    y1 = i[1] - ymin
        else:
            try:
                raise
            except:
                print "Очень большой размер png карты", sizes
        return(xmin,ymin,sizes,im)
        #im.save('vd.png','PNG')
    ''
    def setVector(self):
        a = []
        return a

    def getChange(self):
        return 0

    def getXY(self,lat,lon):
        #получаем тайл
        x,y = tilenames.tileXY(lat,lon, self.zoom+1)
        #получаем его координаты
        zz = tilenames.tileEdges(x,y,self.zoom+1)
        #получаем расстояние до тайла в тайлах
        x1 = (x - self.xyt[self.zoom][0])
        y1 = (y - self.xyt[self.zoom][1])
        #получаем расстояние в тайле
        ys = 256 - int((lat - zz[0])/(zz[2]-zz[0])*256)
        xs = int((lon - zz[1])/(zz[3]-zz[1])*256)

        xx = x1*256 + self.x + xs
        yy = y1*256 + self.y + ys

        s = "[%d, %d, %f, %f] [%f, %f, %f, %f]"%(x,y,lat,lon,zz[0],zz[1],zz[2],zz[3])
        s += "[%d, %d], [%d, %d, %d, %d]"%(xs,ys,xx,yy,x1,y1)
        pygame.draw.rect(self.screen,(255, 255, 255),(20,40,self.textRect3[2],self.textRect3[3]))
        text3 = self.font2.render('%s'%s, True, (255, 255, 255), (159, 182, 205))
        self.textRect3 = text3.get_rect()
        self.screen.blit(text3, (20,40))
        return xx,yy,xs,ys

    def getLatLon(self,x,y):
        #общее смещение
        mx = self.x - x
        my = self.y - y
        #получаем координаты где находится мышь
        zx = -mx/256
        zy = -my/256
        #расчитываем над каким тайлом находится мышь
        ztx = self.xyt[self.zoom][0]+zx
        zty = self.xyt[self.zoom][1]+zy
        #получаем кооридинаты в мировых координатах
        zz = tilenames.tileEdges(ztx,zty,self.zoom+1)
        #получаем смещение относительно тайла
        sx = -(mx + zx*256)
        sy = 256 + my + zy*256
        #получаем разницу в каждом пикселе между координатами
        ssx = (zz[3] - zz[1])/256.0
        ssy = (zz[2] - zz[0])/256.0
        #получаем координату под мышью
        xxx = sx*ssx+zz[1]
        yyy = sy*ssy+zz[0]

        s = "[%d %d] [%f %f %f %f] [%f,%f] [%f,%f]"%(mx,my,zz[0],zz[1],zz[2],zz[3],ssx,ssy,yyy,xxx)
        pygame.draw.rect(self.screen,(255,
                255, 255),(0,20,self.textRect2[2],self.textRect2[3]))

        text2 = self.font2.render('%s'%s, True, (255,
                255, 255), (159, 182, 205))
        self.textRect2 = text2.get_rect()
        self.screen.blit(text2, (0,20))




    ''

