#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from spr.control import control, editWind

class modEditWind(editWind):
    def __init__(self, parent = None):
        editWind.__init__(self, parent)
        lbl1 = QtGui.QLabel(u'Название')
        self.ed1 = QtGui.QLineEdit()
        self.grid.addWidget(lbl1,0,0)
        self.grid.addWidget(self.ed1,0,1)
        self.grid.addWidget(self.btn1,1,0,1,2)

class marsh(control):
    def __init__(self, parent = None, mysql = None):
        self.initSql()
        control.__init__(self, parent, mysql)
        self.edit = modEditWind(self)
        self.connect(self.edit, QtCore.SIGNAL("accepted()"),self.save)
        self.dataview = ["name"]

    def initSql(self):
        self.dataparam = ["id", "name"]
        self.getDataSQL = '''SELECT id, name FROM marshrut WHERE del = 0 ORDER BY name'''
        self.addDataSQL = '''INSERT INTO marshrut (name) VALUES ('%s')'''
        self.editDataSQL = '''UPDATE marshrut SET name = '%s' WHERE id = %d'''
        self.delDataSQL ='''UPDATE marshrut SET del=1 WHERE id = %d'''

    def addDataControl(self):
        self.edit.ed1.setText("")
        control.addDataControl(self)

    def editDataControl(self):
        r = self.list.currentRow()
        self.edit.ed1.setText(self.data[self.datarow[r]][self.dataview[0]])
        control.editDataControl(self)

    def save(self):
        self.datasave = [self.edit.ed1.text()]
        control.save(self)
        self.redraw([])
