#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from db import db

class marshSpis(QtGui.QTableWidget):
    '''класс списка маршрутов
    '''
    def __init__(self, parent=None):
        QtGui.QTableWidget.__init__(self, parent)
        self.setRowCount(1)
        self.setColumnCount(1)
        self.mydb = db()
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        self.reread()
        self.initAction()

    def initAction(self):
        self.viewSpisAct = QtGui.QAction(u'Список маршрутов',self)
        self.viewSpisAct.setShortcut('Alt+O')
        self.viewSpisAct.setIcon(QtGui.QIcon('../ico/database.png'))
        self.connect(self.viewSpisAct,QtCore.SIGNAL("triggered()"),self.reread)

    def reread(self):
        self.mydb.sql = 'SELECT id,name,del FROM marsh ORDER BY name'
        self.mydb.execsql()
        res = self.mydb.cursor.fetchall()
        #n = self.mydb.cursor.rowcount
        self.setRowCount(0)
        i = 0
        self.id = []
        self.name = []
        for row in res:
            if row["del"]!=1:
                self.setRowCount(i+1)
                self.id += [row["id"]]
                self.name += [row["name"]]
                item = QtGui.QTableWidgetItem(row["name"])
                self.setItem(i,0,item)
                i += 1
        self.resizeColumnsToContents()

class marshEditForm(QtGui.QDialog):
    '''Диалоговая форма для добавление и изменения
    '''
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        buttonBox = QtGui.QDialogButtonBox()
        buttonBox.setOrientation(QtCore.Qt.Horizontal)
        buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Save)
        QtCore.QObject.connect(buttonBox, QtCore.SIGNAL("accepted()"), self.accept)
        QtCore.QObject.connect(buttonBox, QtCore.SIGNAL("rejected()"), self.reject)
        label = QtGui.QLabel()
        label.setText(u'Название маршрута')
        self.lineEdit = QtGui.QLineEdit()

        hl = QtGui.QHBoxLayout()

        hl.addWidget(label)
        hl.addWidget(self.lineEdit)

        vl = QtGui.QVBoxLayout()
        vl.addLayout(hl)
        vl.addWidget(buttonBox)
        self.setLayout( vl)

class marshEdit(marshSpis):
    '''расширяющий класс для редактирования записей с остановками
    '''
    def __init__(self):
        super(marshEdit,self).__init__()
        self.diag = marshEditForm(self)
        self.acept = self.__add
        self.connect(self.diag,QtCore.SIGNAL('accepted()'),self.acept)

    def __add(self):
        self.mydb.sql = '''INSERT INTO marsh (name)
            VALUES ('%s')'''%(self.diag.lineEdit.text())
        self.mydb.execsql()
        self.reread()
    def add(self):
        self.diag.setWindowTitle(u'Добавить маршрут')
        self.acept = self.__add
        self.diag.show()
    def __edit(self):
        self.mydb.sql = '''UPDATE marsh SET name='%s'
            WHERE id = %d'''% (self.diag.lineEdit.text(), self.id[self.currentRow()])
        self.mydb.execsql()
        self.reread()
    def edit1(self):
        self.diag.setWindowTitle(u'Изменить маршрут')
        self.diag.lineEdit.setText(self.name[self.currentRow()])
        self.acept = self.__edit
        self.diag.show()
    def delete(self):
        dlg = QtGui.QMessageBox(self)
        dlg.setWindowTitle(u'Удаление записи')
        dlg.setText(u'''Вы действительно хотите удалить маршрут '%s?' '''% self.name[self.currentRow()])
        #dlg.setInformativeText(u'Вы действительно хотите удалить запись?')
        dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
        ok = dlg.exec_()
        if ok==QtGui.QMessageBox.Ok :
            #self.mydb.sql = '''DELETE FROM marsh WHERE id = %d'''%(self.id[self.currentRow()])
            self.mydb.sql = '''UPDATE marsh SET del=1
                WHERE id = %d'''% (self.id[self.currentRow()])
            self.mydb.execsql()
            self.reread()
    def initAction(self):
        super(marshEdit,self).initAction()
        self.addSpisAct = QtGui.QAction(u'Добавить маршрут',self)
        self.addSpisAct.setShortcut('Ins')
        self.addSpisAct.setIcon(QtGui.QIcon('../ico/dbplus2.png'))

        self.connect(self.addSpisAct,QtCore.SIGNAL("triggered()"),self.add)

        self.editSpisAct = QtGui.QAction(u'Редактировать маршрут',self)
        self.editSpisAct.setShortcut('Home')
        self.editSpisAct.setIcon(QtGui.QIcon('../ico/db_update.png'))
        self.connect(self.editSpisAct,QtCore.SIGNAL("triggered()"),self.edit1)

        self.delSpisAct = QtGui.QAction(u'Удалить маршрут',self)
        self.delSpisAct.setShortcut('Del')
        self.delSpisAct.setIcon(QtGui.QIcon('../ico/dbmin2.png'))
        self.connect(self.delSpisAct,QtCore.SIGNAL("triggered()"),self.delete)

class marshMain(QtGui.QDialog):
    '''класс формы маршрутов, содержит список,
    для дальнейшего их редактирования или использования при выборе
    к примеру очередности
    '''
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        mainwidget = QtGui.QWidget()
        self.setWindowTitle(u'маршруты')
        self.setMinimumSize(600,400)
        self.spis = marshEdit() #Spis()
        self.tool = QtGui.QToolBar(self)
        self.tool.setIconSize(QtCore.QSize(64,64))
        self.tool.addAction(self.spis.viewSpisAct)
        self.tool.addAction(self.spis.addSpisAct)
        self.tool.addAction(self.spis.editSpisAct)
        self.tool.addAction(self.spis.delSpisAct)
        #self.spis.add()
        vb = QtGui.QVBoxLayout(self)
        vb.addWidget(self.tool)
        vb.addWidget(self.spis)


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    qb = marshMain()
    qb.show()
    sys.exit(app.exec_())

