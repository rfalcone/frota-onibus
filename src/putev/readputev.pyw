#!/usr/bin/python
# -*- coding: utf-8  -*-
import sys
from PyQt4 import QtGui, QtCore
from dbfpy import dbf
from db import db
import time
import datetime



class timer(QtCore.QTimer):
    def __init__(self, parent=None):
        QtCore.QTimer.__init__(self, parent)

class dial(QtGui.QDialog):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        grid = QtGui.QGridLayout(self)
        self.lbl = QtGui.QLabel(u'<h1>Подключаем базу данных</h1>')
        grid.addWidget(self.lbl)
        #self.setMinimumSize(200,100)


class main(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        mainwidget = QtGui.QWidget()
        self.mydb = db()
        self.setWindowTitle(u'Работа с базой данных')
        self.timer1 = QtCore.QTimer(self)
        self.connect(self.timer1, QtCore.SIGNAL("timeout()"), self.hideWind)

        self.timer = QtCore.QTimer(self)
        self.connect(self.timer, QtCore.SIGNAL("timeout()"), self.view)


        self.trayIcon = QtGui.QSystemTrayIcon(self)
        self.trayIcon.setIcon(QtGui.QIcon("q.png"))
        self.trayIconMenu = QtGui.QMenu(self)
        self.qActionMin = QtGui.QAction(u'Minimize',self)
        self.qActionRest = QtGui.QAction(u'Restore',self)
        self.trayIconMenu.addAction(self.qActionMin)
        self.trayIconMenu.addAction(self.qActionRest)
        self.trayIcon.setContextMenu(self.trayIconMenu)

        self.connect(self.qActionMin, QtCore.SIGNAL("triggered()"), self.hideWind)
        self.connect(self.qActionRest, QtCore.SIGNAL("triggered()"), self.showWind)

        self.d = dial()
        self.n = 0
        self.j = 0
        self.row = 1000
        self.trayIcon.show()
        self.timer1.start(100)
        self.timer.start(1000)
        #self.setVisible(0)
        self.init()

    def hideWind(self):
        self.setVisible(0)
        self.timer1.stop()

    def showWind(self):
        self.setVisible(1)


    def central(self):
        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2,
            (screen.height()-size.height())/2)

    def init(self):
        mw = QtGui.QWidget()
        lay = QtGui.QHBoxLayout(mw)
        spl = QtGui.QSplitter(1)
        self.table1 = QtGui.QListWidget()
        self.readeddb = 0 #флаг, что база была считана
        spl.addWidget(self.table1)
        lay.addWidget(spl)
        self.setCentralWidget(mw)
        self.setMinimumSize(600,400)
        self.central()

    def readfrombase(self):
        if self.mydb.connected:
            try:
                self.mydb.sql = '''SELECT id, viezd, vozvr, nomputlista,
                        garnom, peresmbegin, peresmend, tabelvod1,
                        tabelvod2, marsh1, marsh2, commentmarsh
                    FROM putev '''
                self.mydb.execsql()
                result_set = self.mydb.cursor.fetchall()
                self.datafromsql = {}
                for row in result_set:
                    self.datafromsql [row['nomputlista']] = [row["id"],row["garnom"],
                        row["viezd"], row["vozvr"],row["peresmbegin"], row["peresmend"],
                        row["tabelvod1"], row["tabelvod2"], row["marsh1"], row["marsh2"],
                        row["commentmarsh"]]
                self.readeddb = 1
            except:
                self.table1.addItem("Error from read mysqldb")
                print "Error from read mysqldb"
        else:
            self.table1.addItem('Not connected to mysql, try connect')
            print 'Not connected to mysql, try connect'
            self.mydb.connect()

        
    def view(self):
        if self.n ==1:
            self.table1.addItem('Connected to mysql')
            print 'Connected to mysql'
        if self.n ==2:
            if self.readeddb == 0:
                self.table1.addItem('Read data from mysql')
                print 'Read data from mysql'
                self.readfrombase()
        if self.n ==3:
            if self.readeddb == 1:
                self.table1.addItem('Compare dbf with mysql')
                print 'Compare dbf with mysql'
                self.show2()
            self.timer.start(60000)
            self.n = 1
        self.n += 1
        if self.j == 10:
            self.mydb.disconnect()
            self.row = 1000
        self.j += 1

    def show2(self):
        #считываем данные из дбф файла
        try:
            dbp = dbf.Dbf("E:\PUTEV\DBF\PUTEV.DBF")
            row = self.row        #сколько строк с конца будем контролировать
            rc = dbp.recordCount
            begin = rc - row
            end = rc
            k = 0
            putevdata = {}
            try:
                for i in range(begin,end):
                    #кодируем данные из строки
                    #получаем номер путевки
                    z = self.encodeputev(dbp[i])
                    #print z
                    try:
                        k = int(dbp[i]['NOMPL'])
                    except Exception,e:
                        s = "Error in conversion int(",dbp[i]['NOMPL'],")"
                        self.table1.addItem(s)
                        print s
                        print e
                    
                    #print k
                    putevdata[k] = z 
            except Exception, e:
                s = "Error in reading data "
                self.table1.addItem(s)
                print s, e
                raise
            #начинаем сравнивать с данными из базы
            for data in putevdata:
                try:
                    #при возникновении ошибки - обрабатываем исключение
                    #ошибка keyerror возникнет, только если в базе нет такой путевки
                    #следовательно добавляем данные
                    #если данные не совпадают в полях, то необходимо обновить данные
                    for i in range(0,10):
                        if self.datafromsql[data][i+1] != putevdata[data][i]:
                            self.table1.addItem("update mysql data PL=%d"%data)
                            print "update mysql data PL=%d"%data
                            p = putevdata[data]
                            self.mydb.sql = '''UPDATE putev SET garnom = %d, viezd ='%s',
                                vozvr = '%s', peresmbegin = '%s', peresmend= '%s',
                                tabelvod1 = %d, tabelvod2 = %d, marsh1 =%d, marsh2 = %d,
                                commentmarsh = '%s'
                                WHERE id = %d'''%(p[0],p[1],
                                p[2],p[3],p[4],p[5],
                                p[6], p[7], p[8], p[9],self.datafromsql[data][0])
                            self.mydb.execsql()
                            if self.mydb.connected:
                                for j in range(0,10):
                                    self.datafromsql[data][j+1] = putevdata[data][j]
                                #print self.mydb.sql.encode('utf-8')
                            else:
                                self.table1.addItem("Connected to mysql lost, try again")
                                print "Connected to mysql lost, try again"
                                self.mydb.connect()
                            break
                except KeyError:
                    self.table1.addItem("New data PL=%d"%data)
                    print "New data PL=%d"%data
                    p = putevdata[data]
                    self.mydb.sql = '''INSERT INTO putev (nomputlista, garnom, viezd,
                        vozvr, peresmbegin, peresmend, tabelvod1,
                        tabelvod2, marsh1, marsh2, commentmarsh, del)
                        VALUE (%d, %d, '%s',
                        '%s', '%s','%s',%d,
                        %d, %d, %d, '%s',2) '''%(data,p[0],p[1],
                        p[2],p[3],p[4],p[5],
                        p[6], p[7], p[8], p[9])
                    self.mydb.execsql()
                    self.mydb.sql = '''SELECT id FROM putev WHERE del=2'''
                    self.mydb.execsql()
                    if self.mydb.connected:
                        result_set = self.mydb.cursor.fetchall()
                        for row in result_set:
                            self.datafromsql[data] = [row['id']]
                        for j in range(0,10):
                            self.datafromsql[data] += [p[j]]
                        self.mydb.sql = '''UPDATE putev SET del = 0 WHERE del=2'''
                        self.mydb.execsql()
                    else:
                        self.table1.addItem("Connected to mysql lost, try again")
                        print "Connected to mysql lost, try again"
                        self.mydb.connect()
            #проверяем данные, где произошло несовпадения номера путевого из базы
            #и из дбф файла, такие ид удаляем
            for data in self.datafromsql.keys():
                try:
                    putevdata[data]
                except KeyError:
                    self.table1.addItem("Delete data id= %d"%self.datafromsql[data][0])
                    print "Delete data id= %d"%self.datafromsql[data][0]
                    self.mydb.sql = '''DELETE FROM putev WHERE id =%d '''%self.datafromsql[data][0]
                    self.mydb.execsql()
                    if self.mydb.connected:
                        del self.datafromsql[data]
                    else:
                        self.table1.addItem("Connected to mysql lost, try again")
                        print "Connected to mysql lost, try again"
                        self.mydb.connect()
            dbp.close()
        except Exception, e:
            self.table1.addItem("Error read dbf file")
            print "Error read dbf file",e
        
        self.table1.addItem(str(datetime.datetime.now()))
        self.table1.addItem(' ')
        self.table1.addItem('-------------------------------------------')
        print datetime.datetime.now()
        print
        print '-------------------------------------------'

    def encodeputev(self,dbp):
        #[гаражный номер, дата выезда, дата возвращения, пересменка1, пересменка2,
        #табельный1, табельный2, маршрут1, маршрут2, строковое описание маршрута]
        data = []
        #получаем гаражный номер
        try:
            garnm = int(str(dbp['GARNM'])[1:4])
        except Exception,e:
            print "Garnom '%s'"% str(dbp['GARNM'])
            print "Error in garnom",e
            garnm = 0
        data += [garnm]
        #получаем дату-время выезда
        year = int(str(dbp['TDATA'])[0:4])
        month = int(str(dbp['TDATA'])[4:6])
        try:
            month = int(str(dbp['DVYEZ'])[0:2])
        except:
            s = 'Err in month [%s]'%(str(dbp['DVYEZ'])[0:2])
            print s
            self.table1.addItem(s)
            month = month
        try:
            day = int(str(dbp['DVYEZ'])[2:4])
        except:
            s = 'Err in day [%s]'%(str(dbp['DVYEZ'])[2:4])
            print s
            self.table1.addItem(s)
            day = 1
        try:
            t = dbp['VREMD']
            hour = int(t)
            min = int((t-hour)*100)
        except:
            s = 'Err in hour or minute [%s %s]'%(dbp['VREMD'])
            print s
            self.table1.addItem(s)
            hour = 0
            min = 0
        dataviezd = datetime.datetime(year,month,day,hour,min)
        data += [dataviezd]
        #получаем время прибытия
        try:
            month = int(str(dbp['DVOZV'])[0:2])
        except:
            month = month
        try:
            day = int(str(dbp['DVOZV'])[2:4])
            if day == 0:
                day = 1
        except:
            day = 1
        try:
            t = dbp['VREMP']
            hour = int(t)
            min = int((t-hour)*100)
            if hour>23:
                hour -= 24
        except:
            hour = 0
            min = 0
        try:
            datavozv = datetime.datetime(year,month,day,hour,min)
        except:
            s = 'Err in data [%04d-%02d-%02d %02d:%02d]'%(year,month,day,hour,min)
            print s
            self.table1.addItem(s)
            
        data += [datavozv]
        #забираем данные из пересменки
        try:
            t = dbp['PRSM1']
            hour = int(t)
            min = int((t-hour)*100)
            if hour>23:
                hour -= 24
        except:
            hour = 0
            min = 0
        peresm1 = datetime.timedelta(0,hour*60*60+min*60)
        data += [peresm1]
        try:
            t = dbp['PRSM2']
            hour = int(t)
            min = int((t-hour)*100)
            if hour>23:
                hour -= 24
        except:
            hour = 0
            min = 0
        peresm2 = datetime.timedelta(0,hour*60*60+min*60)
        data += [peresm2]
        #данные о табельных номерах водителей
        tab = [0,0,0,0]
        i = 0
        if int(dbp['TABV1']) != 0:
            tab[i] = int(dbp['TABV1'])
            i += 1
        if int(dbp['TABV2']) != 0:
            tab[i] = int(dbp['TABV2'])
            i += 1
        if int(dbp['TABV3']) != 0:
            tab[i] = int(dbp['TABV3'])
            i += 1
        if int(dbp['TABV4']) != 0:
            tab[i] = int(dbp['TABV4'])
            i += 1
        data += [tab[0],tab[1]]
        #разгребаем данные о маршруте
        mar = [0,0,0,0]
        mar[0] = str(dbp['MARH1']).decode('cp866')
        mar[1] = str(dbp['MARH2']).decode('cp866')
        mar[2] = str(dbp['MARH3']).decode('cp866')
        mar[3] = str(dbp['MARH4']).decode('cp866')
        st = ''
        i = 0
        marsh = [0,0,0,0]
        for m in mar:
            ch = m.strip()
            try:
                #пытаемся получить все цифры
                marsh[i] = int(ch)
                i += 1
            except:
                #все строки объединяем
                if st!= '':
                    st += ', '+ch
                else:
                    st = ch
        data += [marsh[0],marsh[1],st]
        return data

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    qb = main()
    qb.show()
    sys.exit(app.exec_())

