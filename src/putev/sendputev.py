#!/usr/bin/python
# -*- coding: utf-8  -*-
import sys
from PyQt4 import QtCore
from dbfpy import dbf
from db import db as mydb
import time
import datetime

#SELECT * FROM putev WHERE viezd > '2011-05-13' ORDER BY garnom, viezd ASC

class sendPutevFromMysqlToMysql():
    def __init__(self, parent=None):
        print "Init programm"
        self.timer = QtCore.QTimer()
        QtCore.QObject.connect(self.timer, QtCore.SIGNAL("timeout()"), self.view)
        self.i = 0
        self.mydb1 = mydb(host = '10.1.12.52', user = 'asdm', database = 'asdm',
            passwd = 'qyqW5UMc4pKBxvPn')
        self.mydb2 = mydb(host = 'bysel.by', user = 'asdm', database = 'asdm',
            passwd = 'F8rbCsWB87Dn7nnM')
        self.readeddb1 = 0
        self.readeddb2 = 0
        self.datafromsql1 = {}
        self.datafromsql2 = {}

    def view(self):
        #если еще не подлючены
        print datetime.datetime.now()
        if not self.mydb1.connected:
            print "reconnect server 1"
            #подключаемся к серверу 1
            self.mydb1.connect()
        
        print "read data from server 1"
        k = self.readdata(self.mydb1)
        self.readeddb1 = k[0]
        self.datafromsql1 = k[1]

        if not self.mydb2.connected:
            #подключаемся к серверу 1
            print "reconnect server 2"
            self.mydb2.connect()
        if not self.readeddb2:
            print "read data from server 2"
            k = self.readdata(self.mydb2)
            self.readeddb2 = k[0]
            self.datafromsql2 = k[1]
        print "compare data"
        self.comparedata()
        self.i += 1
        print self.i
        self.timer.start(60000)
        print datetime.datetime.now()
        print "-----------------------------------------"

    def show(self):
        print "Begin programm"
        self.timer.start()

    def readdata(self,linkdb):
        readeddb = 0
        datafromsql = {}
        #читаем данные из базы данных, возвращаем полученные данные
        if linkdb.connected:
            try:
                linkdb.sql = '''SELECT id, viezd, vozvr, nomputlista,
                        garnom, peresmbegin, peresmend, tabelvod1,
                        tabelvod2, marsh1, marsh2, commentmarsh
                    FROM putev '''
                linkdb.execsql()
                result_set = linkdb.cursor.fetchall()
                for row in result_set:
                    datafromsql [row['nomputlista']] = [row["id"],row["garnom"],
                        row["viezd"], row["vozvr"],row["peresmbegin"], row["peresmend"],
                        row["tabelvod1"], row["tabelvod2"], row["marsh1"], row["marsh2"],
                        row["commentmarsh"]]
                readeddb = 1
            except:
                print "Error from read mysqldb"
        else:
            print 'Not connected to mysql, try connect'
        return([readeddb,datafromsql])

    def comparedata(self):
        if self.readeddb1 and self.readeddb2:
            for datakey in self.datafromsql1:
                try:
                    for j in range(1,11):
                        if self.datafromsql1[datakey][j] != self.datafromsql2[datakey][j]:
                            print "update mysql data PL=%d"%datakey
                            #несовпадение данных - необходимо обновление
                            d = self.datafromsql1[datakey]
                            self.mydb2.sql = '''UPDATE putev SET garnom = %d, viezd ='%s',
                                vozvr = '%s', peresmbegin = '%s', peresmend= '%s',
                                tabelvod1 = %d, tabelvod2 = %d, marsh1 =%d, marsh2 = %d,
                                commentmarsh = '%s'
                                WHERE id = %d'''%(d[1],d[2],
                                d[3],d[4],d[5],
                                d[6], d[7], d[8], d[9],
                                d[10],self.datafromsql2[datakey][0])
                            self.mydb2.execsql()
                            if self.mydb2.connected:
                                for l in range(1,11):
                                    self.datafromsql2[datakey][l] = d[l]
                            else:
                                print "Connected to mysql lost, try again"
                                self.mydb2.connect()
                            break
                except KeyError:#несовпадение индексов, необходимо добавление
                    print "New data PL=%d"%datakey
                    d = self.datafromsql1[datakey]
                    self.mydb2.sql = '''INSERT INTO putev (nomputlista, garnom, viezd,
                        vozvr, peresmbegin, peresmend, tabelvod1,
                        tabelvod2, marsh1, marsh2, commentmarsh, del)
                        VALUE (%d, %d, '%s',
                        '%s', '%s','%s',%d,
                        %d, %d, %d, '%s',2) '''%(datakey,d[1],d[2],
                        d[3], d[4], d[5], d[6],
                        d[7], d[8], d[9], d[10])
                    self.mydb2.execsql()
                    self.mydb2.sql = '''SELECT id FROM putev WHERE del=2'''
                    self.mydb2.execsql()
                    if self.mydb2.connected:
                        result_set = self.mydb2.cursor.fetchall()
                        for row in result_set:
                            self.datafromsql2[datakey] = [row['id']]
                        for j in range(1,11):
                            self.datafromsql2[datakey] += [d[j]]
                        self.mydb2.sql = '''UPDATE putev SET del = 0 WHERE del=2'''
                        self.mydb2.execsql()
                    else:
                        print "Connected to mysql lost, try again"
                        self.mydb2.connect()
            for datakey in self.datafromsql2.keys():
                try:
                    self.datafromsql1[datakey]
                except KeyError:
                    print "Delete data id= %d"%self.datafromsql2[datakey][0]
                    self.mydb2.sql = '''DELETE FROM putev WHERE id =%d '''%self.datafromsql2[datakey][0]
                    self.mydb2.execsql()
                    if self.mydb2.connected:
                        del self.datafromsql2[datakey]
                    else:
                        print "Connected to mysql lost, try again"
                        self.mydb2.connect()





if __name__ == "__main__":
    app = QtCore.QCoreApplication(sys.argv)
    qb = sendPutevFromMysqlToMysql()
    qb.show()
    sys.exit(app.exec_())
