#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from spr.view import viewtable
from datetime import timedelta, datetime, date

class errorspr(viewtable):
    def __init__(self, parent = None, mysql = None):
        self.initSql()
        viewtable.__init__(self, parent, mysql)
        self.dataview = ["lat", "lon", "datetimepribor",
            "datetimeprogramm", "sluj", "sluj2", "angle", "speed", "alt",
            "koderror", "mess"]
        self.headername = [u"lat", u"lon", u"Дата прибора",u"Дата программы",
            u"Служебная 1", u"Служебная 2", u"Угол", u"Скорость", u"Высота",
            u"Код ошибки", u"Пакет",]

    def initSql(self):
        self.dataparam = ["id", "lat", "lon", "nmbr", "datetimepribor",
            "datetimeprogramm", "sluj", "sluj2", "angle", "speed", "alt",
            "koderror", "mess"]
        self.getDataSQL = '''
            SELECT id, lat, lon, nmbr, datetimepribor, datetimeprogramm, sluj,
                sluj2, angle, speed, alt, koderror, mess
            FROM error
            WHERE nmbr = %d AND datetimeprogramm> '%s' AND datetimeprogramm < '%s'
            LIMIT 0,1000
            '''