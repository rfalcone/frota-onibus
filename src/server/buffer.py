#!/usr/bin/python
# -*- coding: utf-8  -*-
from db import db
from datetime import datetime
from decode import decode

class buffer:
    ''' Класс работы с буфером, хранение в памяти, распределение по базам
    данных, обработка поступающих запросов по онлайну, к примеру не нужно
    будет открывать базу данных, чтобы посмотреть какие автобусы в данный 
    момент в онлайне и где были зафиксированы последние координаты. Можно
    так же запланировать первичное сжатие данных, чтобы в базу не сыпались
    данные по одной и той же координате толпами.
    Для этого необходимо разбивать поступившую информацию по приборам, 
    автообусам и др. состояниям. 
    Буфер является единым для всех приборов. При запуске сервера можно
    предусмотреть начальную запись в ячейки последних поступивших данных
    '''
    def __init__(self):
        self.point = {}
        self.error = {}
        self.config = {}
        self.config["maxsql"] = 20 #количество строк, которые накапливаем для выполнения запроса
        self.buffredirect = {} #словарь для хранения буфера неотправленных редиректов
        self.decode = decode()
        self.crdb = {} #словарь созданных динамических баз данных дабы не делать лищний запрос на создание
        self.mysql = db()
        
    def close(self):
        self.saveallerror()
        self.saveallpoint()
        self.saveallredirect()
    
    def addredirect(self, buff, ip, port):
        '''Сохраняем в буфер недоставленные данные для последующей пересылки
        как только сервер станет доступным
        '''
        ipport = "%s:%d"%(ip,port)
        try:
            # buffredirect 3-е поле при добавлении в буфер по умолчанию 0 - это метка, 
            # что данные этой строки пытаются передать при помощи resender
            self.buffredirect[ipport] += [[buff, datetime.now(),0]]# пробуем добавть буфер
            #если наскладывали в буфер - запихиваем данные в базу
            print "len buffer",len(self.buffredirect[ipport])
            if len(self.buffredirect[ipport]) > self.config["maxsql"]:
                self.saveredirect(ipport)
        except:
            self.buffredirect[ipport] = [[buff, datetime.now(),0]]#если неудача - добавляем новое значение
            print "len buffer",len(self.buffredirect[ipport])
        #print self.buffredirect
        
    def addbuffer(self, buff):
        '''Поступаемый buff будет уже декодирован
        '''
        #делаем запрос по количеству клиентов которые будут запрашивать данные 
        sql = 'SELECT id, name FROM clientdb'
        self.mysql.getData(sql)
        #возвращаемый результат - список поделенных элементов
        for i in buff:
            #читаем буфер, 
            if (len(i) == 1) or (len(i)>1 and self.correcterror(i[1][0]) == 0):#если ошибок нет в пихаем в буфер - без ошибок или #если ошибки не критичны - в буфер без ошибок
                yearmonth = "%04d%02d"%(i[0]["datetime"].year,i[0]["datetime"].month)
                try:
                    self.point[yearmonth] += [i[0]]
                except KeyError:
                    self.point[yearmonth] = [i[0]]
                #если буфер какой-нибудь заполнился - сохраняем в таблице
                if len(self.point[yearmonth]) > self.config["maxsql"]:
                    self.savepoint(yearmonth)
            else:#иначе добавляем к ошибкам
                yearmonth = "%04d%02d"%(i[0]["dtprogramm"].year,i[0]["dtprogramm"].month)
                try:
                    self.error[yearmonth] += [i]
                except KeyError:
                    self.error[yearmonth] = [i]
                if len(self.error[yearmonth]) > self.config["maxsql"]:
                    self.saveerror(yearmonth)

    def correcterror(self, buff):
        r = 0
        for i in buff:#проверяем на критические ошибки
            if i[0] == 1: r = 1#ошибка по дате-времени
            if i[0] == 2: r = 1#ошибка по координатам
            #if i[0] == 3: r = 1#ошибка по скорости
            #if i[0] == 4: r = 1#ошибка по углу
            #if i[0] == 5: r = 1#ошибка по высоте
            if i[0] == 6: r = 1#ошибка по контрольной сумме
        return r

    def saveallredirect(self):
        print "save redirect all"
        for i in self.buffredirect:
            self.saveredirect(i)
    
    def saveredirect(self, ipport = ''):
        print "save redirect", ipport
        '''Сохраняем данные буфера редиректа в базу'''
        
        buffsize = 64 #количество байт в одной записи буфера
        
        tablep = 'buffredirectserver'
        tabled = 'buffredirectdata'
        
        #на случай существования данной записи-порта получаем его ид, иначе, создаем новый
        sql = '''SELECT id FROM %s WHERE ipport = '%s' '''%(tablep,ipport)
        self.mysql.getData(sql)
        #print self.mysql.db.data
        
        if len(self.mysql.datarow) > 0:
            idl = self.mysql.datarow[0]
        else:#иначе создаем запись, и получваем новый ид
            sql = '''INSERT INTO %s (ipport)
                 VALUES ('%s')'''%(tablep, ipport)
            self.mysql.execsql(sql)
            idl = self.mysql.getlastid()
        sql = '''INSERT INTO %s (idserver, buff, dt) 
                 VALUES '''%tabled
        fl = ''
        for i in self.buffredirect[ipport]:
            if i[2] == 0:#если данные не используются в resender - сохраняем
                fl2 = True
                buff = self.decode.ords(i[0])#превращаем в байт строку
                while fl2:
                    s = buff[:buffsize]
                    sql = "%s%s \n (%d, '%s', '%s')"%(sql, fl, idl, s, i[1])
                    fl = ','
                    if len(buff) <= buffsize:
                        fl2 = False
                    else:
                        buff = buff[buffsize:]
        if len(fl) >0:#если была хотя бы одна строка добавлена для сохранения - выполняем запрос
            self.mysql.execsql(sql)                    
            tmp = []
            for i in self.buffredirect[ipport]:#удаляем все позиции с 0
                if i[2] != 0:
                    tmp += [i]
            if len(tmp) > 0:
                self.buffredirect[ipport] = tmp
            else:
                self.buffredirect[ipport] = []

    def saveallpoint(self):
        for i in self.point:
            self.savepoint(i)
    
    def saveallerror(self):
        for i in self.error:
            self.saveerror(i)

    def _crpointdb(self, table):
        '''Проверка и создание если неообходимо таблицы для данных от оборудования'''
        try:
            if self.crdb[table] == True:
                pass
        except KeyError:
            sql = '''CREATE TABLE IF NOT EXISTS %s (
                    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                    lat DOUBLE NOT NULL ,
                    lon DOUBLE NOT NULL ,
                    nmbr MEDIUMINT NOT NULL ,
                    datetimepribor DATETIME NOT NULL ,
                    datetimeprogramm DATETIME NOT NULL ,
                    npoint SMALLINT NOT NULL
                    ) ENGINE = MYISAM; '''%table
            self.mysql.execsql(sql)
            self.crdb[table] = True
            
    def _crerrortable(self,table):
        '''Проверка и создание если неообходимо таблицы для ошибок от оборудования'''
        try:
            if self.crdb[table] == True:
                pass
        except KeyError:
            sql = '''CREATE TABLE IF NOT EXISTS %s (
                    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                    lat DOUBLE NOT NULL ,
                    lon DOUBLE NOT NULL ,
                    nmbr MEDIUMINT NOT NULL ,
                    datetimepribor DATETIME NOT NULL ,
                    datetimeprogramm DATETIME NOT NULL ,
                    sluj SMALLINT NOT NULL,
                    sluj2 SMALLINT NOT NULL,
                    koderror SMALLINT NOT NULL,
                    mess VARCHAR(100) NOT NULL
                    ) ENGINE = MYISAM; '''%table
            self.mysql.execsql(sql)
            self.crdb[table] = True
    
    def savepoint(self, yearmonth):
        if len(self.point)>0:
            table = "point_%s"%yearmonth
            self._crpointdb(table)   
            sql = '''INSERT INTO %s (lat, lon, nmbr, datetimepribor, 
                                     datetimeprogramm, npoint) 
                     VALUES '''%table
            fl = ''
            for i in self.point[yearmonth]:
                sql = "%s%s \n (%2.8f, %2.8f, %d, '%s', '%s', 0)"%(sql, fl,
                    i["lat"], i["lon"], i["nprib"], i["datetime"], i["dtprogramm"])
                fl = ','
            if len(fl)>0:
                self.point[yearmonth] = []
                self.mysql.execsql(sql)
            #print sql
    
    def saveerror(self,yearmonth):
        if len(self.error)>0:
            table = "error_%s"%yearmonth
            self._crerrortable(table)
            sql = '''INSERT INTO %s (lat, lon, nmbr, datetimepribor, 
                                     datetimeprogramm, sluj, sluj2, koderror, mess) 
                     VALUES '''%table
            fl = ''
            for error in self.error[yearmonth]:
                data = error[0]
                koderror = 0
                for j in error[1][0]:
                    koderror += 2**(j[0]-1)            
                sql = "%s%s \n (%2.8f, %2.8f, %d, '%s', '%s', %d, %d, %d, '%s')"%(sql, fl,
                    data["lat"], data["lon"], data["nprib"], data["datetime"], data["dtprogramm"], 
                    data["sluj"], data["sluj2"], koderror, error[1][1])
                fl = ','
            if len(fl)>0:
                self.error[yearmonth] = []
                self.mysql.execsql(sql)
            #print (sql,)