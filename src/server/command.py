#!/usr/bin/python
# -*- coding: utf-8  -*-
import socket, asyncore
from resender import resender
import time

class threadCommand(asyncore.dispatcher):
    '''Класс потока для слушающего сервера 
    
    :param socket conn: октрытый сокет из слушаещего сервера
    :param int id: id потока
    :param function resend: ссылка на функцию для возврата пакета слушающему серверу
    :param function delclient: ссылка на функцию для сообщении слушающему серверу о закрытии потока
    :param string redirect: список ссылок на буфер отправки пакетов
    '''
    def __init__(self, conn, id, resend, delclient):
        asyncore.dispatcher.__init__(self, conn)
        self.buffer_read = ''
        self.id = id
        self.resend = resend
        self.delclient = delclient
        self.buffer_write = ''
        #список команд и методов обработки обработчиков 

    def handle_read(self):
        '''Метод для чтения пакетов '''
        read = self.recv(1024)
        if len(read) > 0:
            print '%04i (command)-->'%len(read)
            if read[:4] == 'stop': 
                self.resend('stop')
            if read[:7] == 'getconn': 
                self.resend('getconn', self.id)
#            #проверяем сообщение на ключевые слова
#            for i in self.comms.comms:
#                if read[:len(i[0])] == i[0]:
#                    i[1]()#если ключевое слово совпало - выполняем метод
            #забираем все что накопилось в буфере класс работы с ключевыми словами
#            self.buffer_write += self.comms.buffer_write
#            self.comms.buffer_write = ''

    def handle_close(self):
        '''При закрытии потока внешним потоком сообщаем слушающему серверу о данном событии'''
        self.delclient(self.id)
        self.close()
    
    def writable(self):
        '''Проверяем необходимость передачи данных '''
        return (len(self.buffer_write) > 0)

#TODO: предусмотреть, если невозможно передать данные, сохранить во временном хранилище, чтобы передать, когда будет возможность
    def handle_write(self):
        '''Передаем данные'''
        sent = self.send(self.buffer_write)
        print '%04i <--'%sent, self.id
        self.buffer_write = self.buffer_write[sent:]
    
class command(asyncore.dispatcher):
    '''Класс управления сервером
    
    Управление сервером: изменение параметров сервера, получение данных
        
    '''
    def __init__(self):
        asyncore.dispatcher.__init__(self)
        self.initDefault()

    def initDefault(self):
        '''Инициализация по умолчанию
        
        :param dict config: словарь для настроек класса:
            
            * "ip" - ip на котором ждем подключения
            * "port" - порт на который ждем подключения
            * "backlog" - количество возможных клиентов
        :param list thread: список открытых потоков класса :py:class:`server.listen.threadListenServer`
        
        clientid - счетчик id потоков для прослушивания
        clientdict - словарь подключений
        command - список функций родителя по выполнению команд
        '''
        self.config = {}
        self.config["ip"] = ''
        self.config["port"] = 21000
        self.config["backlog"] = 10
        self.clientid = 0
        self.clientdict = {}
        self.command = {}
        self.resender = resender()
        self.fl = 0
        
    
    def initListenPort(self):
        '''По данным из конфига начинаем прослушивать порт
        '''
        if self.config != {}:
            self.create_socket(socket.AF_INET,socket.SOCK_STREAM)
            self.set_reuse_addr()
            self.bind((self.config["ip"], self.config["port"]))
            self.listen(self.config["backlog"])
            print "command server starting on ip:'",self.config["ip"],"' port:", self.config["port"], " count:",self.config["backlog"]
        else:
            print u"Конфиг пустой"
    
    def handle_accept(self):
        '''Получаем соединение, включаем поток, добавляем соединение 
        на ретрансляторах'''
        self.addthread()
        
    def writable(self):
        self.resender.run()
        return False
        
    def addthread(self):
        '''Создаем потоки для прослушивания комманд'''
        #создаем потоки для ретрансляции
        conn, addr = self.accept()
        #print '--- Connect --- ',self.clientid, addr
        cl = threadCommand(conn, self.clientid, self.resend, self.delthread)
        self.clientdict[self.clientid] = cl
        self.clientid += 1

    def delthread(self, id):
        '''Метод обработки закрытия потока'''
        print '--- Disconnect --- ',id
        self.clientdict.pop(id)

    def resend(self, command, parametr = ''):
        '''Метод обработки полученных команд от потока'''
        try:
            if parametr  == '':
                self.command[command]()
            else:
                self.command[command](parametr)
        except IndexError:
            print "error index command %s"%command 
   
    def close(self):
        '''Закрываем ретрансляторы'''
        for j in self.clientdict.values():
            j.close()
        self.resender.close()
        print "close command"
        asyncore.dispatcher.close(self)