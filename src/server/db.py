#!/usr/bin/python
# -*- coding: utf-8  -*-
import MySQLdb
import re

class db:
    ''' Класс работы с базой данных
    Переменные:
        * sql - текст SQL запроса, который будет выполнен, если в execsql не будет передана строка'''
    def __init__(self):
        self.sql=''
        self.cursor = ''
        self.conn = ''
        self.connected = False
        self.config = {}
        self.config["host"] = ""
        self.config["user"] = ""
        self.config["passwd"] = ""
        self.config["db"] = ""
        self.error = 0
        self.maxlensqlinsert = 50 #сколько переменных вставлять одним запросом
        #список таблиц, изменения в которых не требуется передавать клинтским базам
        self.excludetable = ['buffredirectdata', 'buffredirectserver', 
                             'clientdb', ' transact']

    def connect(self):
        self.error = 1
        "Подключение к базе данных"
        try:
            self.conn = MySQLdb.connect (host = self.config["host"], user = self.config["user"], 
                passwd = self.config["passwd"], db = self.config["db"], 
                charset = "utf8", use_unicode = True)
            self.cursor = self.conn.cursor (MySQLdb.cursors.DictCursor)
            self.connected = True
            self.error = 0
        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
            self.disconnect()

    def _needtransact(self, sql):
        '''Проверяем нужны ли записи для транзакции и сколько потребуется записей
        '''
        table = ''
        operation = 0
        where = ''
        leninsert = 0
        sql1 = 'SELECT id, name FROM clientdb'
        self.getData(sql1)
        datarow = self.datarow[:]
        if len(datarow) > 0:
            (table,operation,where,leninsert) = self._initvalue(sql)
        #print (datarow, table, operation, where, leninsert)
        for i in self.excludetable:#если таблица находится среди исключаемых
            if table == i:#тогда обнуляем список подключаемых клиентов
                datarow = []
        return (datarow, table, operation, where, leninsert)

    def _preparetransact(self, sql, data):
        sqlinsert = []
        client, table, operation, where, leninsert = data
        #если запрос типа Update или Delete типа - ищем по каким id будут изменения
        if operation == 2 or operation == 3:
            sqlsel = 'SELECT id FROM %s WHERE %s'%(table,where)
            self.getData(sqlsel)
            begstr = 'INSERT INTO transact (idclient, nametable, idtablechange, operation) VALUES '
            sqlinsert = [begstr]#на случай, если id будет найдено много, то разбиваем запрос на списки
            n = 0 #счетчик количества параметров для инсерта
            r = 0 #в какую строку списка sqlinsert пишем
            fl = '' #флаг первой записи, для установки запятых
            for idchange in self.datarow:#формируем список значений, по которым будет вставка
                for idclient in client:
                    if n > self.maxlensqlinsert:
                        n = 0
                        r += 1
                        fl = ''
                        sqlinsert += [begstr]
                    sqlinsert[r] = "%s%s (%d, '%s', %d, %d)"%(sqlinsert[r],fl, 
                        idclient, table, idchange, operation)
                    n += 1
                    fl = ','
        #print sqlinsert
        return sqlinsert

    def _exectransact(self, sql, data):
        client, table, operation, where, leninsert = data
        if operation == 2 or operation == 3:
            for isql in sql:
                self._execsql(isql)
        elif operation == 1:#запрос был типа инсерт, ищем последний вставленный ид
            lastid = self.getlastid()#получаем последний вставленный id
            begstr = 'INSERT INTO transact (idclient, nametable, idtablechange, operation) VALUES '
            sqlinsert = [begstr]#на случай, если id будет найдено много, то разбиваем запрос на списки
            n = 0 #счетчик количества параметров для инсерта
            r = 0 #в какую строку списка sqlinsert пишем
            fl = '' #флаг первой записи, для установки запятых
            #print sqlinsert 
            print lastid + leninsert, lastid
            for idchange in range(lastid, lastid + leninsert):#формируем список значений, по которым будет вставка
                for idclient in client:
                    if n > self.maxlensqlinsert:
                        n = 0
                        r += 1
                        fl = ''
                        sqlinsert += [begstr]
                    sqlinsert[r] = "%s%s (%d, '%s', %d, %d)"%(sqlinsert[r],fl, 
                        idclient, table, idchange, operation)
                    n += 1
                    fl = ','
            print sqlinsert 
            for isql in sqlinsert:
                self._execsql(isql)

    def execsql(self,sql = ''):
        self.error = 1
        "Выполнение SQL запроса по переданному параметру sql или внутренней переменной sql"
        try:
            if (sql == ''):
                sql = self.sql
            data = self._needtransact(sql) #проверяем есть ли клиенты этой базы данных
            if len(data[0]) > 0:
                sqlinsert = self._preparetransact(sql, data)#подготавливаем данные для транзакции
            self.cursor.execute (sql)
            if len(data[0]) > 0:
                self._exectransact(sqlinsert, data)#выполняем транзакционные записи
            self.error = 0
        except:#MySQLdb.Error, e
            self.connect()
            if self.error == 0:
                self.cursor.execute (sql)

                
    def _execsql(self,sql = ''):
        '''Запрос без транзакционных записей'''
        self.error = 1
        "Выполнение SQL запроса по переданному параметру sql или внутренней переменной sql"
        try:
            if (sql == ''):
                sql = self.sql
            self.cursor.execute (sql)
        except:# MySQLdb.Error, e
            self.connect()
            if self.error == 0:
                self.cursor.execute (sql)

    def _getData(self, sql = ''):
        self.error = 1
        self._execsql(sql)
        self.datarow = []
        #self.datafield = []
        self.data = {} #словарь со всеми полями
        self.fields = []#список полей из запроса
        
        for f in self.cursor.description:
            self.fields.append(f[0])
        result_set = self.cursor.fetchall()
        for row in result_set:
            tmpdata = {}
            for i in self.fields:
                tmpdata[i] = row[i] #проход по полям
            self.data[row["id"]] = tmpdata#сохраняем в данных
            self.datarow += [row["id"]]
        self.error = 0
        return (self.fields, self.datarow, self.data)

    
    def getData(self, sql = ''):
        ''' Получаем данные из запроса - в запросе всегда должен присутствовать 
        id таблицы
        Входные данные - SQL запрос, на выходе или в теле класса
        fields - список имен полей, полученные из запроса
        data - словарь с данными, где ключ - id запроса
        datarow - список ключей (для упорядочивания получения данных из data)
        '''
        self.error = 1
        "Выполнение SQL запроса по переданному параметру sql или внутренней переменной sql"
        f = []
        dr = []
        d = {}        
        try:
            if (sql == ''):
                sql = self.sql
            f,dr,d = self._getData(sql)
            self.error = 0
        except:#MySQLdb.Error, e
            self.connect()
            if self.error == 0:
                f,dr,d = self._getData(sql)
        return (f,dr,d)
             
    def disconnect(self):
        "Отключение от базы данных"
        try:
            self.cursor.close ()
            self.conn.close ()
            self.connected = False
        except MySQLdb.Error, e:
             print "Error %d: %s" % (e.args[0], e.args[1])

    def getlastid(self):
        ''' Получить id последнего выполненного запроса insert'''
        try:
            a = self.conn.insert_id()
            return a
        except MySQLdb.Error, e:
             print "Error %d: %s" % (e.args[0], e.args[1])
             
    def _initvalue(self,sql):
        '''По выполненным запросам делаем записи в базе для передачи подчиненным базам
        '''
        table = ''
        operation = 0
        where = ''
        leninsert = 0
        st = sql.replace("\n","")
        print st
        match = (r'INSERT *INTO *(\w*).*VALUES *(\(.*\),? *)*'
                 r'|UPDATE *(\w*) *SET.*WHERE *(.*)'
                 r'|DELETE *FROM *(\w*) *WHERE *(.*)')
        refind = re.finditer(match,st)
        for i in refind:
            if i.groups()[0] != None:
                #print "Insert -- ", (i.groups()[0],)
                operation = 1
                table = i.groups()[0]
                #print "find last id, count = ",len(i.groups()[1].split(")"))-1
                leninsert = len(i.groups()[1].split(")"))-1
            elif i.groups()[2] != None:
                operation = 2
                table = i.groups()[2]
                where = i.groups()[3]
                #print "Update -- ", table
                #print "find updated id"
                #print "Select id FROM",table ,"WHERE",where
            elif i.groups()[4] != None:
                operation = 3
                table = i.groups()[4]
                where = i.groups()[5]
                #print "Delete -- ", table
                #print "find updated id"
                #print "Select id FROM",table ,"WHERE",where
        return (table,operation,where,leninsert)