#!/usr/bin/python
# -*- coding: utf-8  -*-
import re
import datetime

# 16-bit CRCs should detect 65535/65536 or 99.998% of all errors in
# data blocks up to 4096 bytes
MASK_CCITT = 0x1021 # CRC-CCITT mask (ISO 3309, used in X25, HDLC)
MASK_CRC16 = 0xA001 # CRC16 mask (used in ARC files)


class decode:
    def __init__(self, type = 0, st = ''):#type - тип прибора
        self.st = self.ords(st)
        self.type = type
        self.minlat = 51.14
        self.maxlat = 56.4
        self.minlon = 23.0
        self.maxlon = 33.0
        self.maxspeed = 150
        self.alt = 300
        self.timepojas = 60*60*3#сдвиг на 3 часа

    def ords(self,x):#из байт стпроки в 16-ричную систему
        s1 = ''
        for s in x:
            s1 = "%s%02x"%(s1,ord(s))
        return s1

    def sord(self,ord):#из 16-ричной системы в байт строку
        s1 = ''
        for i in range(0,len(ord),2):
            s1 = "%s%s"%(s1,chr(self.hextoint(ord[i:i+2])))
        return s1

    def delslash(self,s1):
        #удаляем экранирующие символы если есть 1712, то оставляем только 17
        s = ''
        fl = 0
        for i in range(0,len(s1),2):
            if fl == 1:
                fl = 0
                if s1[i:i+2] != '12':
                    s = "%s%s"%(s,s1[i:i+2])
            else:
                s = "%s%s"%(s,s1[i:i+2])
            if s1[i:i+2] == '17':
                fl = 1
        return s


    def updcrc(self, data, crc=0xffff, mask=MASK_CCITT):
        for char in data:
            c = ord(char)
            c = c << 8

            for j in xrange(8):
                if (crc ^ c) & 0x8000:
                    crc = (crc << 1) ^ mask
                else:
                    crc = crc << 1
                c = c << 1
        return crc & 0xffff

    def hextoint(self,st):
        j = 1
        sum = 0
        for i in range(len(st)-1,-1,-1):
            st1 = st[i]
            if st1 == 'a': st1 = 10
            if st1 == 'b': st1 = 11
            if st1 == 'c': st1 = 12
            if st1 == 'd': st1 = 13
            if st1 == 'e': st1 = 14
            if st1 == 'f': st1 = 15
            st1 = int(st1)
            sum += st1 * j
            j *= 16
        return sum

    def inttohex(self,st):
        return "%x"%st

    def hextofloat(self,st):
        j = 1 / 16.0
        sum = 0
        for i in range(0,len(st)):
            st1 = st[i]
            if st1 == 'a': st1 = 10
            if st1 == 'b': st1 = 11
            if st1 == 'c': st1 = 12
            if st1 == 'd': st1 = 13
            if st1 == 'e': st1 = 14
            if st1 == 'f': st1 = 15
            st1 = int(st1)
            sum += st1 * j
            j /= 16.0
        return sum

    def decode(self,st='',typest = 0):#typest - 0-байт строка, 1 - в 16-ричном видении
        #минимально-максимальные координаты

        if typest == 0:
            if len(st) == 0:
                self.st = '1713080cea08100b0c291901e3540000fbcc1e04d80f6e1f000035309800720666401705'
            else:
                self.st = self.ords(st)
        elif typest == 1:
            self.st = st
        lenst = len(self.st)
        lenread = 0
        o = re.finditer('(1713)(.*?)(1705)',self.st)
        k = 0
        ret = []
        dh = self.hextoint#сократим длину написания функции
        dt = datetime.datetime.now()
        for i in o:
            error = []
            d = i.groups()
            for j in d:
                if j != None:
                    lenread += len(j)
            ss = d[1]
            ss = self.delslash(ss)
            if self.type ==0:
                sluj = self.hextoint(ss[0:2])
                nprib = self.hextoint(ss[2:6])
                try:
                    dt = datetime.datetime(2000+dh(ss[10:12]),dh(ss[6:8]),dh(ss[8:10]),dh(ss[12:14]),dh(ss[14:16]),dh(ss[16:18])  )
                    dt += datetime.timedelta(0,self.timepojas)#смещение времени по текущему часовому поясу
                except Exception, e:
                    error += [[1,'Datetime - error']]
                    dt = datetime.datetime(2000, 1, 1, 0, 0)
                    #print "decodedata.decodedata.decode ",e
                lat = self.hextoint(ss[18:28])/153600000.0
                lon = self.hextoint(ss[28:34])/600000.0
                if lat > self.maxlat or lat < self.minlat or lon > self.maxlon or lon < self.minlon:
                    error += [[2,'lat or lon - error']]
                    lat = -1
                    lon = -1
                speed = self.hextoint(ss[34:38])/100.0
                if speed > self.maxspeed:
                    error += [[3,'speed - error']]
                    speed = -1
                angle = self.hextoint(ss[38:42])/100.0
                if angle > 360:
                    error += [[4,'angle - error']]
                    angle = -1
                nepotnyatno = ss[42:]
                #if len(nepotnyatno)>0:
                #    error += [[7,'musor detected']]

                sluj2 = 0
                alt = 0#self.hextoint(ss[42:44])
                crc16 = 0#self.hextoint(ss[-4:])
                crc16prov = 0
            if self.type ==1:
                sluj = self.hextoint(ss[0:2])
                sluj2 = self.hextoint(ss[2:4])
                nprib = self.hextoint(ss[4:8])
                try:
                    dt = datetime.datetime(2000+dh(ss[12:14]),dh(ss[8:10]),dh(ss[10:12]),dh(ss[14:16]),dh(ss[16:18]),dh(ss[18:20]))
                    dt += datetime.timedelta(0,self.timepojas)#смещение времени по текущему часовому поясу
                except Exception, e:
                    error += [[1,'Datetime - error']]
                    dt = datetime.datetime(2000, 1, 1, 0, 0)
                    #print "decodedata.decodedata.decode ",e
                lat = self.hextoint(ss[20:30])/153600000.0
                lon = self.hextoint(ss[30:36])/600000.0
                if lat > self.maxlat or lat < self.minlat or lon > self.maxlon or lon < self.minlon:
                    error += [[2,'lat or lon - error']]
                    lat = -1
                    lon = -1
                speed = self.hextoint(ss[36:40])/100.0
                if speed > self.maxspeed:
                    error += [[3,'speed - error']]
                    speed = -1
                angle = self.hextoint(ss[40:44])/100.0
                if angle > 360:
                    error += [[4,'angle - error']]
                    angle = -1
                alt = self.hextoint(ss[44:48])
                if alt > self.alt:
                    error += [[5,'alt - error']]
                    alt = -1
                crc16 = self.hextoint(ss[-4:])
                crc16prov = int(self.updcrc(self.sord(ss[0:-4])))
                if int(crc16) - int(crc16prov) != 0:
                    error += [[6,'crc16 - error']]
                    crc16 = -1
                nepotnyatno = ss[48:-4]
                #if len(nepotnyatno)>0:
                #    error += [[7,'musor detected']]

            
            d = {}
            d ['datetime'] = dt
            d ['sluj'] = sluj
            d ['sluj2'] = sluj2
            d ['nprib'] = nprib
            d ['lat'] = lat
            d ['lon'] = lon
            d ['angle'] = angle
            d ['speed'] = speed
            d ['alt'] = alt
            d ['crc16'] = crc16
            d ['crc16prov'] = crc16prov
            d ['nepotnyatno'] = nepotnyatno
            d ["dtprogramm"] = datetime.datetime.now()
            k += 1
            if len(error)>0:
                ret +=[[d,[error,ss]]]
            else:
                ret +=[[d]]
        #if k>1:
        #    print "---------------====================--------------------",k
        self.lenost = (lenst - lenread)/2
        if self.st[-4:] == '1710':
            self.lenost = 0#если в конце признак конца пакета, значит мусор не в конце
        return ret