#!/usr/bin/python
# -*- coding: utf-8  -*-
import socket, asyncore
'''
Сервер для тестирования соединений
'''

class client(asyncore.dispatcher):
    def __init__(self, conn, id, resend, delclient, addr, mysqldb = None):
        self.resend = resend
        self.delclient = delclient
        self.id = id
        self.addr = addr
        asyncore.dispatcher.__init__(self, conn)
        self.buffer_read = ''
#        self.buffer_write = ''

    def handle_connect(self):
        pass

    def handle_read(self):
        read = self.recv(4096)
        print '%04i -->'%len(read), self.addr, (read,)
        self.buffer_read = read
        if self.buffer_read[0:4] == 'stop':
            self.resend('stop', self.id)
        elif self.buffer_read[0:3] == 'end':
            self.handle_close()

    def handle_close(self):
        self.delclient(self.id, self.addr)
        self.close()
        
    def writable(self):
        'Передавать нечего - поэтому всегда ложь'
        return False

class shkiperServer(asyncore.dispatcher):
    def __init__(self, ip = '', port = 27001, backlog = 200):
        asyncore.dispatcher.__init__(self)
        self.clientdict = []
        self.clientid = 0
        self.create_socket(socket.AF_INET,socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind((ip,port))
        self.listen(backlog)
        self.port = port
    
    
    def resend(self,data,id):
        if data[0:4] == 'stop':
            for i in self.clientdict:
                i[1].close()
            print "stopping"
            self.close()
        
    def handle_accept(self):
        self.addclient()
        
    def handle_connect(self):
        pass

    def handle_read(self):
        read = self.recv(1024)
        
    def handle_close(self):
        pass
    
    def addclient(self):
        conn, addr = self.accept()
        print '--- Connect --- ',self.clientid, addr, "to port:",self.port
        cl = client(conn, self.clientid, self.resend, self.delclient, addr)
        self.clientdict += [[self.clientid, cl]]
        self.clientid += 1

    def delclient(self, id, addr):
        k = 0
        print '--- Disconnect --- ',id, addr
        for i in self.clientdict:
            if i[0] == id:
                self.clientdict = self.clientdict[0:k]+self.clientdict[k+1:]
            k += 1

        
if __name__ == "__main__":
    serv1 = shkiperServer('',21101)
    serv2 = shkiperServer('',21104)
    asyncore.loop()