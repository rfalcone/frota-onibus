#!/usr/bin/python
# -*- coding: utf-8  -*-
import socket
import datetime
import time
import threading
from PyQt4 import QtGui, QtCore
from shkiper.decodedata import decodedata
from db import db
from map.tilenames import lengthMetr

class readPacketShkiper(threading.Thread):
    def __init__(self, channel, details, id,type):#type 0 - шкипер-01; 1 - шкипер-01Е
        threading.Thread.__init__(self)
        self.mysql = db()
        self.channel = channel
        self.details = details
        self.type = type
        self.getflag = self.getflagnull
        self.id = id
        self.rr = 1
        self.delthread = self.delthreadnull#возвращаем родителю сигнал об удалении
        self.maxlength = 10 #расстояние, на котором точки не учитываются
    ''
    def getflagnull(self):
        return 1
    ''
    def delthreadnull(self,id):
        pass
    ''

    def saveData(self,packet):#сохраняем точку
        print  "sluj = %x,%x"%(j['sluj'],j['sluj2'])
        print  "n prib = %d"%j ['nprib']
        print  "date = ",j['datetime']
        print  "lat = %f lon = %f"%(j['lat'],j['lon'])
        print  "speed = %.2f angle = %.2f alt = %d"%(j['speed'],j['angle'], j['alt'])
        print  "crc16 = %x prov = %x"%(j['crc16'],j ['crc16prov'])
        print  "ostatok = %s"%j['nepotnyatno']
    ''

    def insertTempData(self,packet,id=0):#сохраняем временную точку
        try:
            d = packet
            dt = datetime.datetime.now()
            self.mysql.sql = """ select id FROM pointlast WHERE nmbr = %d"""%d['nprib']
            self.mysql.execsql()
            result_set = self.mysql.cursor.fetchall()
            for row in result_set:
                id = row["id"]
            #если в таблице с текущими данными прибор еще не добавлен (не найден его id) - создаем новую запись
            if id == 0:
                n = datetime.datetime.now()
                sql = """ INSERT INTO pointlast (lat, lon, nmbr, datetimepribor,
                        datetimeprogramm, sluj, sluj2, speed, angle, alt)
                    VALUES (%2.8f, %2.8f, %d, '%s',
                        '%s', %d, %d, %d, %d, %d)
                    """%(d['lat'], d['lon'], d['nprib'], str(d['datetime']),
                        str(dt), d['sluj'], d['sluj2'],  d['speed'],  d['angle'],  d['alt'])
                self.mysql.execsql(sql)
                id = self.mysql.getlastid()
            else:
                #иначе подновляем запись
                self.mysql.sql = """ UPDATE  pointlast SET lat = %2.8f, lon = %2.8f,
                    datetimepribor = '%s', datetimeprogramm = '%s', sluj = %d, sluj2 = %d,
                    speed = %d, angle=%d, alt=%d
                    WHERE  id = %d"""%(d['lat'], d['lon'],
                    str(d['datetime']), str(dt), d['sluj'],d['sluj2'],
                    d['speed'], d['angle'], d['alt'], id)
                self.mysql.execsql()
            return id
        except Exception as e:
            print "readdatasock.readPacketShkiper.insertTempData ",e
    ''
    def insertData(self,packet):
        try:
            d = packet
            dt = datetime.datetime.now()
            sql = """ INSERT INTO point (lat, lon, nmbr, datetimepribor,
                    datetimeprogramm, sluj, sluj2, speed, angle, alt, npoint)
                VALUES (%2.8f, %2.8f, %d, '%s',
                    '%s', %d, %d, %d, %d, %d, %d)
                """%(d['lat'], d['lon'], d['nprib'], str(d['datetime']),
                    str(dt), d['sluj'], d['sluj2'],  d['speed'],  d['angle'],  d['alt'], 0)
            #print self.mysql.sql
            self.mysql.execsql(sql)
            id2 = self.mysql.getlastid()
            return id2
        except Exception as e:
            print "readdatasock.readPacketShkiper.insertData ",e
    ''
    def updateData(self,id2,npoint):
        sql = """ UPDATE  point SET npoint = %d
            WHERE  id = %d """%(npoint,id2)
        self.mysql.execsql(sql)
    ''
    def insertError(self,packet):
        try:
            print "error ",packet
            data = packet[0]
            error = packet[1]
            dt = datetime.datetime.now()
            koderror = 0
            for i in error[0]:
                if i[0] == 1:
                    data ['datetime'] = datetime.datetime(2000, 1, 1, 0, 0)
                if i[0] == 2:
                    data ['lat'] = -1
                    data ['lon'] = -1
                if i[0] == 3:
                    data ['speed'] = -1
                if i[0] == 4:
                    data ['angle'] = -1
                if i[0] == 5:
                    data ['alt'] = -1
                if i[0] == 6:
                    data ['crc16'] = -1
                koderror += 2**(i[0]-1)
            
            dt = datetime.datetime.now()
            sql = """ INSERT INTO error (lat, lon, nmbr, datetimepribor,
                    datetimeprogramm, sluj, sluj2, speed, angle, alt, koderror, mess)
                VALUES (%2.8f, %2.8f, %d, '%s',
                    '%s', %d, %d, %d, %d, %d, %d, '%s')
                """%(data['lat'], data['lon'], data['nprib'], str(data['datetime']),
                    str(dt), data['sluj'], data['sluj2'],  data['speed'],  data['angle'],
                    data['alt'], koderror, error[1])
            #print self.mysql.sql
            self.mysql.execsql(sql)
        except Exception as e:
            print "readdatasock.readPacketShkiper.insertError ",e
    ''
    def run(self):
        decode = decodedata(self.type)
        self.channel.settimeout(120)
        lat1 = 0
        lat2 = 0
        lon1 = 0
        lon2 = 0
        id = 0
        id2 = 0
        npoint = 0
        lenghtM = [0,0]
        while self.rr:
            try:
                data = self.channel.recv(1024)
                if not data:
                    self.rr = 0
                    print "\nEnd thread %d\n"%(self.id)
                else:
                    l = decode.decode(data)#расшифровываем пакет
                    for i in l:#т.к. может быть несколько данных, перебираем все
                        if len(i)==1:#если в данном пакете нет ошибок
                            j = i[0]
                            print self.id,j
                            if id == 0:#если это первый проход, то добавляем данные
                                id = self.insertTempData(j)
                                id2 = self.insertData(j)
                                lat1 = j['lat']
                                lon1 = j['lon']
                            else:#иначе, если данные уже приходили
                                lat1 = j['lat']
                                lon1 = j['lon']
                                lenghtM = lengthMetr(lat1,lon1,lat2,lon2)
                                if lenghtM[0] >= self.maxlength:
                                    id = self.insertTempData(j,id)
                                    id2 = self.insertData(j)
                                    npoint = 0
                                else:
                                    npoint += 1
                                    self.updateData(id2,npoint)
                            lat2 = lat1
                            lon2 = lon1
                        else:#обработка пакета с ошибками
                            self.insertError(i)
                            print "------------=================--------------"
            except Exception, e:
                print "readdatasock.readPacketShkiper.run ",e
                self.rr = 0
            if self.rr:
                self.rr = 1 - self.getflag()
        self.rr = 0
        self.mysql.disconnect()
        self.channel.close()
        self.delthread(self.id)
        print "thread deleted"
    ''


class listenServer(threading.Thread):
    def __init__(self,ip = '10.1.12.5',port = 21001,type = 0):
        threading.Thread.__init__(self)
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.ip = ip
        self.port = port
        self.type = type #тип оборудования 0 - шкипер-01, 1 - шкипер-01Е
        self.needconnect = 1
        self.rr = 1
        self.id = 0
        self.pool = []
        self.active = 1
        self.getthread = self.getthreadnull#возвращаем родителю сколько потоков открыто
        self.needclose = self.needclosenull#функция возвращающая от родителя необходимость закрытия сервера
    ''
    def getthreadnull(self,x,type):
        pass
    ''
    def needclosenull(self):
        return 0
    ''
    def connect(self):
        if self.needconnect:
            try:
                self.server.bind((self.ip, self.port))
                self.server.listen(200)
                self.needconnect = 0
                print "Socket listen %s:%d"%(self.ip, self.port)
            except:
                print "Socket denied %s:%d"%(self.ip, self.port)
                time.sleep(1)
    ''
    def run(self):
        while self.needconnect and (self.needclose()==0):
            self.connect()
        self.rr = 1 - self.needclose()
        while self.rr:
            try:
                channel, details = self.server.accept()
                self.addthread(channel, details)
            except Exception as e:
                print "Error add thread or accept server %s:%d"%(self.ip,self.port)
                print e
                self.rr = 0
            if self.rr:
                self.rr = 1 - self.needclose()
        while len(self.pool)>0:
            pass
        print "end server ",self.type
        self.active = 0
        self.server.close()
    ''
    def addthread(self,channel, details):
        k = readPacketShkiper(channel, details,self.id,self.type)
        k.delthread = self.delthread
        k.getflag = self.needclose
        k.start()
        self.pool += [k]
        self.id += 1

        self.getthread(len(self.pool),self.type)
        print "Thread added type = %d id = %d count = %d"%(self.type,self.id,len(self.pool))
    ''
    def delthread(self,id):
        j = 0
        for i in self.pool:
            if (i.id ==id):
                if len(self.pool)>1:
                    self.pool = self.pool[0:j]+self.pool[j+1:]
                else:
                    self.pool = []
            j += 1
        self.getthread(len(self.pool),self.type)
        print "Thread deleted type = %d id = %d count = %d"%(self.type,id,len(self.pool))
    ''
    def getflagstop(self):
        return self.rr


class readSock(QtGui.QDialog):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setMinimumSize(300,200)
        self.tt = QtGui.QLabel()
        gl = QtGui.QGridLayout()
        gl.addWidget(self.tt)
        self.setLayout(gl)
        self.needclose = 0

        self.getpool = [0,0]
        ip = '178.125.130.75'
        self.lServer = listenServer(ip,21001,0)
        self.lServer.getthread = self.getThread
        self.lServer.needclose = self.getClose
        self.lServer.start()
        
        self.lServer1 = listenServer(ip, 21003,1)
        self.lServer1.getthread = self.getThread
        self.lServer1.needclose  = self.getClose
        self.lServer1.start()

        self.connect(self,QtCore.SIGNAL("finished()"),self.closeDialog)
        self.connect(self,QtCore.SIGNAL("rejected()"),self.closeDialog)
        
        
    ''
    def getClose(self):
        return self.needclose
    ''
    def closeDialog(self):
        self.needclose = 1
        print "close dialog"
    ''
    def getThread(self,x,type):
        self.getpool[type] = x
        self.tt.setText("%d - %d"%(self.getpool[0],self.getpool[1]))

def getClose():
    return 0

''
def getThread(x,type):
    pass


if __name__ == "__main__":
    lServer = listenServer('178.125.166.170',21001,0)
    lServer.getthread = getThread
    lServer.needclose = getClose
    lServer.start()

    lServer1 = listenServer('178.125.166.170', 21003,1)
    lServer1.getthread = getThread
    lServer1.needclose  = getClose
    lServer1.start()

