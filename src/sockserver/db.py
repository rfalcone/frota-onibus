#!/usr/bin/python
# -*- coding: utf-8  -*-
import MySQLdb

class db():
    ''' Класс работы с базой данных
    Переменные:
        * sql - текст SQL запроса, который будет выполнен, если в execsql не будет передана строка'''
    def __init__(self):
        self.sql=''
        self.cursor = ''
        self.conn = ''
        self.connected = False
        self.connect()
        self.config = {}
        self.config["host"] = ""
        self.config["user"] = ""
        self.config["passwd"] = ""
        self.config["db"] = ""

    def connect(self):
        "Подключение к базе данных"
        try:
            self.conn = MySQLdb.connect (host = self.config["host"], user = self.config["user"], 
                passwd = self.config["passwd"], db = self.config["db"], 
                charset = "utf8", use_unicode = True)
            self.cursor = self.conn.cursor (MySQLdb.cursors.DictCursor)
            self.connected = True
        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
            self.disconnect()

    def execsql(self,sql=''):
        "Выполнение SQL запроса по переданному параметру sql или внутренней переменной sql"
        try:
            if (sql != ''):
                self.sql = sql
            self.cursor.execute (self.sql)
        except MySQLdb.Error, e:
             print "Error %d: %s" % (e.args[0], e.args[1])
             self.connect()
             
    def disconnect(self):
        "Отключение от базы данных"
        try:
            self.cursor.close ()
            self.conn.close ()
            self.connected = False
        except MySQLdb.Error, e:
             print "Error %d: %s" % (e.args[0], e.args[1])

    def getlastid(self):
        ''' Получить id последнего выполненного запроса insert'''
        try:
            a = self.conn.insert_id()
            return a
        except MySQLdb.Error, e:
             print "Error %d: %s" % (e.args[0], e.args[1])