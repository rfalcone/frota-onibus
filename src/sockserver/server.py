#!/usr/bin/python
# -*- coding: utf-8  -*-
import socket
import threading
import time
from decodedata import decodedata
from tilenames import lengthMetr
from db import db
import datetime

class writeSqlData:
    def __init__(self):
        self.mysql = db('localhost', 'asdm', 'root', '234912234912')
        self.dec = decodedata()
        self.lat1 = 0
        self.lat2 = 0
        self.lon1 = 0   
        self.lon2 = 0
        self.id = 0
        self.id2 = 0
        self.npoint = 0
        self.lenghtM = [0,0]
        self.maxlength = 10 #расстояние, на котором точки не учитываются
    ''
    def sendData(self,data):
        l = self.dec.decode(data)#расшифровываем пакет
        for i in l:#т.к. может быть несколько данных, перебираем все
            if len(i)==1:#если в данном пакете нет ошибок
                j = i[0]
                if self.id == 0:#если это первый проход, то добавляем данные
                    self.id = self.insertTempData(j)
                    self.id2 = self.insertData(j)
                    self.lat1 = j['lat']
                    self.lon1 = j['lon']
                else:#иначе, если данные уже приходили
                    self.lat1 = j['lat']
                    self.lon1 = j['lon']
                    self.lenghtM = lengthMetr(self.lat1,self.lon1,self.lat2,self.lon2)
                    self.id = self.insertTempData(j,self.id)
                    if self.lenghtM[0] >= self.maxlength:
                        self.id2 = self.insertData(j)
                        self.npoint = 0
                    else:
                        self.npoint += 1
                        self.updateData(self.id2,self.npoint)
                self.lat2 = self.lat1
                self.lon2 = self.lon1
            else:#обработка пакета с ошибками
                self.insertError(i)
                print "------------=================--------------"
    ''
    def insertTempData(self,packet,id=0):#сохраняем временную точку
        try:
            d = packet
            dt = datetime.datetime.now()
            self.mysql.sql = """ select id FROM pointlast WHERE nmbr = %d"""%d['nprib']
            self.mysql.execsql()
            result_set = self.mysql.cursor.fetchall()
            for row in result_set:
                id = row["id"]
            #если в таблице с текущими данными прибор еще не добавлен (не найден его id) - создаем новую запись
            if id == 0:
                sql = """ INSERT INTO pointlast (lat, lon, nmbr, datetimepribor,
                        datetimeprogramm, sluj, sluj2, speed, angle, alt)
                    VALUES (%2.8f, %2.8f, %d, '%s',
                        '%s', %d, %d, %d, %d, %d)
                    """%(d['lat'], d['lon'], d['nprib'], str(d['datetime']),
                        str(dt), d['sluj'], d['sluj2'],  d['speed'],  d['angle'],  d['alt'])
                self.mysql.execsql(sql)
                id = self.mysql.getlastid()
            else:
                #иначе подновляем запись
                self.mysql.sql = """ UPDATE  pointlast SET lat = %2.8f, lon = %2.8f,
                    datetimepribor = '%s', datetimeprogramm = '%s', sluj = %d, sluj2 = %d,
                    speed = %d, angle=%d, alt=%d
                    WHERE  id = %d"""%(d['lat'], d['lon'],
                    str(d['datetime']), str(dt), d['sluj'],d['sluj2'],
                    d['speed'], d['angle'], d['alt'], id)
                self.mysql.execsql()
            return id
        except Exception, e:
            print "readdatasock.readPacketShkiper.insertTempData ",e
    ''
    def insertData(self,packet):
        try:
            print "insert ",packet
            d = packet
            dt = datetime.datetime.now()
            sql = """ INSERT INTO point (lat, lon, nmbr, datetimepribor,
                    datetimeprogramm, sluj, sluj2, speed, angle, alt, npoint)
                VALUES (%2.8f, %2.8f, %d, '%s',
                    '%s', %d, %d, %d, %d, %d, %d)
                """%(d['lat'], d['lon'], d['nprib'], str(d['datetime']),
                    str(dt), d['sluj'], d['sluj2'],  d['speed'],  d['angle'],  d['alt'], 0)
            #print self.mysql.sql
            self.mysql.execsql(sql)
            id2 = self.mysql.getlastid()
            return id2
        except Exception, e:
            print "readdatasock.readPacketShkiper.insertData ",e
    ''
    def updateData(self,id2,npoint):
        sql = """ UPDATE  point SET npoint = %d
            WHERE  id = %d """%(npoint,id2)
        self.mysql.execsql(sql)
    ''
    def insertError(self,packet):
        try:
            print "error ",packet
            data = packet[0]
            error = packet[1]
            dt = datetime.datetime.now()
            koderror = 0
            for i in error[0]:
                if i[0] == 1:
                    data ['datetime'] = datetime.datetime(2000, 1, 1, 0, 0)
                if i[0] == 2:
                    data ['lat'] = -1
                    data ['lon'] = -1
                if i[0] == 3:
                    data ['speed'] = -1
                if i[0] == 4:
                    data ['angle'] = -1
                if i[0] == 5:
                    data ['alt'] = -1
                if i[0] == 6:
                    data ['crc16'] = -1
                koderror += 2**(i[0]-1)
            dt = datetime.datetime.now()
            sql = """ INSERT INTO error (lat, lon, nmbr, datetimepribor,
                    datetimeprogramm, sluj, sluj2, speed, angle, alt, koderror, mess)
                VALUES (%2.8f, %2.8f, %d, '%s',
                    '%s', %d, %d, %d, %d, %d, %d, '%s')
                """%(data['lat'], data['lon'], data['nprib'], str(data['datetime']),
                    str(dt), data['sluj'], data['sluj2'],  data['speed'],  data['angle'],
                    data['alt'], koderror, error[1])
            #print self.mysql.sql
            self.mysql.execsql(sql)
        except Exception, e:
            print "readdatasock.readPacketShkiper.insertError ",e
    ''

class sendData:
    def __init__(self,ip,port):
        self.ip = ip
        self.port = port
        self.connect()
    ''    
    def send(self,data):
        try:
            self.sock.send(data)
        except Exception, e:
            print e
            self.connect()
    ''    
    def connect(self):
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.settimeout(10)
            self.sock.connect((self.ip,self.port))
        except Exception, e:
            print "Connection %s:%d error"%(self.ip,self.port),e
    ''

class readPacketShkiper(threading.Thread):
    def __init__(self, channel, details, id, typePribor):#type 0 - шкипер-01; 1 - шкипер-01Е
        threading.Thread.__init__(self)
        self.channel = channel
        self.details = details
        self.typePribor = typePribor
        self.id = id
        if self.typePribor == 0:
            self.port = 21001
        if self.typePribor == 1:
            self.port = 21003
        self.rr = 1
        self.delthread = 0
        self.forward = []
        self.fl_mysql = 0
        
    ''
    def run(self):
        if self.fl_mysql:
            self.saveToMysql = writeSqlData()
            self.saveToMysql.dec.type = self.typePribor 
        send = []
        for i in self.forward:
            send += [sendData(i,self.port)]
        self.channel.settimeout(70)
        while self.rr:
            try:
                data = self.channel.recv(1024)
                if not data:
                    self.rr = 0
                    print "\nEnd thread %d\n"%(self.id)
                else:
                    print  "from %s:%d received %d byte"%(self.details[0],self.details[1], len(data))
                    for i in send:
                        i.send(data)
                    if self.fl_mysql:
                        self.saveToMysql.sendData(data)
            except Exception, e:
                print "sockserver.server.readPacketShkiper.run ",e
                self.rr = 0
        self.channel.close()
        self.delthread = 1
        while self.delthread:
            time.sleep(1)
        print "end thread-%d %s:%d"%(self.id,self.details[0],self.details[1])
    ''


class listenServer(threading.Thread):
    def __init__(self,ip = '10.1.12.5',port = 21001,typepribor = 0):
        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.typepribor = typepribor
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.fl_connect = 1
        self.fl_close = 0
        self.pool = []
        self.id = 0
        self.delserver = 0
        self.forward = []
        self.fl_mysql = 0
    ''
    def connect(self):#реконнект, если порт занят
        if self.fl_connect:
            try:
                self.server.bind((self.ip, self.port))
                self.server.listen(200)
                self.fl_connect = 0
                print "Listen port %s:%d"%(self.ip, self.port)
            except:
                print "Port denied %s:%d"%(self.ip, self.port)
                time.sleep(1)
    ''
    def run(self):#основной цикл - плодим потоки сокетов
        while self.fl_connect and self.fl_close == 0:
            self.connect()
        self.rr = 1 - self.fl_close
        while self.rr:
            try:
                self.server.settimeout(1)
                channel, details = self.server.accept()
                self.addthread(channel, details)
            except socket.timeout:
                pass
            except Exception, e:
                print "Error add thread or accept server %s:%d"%(self.ip,self.port)
                print e
                self.rr = 0
            self.delthread()
        for i in self.pool:
            i.rr = 0
        while len(self.pool)>0:
            self.delthread()
            time.sleep(1)
        self.server.close()
        #выставляем флаг завершения и ждем команды от родителя на завершение
        self.delserver = 1
        while self.delserver:
            time.sleep(1)
        print "end server %s:%d"%(self.ip,self.port)
    ''
    def addthread(self,channel, details):
        k = readPacketShkiper(channel, details,self.id,self.typepribor)
        k.forward = self.forward
        k.fl_mysql = self.fl_mysql
        k.start()
        self.pool += [k]
        self.id += 1
        print "Thread added type = %d id = %d count = %d"%(self.typepribor,self.id,len(self.pool))
    ''
    def delthread(self):#проверка на выставленный флаг у потока, что он хочет закрыться
        j = 0
        needdel = []
        for i in self.pool:
            if i.delthread:
                i.delthread = 0
                needdel += [j]
                print "Thread deleted type = %d id = %d count = %d"%(self.typepribor,i.id,len(self.pool))
            j += 1
        needdel.sort(None,None,True)
        for i in needdel:
            if len(self.pool)==1:
                self.pool = []
            else:
                self.pool = self.pool[:i] + self.pool[i+1:]
    ''

#получение команд от внешнего источника
class mainServer:
    def __init__(self):
        self.run = 1
        self.forward = []
        self.fl_mysql = 0
        self.readConfig()
        self.initServer()

    def readConfig(self):
        ff = open('config')
        s = ff.readlines()
        for i in s:
            conf = i.split()
            print conf
            if conf[0] == 'ip':
                self.ip = conf[1] 
            if conf[0] == 'port':
                self.port = int(conf[1])
            if conf[0] == 'portshkiper01':
                self.portshkiper01 = int(conf[1])
            if conf[0] == 'portshkiper01e':
                self.portshkiper01e = int(conf[1])
            if conf[0] == 'forward':
                self.forward += [conf[1]]
            if conf[0] == 'mysql':
                self.fl_mysql = int(conf[1])

    def initServer(self):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        rr = 1
        n = 0
        while rr:
            try:
                self.server.bind((self.ip, self.port))
                rr = False
                print "ASDM server listen"
            except:
                n += 1
                print "Socket denied %d"%n
                time.sleep(1)
        self.server.listen(1)

    def start(self):
        self.ls = listenServer(self.ip,self.portshkiper01,0)
        self.ls.forward = self.forward
        self.ls.fl_mysql = self.fl_mysql
        self.ls.start()
        self.ls1 = listenServer(self.ip,self.portshkiper01e,1)
        self.ls1.forward = self.forward
        self.ls1.fl_mysql = self.fl_mysql
        self.ls1.start()
        while self.run:
            self.server.settimeout(10)
            try:
                self.channel, details = self.server.accept()
                print "Connect from: ",details
                hl = 0
                if hl == 0:#ожидаем приветствие
                    data = self.channel.recv(1024)
                    data = data.split()
                    if len(data)>1:
                        if data[0] == 'client' and data[1] == 'version' and data[2] == '1.0':
                            hl =1
                    data1 = 'Server ASDM version 1.0'
                    try:
                        self.channel.send(data1)
                    except socket.timeout:
                        pass
                    except Exception, e:
                        print e
                if hl == 1:#если приветствие получено
                    data = self.channel.recv(1024)
                    data = data.split()
                    print data
                    if len(data)>0:
                        if data[0] == 'stop':
                            data1 = self.stopServer()
                        if data[0] == 'get':
                            data1 = self.getThreadData()
                        if len(data)>1:
                            if data[0] == 'forward':
                                data1 = self.forwardData(data[1])
                            if data[0] == 'stopforward':
                                data1 = self.stopForwardData(data[1])
                            if data[0] == 'mysql':
                                data1 = self.setMysql()
                            if data[0] == 'stopmysql':
                                data1 = self.stopMysql()
                    try:
                        self.channel.send(data1)
                    except socket.timeout:
                        pass
                    except Exception, e:
                        print e
                    hl = 0
                self.channel.close()
            except socket.timeout:
                pass
            except Exception, e:
                print e
        while self.ls.delserver == 0:
            time.sleep(1)
        self.ls.delserver = 0
        while self.ls1.delserver == 0:
            time.sleep(1)
        self.ls1.delserver = 0
        time.sleep(5)
        print 'Server ASDM version 1.0 stop'

    def stopServer(self):
        self.run = 0
        self.ls.rr = 0
        self.ls.fl_close = 1
        self.ls1.rr = 0
        self.ls1.fl_close = 1
        return 'Server ASDM now stoping...'

    def getThreadData(self):
        ff = open('serverthread','wb')
        s = "Server 1 (%d threading)"%len(self.ls.pool)
        print (s)
        ff.write("%s\n"%s)
        for i in self.ls.pool:
            s = "   %s:%d %d"%(i.details[0],i.details[1],i.id)
            print (s)
            ff.write("%s\n"%s)
        s = "Server 2 (%d threading)"%len(self.ls1.pool)
        print (s)
        ff.write("%s\n"%s)
        for i in self.ls1.pool:
            s = "   %s:%d %d"%(i.details[0],i.details[1],i.id)
            print (s)
            ff.write("%s\n"%s)
        ff.close()
        return 'Thread data printing'

    def forwardData(self,ip):
        self.ls.forward += [ip]
        self.ls1.forward += [ip]
        for i in self.ls.pool:
            i.rr = 0
        for i in self.ls1.pool:
            i.rr = 0
        return 'Data forwarding begining'

    def stopForwardData(self,ip):
        j = 0
        for i in self.ls.forward:
            if i == ip:
                if len(self.ls.forward)==1:
                    self.ls.forward = []
                else:
                    self.ls.forward = self.ls.forward[:j] + self.ls.forward[j+1:]
        j = 0
        for i in self.ls1.forward:
            if i == ip:
                if len(self.ls1.forward)==1:
                    self.ls1.forward = []
                else:
                    self.ls1.forward = self.ls1.forward[:j] + self.ls1.forward[j+1:]
        for i in self.ls.pool:
            i.rr = 0
        for i in self.ls1.pool:
            i.rr = 0
        return 'Data forwrding stoping'
    ''
    def setMysql(self):
        self.fl_mysql = 1
        self.ls.fl_mysql = 1
        self.ls1.fl_mysql = 1
        for i in self.ls.pool:
            i.rr = 0
        for i in self.ls1.pool:
            i.rr = 0
        return 'Data save to mysql begining'

    def stopMysql(self):
        self.fl_mysql = 0
        j = 0
        for i in self.ls.forward:
            if i == self.ip:
                if len(self.ls.forward)==1:
                    self.ls.forward = []
                else:
                    self.ls.forward = self.ls.forward[:j] + self.ls.forward[j+1:]
        j = 0
        for i in self.ls1.forward:
            if i == self.ip:
                if len(self.ls1.forward)==1:
                    self.ls1.forward = []
                else:
                    self.ls1.forward = self.ls1.forward[:j] + self.ls1.forward[j+1:]
        for i in self.ls.pool:
            i.rr = 0
        for i in self.ls1.pool:
            i.rr = 0
        return 'Data save to mysql stop'


if __name__ == "__main__":
    m = mainServer()
    m.start()