#!/usr/bin/python
# -*- coding: utf-8  -*-
from view import view, viewtable, viewtable2, viewfreezetable
from PyQt4 import QtGui, QtCore

class editWind(QtGui.QDialog):#вспомогательное диалоговое окно
    def __init__(self, parent = None):
        QtGui.QDialog.__init__(self, parent)
        self.grid = QtGui.QGridLayout(self)
        self.btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(self.btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(self.btn1 , QtCore.SIGNAL("rejected()"), self.reject)

    def testInitWidget(self):
        self.lbl1 = QtGui.QLabel(u'Название')
        self.ed1 = QtGui.QLineEdit()
        self.grid.addWidget(self.lbl1,0,0)
        self.grid.addWidget(self.ed1,0,1)
        self.grid.addWidget(self.btn1,1,0,1,2)
#####################################################################
class control(view):
    def __init__(self, parent = None, mysql = None):
        view.__init__(self, parent, mysql)
        self.mode = 0 #режим добавления или изменения

    def initWidget(self):#изменяем виджет, т.е. в контроле виджет будет выглядеть иначе, чем просто в просмотре
        grid = QtGui.QGridLayout(self)
        self.list = QtGui.QListWidget(self)#основной виджет, куда выводим список данных
        self.tool = QtGui.QToolBar()
        self.initConnect()
        self.tool.addAction(self.addDataAct)
        self.tool.addAction(self.editDataAct)
        self.tool.addAction(self.delDataAct)
        self.tool.setIconSize(QtCore.QSize(48,48))
        grid.addWidget(self.tool)
        grid.addWidget(self.list)
        self.redraw([0])

    def initConnect(self):
        self.addDataAct = QtGui.QAction(u'Добавить',self)
        self.addDataAct.setIcon(QtGui.QIcon("../ico/dbplus2.png"))
        self.editDataAct = QtGui.QAction(u'Изменить',self)
        self.editDataAct .setIcon(QtGui.QIcon("../ico/db_update.png"))
        self.delDataAct = QtGui.QAction(u'Удалить',self)
        self.delDataAct .setIcon(QtGui.QIcon("../ico/dbmin2.png"))
        self.connect(self.addDataAct, QtCore.SIGNAL("triggered()"),self.addDataControl)
        self.connect(self.editDataAct, QtCore.SIGNAL("triggered()"),self.editDataControl)
        self.connect(self.delDataAct, QtCore.SIGNAL("triggered()"),self.delDataControl)
        self.edit = editWind(self)
        self.connect(self.edit, QtCore.SIGNAL("accepted()"),self.save)

    def addDataControl(self):
        self.edit.setWindowTitle(u'Добавить карточку')        
        self.edit.setModal(1)
        self.edit.show()
        self.mode = 1

    def editDataControl(self):
        self.edit.setWindowTitle(u'Изменить карточку')        
        self.edit.setModal(1)
        self.edit.show()
        self.mode = 2

    def delDataControl(self):
        r = self.list.currentRow()
        id = self.datarow[r]
        s = self.list.item(r).text()
        dlg = QtGui.QMessageBox(self)
        dlg.setWindowTitle(u'Удаление записи')
        dlg.setText(u'''Вы действительно хотите удалить %s?'''%s)
        dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
        ok = dlg.exec_()
        if ok==QtGui.QMessageBox.Ok :
            self._delData(id)

    def save(self, datasave = [], dataview = []):
        self.r = 0
        if self.mode == 1:
            self._addData(self.datasave)
        elif self.mode == 2:
            r = self.list.currentRow()
            id = self.datarow[r]
            self._editData(self.datasave+[id])
#####################################################################
class controltable(viewtable):
    def __init__(self, parent = None, mysql = None):
        viewtable.__init__(self, parent, mysql)
        self.edit = editWind(self)
        self.connect(self.edit, QtCore.SIGNAL("accepted()"),self.save)

    def initWidget(self):#изменяем виджет, т.е. в контроле виджет будет выглядеть иначе, чем просто в просмотре
        grid = QtGui.QGridLayout(self)
        self.list = QtGui.QListWidget(self)#основной виджет, куда выводим список данных
        self.tool = QtGui.QToolBar()
        self.initConnect()
        self.tool.addAction(self.addDataAct)
        self.tool.addAction(self.editDataAct)
        self.tool.addAction(self.delDataAct)
        self.tool.setIconSize(QtCore.QSize(48,48))
        grid.addWidget(self.tool)
        grid.addWidget(self.list)
        self.redraw([0])

    def initConnect(self):
        self.addDataAct = QtGui.QAction(u'Добавить',self)
        self.addDataAct.setIcon(QtGui.QIcon("../ico/dbplus2.png"))
        self.editDataAct = QtGui.QAction(u'Изменить',self)
        self.editDataAct .setIcon(QtGui.QIcon("../ico/db_update.png"))
        self.delDataAct = QtGui.QAction(u'Удалить',self)
        self.delDataAct .setIcon(QtGui.QIcon("../ico/dbmin2.png"))
        self.connect(self.addDataAct, QtCore.SIGNAL("triggered()"),self.addDataControl)
        self.connect(self.editDataAct, QtCore.SIGNAL("triggered()"),self.editDataControl)
        self.connect(self.delDataAct, QtCore.SIGNAL("triggered()"),self.delDataControl)

    def addDataControl(self):
        self.edit.setModal(1)
        self.edit.show()
        self.mode = 1

    def editDataControl(self):
        self.edit.setModal(1)
        self.edit.show()
        self.mode = 2

    def delDataControl(self):
        r = self.currentRow()
        try:
            id = self.datarow[r]
            s = ''
            for i in range(self.columnCount()):
                s = "%s %s"%(s,self.item(self.currentRow(),i).text())
            dlg = QtGui.QMessageBox(self)
            dlg.setWindowTitle(u'Удаление записи')
            dlg.setText(u'''Вы действительно хотите удалить %s?'''%s)
            dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
            if ok==QtGui.QMessageBox.Ok :
                self._delData(id)
        except:
            print "nothing to delete"

    def save(self, datasave = [], dataview = []):
        self.r = 0
        if self.mode == 1:
            self._addData(self.datasave)
        elif self.mode == 2:
            r = self.currentRow()
            id = self.datarow[r]
            self._editData(self.datasave+[id])
#####################################################################
class controltable2(viewtable2):
    def __init__(self, parent = None, mysql = None):
        viewtable2.__init__(self, parent, mysql)
        self.edit = editWind(self)
        self.connect(self.edit, QtCore.SIGNAL("accepted()"),self.save)

    def initWidget(self):#изменяем виджет, т.е. в контроле виджет будет выглядеть иначе, чем просто в просмотре
        grid = QtGui.QGridLayout(self)
        self.table = QtGui.QTableWidget(self)#основной виджет, куда выводим список данных
        self.tool = QtGui.QToolBar()
        self.initConnect()
        self.tool.addAction(self.addDataAct)
        self.tool.addAction(self.editDataAct)
        self.tool.addAction(self.delDataAct)
        self.tool.setIconSize(QtCore.QSize(48,48))
        grid.addWidget(self.tool)
        grid.addWidget(self.table)
        self.redraw([0])
        self.initConnect()

    def initConnect(self):
        self.addDataAct = QtGui.QAction(u'Добавить',self)
        self.addDataAct.setIcon(QtGui.QIcon("../ico/dbplus2.png"))
        self.editDataAct = QtGui.QAction(u'Изменить',self)
        self.editDataAct .setIcon(QtGui.QIcon("../ico/db_update.png"))
        self.delDataAct = QtGui.QAction(u'Удалить',self)
        self.delDataAct .setIcon(QtGui.QIcon("../ico/dbmin2.png"))
        self.connect(self.addDataAct, QtCore.SIGNAL("triggered()"),self.addDataControl)
        self.connect(self.editDataAct, QtCore.SIGNAL("triggered()"),self.editDataControl)
        self.connect(self.delDataAct, QtCore.SIGNAL("triggered()"),self.delDataControl)

    def addDataControl(self):
        self.edit.setModal(1)
        self.edit.show()
        self.mode = 1

    def editDataControl(self):
        self.edit.setModal(1)
        self.edit.show()
        self.mode = 2

    def delDataControl(self):
        r = self.currentRow()
        try:
            id = self.datarow[r]
            s = ''
            for i in range(self.table.columnCount()):
                s = "%s %s"%(s,self.item(self.table.currentRow(),i).text())
            dlg = QtGui.QMessageBox(self)
            dlg.setWindowTitle(u'Удаление записи')
            dlg.setText(u'''Вы действительно хотите удалить %s?'''%s)
            dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
            if ok==QtGui.QMessageBox.Ok :
                self._delData(id)
        except:
            print "nothing to delete"

    def save(self, datasave = [], dataview = []):
        self.r = 0
        if self.mode == 1:
            self._addData(self.datasave)
        elif self.mode == 2:
            r = self.table.currentRow()
            id = self.datarow[r]
            self._editData(self.datasave+[id])            
#####################################################################        
class controlfreezetable(viewfreezetable):
    def __init__(self, parent = None, mysql = None):
        viewfreezetable.__init__(self, parent, mysql)
        self.initAction()
    
    def initAction(self):
        self.connect(self.horizontalHeader(),QtCore.SIGNAL('sectionResized(int,int,int)'), self.updateSectionWidth)
        self.connect(self.verticalHeader(),QtCore.SIGNAL('sectionResized(int,int,int)'), self.updateSectionHeight)
        self.connect(self.frozenTableView.verticalScrollBar(), QtCore.SIGNAL('valueChanged(int)'),
               self.verticalScrollBar(), QtCore.SLOT('setValue(int)'))
        self.connect(self.verticalScrollBar(), QtCore.SIGNAL('valueChanged(int)'),
               self.frozenTableView.verticalScrollBar(), QtCore.SLOT('setValue(int)'))
