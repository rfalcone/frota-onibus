#!/usr/bin/python
# -*- coding: utf-8  -*-
from db import db

#ver 1.0
class model:
    '''Абстрактный класс справочников
    
    Переменные:
        * mysql - ссылка на :py:class:`db.db` класс (если не будет объявлена будет создан новый вызов класса)
    Переменные определяемые в классах наследниках
        * data - словарь с ключем id и значением-словарем с ключами хранящимися в dataparam
        * datarow - список ключей для словаря data
        * dataparam - список ключей для значения словаря data
        * getDataSQL - SQL запрос для сбора данных (SELECT id, field FROM table)
        * addDataSQL - SQL запрос для вставки элемента (INSERT INTO table (field) VALUES (%d) )
        * editDataSQL - SQL запрос для изменения элемента (UPDATE table SET field = %d WHERE id = %d )
        * delDataSQL - SQL запрос для удаления или установки метки на удаления элемента (DELETE FROM table WHERE id = %d ) (UPDATE table SET del = 1 WHERE id = %d )'''
    
    
    def __init__(self,mysql=None):
        if mysql == None: #если не передали указатель, то конектимся к базе
            self.mysql = db()
        else:
            self.mysql = mysql

    def _getData(self,data):#tupledata - содержит данные для вывода запроса
        #загрузить данные
        self.datarow = []
        self.data = {}
        try:
            if len(self.getDataSQL)>0:
                try:
                    (f,dr,d) = self.mysql.getData(self.getDataSQL%tuple(data))
                except TypeError:#на случай если нет входных параметров
                    (f,dr,d) = self.mysql.getData(self.getDataSQL)
                except:
                    print self.getDataSQL%tuple(data)
                    raise
                self.data = d #словарь со всеми полями
                self.datarow = dr
        except Exception,e:
            print e


    def _addData(self, data):
        self.mysql.execsql(self.addDataSQL%(tuple(data)))

    def _editData(self, data):
        self.mysql.execsql(self.editDataSQL%(tuple(data)))

    def _delData(self, id):
        self.mysql.execsql(self.delDataSQL%(id))#id

    def _getLastId(self):
        return self.mysql.getlastid()

    def getData(self, data = []):
        '''Метод получения данных, data - список параметров для запроса'''
        
        self._getData(data)
    
    def addData(self, data = []):
        '''Метод вставки данных, data - список параметров для запроса'''
        
        self._addData(data)
        
    def editData(self, data = []):
        '''Метод изменения данных, data - список параметров для запроса'''
        
        self._editData(data)
        
    def delData(self, id = 0):
        '''Метод удаления данных, id - индекс для удаления'''
        
        self._delData(id)
        
    def getLastId(self):
        '''Метод возвращающий id - индекс последнего вставленного элемента'''
        
        return self._getLastId()
        