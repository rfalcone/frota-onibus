#!/usr/bin/python
# -*- coding: utf-8  -*-
from spravochnik.control import control
from PyQt4 import QtGui, QtCore

class editWind(QtGui.QDialog):
    def __init__(self, parent = None):
        QtGui.QDialog.__init__(self, parent)
        grid = QtGui.QGridLayout(self)
        lbl1 = QtGui.QLabel(u'Гаражный номер')
        lbl2 = QtGui.QLabel(u'Гос номер')
        lbl3 = QtGui.QLabel(u'марка автобуса')
        self.ed1 = QtGui.QLineEdit()
        self.ed2 = QtGui.QLineEdit()
        self.ed3 = QtGui.QLineEdit()
        self.btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)

        grid.addWidget(lbl1,0,0)
        grid.addWidget(lbl2,1,0)
        grid.addWidget(lbl3,2,0)
        grid.addWidget(self.ed1,0,1)
        grid.addWidget(self.ed2,1,1)
        grid.addWidget(self.ed3,2,1)
        grid.addWidget(self.btn1,3,0,1,2)


class buscontrol(control):
    def __init__(self, parent = None, mysql = None):
        control.__init__(self, parent, mysql)
        self.edit = editWind(self)
        self.initConnect()

    #переопределение методов
    def getData(self):#получаем список автобусов
        s = '''SELECT id, garnom, gosnom, marka, del
                   FROM bus
                   WHERE del = 0
                   ORDER BY garnom'''
        self.mysql.execsql(s)
        result_set = self.mysql.cursor.fetchall()
        self.data = {}
        self.garnom = {}
        self.datarow = []
        for row in result_set:
            self.data[row["id"]] = [row["garnom"],row["gosnom"],row["marka"]]
            self.garnom[row["gosnom"]] = row["id"]
            self.datarow += [row["id"]]

    def editData(self, id = 0, garnom = 0, gosnom = '', marka = ''):
        if id > 0:
            s = '''UPDATE bus SET garnom = %d, gosnom = '%s',
                marka = '%s' WHERE id=%d '''%(garnom, gosnom, marka,id)
            self.mysql.execsql(s)

    def addData(self, garnom = 0, gosnom = '', marka = ''):
        s = '''INSERT INTO bus (garnom, gosnom, marka)
               VALUES (%d, '%s', '%s')'''%(garnom, gosnom, marka)
        self.mysql.execsql(s)

    def delData(self,id = 0, garnom = 0):
        if id != 0:
            s = '''UPDATE bus SET del = 1 WHERE id=%d '''%id
            self.mysql.execsql(s)
        elif garnom != 0:
            s = '''UPDATE bus SET del = 1 WHERE id=%d '''%self.garnom[garnom]
            self.mysql.execsql(s)

    def addDataControl(self):
        self.edit.ed1.setText('')
        self.edit.ed2.setText('')
        self.edit.ed3.setText('')
        self.edit.setModal(1)
        self.edit.show()
        self.mode = 1

    def editDataControl(self):
        r = self.list.currentRow()
        self.data[self.datarow[r]]
        self.edit.ed1.setText(str(self.data[self.datarow[r]][0]))
        self.edit.ed2.setText(self.data[self.datarow[r]][1])
        self.edit.ed3.setText(self.data[self.datarow[r]][2])
        self.edit.setModal(1)
        self.edit.show()
        self.mode = 2

    def delDataControl(self):
        r = self.list.currentRow()
        id = self.datarow[r]
        dlg = QtGui.QMessageBox(self)
        dlg.setWindowTitle(u'Удаление записи')
        dlg.setText(u'''Вы действительно хотите удалить автобус №%d %s %s?'''%
            (self.data[id][0],self.data[id][1],self.data[id][2]))
        dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
        ok = dlg.exec_()
        if ok==QtGui.QMessageBox.Ok :
            self.delData(id)
            self.redraw()

    def save(self):
        self.edit.close()
        r = self.list.currentRow()
        garnom = int(self.edit.ed1.text())
        gosnom = self.edit.ed2.text()
        marka = self.edit.ed3.text()
        if self.mode == 1:
            self.addData(garnom,gosnom,marka)
        elif self.mode == 2:
            id = self.datarow[r]
            self.editData(id,garnom,gosnom,marka)
        self.redraw()

    def redraw(self):
        self.list.clear()
        self.getData()
        for i in self.datarow:
            a = self.data[i]
            #выводим список данных 0 - гаражный номер, 1 - госномер, 2 - марка
            self.list.addItem("%d %s %s"%(a[0],a[1],a[2]))