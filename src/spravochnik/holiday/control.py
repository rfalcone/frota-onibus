#!/usr/bin/python
# -*- coding: utf-8  -*-
from spravochnik.control import control
from PyQt4 import QtGui, QtCore
from datetime import date

class editWind(QtGui.QDialog):
    def __init__(self, parent = None):
        QtGui.QDialog.__init__(self, parent)
        grid = QtGui.QGridLayout(self)
        lbl1 = QtGui.QLabel(u'Название праздника')
        lbl2 = QtGui.QLabel(u'Дата')
        self.ed1 = QtGui.QLineEdit()
        self.cw = QtGui.QCalendarWidget()
        self.cw.setSelectedDate(QtCore.QDate(date.today()))
        self.btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)

        grid.addWidget(lbl1,0,0)
        grid.addWidget(lbl2,1,0)
        grid.addWidget(self.ed1,0,1)
        grid.addWidget(self.cw,1,1)
        grid.addWidget(self.btn1,2,0,1,2)


class holidaycontrol(control):
    def __init__(self, parent = None, mysql = None):
        control.__init__(self, parent, mysql)
        self.edit = editWind(self)
        self.initConnect()

    #переопределение методов
    def getData(self):#получаем список праздничных дней
        s = '''SELECT id, name, date
                   FROM holiday
                   ORDER BY date'''
        self.mysql.execsql(s)
        result_set = self.mysql.cursor.fetchall()
        self.data = {}
        self.datarow = []
        for row in result_set:
            self.data[row["id"]] = [row["name"],row["date"]]
            self.datarow += [row["id"]]

    def editData(self, id = 0, name = '', mdate = date.today()):
        if id > 0:
            s = '''UPDATE holiday SET name = '%s', date ='%s'
                   WHERE id=%d '''%(name, str(mdate), id)
            self.mysql.execsql(s)

    def addData(self, name = '', mdate = date.today()):
        s = '''INSERT INTO holiday (name, date)
               VALUES ('%s', '%s')'''%(name, str(mdate))
        self.mysql.execsql(s)

    def delData(self,id = 0):
        if id != 0:
            s = '''DELETE FROM holiday WHERE id=%d '''%id
            self.mysql.execsql(s)
        
    def addDataControl(self):
        self.edit.ed1.setText('')
        self.edit.cw.setSelectedDate(QtCore.QDate(date.today()))
        self.edit.setModal(1)
        self.edit.show()
        self.mode = 1

    def editDataControl(self):
        r = self.list.currentRow()
        self.data[self.datarow[r]]
        self.edit.ed1.setText(self.data[self.datarow[r]][0])
        self.edit.cw.setSelectedDate(QtCore.QDate(self.data[self.datarow[r]][1]))
        self.edit.setModal(1)
        self.edit.show()
        self.mode = 2

    def delDataControl(self):
        r = self.list.currentRow()
        id = self.datarow[r]
        dlg = QtGui.QMessageBox(self)
        dlg.setWindowTitle(u'Удаление записи')
        dlg.setText(u'''Вы действительно хотите удалить праздничный день "%s"?'''%
            (self.data[id][0]))
        dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
        ok = dlg.exec_()
        if ok==QtGui.QMessageBox.Ok :
            self.delData(id)
            self.redraw()

    def save(self):
        self.edit.close()
        r = self.list.currentRow()
        name = self.edit.ed1.text()
        mdate = self.edit.cw.selectedDate().toPyDate()
        
        if self.mode == 1:
            self.addData(name,mdate)
        elif self.mode == 2:
            id = self.datarow[r]
            self.editData(id,name,mdate)
        self.redraw()

    def redraw(self):
        self.list.clear()
        self.getData()
        for i in self.datarow:
            a = self.data[i]
            #выводим список данных 0 - гаражный номер, 1 - госномер, 2 - марка
            self.list.addItem("%s %s"%(a[0],a[1]))