#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from karta import karta
from kartarange import kartarange
from kartarangemarsh import kartarangemarsh

class kartaedit(QtGui.QDialog):
    def __init__(self, parent = None, mysql = None):
        QtGui.QDialog.__init__(self, parent)
        self.krt = karta(self,mysql)
        self.krt.redraw([0])
        self.krtrng = kartarange(self,mysql)
        self.krtrngmrsh = kartarangemarsh(self,mysql)
        self.initWidget()
        self.initAction()
        self.krt.list.setCurrentRow(0)
        self.krtrng.list.setCurrentRow(0)

    def initWidget(self):
        self.grid = QtGui.QGridLayout(self)
        spl1 = QtGui.QSplitter(QtCore.Qt.Horizontal)
        spl2 = QtGui.QSplitter(QtCore.Qt.Vertical)
        spl2.addWidget(self.krtrng)
        spl2.addWidget(self.krtrngmrsh)
        spl1.addWidget(self.krt)
        spl1.addWidget(spl2)
        self.grid.addWidget(spl1)

    def initAction(self):
        self.connect(self.krt.list, QtCore.SIGNAL("itemSelectionChanged ()"),self.krtChange)
        self.connect(self.krtrng.list, QtCore.SIGNAL("itemSelectionChanged ()"),self.krtrngChange)

    def krtChange(self):
        if self.krt.list.count()>0:
            id = self.krt.datarow[self.krt.list.currentRow()]
            self.krtrng.redraw([id])
            self.krtrng.list.setCurrentRow(0)
            self.krtrngChange()

    def krtrngChange(self):
        if self.krtrng.list.count()>0:
            id = self.krtrng.datarow[self.krtrng.list.currentRow()]
            self.krtrngmrsh.redraw([id])
        else:
            self.krtrngmrsh.redraw([0])

