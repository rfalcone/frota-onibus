#!/usr/bin/python
# -*- coding: utf-8  -*-
from spr.control import control
from kartarangeday import kartarangeday
from PyQt4 import QtGui, QtCore

class editWind(QtGui.QDialog):#вспомогательное диалоговое окно
    def __init__(self, parent = None):
        QtGui.QDialog.__init__(self, parent)

    def initWidget(self, namelabel = [],mysql=None):
        grid = QtGui.QGridLayout(self)
        self.rangeday = kartarangeday(self,mysql)#во вспомогательном окне содержим экземпляр работы с датами действия
        k = 0
        self.chbx = []
        for i in namelabel:
            self.chbx += [QtGui.QCheckBox(i)]
            self.chbx[k].setCheckState(2)
            grid.addWidget(self.chbx[k],k,0)
            k += 1
        self.chbx[12].setCheckState(0)
        grid.addWidget(self.rangeday,0,1,0,k-1)
        self.btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(self.btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(self.btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        grid.addWidget(self.btn1,k,0)
        self._setAction()

    def _blockSignals(self,state):
        for k in range(0,len(self.chbx)):
            self.chbx[k].blockSignals(state)
    def _setAction(self):
        for k in range(0,5):
            self.connect(self.chbx[k],QtCore.SIGNAL('stateChanged(int)'),self._setWorkDay)
        for k in range(5,7):
            self.connect(self.chbx[k],QtCore.SIGNAL('stateChanged(int)'),self._setWeekDay)
        self.connect(self.chbx[7],QtCore.SIGNAL('stateChanged(int)'),self._setWork)
        self.connect(self.chbx[8],QtCore.SIGNAL('stateChanged(int)'),self._setWeek)
        for k in range(9,11):
            self.connect(self.chbx[k],QtCore.SIGNAL('stateChanged(int)'),self._setHoly)
        self.connect(self.chbx[11],QtCore.SIGNAL('stateChanged(int)'),self._setAllDay)
        self.connect(self.chbx[12],QtCore.SIGNAL('stateChanged(int)'),self._setNotHoliDay)

    def _setNotHoliDay(self,chk):
        self._blockSignals(True)
        if chk == 2:
            self.chbx[9].setCheckState(0)
            self.chbx[11].setCheckState(0)
        self._blockSignals(False)
    def _setWorkDay(self,chk):
        self._blockSignals(True)
        if chk == 0:
            self.chbx[7].setCheckState(0)
            self.chbx[11].setCheckState(0)
        if chk ==2:
            f = 1
            for k in range(0,5):
                if self.chbx[k].checkState()==0:
                    f = 0
            if f == 1: self.chbx[7].setCheckState(2)
            for k in range(5,11):
                if self.chbx[k].checkState() == 0:
                    f = 0
            if f == 1: self.chbx[11].setCheckState(2)
        self._blockSignals(False)
    def _setWeekDay(self,chk):
        self._blockSignals(True)
        if chk == 0:
            self.chbx[8].setCheckState(0)
            self.chbx[11].setCheckState(0)
        if chk ==2:
            f = 1
            for k in range(5,7):
                if self.chbx[k].checkState()==0:
                    f = 0
            if f == 1: self.chbx[8].setCheckState(2)
            for k in range(0,11):
                if self.chbx[k].checkState()==0:
                    f = 0
            if f == 1: self.chbx[11].setCheckState(2)
        self._blockSignals(False)
    def _setWork(self,chk):
        self._blockSignals(True)
        for k in range(0,5):
            self.chbx[k].setCheckState(chk)
        if chk == 0:
            self.chbx[11].setCheckState(0)
        if chk ==2:
            f = 1
            for k in range(0,11):
                if self.chbx[k].checkState() == 0:
                    f = 0
            if f == 1: self.chbx[11].setCheckState(2)
        self._blockSignals(False)
    def _setWeek(self,chk):
        self._blockSignals(True)
        for k in range(5,7):
            self.chbx[k].setCheckState(chk)
        if chk == 0:
            self.chbx[11].setCheckState(0)
        if chk ==2:
            f = 1
            for k in range(0,11):
                if self.chbx[k].checkState() == 0:
                    f = 0
            if f == 1: self.chbx[11].setCheckState(2)
        self._blockSignals(False)
    def _setAllDay(self,chk):
        self._blockSignals(True)
        for k in range(0,11):
            self.chbx[k].setCheckState(chk)
        self.chbx[12].setCheckState(0)
        self._blockSignals(False)
    def _setHoly(self,chk):
        self._blockSignals(True)
        if chk == 0:
            self.chbx[11].setCheckState(0)
        if chk == 2:
            f = 1
            for k in range(0,11):
                if self.chbx[k].checkState() == 0:
                    f = 0
            if f == 1: self.chbx[11].setCheckState(2)
            self.chbx[12].setCheckState(0)
        self._blockSignals(False)

class kartarange(control):
    def __init__(self, parent = None, mysql = None):
        self.initSql()
        control.__init__(self, parent, mysql)
        self.edit = editWind(self)
        self.edit.initWidget(self.namelabel,mysql)
        self.rangeday = self.edit.rangeday #привязка по числам
        self.connect(self.edit, QtCore.SIGNAL("accepted()"),self.save)
        self.dataview = ["monday", "tuesday", "wednesday",
            "thursday", "friday", "saturday", "sunday", "workday", "weekend",
            "holiday", "bholiday", "allday","noholiday"]

    def initSql(self):
        fl = 0#флаг, чтобы после SELECT не писало запятую
        #список полей, сохраняемые в структуре
        self.dataparam = ["id", "idkarta", "monday", "tuesday", "wednesday",
            "thursday", "friday", "saturday", "sunday", "workday", "weekend",
            "holiday", "bholiday", "allday", "noholiday"]
        self.namelabel = [u"Понедельник", u"Вторник", u"Среда",
            u"Четверг", u"Пятница", u"Суббота", u"Воскресенье", u"Рабочие дни", u"Выходные",
            u"Праздничные", u"Предпраздничные", u"Все дни", u"Кроме праздничных"]
        #######################################################################
        #создаем запрос на получение списка данных
        for i in self.dataparam:
            if fl:
                self.getDataSQL = '%s, %s'%(self.getDataSQL,i)
            else:
                self.getDataSQL = 'SELECT %s'%(i)
                fl = 1
        self.getDataSQL = self.getDataSQL + ''' FROM kartarange
                   WHERE del = 0 AND idkarta = %d'''#%d - здесь ожидаем, что запрос будет выводится при наличии входного параметра
        #######################################################################
        #запрос на добавление
        s = '' #складываем имена полей, через запятую
        d = '' #складываем их значение в VALUES через запятую
        self.addDataSQL = 'INSERT INTO kartarange ('
        for i in self.dataparam[1:]:#пропускаем id
            s = "%s, %s"%(s,i)
            d = d + ", %d"
        self.addDataSQL = self.addDataSQL + s[2:] +') VALUES (' + d[2:] + ')'
        #######################################################################
        #запрос на изменение
        s = ''
        self.editDataSQL = '''UPDATE kartarange SET '''
        for i in self.dataparam[2:]:#пропускаем id и idkarta
            s = s + "%s ="%i + "%d, "
        self.editDataSQL = self.editDataSQL + s[:-2] + ' WHERE id=%d '
        #######################################################################
        #запрос на удаление
        self.delDataSQL ='UPDATE kartarange SET del = 1 WHERE id = %d'

    def addDataControl(self):
        self.rangeday.redraw([0])#ставим временный id=0, при сохранении в kartarangeday изменяем на правильный
        self.edit._blockSignals(True)
        for k in range(0,len(self.namelabel)):
            self.edit.chbx[k].setCheckState(2)
        self.edit.chbx[12].setCheckState(0)#Кроме выходных выключаем
        self.edit._blockSignals(False)
        self.edit.setModal(1)
        self.edit.show()
        self.mode = 1

    def editDataControl(self):
        r = self.list.currentRow()#получаем номер строки
        id = self.datarow[r]#получаем id
        st = self.data[id]#получаем словарь под этим id
        self.rangeday.redraw([id])
        self.edit._blockSignals(True)#блокируем сигналы
        k = 0
        for dv in self.dataview:#проходимся по именам словаря
            self.edit.chbx[k].setCheckState(st[dv])#получаем состояние словаря по его имени
            k += 1
        self.edit._blockSignals(False)
        self.edit.setModal(1)
        self.edit.show()
        self.mode = 2

    def save(self):
        r = 0
        data = []
        for k in range(len(self.dataview)):#проходимся по именам словаря
            data += [self.edit.chbx[k].checkState()]#получаем состояние чека по его имени
            k += 1
        if self.mode == 1:
            self._addData([self.idkarta]+data)
            self.rangeday.updateTemp(self._getLastId())
        elif self.mode == 2:
            r = self.list.currentRow()
            id = self.datarow[r]
            self._editData(data+[id])
        self.redraw([self.idkarta])
        self.list.setCurrentRow(r)


    def delDataControl(self):
        control.delDataControl(self)
        self.redraw([self.idkarta])

    def redraw(self,data):#data = [idkarta]
        self.list.clear()
        self._getData(data)
        self.idkarta = data[0]#запоминаем idkarta для добавления или изменения
        for id in self.datarow:#проходим по id
            st = []
            for j in self.dataview:
                st += [self.data[id][j]]
            s = self.formatstr(st)
            #необходима еще отрисовка дней
            sdate = ''#сюда складываем все полученные промежутки
            self.rangeday._getData([id])#получаем данные только от model
            for id2 in self.rangeday.datarow:
                s2 = []
                for j2 in self.rangeday.dataview:
                    s2 += [str(self.rangeday.data[id2][j2])]
                sdate = "%s [%s --- %s]"%(sdate,s2[0],s2[1])
            self.list.addItem("%s %s"%(s[0],sdate))

    def formatstr(self,week):#data соответстиве self.dataview и данными
        b = 0 #флаг для объединения дней
        c = 0
        rab = '' #строка для рабочих дней
        vih = '' #строка для выходных дней
        st = ''
        if week[0] == 2:
            b = 1
            rab = u'Пн'
        if week[1] == 2:
            if b == 1:
                rab += u', вт'
            else:
                rab = u'Вт'
                b = 1
        if week[2] == 2:
            if b == 1:
                rab += u', ср'
            else:
                rab = u'Ср'
                b = 1
        if week[3] == 2:
            if b == 1:
                rab += u', чт'
            else:
                rab = u'Чт'
                b = 1
        if week[4] == 2:
            if b == 1:
                rab += u', пт'
            else:
                rab = u'Пт'
                b = 1
        if week[5] == 2:
            if b == 1:
                vih += u', сб'
            else:
                vih = u'Сб'
                c = 1
        if week[6] == 2:
            if b == 1:
                vih += u', вс'
            else:
                vih = u'Вс'
                c = 1
        if week[7] == 2:
            b = 1
            st = u'По рабочим'
            rab = ''
        if week[8] == 2:
            if b == 1:
                st += u', выходным'
                vih = ''
            else:
                st = u'По выходным'
                vih = ''
                b = 1
        st = rab + st + vih
        if week[9] == 2:
            if b == 1:
                st += u', праздничным'
            else:
                st = u'По праздничным'
                b = 1
        if week[10] == 2:
            if b == 1:
                st += u', предпраздничным'
            else:
                st = u'По предпраздничным'
        if week[12] == 2:
            if b == 1:
                st += u', кроме праздничных'
            else:
                st = u'Кроме праздничных'
        #если все дни, то все дни, что ж тут сделаешь
        if week[11] == 2:
            st = u'Все дни'
        #print st
        return [st,week]

