#!/usr/bin/python
# -*- coding: utf-8  -*-
from db import db

class model:
    def __init__(self,mysql=None):
        if mysql == None: #если не передали указатель, то конектимся к базе
            self.mysql = db()
        else:
            self.mysql = mysql
        self.data = {}
        self.nametable = 'marshrut'

    def getData(self):#получаем список автобусов
        s = '''SELECT id, name, del
                   FROM %s
                   WHERE del = 0
                   ORDER BY name'''%self.nametable
        self.mysql.execsql(s)
        result_set = self.mysql.cursor.fetchall()
        self.data = {}
        self.datarow = []
        for row in result_set:
            self.data[row["id"]] = row["name"]
            self.datarow += [row["id"]]

    def editData(self, id = 0, name = ''):
        if id > 0:
            s = '''UPDATE %s SET name = '%s'
                   WHERE id = %d '''%(self.nametable, name, id)
            self.mysql.execsql(s)

    def addData(self, name = ''):
        s = '''INSERT INTO %s (name)
               VALUES ('%s')'''%(self.nametable, name)
        self.mysql.execsql(s)

    def delData(self,id = 0):
        if id != 0:
            s = '''UPDATE %s SET del = 1
                   WHERE id=%d '''%(self.nametable, id)
            self.mysql.execsql(s)
