#!/usr/bin/python
# -*- coding: utf-8  -*-
from control import control
from PyQt4 import QtGui, QtCore

class form(QtGui.QDialog):
    def __init__(self, parent = None, mysql = None):
        QtGui.QDialog.__init__(self, parent)
        self.c = control(self, mysql)
        grid = QtGui.QGridLayout(self)
        tool = QtGui.QToolBar()
        tool.addAction(self.c.addDataAct)
        tool.addAction(self.c.editDataAct)
        tool.addAction(self.c.delDataAct)
        grid.addWidget(tool)
        grid.addWidget(self.c)