#!/usr/bin/python
# -*- coding: utf-8  -*-
from viewmap import viewmap
import pygame
from pygame.locals import *
import time

class controlmap(viewmap):
    def __init__(self):
        viewmap.__init__(self)
        self.runflag = 1
        self.moveflag = 0
        self.xk = 0
        self.yk = 0
        self.x1 = self.y1 = 0

    def calcfps(self):
        self.fps += 1
        if time.time()-self.st>1:
            print "fps = %d"%self.fps 
            self.fps = 0
            self.st = time.time()

    def run(self):
        self.loadnotile()
        self.loadtile()
        self.redraw()
        self.draw()
        self.fps = 0
        self.st = time.time()
        while self.runflag:
            self.readevent()
            pygame.display.update()
            self.calcfps()
        pygame.quit()

    def readevent(self):
        for event in pygame.event.get():
            #self.keyb(event)
            if event.type == QUIT: self.runflag = 0
            if event.type == VIDEORESIZE:
                self.mode = event.size
                self.initScreen()
                self.loadtile()
                self.redraw()
                self.draw()
            if event.type == MOUSEMOTION:
                self.xk, self.yk = event.pos
                if self.moveflag: self.moveMap(event.pos)
            if event.type == MOUSEBUTTONUP:
                if event.button == 1:
                    if self.moveflag: self.endMoveMap(event.pos)
            if event.type == MOUSEBUTTONDOWN:
                if event.button == 1: self.beginMoveMap(event.pos)
                if event.button == 4:self.zoomUpMap(event.pos)
                if event.button == 5:self.zoomDownMap(event.pos)

    def beginMoveMap(self,eventpos):
        self.xm, self.ym = eventpos#смещение от угла экрана
        self.x1 = self.x #старые координаты для проверки необходимости подгрузки
        self.y1 = self.y #при прохождении расстояния 256
        self.moveflag = 1
    ''
    def moveMap(self,eventpos):
        #если угол карты остается меньше угла экрана, то двигаем карту
        xm1, ym1  = eventpos
        dxm = xm1 - self.xm
        dym = ym1 - self.ym
        self.x = self.x1 + dxm
        self.y = self.y1 + dym
        if not (-256 < dxm < 256 and -256< dym < 256):
            self.xm = xm1
            self.ym = ym1
            self.x1 = self.x
            self.y1 = self.y
            self.loadtile()
        self.redraw()
        self.draw()
        
    ''
    def endMoveMap(self,eventpos):
        self.moveflag = 0
        self.loadtile()
        self.redraw()
        self.draw()

    def zoomUpMap(self,eventpos):#приближение
        #увеличиваем зум
        if self.zoom < 17:
            self.zoom += 1
            if self.xyt[self.zoom-1][0] != self.xyt[self.zoom][0]*2:
                self.x += 128
            if self.xyt[self.zoom-1][1] != self.xyt[self.zoom][1]*2:
                self.y += 128
            x = - self.mode[0]/2 #eventpos[0] - self.mode[0]/2
            y = - self.mode[1]/2#eventpos[1] - self.mode[1]/2
            self.x = (self.x)*2 + x
            self.y = (self.y)*2 + y
            #if self.getChange()>0:
            #    self.createpng()
            self.loadtile()
            self.redraw()
            self.draw()
    ''
    def zoomDownMap(self,eventpos):#приближение
        #увеличиваем зум
        if self.zoom >0:
            self.zoom -= 1
            if self.xyt[self.zoom+1][0] != self.xyt[self.zoom][0]*2:
                self.x -= 128
            if self.xyt[self.zoom+1][1] != self.xyt[self.zoom][1]*2:
                self.y -= 128

            x = - self.mode[0]/2 #eventpos[0] - self.mode[0]/2
            y = - self.mode[1]/2#eventpos[1] - self.mode[1]/2
            self.x = (self.x - x)/2
            self.y = (self.y - y)/2
            #if self.getChange()>0:
            #    self.createpng()
            
            if self.x>0 or self.y>0:
                self.screen.fill((255,255,255,0))
            self.loadtile()
            self.redraw()
            self.draw()