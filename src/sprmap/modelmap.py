#!/usr/bin/python
# -*- coding: utf-8  -*-
import tilenames

class modelmap:
    def __init__(self):
        self.path = '/home/dark123us/osm/tiles2/'#где храним тайлы
        self.xyt = self.initTilesXY()#храним [x,y,x1,y1] для каждого зума карты
        self.zoom = 12 #начальный уровень зума
        self.x = -26216 #смещение правого края карты
        self.y = -36311 #смещение верхнего края карты
        self.tileimage = []#углы загруженных тайлов x,y,тип картинки - 0 нет, 1 - есть [x,y,тип]
        self.mode = (640, 480) #размеры экрана
        self.tileangle = [0,0,0,0]#углы загруженных тайлов

    def initTilesXY(self):
        #начальная загрузка всех углов для наблюдаемой области карты, для всех зумов
        self.xyt = []
        for z in range(1,21):#перебор зумов
            x,y = tilenames.tileXY(56.4,23.0, z)
            x1,y1 = tilenames.tileXY(51.14,33.0, z)
            self.xyt += [[x,y,x1,y1]]
        return self.xyt

    def loadtile(self):
        #перенос старых изображений на новую сетку
        #создаем массив на количество загружаемых тайлов
        mas = []
        for i in range(self.tileangle[0],self.tileangle[2]+1):
            mas2 = []
            for j in range(self.tileangle[1],self.tileangle[3]+1):
                mas2 += [[i,j,0,0]]
            mas += [mas2]
        #проход по всем загруженным тайлам
        #если не вписываются - выбрасываем из массива
        for i in self.tileimage:
            x = i[0]- self.tileangle[0]
            y = i[1]- self.tileangle[1]
            if (x>=0 and y>=0 and i[0]<=self.tileangle[2] and
                    i[1]<=self.tileangle[3] and i[2]):#i[2] - 1 - тайл существует, 0 - тайл пустой
                #если есть ставим отметку в массив- такой тайл есть
                mas[x][y][2] = 1
                mas[x][y][3] = i[3]#переносим копию изображения
        return mas

    def calcScreenTile(self):
        #получаем углы тайлов
        xt1 = self.xyt[self.zoom][0]
        yt1 = self.xyt[self.zoom][1]
        xt2 = self.xyt[self.zoom][2]
        yt2 = self.xyt[self.zoom][3]
        #получаем разницу между тайлами
        dxt = xt2 - xt1 + 1
        dyt = yt2 - yt1 + 1
        #если тайлов меньше чем размер экрана
        #то эти тайлы центрируем на экране
        dxt256 = dxt*256
        dyt256 = dyt*256
        if dxt256<=self.mode[0]:
            self.x = (self.mode[0]-dxt256)/2
        if dyt256<=self.mode[1]:
            self.y = (self.mode[1]-dyt256)/2
        #если же тайлов больше, чем размер экрана
        #а в предудыщем не хватало тайлов на экран, то отсчет карты в угол экрана
        if dxt256 > self.mode[0]:
            if self.x > 0:
                self.x = 0
        if dyt256 > self.mode[1]:
            if self.y > 0:
                self.y = 0
        #получаем номера тайлов для вывода на экран
        xt1a = xt1-(self.x/256)-2#-2 - чтобы при движении карты не было рывков
        yt1a = yt1-(self.y/256)-2
        xt2a = xt1a + self.mode[0]/256+2#+2 тайла за пределы экрна
        yt2a = yt1a + self.mode[1]/256+3
        #если разница в пикселях меньше размеров экрана,
        #то экранные тайлы равны всем тайлам
        if dxt256 <= self.mode[0]:
            xt1a = xt1
            xt2a = xt2
        if dyt256 <= self.mode[1]:
            yt1a = yt1
            yt2a = yt2
        #если номера экранных тайлов выходят за границы, то присвоить границы
        if xt1a < xt1: xt1a = xt1
        if yt1a < yt1: yt1a = yt1
        if xt2a > xt2: xt2a = xt2
        if yt2a > yt2: yt2a = yt2
        #получаем отсчет для экранных тайлов
        self.xs = (xt1a - xt1) * 256
        self.ys = (yt1a - yt1) * 256
        #возвращаем экранные тайлы
        self.tileangle = [xt1a,yt1a,xt2a,yt2a]
        return self.tileangle

class track:
    def __init__(self, xyt):
        self.xyt = xyt

    def getXY(self,lat,lon,zoom):
        #получаем тайл
        x,y = tilenames.tileXY(lat,lon, zoom+1)
        #получаем его координаты
        zz = tilenames.tileEdges(x,y,zoom+1)
        #получаем расстояние до тайла в тайлах
        x1 = (x - self.xyt[zoom][0])
        y1 = (y - self.xyt[zoom][1])
        #получаем расстояние в тайле
        ys = 256 - int((lat - zz[0])/(zz[2]-zz[0])*256)
        xs = int((lon - zz[1])/(zz[3]-zz[1])*256)

        xx = x1*256 + xs #self.x + xs
        yy = y1*256 + ys #self.y + ys

        return xx,yy,xs,ys

    def getLatLon(self,x,y,zoom):
        #общее смещение
        mx = -x
        my = -y
        #получаем координаты где находится мышь
        zx = -mx/256
        zy = -my/256
        #расчитываем над каким тайлом находится мышь
        ztx = self.xyt[zoom][0]+zx
        zty = self.xyt[zoom][1]+zy
        #получаем кооридинаты в мировых координатах
        zz = tilenames.tileEdges(ztx,zty,zoom+1)
        #получаем смещение относительно тайла
        sx = -(mx + zx*256)
        sy = 256 + my + zy*256
        #получаем разницу в каждом пикселе между координатами
        ssx = (zz[3] - zz[1])/256.0
        ssy = (zz[2] - zz[0])/256.0
        #получаем координату под мышью
        xxx = sx*ssx+zz[1]
        yyy = sy*ssy+zz[0]
        return xxx,yyy

