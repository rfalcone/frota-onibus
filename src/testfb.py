#!/usr/bin/python
# -*- coding: utf-8  -*-
import kinterbasdb as k
class testVdFirebird():
    """
    класс для тестирования возможностей firebird
    с работой над реальной базой данных
    """
    def __init__(self):
        self.con = 0

    def connect(self,host1,database1,user1,password1):
        self.con = k.connect(host=host1, database=database1,
            user=user1, password=password1,charset='UNICODE_FSS')#WIN1251')#UNICODE_FSS')
    def viewtable(self):
        try:
            cc = self.con.cursor()
            query = ('''select rdb$relation_name from rdb$relations
                        where rdb$system_flag = 0
                        order by rdb$relation_name''');
            cc.execute(query)
            tablename = []
            for tablenames in cc:
                tablename += [tablenames]
            return tablename
        except:
            print ("невозможно получить список имен таблиц ")
    def countRecord(self,nameTable):
        try:
            cc = self.con.cursor()
            query = ('''select count(*) from %s
                        '''%nameTable);
            cc.execute(query)
            tablename = []
            for tablenames in cc:
                tablename += [tablenames]
            return tablename
        except:
            print ("невозможно получить список имен таблиц ")
    def viewfieldsintable(self):
        try:
            cc = self.con.cursor()
            query = ('''select R.RDB$RELATION_NAME, R.RDB$FIELD_POSITION, R.RDB$FIELD_NAME,
                        F.RDB$FIELD_LENGTH, F.RDB$FIELD_TYPE, F.RDB$FIELD_SCALE, F.RDB$FIELD_SUB_TYPE
                        from RDB$FIELDS F, RDB$RELATION_FIELDS R
                        where F.RDB$FIELD_NAME = R.RDB$FIELD_SOURCE and R.RDB$SYSTEM_FLAG = 0
                        order by R.RDB$RELATION_NAME, R.RDB$FIELD_POSITION
                    ''')

            cc.execute(query)
            fieldsintable = []
            for fieldsintables in cc:
                fieldsintable +=[fieldsintables]
            return fieldsintable 
        except:
            print ("невозможно получить список имен полей")
    def select(self,sql):
        try:
            cc = self.con.cursor()
            query = sql
            cc.execute(query)
            return cc
        except:
            print ("невозможно выполнить запрос")


