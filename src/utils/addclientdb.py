#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, getpass
from db import db
import time

class startdump:
    '''Создание нового клиента. Создаем в базе запись, для отложенных записей этому клиенту
    затем делаем дамп базы данных необходимых для начальной работы'''
    def __init__(self):
        self.mysql = db()
        self.config = {}
        self.config["host"] = "localhost"
        self.config["user"] = "asdm"
        self.config["passwd"] = "f4E4HwTcBWQRwDAy"
        self.config["db"] = "asdm"
        self.mysql.config = self.config
        self.mysql.connect()
        
    
    def run(self):
        t = time.time()
        namedbclient = 'client01'
        tabled = "clientdb"
        sql = '''CREATE TABLE IF NOT EXISTS %s (
                    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                    nameclient VARCHAR(50) NOT NULL
                    ) ENGINE = MYISAM; '''%(tabled)
        self.mysql.execsql(sql)
        sql = ''' INSERT INTO %s (nameclient) VALUES ('%s')'''%(tabled, namedbclient)
        self.mysql.execsql(sql)
        os.system("mysqldump -u %s --password=%s %s > asdm.sql"%(self.config["user"],
            self.config["passwd"], self.config["db"]))
        self.close()
        print time.time() - t, " sec"
    
    def close(self):
        self.mysql.disconnect()
    

if __name__ == "__main__":
    print "run"
    s = startdump()
    s.run()
    print "close"
    