#!/usr/bin/python
# -*- coding: utf-8  -*-
import MySQLdb

class db:
    ''' Класс работы с базой данных
    Переменные:
        * sql - текст SQL запроса, который будет выполнен, если в execsql не будет передана строка'''
    def __init__(self):
        self.sql=''
        self.cursor = ''
        self.conn = ''
        self.connected = False
        self.config = {}
        self.config["host"] = ""
        self.config["user"] = ""
        self.config["passwd"] = ""
        self.config["db"] = ""
        self.error = 0

    def connect(self):
        self.error = 0
        "Подключение к базе данных"
        try:
            self.conn = MySQLdb.connect (host = self.config["host"], user = self.config["user"], 
                passwd = self.config["passwd"], db = self.config["db"], 
                charset = "utf8", use_unicode = True)
            self.cursor = self.conn.cursor (MySQLdb.cursors.DictCursor)
            self.connected = True
        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
            self.disconnect()

    def execsql(self,sql = ''):
        self.error = 0
        "Выполнение SQL запроса по переданному параметру sql или внутренней переменной sql"
        try:
            if (sql != ''):
                self.sql = sql
            self.cursor.execute (self.sql)
        except MySQLdb.Error, e:
            self.connect()
            try:
                self.cursor.execute (self.sql)
            except:
                print "Error %d: %s" % (e.args[0], e.args[1])
                print self.sql
                self.error = 1
                
    def getData(self, sql = ''):
        ''' Получаем данные из запроса - в запросе всегда должен присутствовать 
        id таблицы
        Входные данные - SQL запрос, на выходе или в теле класса
        fields - список имен полей, полученные из запроса
        data - словарь с данными, где ключ - id запроса
        datarow - список ключей (для упорядочивания получения данных из data)
        '''
        self.error = 0
        if (sql != ''):
            self.sql = sql
        self.execsql()
        self.datarow = []
        self.datafield = []
        self.data = {} #словарь со всеми полями
        self.fields = []#список полей из запроса
        if self.error == 0:
            for f in self.cursor.description:
                self.fields.append(f[0])
            result_set = self.cursor.fetchall()
            for row in result_set:
                tmpdata = {}
                for i in self.fields:
                    tmpdata[i] = row[i] #проход по полям
                self.data[row["id"]] = tmpdata#сохраняем в данных
                self.datarow += [row["id"]]
        return (self.fields, self.datarow, self.data)
             
    def disconnect(self):
        "Отключение от базы данных"
        try:
            self.cursor.close ()
            self.conn.close ()
            self.connected = False
        except MySQLdb.Error, e:
             print "Error %d: %s" % (e.args[0], e.args[1])

    def getlastid(self):
        ''' Получить id последнего выполненного запроса insert'''
        try:
            a = self.conn.insert_id()
            return a
        except MySQLdb.Error, e:
             print "Error %d: %s" % (e.args[0], e.args[1])