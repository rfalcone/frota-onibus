#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from datetime import date
from spr.control import controlfreezetable
from spr.model import model
from spr.view import viewtable
import calendar
import time
from vodgrafik.modelgrafik import holiday, dayFlag, getIdKartaRangeToMonth, tableLink, getInfoKartaRange


class kartaModel:
    '''Класс для получения списка карточек для определенной бригады, т.е. 
    draw вызывается с параметром [номер бригады]. Инициализация с сылкой на db.db'''
    
    def __init__(self, mysql = None):
        self.initSql()
        self.mysql = mysql
    
    def initSql(self):
        '''Описание запросов, параметров '''
        
        self.dataparam = ["name", "put", "sokr", "idkarta", "t"]
        self.getDataSQL = '''
            SELECT v.id, v.sort, k.name, k.put, k.id as idkarta, v.t, '' as sokr
            FROM karta as k, vodgrkartasort as v
            WHERE v.idkarta = k.id AND v.brigada = %d AND k.del = 0 AND v.t = 1

            UNION            
            
            SELECT v.id, v.sort, k.name, 0 as put, k.id as idkarta, v.t, k.sokr
            FROM dopoln as k, vodgrkartasort as v
            WHERE v.idkarta = k.id AND v.brigada = %d AND k.del = 0 AND v.t = 0
            
            ORDER BY 1'''
######################################################################################
class kartaShowTable(kartaModel, viewtable):
    '''Модернизация виджета таблицы для показа диалогового окна со списком возможных карточек
    для выбранной бригады'''
    
    def __init__(self, parent = None, mysql = None):
        viewtable.__init__(self, parent, mysql)
        kartaModel.__init__(self, mysql)
        self.dataview = ["name", "put"]
        self.headername = [u"Название", u"В путевых"]
    
    def itemtable(self,row,col,s):
        '''Особая отрисовка второй колонки - отрисовывается в виде XXXX-XX, где X - цифра кода'''
        if self.datarow[self.data[row]]["t"]:
            if col == 1:
                s = "%s-%s"%(s[:4],s[4:])
        else:
            s = self.datarow[self.data[row]]["sokr"]
        item = QtGui.QTableWidgetItem("%s"%s)
        self.setItem(row,col,item)
######################################################################################
class kartaShow(QtGui.QDialog):
    '''Диалоговое окно со списком карточек для бригады для выбора'''
    
    def __init__(self, parent = None, mysql = None):
        QtGui.QDialog.__init__(self,parent)
        self.table = kartaShowTable(parent, mysql)
        self.btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(self.btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(self.btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        gr = QtGui.QGridLayout(self)
        gr.addWidget(self.table)
        gr.addWidget(self.btn1)

######################################################################################
class tblvodgrafik(controlfreezetable):
    '''Класс составления графиков работы
    
    Используемые переменные:
        * hlday - экземпляр класса :py:class:`vodgrafik.modelgrafik.holiday`
        * kartaspis - экземпляр класса :py:class:`kartaModel`
        * idKartaRangeToMonth - экземпляр класса :py:class:`vodgrafik.modelgrafik.getIdKartaRangeToMonth` для заполнения списка spisKartaRange 
        * kartaRangeInfo - экземпляр класса :py:class:`vodgrafik.modelgrafik.getInfoKartaRange`
        * dayflag - экземпляр класса :py:class:`vodgrafik.modelgrafik.dayFlag`
        * tablelink - экземпляр класса :py:class:`vodgrafik.modelgrafik.tableLink`
        * spisKartaRange - словарь с ключами idkarta и указатель на значения из idKartaRangeToMonth.data для каждой карточки
        * datas - словарь с ключем id от таблицы voditel базы данных, с дополнительным ключем:
            * row - в какой строке запись для получения этой строки по id водителя
        * datarow - список ключей для словаря datas
            
        * dataparam - список ключей словаря для значения datas:
            * fio - сборный параметр из полей таблицы family, name, surname в виде фамилии с инициалами
            * garnom - гаражный привязанный к водителю, если гаражного номера нет, то 0
            * tabelnmbr - табельный номер
            
        * datatable - словарь с ключем id из таблицы vodgrafik и vodgrafikdop и значениями из dataparamtable 
        * datarowtable - список ключей id из таблицы vodgrafik и vodgrafikdop 
        * dataparamtable - список ключей словаря для значений :
            * idkartarange
            * idvoditel 
            * date
            * name
            * dop
        * dataview - ключи словаря datatable выводимые в таблице:
            * fio
            * garnom
        * headername - заголовки таблицы:
            * ФИО
            * Гаражный
            * номер дня
        * freezeColumnCount - количество замороженных колонок
        * freeColumnCount - количество свободных колонок
        * year - год, с которым ведется работа
        * month - месяц, с которым ведется работа
        * brigada - бригада, с которой ведется работа
        '''
    
    def __init__(self, parent = None, mysql = None):
        self.initSql()
        self.hlday = holiday(mysql)
        
        self._dataview = ["fio", "garnom", "worktime"]
        self.dataview = self._dataview[:] 
        self._headername = [u"ФИО", u"Гаражный", u"Отработано, ч"]
        self.headername = self._headername[:] 
        self.freezeColumnCount = 3
        self.freeColumnCount = 20
        self.year = 2011
        self.month = 1
        self.brigada = 1
        self.kartaspis = kartaModel(mysql)#список карточек для данной бригады
        self.kartaspis._getData([self.brigada, self.brigada])
#        self.widthFreezeColumn = [100,150]
        controlfreezetable.__init__(self, parent, mysql)
        self.idKartaRangeToMonth = getIdKartaRangeToMonth(mysql)
        self.kartaRangeInfo = getInfoKartaRange(mysql, self.brigada) #данные по периодам карточек
        self.kartaRangeInfo.redraw()
        #считываем данные по карточкам
        self.spisKartaRange = {}
        self.dayflag = dayFlag(date(self.year, self.month, 1), self.hlday)
        for id in self.kartaspis.datarow:
            idkarta = self.kartaspis.data[id]["idkarta"]
            self.idKartaRangeToMonth.dayflag = self.dayflag
            self.idKartaRangeToMonth.redraw([idkarta,date(self.year, self.month, 1)])
            self.spisKartaRange[idkarta] = self.idKartaRangeToMonth.data[:]
        self.tablelink = tableLink(self.kartaRangeInfo, self.dayflag)#таблица для проверки данных
        self.statePrintName = 1 #что выводить в таблицу имена или коды
  
    def redrawOutValue(self, day):
        '''Пересчет значений дополнительных классов '''
        
        self.hlday.redraw([self.year, self.month, day])#обновляем список праздничных
        self.kartaspis._getData([self.brigada, self.brigada])#получаем новый список используемых карточек
        self.dayflag.redraw(date(self.year,self.month,1))#перечитываем флаги дней
        #обновляем списки действия карточек
        self.spisKartaRange = {}
        for id in self.kartaspis.datarow:
            idkarta = self.kartaspis.data[id]["idkarta"] 
            self.idKartaRangeToMonth.redraw([idkarta,date(self.year, self.month, 1)])
            self.spisKartaRange[idkarta] = self.idKartaRangeToMonth.data[:]
        #подготовливаем таблицу с сылками
        self.tablelink.days = day
        self.tablelink.rowfio = len(self.datarow)
        self.tablelink.clear()
    
    def redraw(self,data):#data [brigada, year, month]
        '''Отрисовка данных и перерасчет данных
        
        Входные данные:
            * data - [brigada, year, month], id бригады, год и месяц
        Переменные:
            * dataview             
            '''
            
        self.brigada = data[0]
        self.kartaRangeInfo.brigada = self.brigada 
        self.kartaRangeInfo.redraw()
        self.year = data[1]
        self.month = data[2]
        day = calendar.monthrange(self.year, self.month)[1] 
        self.headername = self._headername[:]
        self.dataview = self._dataview[:]
        self.freeColumnCount = day       
        for i in range(day):
            self.headername += [str(i+1)] 
            #self.dataview += ["day%d"%(i+1)]
        controlfreezetable.redraw(self,data)
        self.redrawOutValue(day)#пересчитываем значения внешних классов
        #выделяем колонки с выходными и праздничными
        for i in range(day):
            for j in range(len(self.datarow)):
                self._draw(j,i,'')
        #заполняем данными таблицу
        for id in self.datarowtable:
            rw = self.datatable[id]["idvoditel"]#получаем id водителя
            try:
                row = self.datas[rw]["row"]#получаем строку соответствующей водителю
                day = self.datatable[id]["date"].day 
                tmp = {}
                if self.datatable[id]["dop"] == 0:
                    #в зависимости от состояния чекбокса выдавать либо имя, либо код карточки
                    if self.statePrintName:
                        s = self.datatable[id]["name"]
                    else:
                        s = str(self.datatable[id]["put"])
                    tmp["idkartarange"] = self.datatable[id]["idkartarange"]
                else:
                    iddop = self.datatable[id]["idkartarange"]
                    s = self.tablelink.iddop[iddop]
                    tmp["iddop"] = iddop 
                #заполняем таблицу с линками
                self.tablelink.data[row][day-1] = tmp
                #отрисовываем на виджете
                self._draw(row, day-1, s)
            except KeyError:#возникает, если водителя забили в график, а затем указали в другой бригаде
                if self.datatable[id]["dop"] == 0:
                    print u"Водитель id = %d (запись vodgrafik.id = %d) перевели в другую бригаду, но на него есть запись в графике бригады %d"%(self.datatable[id]["idvoditel"],id,self.brigada)
                else:
                    print u"Водитель id = %d (запись vodgrafikdop.id = %d) перевели в другую бригаду, но на него есть запись в графике бригады %d"%(self.datatable[id]["idvoditel"],id,self.brigada)
                
        self.provdata()
        self.resizeColumnsToContents()
        self.resizeRowsToContents()
        self.updateFrozenTableGeometry()

    def _draw(self, row, col, s):
        colfix = col + self.freezeColumnCount
        if self.dayflag.flag[col] & 512:#выделяем колонки с праздничными
            self.itemtable(row, colfix, s, 2)
        elif self.dayflag.flag[col] & 256:#выделяем колонки с выходными
            self.itemtable(row, colfix, s, 1)
        else:
            self.itemtable(row, colfix, s, 0)    
    
    def itemtable(self,row,col,st, typedata = 0):
        try:
            item = QtGui.QStandardItem(st)
#            f = item.font()
#            f.setBold(True)
#            item.setFont(f)
            if typedata == 1:
                br = QtGui.QBrush(QtCore.Qt.lightGray)
                item.setBackground(br)
            elif typedata == 2:
                cl = QtGui.QColor()
                cl.setNamedColor('#ff7f7f')
                br = QtGui.QBrush(cl)
                item.setBackground(br)
            elif typedata == 0:
                if col >= self.freezeColumnCount:
                    d = col - self.freezeColumnCount
                    if self.dayflag.flag[d] & 256:#выходной
                        br = QtGui.QBrush(QtCore.Qt.lightGray)
                        item.setBackground(br)
            elif typedata == 3:
                cl = QtGui.QColor()
                cl.setNamedColor('#ff0000')
                br = QtGui.QBrush(cl)
                item.setBackground(br)
            self.mdl.setItem(row, col, item)
        except Exception, e:
            print e
            raise

    def initSql(self):
        self.dataparam = ["fio", "garnom", "tabelnmbr"]
        #TODO изменить запрос с учетом должности и времени принятия и увольнения с работы
        self.getHeaderSQL = '''
            SELECT v.id, v.family, v.name, v.surname, v.tabelnmbr, 0 as garnom
            FROM voditel as v
            WHERE v.idbus = 0 AND v.brigada = %d
            UNION
            SELECT v.id, v.family, v.name, v.surname, v.tabelnmbr, b.garnom
            FROM voditel as v, bus as b
            WHERE (v.idbus = b.id) AND b.del = 0 AND v.brigada = %d
            ORDER BY 6, 2, 3, 4
            '''
        self.dataparamtable = ["idkartarange", "idvoditel", "date", "name", "put", "t"]
        self.getTableSQL = '''
            SELECT v.id, v.idkartarange, v.idvoditel, v.date, k.name, k.put, v.t
            FROM vodgrafik as v, karta as k, kartarange as kr
            WHERE v.brigada = %d AND v.idkartarange = kr.id AND kr.idkarta = k.id 
                AND v.date>='%s' AND v.date <='%s' AND v.t = 1
            UNION
            SELECT v.id, v.iddop as idkartarange, v.idvoditel, v.date, '' as name, 0 as put, v.t
            FROM vodgrafik as v, dopoln as d
            WHERE v.brigada = %d AND v.date>='%s' AND v.date <='%s'
            '''
        self.addKartaSQL = '''INSERT INTO vodgrafik (idkartarange, idvoditel, date, brigada)
                              VALUES %s'''#(%d, %d, '%s', %d)
        
        self.delKartaSQL = ''' DELETE FROM vodgrafik WHERE idvoditel=%d AND date='%s' AND brigada=%d'''
        self.addDataSQL = self.addKartaSQL 
        self.delDataSQL = self.delKartaSQL

    def _getData(self,data):#tupledata - содержит данные для вывода запроса
        #входные данные data[0] - бригада data[1] datetime.date(день с необходимой датой)
        #загрузить данные
        self.datarow = []
        try:
            self.brigada = data[0]
            self.mysql.execsql(self.getHeaderSQL%(self.brigada, self.brigada))
            result_set = self.mysql.cursor.fetchall()
            self.datas = {} #словарь со всеми полями
            self.datarow = []
            #self.datafiorow = {}
            i = 0
            for row in result_set:
                tmpdata = {}
                try:
                    tmpdata["fio"] = "%s %s.%s."%(row["family"],row["name"][0],row["surname"][0])
                except:
                    tmpdata["fio"] = "%s"%row["family"]
                tmpdata["garnom"] = row["garnom"]
                tmpdata["tabelnmbr"] = row["tabelnmbr"]
                tmpdata["row"] = i#сохраняем в какой строке будет выводится фамилия
                i += 1
                self.datas[row["id"]] = tmpdata
                self.datarow += [row["id"]]
                
            #считываем данные таблицы
            year = self.year
            month = self.month
            day = calendar.monthrange(year, month)[1]  
            date1 = str(date(year,month,1))
            date2 = str(date(year,month,day))
            self.mysql.execsql(self.getTableSQL%(self.brigada, date1, date2, 
                                                 self.brigada, date1, date2))
            result_set = self.mysql.cursor.fetchall()
            self.datatable = {} #словарь со всеми полями
            self.datarowtable = []
            for row in result_set:
                tmpdata = {}
                for i in self.dataparamtable:
                    tmpdata[i] = row[i]
                self.datatable[row["id"]] = tmpdata
                self.datarowtable += [row["id"]]
        except Exception, e:
            print e
            raise

    def sqlcut(self, a, an, am, maxn=100):
        an += 1
        if an > maxn:
            an = 0 
            am += [a]
            a = ''
        return a, an, am
        
    def _getSdvigIdKarta(self, col, row, idKarta):
        cols = col
        rows = row
        k = 0
        for id in self.kartaspis.datarow:
            if self.kartaspis.data[id]["idkarta"] == idKarta:
                idkartas = k #порядок карты сохраняем
                break
            k += 1
        return cols,rows, idkartas

    def addKarta(self, selectedIndexes, idKarta, dop=0):
        #maxn = 100#по сколько максимум элементов для одного запроса вставки
        skarta = ''
        skartan = 0#счетчик сколько данных одновременно вставляется
        skartam = []#массив запросов
        sdop = ''
        sdopn = 0
        sdopm = []#массив запросов
        fl = 1 #флаг первого прохода, чтобы сохранить откуда начинать расчет перебора карточек
        self.clearKarta(selectedIndexes)
        for i in selectedIndexes:
            row = i.row()
            col = i.column() 
            if col >= self.freezeColumnCount:
                col = col - self.freezeColumnCount
                if fl and dop ==0:#получаем переменные для сдвига карточек с прибавлением дня 
                    cols, rows, idkartas = self._getSdvigIdKarta(col,row,idKarta)
                    fl = 0
                day = col + 1
                curdate = date(self.year, self.month,day)
                idvoditel = self.datarow[row]
                if dop == 0:
                    sdvig = (col + row - cols - rows + idkartas)%len(self.kartaspis.datarow)#сдвиг карточки
                    #print col, row, cols, rows, idkartas, len(self.kartaspis.datarow)
                    idKarta = self.kartaspis.data[self.kartaspis.datarow[sdvig]]["idkarta"]#получаем ее ид после сдвига
                    kartarange = self.spisKartaRange[idKarta][col]
                    idkartarange = kartarange["id"] 
                    if idkartarange > 0:
                        if skarta == '': skarta = "(%d, %d, '%s', %d)"%(idkartarange, idvoditel, str(curdate), self.brigada)
                        else: skarta = "%s, (%d, %d, '%s', %d)"%(skarta, idkartarange, idvoditel, str(curdate), self.brigada)
                        skarta, skartan, skartam = self.sqlcut(skarta, skartan, skartam)
                        tmp = {}
                        tmp["idkartarange"] = idkartarange
                        self.tablelink.data[row][col] = tmp
                        #при множественной вставке нельзя определить id для self.datatable поэтому self.datatable не заполняем                        
                        #в зависимости от состояния чекбокса выдавать либо имя, либо код карточки
                        if self.statePrintName:
                            s = kartarange["name"]
                        else:
                            s = str(s = kartarange["put"])
                        
                        self._draw(row, col, s)
                    else:
                        if sdop == '': sdop = "(0, %d, '%s', %d)"%(idvoditel, str(curdate), self.brigada)
                        else: sdop = "%s, (0, %d, '%s', %d)"%(sdop, idvoditel, str(curdate), self.brigada)
                        sdop, sdopn, sdopm = self.sqlcut(sdop, sdopn, sdopm)
                        tmp = {}
                        tmp["iddop"] = 0
                        self.tablelink.data[row][col] = tmp
                        s = kartarange["name"]
                        self._draw(row, col, s)
                elif dop == 1:
                    if sdop == '': sdop = "(%d, %d, '%s', %d)"%(idKarta, idvoditel, str(curdate), self.brigada)
                    else: sdop = "%s, (%d, %d, '%s', %d)"%(sdop, idKarta, idvoditel, str(curdate), self.brigada)
                    sdop, sdopn, sdopm = self.sqlcut(sdop, sdopn, sdopm)
                    tmp = {}
                    tmp["iddop"] = idKarta 
                    self.tablelink.data[row][col] = tmp
                    s = self.tablelink.iddop[idKarta]
                    self._draw(row, col, s)
                    
        skartam += [skarta]                        
        for skarta in  skartam:
            if skarta != '':
                self.mysql.execsql(self.addKartaSQL%skarta)
        sdopm += [sdop]
        for sdop in  sdopm:
            if sdop != '':
                self.mysql.execsql(self.addDopSQL%sdop)
        self.resizeColumnsToContents()
        self.resizeRowsToContents()
        self.updateFrozenTableGeometry()
        self.mdls.select(i,QtGui.QItemSelectionModel.SelectCurrent)        
        #self.modelselect.clear()
        
    def clearKarta(self,selectedIndexes):
        s = ''
        for i in selectedIndexes:
            row = i.row()
            col = i.column() 
            if col >= self.freezeColumnCount:
                col = col - self.freezeColumnCount
                day = col + 1
                if self.tablelink.data[row][col] != 0:
                    curdate = date(self.year, self.month,day)
                    idvoditel = self.datarow[row]
                    try:
                        if self.tablelink.data[row][col]["iddop"]>=0:
                            self.mysql.execsql(self.delDopSQL%(idvoditel, curdate, self.brigada))
                    except:
                        self.mysql.execsql(self.delKartaSQL%(idvoditel, curdate, self.brigada))
                    self._draw(row, col, s)
                    self.tablelink.data[row][col] = 0

        #self.modelselect.clear()
        self.resizeColumnsToContents()
        self.resizeRowsToContents()
        self.updateFrozenTableGeometry()
        self.mdls.select(i,QtGui.QItemSelectionModel.SelectCurrent)        
        
    def _drawerror(self, fil = 1):
        '''Перерисовка ячеек с ошибками fil = 1 рисовать ошибки, при 0 - стирать'''
        #отчет по ошибкам
        err = 0
        for row in self.tablelink.dataerr.keys():
            for tmp in self.tablelink.dataerr[row]:
                for col, v in tmp.iteritems():
                    if col >= 0:
                        try:
                            #получаем idkartarange
                            idkartarange = self.tablelink.data[row][col]
                            if idkartarange != 0:
                                idkartarange = idkartarange["idkartarange"]
                            #по ней получаем idkarta и получаем имя
                            idkarta = self.kartaRangeInfo.data[idkartarange]["idkarta"]
                            #и наконец получаем имя карточки
                            #в зависимости от состояния чекбокса выдавать либо имя, либо код карточки
                            if self.statePrintName:
                                name = self.spisKartaRange[idkarta][col]["name"]
                            else:
                                name = str(self.spisKartaRange[idkarta][col]["put"])
                            #name = self.spisKartaRange[idkarta][col]["name"]
                            self.itemtable(row, col + self.freezeColumnCount, name, fil*3)
                        except KeyError:
                            pass
                        except IndexError:
                            pass
                            #print row, col, self.tablelink.data
                        
                        #print u"В строке %d, день %d - %s"%(row+1,col+1,v)
                        err += 1
                    else:
                        #print u"В строке %d - %s"%(row+1,v)
                        err += 1
        if fil:
            print u"Найдено %d ошибок"%err
        
    
    def provdata(self):
        '''Дополнительные расчеты после изменений в таблице - проверка условий, заполнение ошибок
        расчет рабочего времени и др.'''
        self._drawerror(0)#стираем ошибки и пересчитываем
        self.tablelink.provdata()
        self.tablelink.calctime()
        #отчет по рабочему времени
        rows = len(self.tablelink.datafio)
        for row in range(rows):
            s = "%.2f"%(self.tablelink.datafio[row]["worktime"]/60.00)
            self.datas[self.datarow[row]]["worktime"] = s
            self._draw(row, -1, s)
        self._drawerror(1)#выделяем ошибки
        
        

