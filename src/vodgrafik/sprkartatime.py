#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from datetime import time
#from karta.karta import karta
from karta.karta import karta
from spravochnik.kartarange.kartarange import kartarange
from spravochnik.kartarange.kartarangemarsh import kartarangemarsh
from spr.model import model

class kartaTable(karta):
    def __init__(self, parent = None, mysql = None):
        karta.__init__(self,parent,mysql)
        
class kartaRangeTable(kartarange):
    def __init__(self, parent = None, mysql = None):
        kartarange.__init__(self,parent,mysql)

class kartaRangeMarshTable(kartarangemarsh):
    def __init__(self, parent = None, mysql = None):
        kartarangemarsh.__init__(self,parent,mysql)
        
class kartaRangeTime(model, QtGui.QWidget):
    def __init__(self, parent = None, mysql = None):
        QtGui.QWidget.__init__(self, parent)
        self.initSql()
        model.__init__(self, mysql)
        self.initWidget()
        self.initAction()
        self.idkartarange = 0
        
    def initWidget(self):
        hl = QtGui.QHBoxLayout()
        cmb = QtGui.QGroupBox()
        cmb.setTitle(u'Время по путевому')
        hl2 = QtGui.QHBoxLayout()
        lbl1 = QtGui.QLabel(u"выезд")
        lbl2 = QtGui.QLabel(u"возвращение")
        lbl3 = QtGui.QLabel(u"день")
        self.ted1 = QtGui.QTimeEdit()
        self.ted2 = QtGui.QTimeEdit()
        self.sb = QtGui.QSpinBox()
        hl2.addWidget(lbl1)
        hl2.addWidget(self.ted1)
        hl2.addWidget(lbl2)
        hl2.addWidget(self.ted2)
        hl2.addWidget(lbl3)
        hl2.addWidget(self.sb)
        cmb.setLayout(hl2)
        hl.addWidget(cmb)
        self.setLayout(hl)
    
    def initSql(self):
        self.dataparam = ["idkartarange", "begintime", "endtime", "endday"]
        self.getDataSQL = '''SELECT id, idkartarange, begintime, endtime, endday 
                             FROM kartarangetime  
                             WHERE idkartarange = %d'''
        self.addDataSQL = '''INSERT INTO kartarangetime (idkartarange, begintime, endtime, endday ) 
                             VALUES (%d, '%s', '%s', %d) '''
        self.editDataSQL = ''' UPDATE kartarangetime SET begintime = '%s', endtime = '%s', endday = %d 
                             WHERE idkartarange = %d '''
    
    def initAction(self):
        self.connect(self.ted1, QtCore.SIGNAL("timeChanged(QTime)"),self.saveTime)
        self.connect(self.ted2, QtCore.SIGNAL("timeChanged(QTime)"),self.saveTime)
        self.connect(self.sb, QtCore.SIGNAL("valueChanged(int)"),self.saveDay)
    
    def _save(self,data):#idkartarange, begintime, endtime, endday 
        if len(self.datarow)==0:
            self._addData([self.idkartarange]+data)
        else:
            self._editData(data+[self.idkartarange])
        self.redraw([self.idkartarange])

    def _blockSignals(self, b):
        self.ted1.blockSignals(b)
        self.ted2.blockSignals(b)
        self.sb.blockSignals(b)
    
    def saveTime(self, tm):
        if self.idkartarange > 0:
            tmbegin = str(self.ted1.time().toPyTime())
            tmend = str(self.ted2.time().toPyTime())
            day = self.sb.value()
            self._save([tmbegin, tmend, day])
    
    def saveDay(self, day):
        if self.idkartarange > 0:
            tmbegin = str(self.ted1.time().toPyTime())
            tmend = str(self.ted2.time().toPyTime())
            day = self.sb.value()
            self._save([tmbegin, tmend, day])
    
    def redraw(self, data = []):#idkartarange
        self.idkartarange = data[0]
        self._getData(data)
        if len(self.datarow) == 0:
            self._blockSignals(True)
            self.ted1.setTime(QtCore.QTime(time(0,0)))
            self.ted2.setTime(QtCore.QTime(time(0,0)))
            self.sb.setValue(0)
            self._blockSignals(False)
        else:
            self._blockSignals(True)
            t = self.data[self.datarow[0]]["begintime"].seconds
            h = t/3600
            m = (t/60-h*60)
            self.ted1.setTime(QtCore.QTime(time(h,m)))
            t = self.data[self.datarow[0]]["endtime"].seconds
            h = t/3600
            m = (t/60-h*60)
            self.ted2.setTime(QtCore.QTime(time(h,m)))
            self.sb.setValue(self.data[self.datarow[0]]["endday"])
            self._blockSignals(False)

class sprKartaTime(QtGui.QMdiSubWindow):
    def __init__(self, mysql, parent = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Время работы по карточке')        
        self.kartatable = kartaTable(parent, mysql)
        self.kartarangetable = kartaRangeTable(parent, mysql)
        self.kartarangemarshtable = kartaRangeMarshTable(parent,mysql)
        self.kartatime = kartaRangeTime(parent,mysql)
        self.initWidget()
        self.initAction()
        self.kartatable.redraw([0])
        #self.kartatable.list.setCurrentRow(0)
        self.kartatable.table.setCurrentCell(0,0)
        self.kartarangetable.list.setCurrentRow(0)
            
    def initWidget(self):
        grid = QtGui.QGridLayout(self)
        spl1 = QtGui.QSplitter(QtCore.Qt.Horizontal)
        spl2 = QtGui.QSplitter(QtCore.Qt.Vertical)
        spl2.addWidget(self.kartarangetable)
        spl2.addWidget(self.kartatime)
        spl2.addWidget(self.kartarangemarshtable)
        spl1.addWidget(self.kartatable)
        spl1.addWidget(spl2)
        grid.addWidget(spl1)        
        widg = QtGui.QWidget()
        vh = QtGui.QVBoxLayout()
        vh.addLayout(grid)
        widg.setLayout(vh)
        self.setWidget(widg)

    def initAction(self):
        self.connect(self.kartatable.table, QtCore.SIGNAL("itemSelectionChanged ()"),self.krtChange)
        self.connect(self.kartarangetable.list, QtCore.SIGNAL("itemSelectionChanged ()"),self.krtrngChange)

    def krtChange(self):
        if self.kartatable.table.rowCount()>0:
            id = self.kartatable.datarow[self.kartatable.table.currentRow()]
            self.kartarangetable.redraw([id])
            self.kartarangetable.list.setCurrentRow(0)
            self.krtrngChange()

    def krtrngChange(self):
        if self.kartarangetable.list.count()>0:
            id = self.kartarangetable.datarow[self.kartarangetable.list.currentRow()]
            self.kartarangemarshtable.redraw([id])
            self.kartatime.redraw([id])
        else:
            self.kartarangemarshtable.redraw([0])
            self.kartatime.redraw([0])